import React, { Component } from 'react';
import { BrowserRouter } from 'react-router-dom';

import { MuiThemeProvider, Grid, Hidden, withStyles } from '@material-ui/core';

import { AppRouter } from './shared/routing/app-router';
import './App.css';
import customTheme from './shared/theme/themeProvider';
import Navbar from './modules/navigation/components/navbar/navbar';
import NavbarMobile from './modules/navigation/components/navbar-mobile/navbar-mobile';
import ErrorMessage from './shared/components/error-message/error-message';
import Footer from './modules/navigation/components/footer/footer';

interface AppProps {
  classes: any;
}

const styles = (theme: any) => {
  return {
    mainContainerStyle: {
      [theme.breakpoints.up('md')]: {
        padding: '0vh 3vw'
      },
      padding: '0'
    },
    container: {
      position: 'relative' as 'relative'
    },
    routingContainerStyle: {
      [theme.breakpoints.up('md')]: {
        marginTop: '10vh'
      },
      marginTop: '6vh'
    }
  };
};

class App extends Component<AppProps> {
  constructor(props: AppProps) {
    super(props);
  }

  public render() {
    const { classes } = this.props;
    const { mainContainerStyle, routingContainerStyle, container } = classes;
    return (
      <MuiThemeProvider theme={customTheme}>
        <BrowserRouter>
          <>
            <div className={container}>
              <Grid className={mainContainerStyle}>
                <div className="App">
                  <header>
                    <Hidden mdDown>
                      <Navbar />
                    </Hidden>
                    <Hidden lgUp>
                      <NavbarMobile />
                    </Hidden>
                  </header>
                  <div className={routingContainerStyle}>
                    <AppRouter />
                  </div>
                  <ErrorMessage />
                </div>
              </Grid>
              <Footer />
            </div>
          </>
        </BrowserRouter>
      </MuiThemeProvider>
    );
  }
}

export default withStyles(styles)(App);
