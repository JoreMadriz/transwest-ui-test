import React from 'react';
import { withStyles, Button } from '@material-ui/core';

import styles from './custom-button.style';
import { CustomButtonProps } from './custom-button.props';

const CustomButton = (props: CustomButtonProps) => {
  const { onClickAction, text } = props;
  const { button } = props.classes;
  return (
    <Button onClick={() => onClickAction()} className={button}>
      {text}
    </Button>
  );
};

export default withStyles(styles)(CustomButton);
