import { PRIMARY_COLOR, WHITE } from '../../theme/colors';

const styles = (theme: any) => {
  return {
    button: {
      backgroundColor: PRIMARY_COLOR,
      color: WHITE,
      textTransform: 'capitalize' as 'capitalize',
      disableFocusRipple: 'true' as 'true',
      disableRipple: 'true' as 'true',
      whiteSpace: 'nowrap' as 'nowrap'
    },
    link: {
      textDecoration: 'none !important',
      underline: 'hover' as 'hover',
      color: WHITE,
      textTransform: 'capitalize' as 'capitalize',
      [theme.breakpoints.down('md')]: {
        fontSize: 'smaller' as 'smaller'
      }
    }
  };
};

export default styles;
