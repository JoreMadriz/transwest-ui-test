export interface CustomButtonProps {
  classes: any;
  onClickAction: Function;
  path?: string;
  text: string;
}
