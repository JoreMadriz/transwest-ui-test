import { Subject } from 'rxjs';
interface errorObservableState {
  message: string;
  isOpen: boolean;
  customColor?: 'red' | 'green';
}
export const state: errorObservableState = {
  message: '',
  isOpen: false,
  customColor: 'red'
};
export const errorObservable = new Subject();

export const triggers = {
  handleClose: () => {
    (state.isOpen = false), (state.message = '');
    errorObservable.next(state);
  }
};

export const handleErrorMessage = (message: string, customColor?: 'red' | 'green') => {
  state.isOpen = true;
  state.message = message;
  state.customColor = customColor;
  errorObservable.next(state);
};
