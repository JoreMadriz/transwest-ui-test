import { Close, Warning } from '@material-ui/icons';
import React from 'react';
import { Snackbar, IconButton, withStyles } from '@material-ui/core';

import { errorObservable, state, triggers } from './error-message.observable';
import styles from './error-message.style';
import withObservableStream from '../../react-rx/withObservableStream';
interface ErrorMessageProps {
  isOpen: boolean;
  triggers: any;
  message: string;
  classes: any;
  customColor?: 'red' | 'green';
}

class ErrorMessage extends React.Component<ErrorMessageProps> {
  render() {
    const { customColor, isOpen, triggers, message, classes } = this.props;
    const greenColor = customColor === 'green' ? '#43a047' : '';
    const { handleClose } = triggers;
    const { errorCard, messageCard, warningIcon } = classes;

    return (
      <Snackbar
        open={isOpen}
        anchorOrigin={{
          vertical: 'top',
          horizontal: 'center'
        }}
        message={
          <span
            id="client-snackbar"
            className={messageCard}
            style={{ backgroundColor: `${greenColor} !important` }}
          >
            {customColor === 'red' && <Warning className={warningIcon} />}
            {message}
          </span>
        }
        autoHideDuration={5000}
        onClose={handleClose}
        className={errorCard}
        action={[
          <IconButton key="close" aria-label="Close" color="inherit" onClick={handleClose}>
            <Close />
          </IconButton>
        ]}
      />
    );
  }
}

export default withObservableStream(errorObservable, triggers, state)(
  // @ts-ignore
  withStyles(styles)(ErrorMessage)
);
