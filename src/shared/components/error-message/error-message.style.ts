import { ERROR_COLOR } from '../../theme/colors';
import { MARGIN_1 } from '../../theme/gutters';

const styles = {
  errorCard: {
    '& div': {
      backgroundColor: ERROR_COLOR
    }
  },
  messageCard: {
    display: 'flex',
    alignItems: 'center',
  },
  warningIcon: {
      marginRight:`${MARGIN_1}px`
  }
};

export default styles;
