import React from 'react';
import { List, ListItem, ListItemText, withStyles } from '@material-ui/core';

import SideMenuProps from './side-menu.props';
import styles from './side-menu.styles';
import { Link } from 'react-router-dom';

export class SideMenu extends React.Component<SideMenuProps> {
  private renderMenuItems = () => {
    const { selectedUri, handleItemClick, classes } = this.props;
    const { listItem, removeUnderline } = classes;
    return this.props.menuItems.map((menuItem) => {
      const { name, uri } = menuItem;
      return (
        <Link to={uri} key={uri} className={removeUnderline}>
          <ListItem
            className={listItem}
            selected={selectedUri === uri}
            onClick={() => {
              handleItemClick(uri);
            }}
            button
          >
            <ListItemText disableTypography={true} color="primary" primary={name} />
          </ListItem>
        </Link>
      );
    });
  };

  public render() {
    const { classes } = this.props;
    const { subMenu } = classes;
    return (
      <List className={subMenu} component="nav">
        {this.renderMenuItems()}
      </List>
    );
  }
}

export default withStyles(styles)(SideMenu);
