import React from 'react';
import Enzyme, { shallow } from 'enzyme';

import ISideMenuProps from './side-menu.props';
import { SideMenu } from './side-menu';

type SideMenuType = Enzyme.ShallowWrapper<{}, {}, SideMenu>;

describe('Side Menu test suite', () => {
  function getSideMenuWrapper(props: ISideMenuProps): SideMenuType {
    return shallow(
      <SideMenu
        handleItemClick={props.handleItemClick}
        classes={props.classes}
        menuItems={props.menuItems}
        selectedIndex={props.selectedIndex}
        selectedUri={props.selectedUri}
      />
    );
  }

  function getDefaultProps() {
    return {
      handleItemClick: jest.fn(),
      classes: {},
      title: 'title',
      menuItems: [{ name: 'test', uri: '/test' }],
      selectedIndex: 1,
      selectedUri: '/'
    };
  }

  it('should handle clicking an item by calling the supplied handleClick function', () => {
    const sideMenuProps = getDefaultProps();
    const mockHandleItemClick = sideMenuProps.handleItemClick;
    const sideMenu = getSideMenuWrapper(sideMenuProps).dive();
    const menuItemToTest = sideMenuProps.menuItems[0];
    const menuItem = sideMenu.findWhere(
      (node) => node.name() === 'WithStyles(ListItem)' && node.key() === menuItemToTest.uri
    );
    menuItem.simulate('click');
    expect(mockHandleItemClick).toHaveBeenCalledTimes(2);
    expect(mockHandleItemClick).toHaveBeenCalledWith(menuItemToTest.uri, 0);
  });
});
