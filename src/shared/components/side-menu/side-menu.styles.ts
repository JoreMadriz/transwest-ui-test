import { PRIMARY_COLOR, WHITE, BACKGROUND_COLOR1 } from '../../theme/colors';

const styles = {
  removeUnderline: { textDecoration: 'none' as 'none' },
  subMenu: {
    backgroundColor: BACKGROUND_COLOR1,
    color: PRIMARY_COLOR,
    padding: '0px'
  },
  listItem: {
    color: PRIMARY_COLOR,
    fontSize: '20px',
    borderBottom: `2px solid ${WHITE}`
  }
};
export default styles;
