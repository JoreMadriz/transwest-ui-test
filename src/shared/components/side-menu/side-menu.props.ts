import MenuItem from '../models/menuItem';
export default interface MenuProps {
  classes: any;
  menuItems: MenuItem[];
  handleItemClick: any;
  selectedUri: string;
  selectedIndex?: number;
}
