import React from 'react';
import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  Typography,
  withStyles
} from '@material-ui/core';

import CustomDialogProps from './custom-dialog.props';
import style from './custom-dialog.style';
class CustomDialog extends React.Component<CustomDialogProps> {
  public render() {
    const {
      open,
      handleClose,
      confirmationButton,
      confirmationMessage,
      title,
      children,
      classes
    } = this.props;
    const { dialogTitle, root } = classes;
    return (
      <Dialog
        open={open}
        className={root}
        onClose={handleClose}
        aria-labelledby="form-dialog-title"
        maxWidth={'md'}
      >
        <DialogTitle disableTypography id="form-dialog-title" className={dialogTitle}>
          <Typography variant="h4" gutterBottom>
            {title}
          </Typography>
        </DialogTitle>
        <DialogContent>{children}</DialogContent>
        {confirmationButton && (
          <DialogActions className="actions">
            <Button className="submit" onClick={handleClose}>
              {confirmationMessage}
            </Button>
          </DialogActions>
        )}
      </Dialog>
    );
  }
}

export default withStyles(style)(CustomDialog);
