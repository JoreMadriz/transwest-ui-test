import React from 'react';
import Enzyme, { shallow } from 'enzyme';

import CustomDialog from './custom-dialog';
import CustomDialogProps from './custom-dialog.props';
import style from './custom-dialog.style';
type CustomDialogType = Enzyme.ShallowWrapper<CustomDialogProps, {}, CustomDialog>;

describe('Custom Dialog test suite', () => {
  function getDefaultCustomDialogProps(): CustomDialogProps {
    return {
      open: true,
      confirmationButton: true,
      confirmationMessage: 'test',
      handleClose: () => {},
      title: 'test',
      classes: style
    };
  }

  function getCustomDialog(props: CustomDialogProps): CustomDialogType {
    return shallow(
      <CustomDialog
        open={props.open}
        confirmationButton={props.confirmationButton}
        confirmationMessage={props.confirmationMessage}
        handleClose={props.handleClose}
        title={props.title}
      />
    );
  }

  it('should render dialog actions when the confirmationButton flag is on', () => {
    const customDialog = getCustomDialog(getDefaultCustomDialogProps()).dive();
    const dialogActions = customDialog.find('WithStyles(DialogActions)');
    expect(dialogActions.length).toBe(1);
  });

  it("shouldn't render dialog actions when the confirmationButton flag is off", () => {
    const customDialogProps = getDefaultCustomDialogProps();
    customDialogProps.confirmationButton = false;
    const customDialog = getCustomDialog(customDialogProps).dive();
    const dialogActions = customDialog.find('WithStyles(DialogActions)');
    expect(dialogActions.length).toBe(0);
  });
});
