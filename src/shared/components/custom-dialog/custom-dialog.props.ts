export default interface CustomDialogProps {
  open: boolean;
  confirmationButton?: boolean;
  confirmationMessage?: string;
  handleClose: any;
  title: string;
  classes: any;
}
