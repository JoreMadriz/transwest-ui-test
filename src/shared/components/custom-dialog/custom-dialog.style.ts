import { MARGIN_0, MARGIN_2 } from '../../theme/gutters';

const styles = {
  dialogTitle: {
    textTransform: 'capitalize' as 'capitalize'
  },
  root: {
    margin: `0 ${MARGIN_2}px`,
    '& div': {
      marginLeft: MARGIN_0
    }
  }
};

export default styles;
