export default interface StaticPageState {
  html: string;
  selectedIndex: number;
  selectedUri: string;
}
