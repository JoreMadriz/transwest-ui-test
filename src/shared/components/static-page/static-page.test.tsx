import Enzyme from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import React from 'react';
import StaticPage from './static-page';
import IStaticPageState from './static-page.state';
import HtmlService from '../../services/htmlInjection.service';
import IMenuItem from '../../models/menu-Item';
Enzyme.configure({ adapter: new Adapter() });
type WrapperType = Enzyme.ShallowWrapper<{}, IStaticPageState, StaticPage>;

describe('Services page test', () => {
  let wrapper: WrapperType | null;

  const menuItems: IMenuItem[] = [
    { name: 'Careers', uri: 'Careers' },
    { name: 'Report Baggage Irregularity', uri: 'Report Baggage Irregularity' }
  ];

  const app = (): WrapperType => {
    if (!wrapper) {
      wrapper = Enzyme.shallow(<StaticPage title="Test" menuItems={menuItems} />);
    }
    return wrapper;
  };

  beforeEach(() => {
    wrapper = null;
  });

  it('Should return a new html string for the injection', () => {
    jest.spyOn(HtmlService.prototype, 'getHTML').mockReturnValue('Test');
    const servicesComponent = app().instance();
    const clickItem = servicesComponent.handleMenuClick('charset', 0);
    clickItem();
    const newHtml = servicesComponent.state.html;
    expect(newHtml).toEqual('Test');
  });
});
