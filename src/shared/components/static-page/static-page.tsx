import React from 'react';
import SideMenu from '../side-menu/side-menu';
import { Grid } from '@material-ui/core';
import StaticCard from '../static-card/static-card';
import HtmlService from '../../services/htmlInjection.service';
import StaticPageState from './static-page.state';
import StaticPageProps from './static-page.props';

class StaticPage extends React.Component<StaticPageProps, StaticPageState> {
  private htmlService = new HtmlService();
  constructor(props: any) {
    super(props);
    this.state = {
      html: '',
      selectedIndex: -1,
      selectedUri: '/'
    };
  }

  public handleMenuClick = (uri: string, index: number) => {
    return () => {
      const newPage = this.htmlService.getHTML(uri);
      this.setState({ html: newPage, selectedIndex: index });
    };
  };
  public render() {
    const { selectedUri } = this.state;
    const { menuItems } = this.props;
    return (
      <Grid container={true} justify="flex-start">
        <Grid item={true} xs={2}>
          <SideMenu
            handleItemClick={this.handleMenuClick}
            menuItems={menuItems}
            selectedUri={selectedUri}
          />
        </Grid>
        <Grid item={true} xs={10}>
          <StaticCard />
        </Grid>
      </Grid>
    );
  }
}

export default StaticPage;
