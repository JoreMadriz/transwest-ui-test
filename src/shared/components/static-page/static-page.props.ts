import IMenuItem from '../models/menuItem';

export default interface StaticPageProps {
  title: string;
  menuItems: IMenuItem[];
}
