export default interface MenuItem {
  name: string;
  uri: string;
}
