export default interface IStaticCardState {
    fontSize:string,
    isDialogOpen: boolean
}