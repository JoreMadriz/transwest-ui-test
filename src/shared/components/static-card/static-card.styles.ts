import { PADDING_0, PADDING_5 } from '../../theme/gutters';
const styles = {
  richContent: {
    padding: `${PADDING_0} ${PADDING_5}px`
  }
};

export default styles;
