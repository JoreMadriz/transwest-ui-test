import Enzyme, { shallow, ShallowWrapper } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import React from 'react';
import StaticCardProps from './static-card.props';
import StaticCardState from './static-card.state';
import { StaticCard } from './static-card';
import styles from './static-card.styles';
Enzyme.configure({ adapter: new Adapter() });

type WrapperType = Enzyme.ShallowWrapper<StaticCardProps, StaticCardState, StaticCard>;

describe('Static Card test', () => {
  let wrapper: WrapperType | null;
  const app = (): WrapperType => {
    if (!wrapper) {
      wrapper = shallow(<StaticCard classes={styles} />);
    }
    return wrapper;
  };
  beforeEach(() => {
    wrapper = null;
  });
  const clickOnIconButton = (buttonClickName: string) => {
    const bookingShallow = app();
    const addButton = bookingShallow
      .findWhere((node: ShallowWrapper) => {
        return node.name() === buttonClickName;
      })
      .parent();
    addButton.simulate('click');
  };
  it('Should increase font size when increase font is called', () => {
    const bookingComp = app().instance();
    const fontSize = parseInt(bookingComp.state.fontSize);
    clickOnIconButton('pure(AddIcon)');
    const newFontSize = parseInt(bookingComp.state.fontSize);
    expect(fontSize).toBeLessThan(newFontSize);
  });

  it('Should decrease font size when decrease font is called', () => {
    const bookingComp = app().instance();
    const fontSize = parseInt(bookingComp.state.fontSize);
    clickOnIconButton('pure(RemoveIcon)');
    const newFontSize = parseInt(bookingComp.state.fontSize);
    expect(newFontSize).toBeLessThan(fontSize);
  });

  it('Should reset the font to 100%', async () => {
    const bookingComp = app().instance();
    await bookingComp.setState({ fontSize: '130%' });
    clickOnIconButton('pure(TitleIcon)');
    const font = bookingComp.state.fontSize;
    expect(font).toEqual('100%');
  });
  it('Should not increase font size further than 150%', async () => {
    const bookingComp = app().instance();
    await bookingComp.setState({ fontSize: '150%' });
    clickOnIconButton('pure(AddIcon)');
    const font = bookingComp.state.fontSize;
    const dialogOpen = bookingComp.state.isDialogOpen;
    expect(font).toEqual('150%');
    expect(dialogOpen).toBeTruthy();
  });
  it('Should not decrease font size further than 50%', async () => {
    const bookingComp = app().instance();
    await bookingComp.setState({ fontSize: '50%' });
    clickOnIconButton('pure(RemoveIcon)');
    const font = bookingComp.state.fontSize;
    const dialogOpen = bookingComp.state.isDialogOpen;
    expect(font).toEqual('50%');
    expect(dialogOpen).toBeTruthy();
  });
});
