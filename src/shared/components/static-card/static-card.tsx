import React from 'react';
import TitleIcon from '@material-ui/icons/Title';
import RemoveIcon from '@material-ui/icons/Remove';
import AddIcon from '@material-ui/icons/Add';
import { Grid, IconButton, withStyles } from '@material-ui/core';
import IStaticCardProps from './static-card.props';
import IStaticCardState from './static-card.state';
import styles from './static-card.styles';
import CustomDialog from '../custom-dialog/custom-dialog';

export class StaticCard extends React.Component<IStaticCardProps, IStaticCardState> {
  private FONT_STEP = 10;
  private MAX_FONT = 150;
  private MIN_FONT = 50;
  private RESET_FONT = 100;

  constructor(props: IStaticCardProps) {
    super(props);
    this.state = {
      fontSize: '100%',
      isDialogOpen: false
    };
  }

  public handleCloseDialog = () => {
    this.setState({ isDialogOpen: false });
  };

  private getFontMessage = () => {
    return parseInt(this.state.fontSize) === this.MAX_FONT ? 'maximum' : 'minimun';
  };

  private modifyFont = (action: string) => {
    return () => {
      let isOpen = false;
      let size = parseInt(this.state.fontSize);
      switch (action) {
        case 'increase':
          if (size === this.MAX_FONT) {
            isOpen = true;
          }
          size = this.increase(size);
          break;
        case 'decrease':
          if (size === this.MIN_FONT) {
            isOpen = true;
          }
          size = this.decrease(size);
          break;
        case 'reset':
          size = this.RESET_FONT;
          break;
      }

      this.setState({ fontSize: `${size}%`, isDialogOpen: isOpen });
    };
  };

  public increase = (size: number) => {
    return size < this.MAX_FONT ? (size += this.FONT_STEP) : size;
  };

  private decrease = (size: number) => {
    return size > this.MIN_FONT ? (size -= this.FONT_STEP) : size;
  };

  public render() {
    const { classes, children } = this.props;
    return (
      <>
        <Grid container={true} justify="flex-start" direction="column">
          <Grid container={true} justify="flex-end">
            <Grid item={true}>
              <IconButton onClick={this.modifyFont('increase')}>
                <AddIcon />
              </IconButton>
              <IconButton onClick={this.modifyFont('reset')}>
                <TitleIcon />
              </IconButton>
              <IconButton onClick={this.modifyFont('decrease')}>
                <RemoveIcon />
              </IconButton>
            </Grid>
          </Grid>
          <Grid
            className={classes.richContent}
            item={true}
            style={{ fontSize: this.state.fontSize }}
          >
            {children}
          </Grid>
        </Grid>
        <CustomDialog
          title="Font limit reached"
          open={this.state.isDialogOpen}
          confirmationButton={true}
          confirmationMessage="ok"
          handleClose={this.handleCloseDialog}
        >
          You have already selected the {this.getFontMessage()} font size
        </CustomDialog>
      </>
    );
  }
}
export default withStyles(styles)(StaticCard);
