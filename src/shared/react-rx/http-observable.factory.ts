import { AxiosResponse, AxiosError } from 'axios';
import { Observable } from 'rxjs';
import HttpService from '../services/http.service';

export default class HttpObservableFactory {
  public static getObservable(url: string, headers: any, modelKey: string) {
    return Observable.create((observer: any) => {
      HttpService.get(url, headers)
        .then((response: AxiosResponse) => {
          return observer.next({ [modelKey]: response });
        })
        .catch((error: AxiosError) => {
          // manage observable error here
        });
    });
  }

  public static postObservable(url: string, headers: any, body: any, modelKey: string) {
    return Observable.create((observer: any) => {
      HttpService.post(url, body, { headers })
        .then((response: AxiosResponse) => {
          return observer.next({ [modelKey]: response });
        })
        .catch((error: AxiosError) => {
          // manage observable error here
        });
    });
  }
}
