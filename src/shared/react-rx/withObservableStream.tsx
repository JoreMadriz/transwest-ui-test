import React from 'react';
import { Subscription } from 'rxjs';

const withObservableStream = <P extends object>(observable:any, triggers:any, initialValue:any) => {
  return (Component: React.ComponentType<P>) => {
    return class extends React.Component<P, P> {
      private subscription: Subscription | null = null;

      constructor(props: any) {
        super(props);

        this.state = {
          ...observable.value,
          ...initialValue
        };
      }

      componentDidMount() {
        this.subscription = observable.subscribe((newState: any) => {
          this.setState({ ...newState }, () => {});
        });
      }
      componentWillUnmount() {
        if (this.subscription) this.subscription.unsubscribe();
      }
      render() {
        return <Component {...this.props} {...this.state} {...{triggers}} />;
      }
    };
  };
};

export default withObservableStream;
