import { createMuiTheme } from '@material-ui/core';
import { ThemeOptions } from '@material-ui/core/styles/createMuiTheme';
import * as colors from './colors';
import { MARGIN_2 } from './gutters';
const defaultTheme = createMuiTheme({
  breakpoints: {
    values: {
      xs: 0,
      sm: 600,
      md: 960,
      lg: 1100,
      xl: 1920
    }
  },
  palette: {
    background: {
      paper: colors.WHITE,
      default: colors.DEFAULT
    },
    action: {
      active: colors.PRIMARY_COLOR
    },
    primary: {
      light: colors.LIGHT_PRIMARY_COLOR,
      main: colors.WHITE,
      dark: colors.DARKER_PRIMARY_COLOR,
      contrastText: colors.PRIMARY_COLOR
    },
    secondary: {
      light: colors.LIGHT_PRIMARY_COLOR,
      main: colors.LIGHT_PRIMARY_COLOR,
      dark: colors.DARKER_PRIMARY_COLOR,
      contrastText: colors.WHITE
    },
    error: {
      light: colors.ERROR_COLOR,
      main: colors.ERROR_COLOR,
      dark: colors.SECONDARY_ERROR_COLOR,
      contrastText: colors.WHITE
    },
    text: {
      primary: colors.PRIMARY_COLOR,
      secondary: colors.DARKER_PRIMARY_COLOR,
      disabled: colors.DISABLED,
      hint: colors.SECONDARY_COLOR
    }
  },
  typography: {
    useNextVariants: true,
    fontFamily: '"Arial", "Roboto"'
  }
} as ThemeOptions);

const { breakpoints } = defaultTheme;
export default {
  ...defaultTheme,
  overrides: {
    MuiAppBar: {
      root: {
        boxShadow: 'none',
        paddingTop: '10px'
      }
    },
    MuiExpansionPanelSummary: {
      root:{
        '&$expanded': {
          minHeight:'0'
        }
      },
      content:{
        '&$expanded': {
          margin:`${MARGIN_2}px  0 0 0`
        }
      }
    },
    MuiFormLabel: {
      root: {
        '&$focused': {
          color: colors.PRIMARY_COLOR
        }
      }
    },
    MuiListItem: {
      root: {
        '&$selected': {
          backgroundColor: '#BCE0FD',
          color: '#2699FB'
        }
      }
    },

    MuiButton: {
      root: {
        fontWeight: 'bold' as 'bold'
      },
      outlinedPrimary: {
        border: `2px solid ${colors.BORDER_COLOR}`
      }
    },
    MuiTypography: {
      h1: {
        fontSize: '48pt',
        lineHeight: '40px',
        [breakpoints.down('xs')]: {
          fontSize: '40pt',
          lineHeight: '40px'
        }
      },
      h2: {
        fontSize: '36pt',
        lineHeight: '40px',
        [breakpoints.down('xs')]: {
          fontSize: '36pt',
          lineHeight: '35px'
        }
      },
      h3: {
        fontSize: '28pt',
        lineHeight: '22px',
        [breakpoints.down('xs')]: {
          fontSize: '28pt',
          lineHeight: '22px'
        }
      },
      h4: {
        fontSize: '20pt',
        lineHeight: '24px',
        [breakpoints.down('xs')]: {
          fontSize: '20pt',
          lineHeight: '24px'
        }
      },
      h5: {
        fontSize: '16pt',
        lineHeight: '22px',
        [breakpoints.down('xs')]: {
          fontSize: '16pt',
          lineHeight: '22px'
        }
      },
      h6: {
        fontSize: '14pt',
        lineHeight: '20px',
        [breakpoints.down('xs')]: {
          fontSize: '14pt',
          lineHeight: '20px'
        }
      },
      colorPrimary: {
        color: colors.PRIMARY_COLOR
      }
    },
    MuiDialog: {
      paper: {
        position: 'absolute' as 'absolute',
        top: '15%'
      }
    }
  }
};
