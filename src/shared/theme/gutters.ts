export const MARGIN_0 = 0;
export const MARGIN_1 = 5;
export const MARGIN_2 = 10;
export const MARGIN_3 = 15;
export const MARGIN_4 = 20;
export const MARGIN_5 = 25;
export const MARGIN_12 = 60;

export const PADDING_0 = 0;
export const PADDING_1 = 5;
export const PADDING_2 = 10;
export const PADDING_3 = 15;
export const PADDING_4 = 20;
export const PADDING_5 = 25;
export const PADDING_6 = 30;
export const PADDING_8 = 45;
export const PADDING_11 = 55;
export const PADDING_28 = 140;
