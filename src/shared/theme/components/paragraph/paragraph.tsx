import { withStyles, Typography } from '@material-ui/core';
import React from 'react';

import paragraphStyles from './paragraph.styles';
import ParagraphProps from './paragraph-props';

export const Paragraph = (props: ParagraphProps) => {
  const { classes, children, title, paragraphFont, titleFont } = props;
  const { paragraphStyle, titleStyle } = classes;
  return (
    <>
      {title && title !== '' && (
        <Typography style={{ fontSize: titleFont || '' }} className={titleStyle}>
          {title}
        </Typography>
      )}
      <p style={{ fontSize: paragraphFont || '' }} className={paragraphStyle}>
        {children}
      </p>
    </>
  );
};

export default withStyles(paragraphStyles)(Paragraph);
