interface ParagraphProps {
  classes: any;
  children: any;
  title?: string;
  paragraphFont?: number;
  titleFont?: number;
}

export default ParagraphProps;
