import { shallow, ShallowWrapper } from 'enzyme';
import React from 'react';

import { Paragraph } from './paragraph';
import ParagraphProps from './paragraph-props';

type ParagraphType = ShallowWrapper<ParagraphProps>;

describe('Paragraph test suite', () => {
  const getDefaultParagraphProps = (): ParagraphProps => {
    return {
      children: 'something',
      classes: ''
    };
  };

  const getParagraphWrapper = (props: ParagraphProps): ParagraphType => {
    return shallow(<Paragraph classes={props.classes}>{props.children}</Paragraph>);
  };

  it('renders the component properly', () => {
    const paragraph = getParagraphWrapper(getDefaultParagraphProps());
    expect(paragraph).toMatchSnapshot();
  });
});
