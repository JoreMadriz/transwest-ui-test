import { PRIMARY_COLOR } from '../../colors';

export default (theme: any) => {
  const { breakpoints } = theme;
  return {
    paragraphStyle: {
      color: PRIMARY_COLOR,
      fontSize: '16pt',
      lineHeight: '22px',
      [breakpoints.down('xs')]: {
        fontSize: '16pt',
        lineHeight: '22px'
      }
    },
    titleStyle: {
      color: PRIMARY_COLOR,
      fontWeight: 600
    }
  };
};
