import React from 'react';

import theme from '../../themeProvider';
import { SpacerType } from './SpacerType';
import { withStyles } from '@material-ui/core';

const { breakpoints } = theme;

interface SectionSpacerProps {
  spacerType: SpacerType;
  classes: any;
}

const SpacerMap: any = {
  LARGE: {
    height: '125px',
    width: '1px',
    [breakpoints.down('xs')]: {
      width: '1px',
      height: '60px'
    }
  },
  MEDIUM: {
    height: '60px',
    width: '1px',
    [breakpoints.down('xs')]: {
      width: '1px',
      height: '35px'
    }
  },
  SMALL: {
    height: '40px',
    width: '1px',
    [breakpoints.down('xs')]: {
      height: '20px'
    }
  },
  TINY: {
    height: '14px',
    width: '1px',
    [breakpoints.down('xs')]: {
      width: '1px',
      height: '12px'
    }
  }
};

const SectionSpacer = (props: SectionSpacerProps) => {
  const { spacerType, classes } = props;
  return <div className={classes[SpacerType[spacerType]]} />;
};

export default withStyles(SpacerMap)(SectionSpacer);
