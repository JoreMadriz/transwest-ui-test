export enum SpacerType {
  LARGE = 0,
  MEDIUM = 1,
  SMALL = 2,
  TINY = 3
}
