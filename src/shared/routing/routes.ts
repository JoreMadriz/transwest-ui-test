import HelpCenter from '../../modules/help-center/help-center';
import Schedules from '../../modules/schedules/schedules';
import Services from '../../modules/services/services';
import Booking from '../../modules/booking/booking';
import LandingPage from '../../modules/landing-page/landing-page';

export interface Route {
  path: string;
  component: any;
}
export interface AppRoutes {
  help: Route;
  schedule: Route;
  services: Route;
  booking: Route;
  landingPage: Route;
}

const appRoutes: AppRoutes = {
  help: { path: '/helpcenter', component: HelpCenter },
  schedule: { path: '/schedules/:page?', component: Schedules },
  services: { path: '/services/:page?', component: Services },
  booking: { path: '/booking', component: Booking },
  landingPage: { path: '/', component: LandingPage }
};

export default appRoutes;
