import React from 'react';
import { Route, Switch } from 'react-router-dom';
import appRoutes, { AppRoutes, Route as AppRoute } from './routes';

export class AppRouter extends React.Component {
  private getRoutes(appRoutes: AppRoutes): AppRoute[] {
    const routes: AppRoute[] = Object.values(appRoutes).map((routeValue) => {
      return routeValue;
    });
    return routes;
  }

  public render() {
    const routes = this.getRoutes(appRoutes);
    return (
      <Switch>
        {routes.map((route) => {
          return <Route key={route.path} path={route.path} component={route.component} />;
        })}
      </Switch>
    );
  }
}
