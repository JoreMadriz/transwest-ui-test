import axios from 'axios';
import apiConfig from './apiConfig';
import errorHandler from './apiErrorHandler';

axios.defaults.baseURL = apiConfig.apiUrl;
axios.defaults.headers.get['Access-Control-Allow-Origin'] = '*';
axios.defaults.headers.post['Access-Control-Allow-Origin'] = '*';
axios.defaults.headers.post['Content-Type'] = apiConfig.contentType;
axios.defaults.headers.put['Content-Type'] = apiConfig.contentType;

axios.interceptors.response.use(
    response => {
        if(response.data.status === 'error'){
            const errorResponse = errorHandler(response.data);
            return Promise.reject(errorResponse);
        }  
       return Promise.resolve(response)
    },
    error => {      
        const errorResponse = errorHandler(error.response);
        return Promise.reject(errorResponse);
    }
);

export default axios;
