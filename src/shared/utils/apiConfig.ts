const apiConfig = {
    apiUrl: 'https://twawebservices.transwestair.com:40411',
    contentType: 'application/json',
    env: 'develop'
};

export default apiConfig;
