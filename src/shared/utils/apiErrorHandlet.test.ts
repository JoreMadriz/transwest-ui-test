import apiErrorHandler from './apiErrorHandler';

describe('Api error Handler test suite', () => {
  it('populates the payload for 400 errors', () => {
    const badRequestError = apiErrorHandler({ status: 400 });
    expect(badRequestError.payload).toEqual('Bad Request');
  });

  it('populates the payload for 401 errors', () => {
    const unauthorizedError = apiErrorHandler({ status: 401 });
    expect(unauthorizedError.payload).toEqual('Unauthorized');
  });

  it('populates the payload for 403 errors', () => {
    const forbiddenError = apiErrorHandler({ status: 403 });
    expect(forbiddenError.payload).toEqual('Forbidden');
  });

  it('populates the payload for 500 errors', () => {
    const internalServerError = apiErrorHandler({ status: 500 });
    expect(internalServerError.payload).toEqual('Internal Server Error');
  });

  it('uses the custom error provided by the SKYLINE Api as an error', () => {
    const expectedCustomError = 'this is a custom error';
    const customError = apiErrorHandler({ status: 'error', errors: [expectedCustomError] });
    expect(customError.payload).toEqual(expectedCustomError);
  });

  it('populates the payload when there is no matching error', () => {
    const defaultError = apiErrorHandler({ status: 'something' });
    expect(defaultError.payload).toEqual('Transaction Error');
  });
});
