import moment from 'moment';

export const formatDate = (date: Date | string, format: string) => {
  return moment(date).format(format);
};

export const formatTime = (time: string) => {
  const timeList = time.split(':');
  timeList[0] = timeList[0].charAt(0) == '0' ? timeList[0].charAt(1) : timeList[0];
  return `${timeList[0]}h ${timeList[1]}m`;
};

export const isDateEqual = (startDate:Date, endDate:Date) => {
  const format = 'DD-MM-YYYY'
  return moment(startDate).format(format) === moment(endDate).format(format)
}
export default formatDate;
