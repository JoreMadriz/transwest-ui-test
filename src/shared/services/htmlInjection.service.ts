export default class HtmlInjectionService {
  public getHTML(page: string) {
    switch (page) {
      case 'Charters':
        return `<h1>Charters</h1>
                <h4 style="text-align:left">TRANSWEST AIR HAS THE LARGEST FLEET OF FIXED WING AND ROTARY CHARTER AIRCRAFT IN SASKATCHEWAN
                <span style="font-weight:normal"> including float, cargo, med-evac and corporate travel aircraft. We also have a charter base located in Lynn Lake, Manitoba.</span></h4>
                <p style="text-align:left">Our charter services are used for surveying, construction, employee transport, medical evacuation, corporate travel, line patrol, cargo movement, sight seeing, filming and much more. Whether you have flown with us before, or are looking to book your first trip, our experienced Dispatch team will be happy to walk you through the process to find the aircraft that best suits your needs. We can also provide commissary onboard the flight, as well as customer parking at our hangars. </p>
                <p style="text-align:left"><b>If you need to charter an aircraft immediately, or require other information, please call one of the bases nearest you:</b><p>
                <ul style="list-style:none; padding:0; text-align: left">
                <li><b>Prince Albert:</b> (306) 764-1408/1404</li>
                <li><b>La Ronge:</b> (306) 425-2382</li>
                <li><b>Stony Rapids:</b> (306) 439-2040</li>
                <li><b>Wollaston:</b> (306) 633-2022</li>
                <li><b>Lynn Lake, Manitoba:</b> (204) 356-2457</li>
                </ul>
                <p style="text-align:left"><b>Or call toll free</b> 1-877-989-2677 24 hours a day.</p>
                <p>
                <a target="_blank" href="http://www.booktranswest.com/CharterManager/CharterManager.dll/EXEC" name="Charter_login">CHARTER LOG-IN Click here</a>
                </p>
                <p>
                <a href="https://www.transwestair.com/images/file/transwest-schedules/Local%20International%20Charter%20Tariff%202015%20Transwest%20Air.pdf">LOCAL INTERNATIONALCHARTER TARIFF</a>
                </p>
                `;
      case 'HServices':
        return `<h1>Helicopter Services</h1>
                <p style="text-align:left"><a href="http://northernshieldhelicopters.ca/">Northern Shield Helicopters</a> has been providing helicopter services to Saskatchewan for over 50 years. We started with a Bell 47G, and have maintained a Bell fleet ever since. We currently operate two Bell 206 JetRangers, one Bell 206LIV Long Ranger, two Bell 205 A1+'s and three Bell 407's. We pride ourselves on offering the best flight crews and engineers who can meet your needs safely, efficiently, and with an emphasis on impeccable customer service. Our 24 hour Dispatch team will work directly with our pilots to ensure that your satisfaction is met on every trip.</p>
                <p style="text-align:left">For more information on our helicopter services please visit <a href="http://northernshieldhelicopters.ca/">NorthernShieldHelicopters.ca/</a>a/</p>
                <p style="text-align:left">Among the services we provide with our helicopter fleet</p>
                <ul style="text-align: left">
                    <li>Forest fire suppression</li>
                    <li>Geo-technical and drill rig support</li>
                    <li>Urban construction</li>
                    <li>Power line and pipeline patrol</li>
                    <li>Med-evac</li>
                    <li>Crew changes in remote locations</li>
                    <li>Sightseeing rides</li>
                    <li>Aerial photography and cinematography</li>
                    <li>General cargo slinging</li>
                </ul>
                <p style="text-align:left">We will respond to charter pricing requests within twenty four hours of receipt during business hours.</p>
                <p style="text-align:left"><b>If you require immediate charter, call toll free</b> 1-877-989-2677 24 hours a day.</p>
            
                `;
      case 'Careers':
        return ` <h1>Contact Us</h1>
            <p><span class="pageSubSubTitle">Reservation Call Centre: </span>1-800-667-9356 (306-665-2370 in Saskatoon)</p>
            <p><span class="pageSubSubTitle">Charter Quotes &amp; BookingS: </span>Request quotes online: <a href="http://www.transwestair.com/html/Services/Charters/index.cfm">Airplanes</a>&nbsp; <a href="http://www.transwestair.com/html/Services/helicopter-srvices/index.cfm">Helicopters</a><br>
            1-877-989-2677 24 hours a day.&nbsp; If you need to charter an aircraft immediately, or require other information, please call one of the bases nearest you.</p>
            <table border="0" cellpadding="1" cellspacing="5" height="391" width="717">
            <tbody>
            <tr>
            <td align="left" valign="top">
            <p><span class="copyBoldBlack">Transwest Air - Prince Albert<br>
            (Head office)</span><br>
            Box 100, Hangar 21<br>
            Prince Albert Municipal Airport<br>
            Prince Albert, SK<br>
            Canada S6V 5R4<br>
            Tel: (306) 764-1404<br>
            Fax: (306) 764-3775</p>
            <p><span class="copyBoldBlack">Transwest Air - Prince Albert Terminal</span><br>
            Tel: (306) 764-4931<br>
            Fax: (306) 764-4946</p>
            </td>
            <td align="left" valign="top">
            <p><span class="copyBoldBlack">Transwest Air - Saskatoon</span><br>
            Hangar 9, Thayer Avenue<br>
            Saskatoon, SK<br>
            Canada S7L 5X4<br>
            Tel: (306) 665-2700<br>
            Fax: (306) 665-1606</p>
            <p><strong>Cargo:</strong><br>
            3B Hangar Rd<br>
            Cargo Phone:&nbsp;<a href="tel:18776007999">1.877.600.7999</a></p>
            </td>
            <td align="left" valign="top">
            <p><span class="copyBoldBlack">Transwest Air - La Ronge Waterbase<br>
            (Charters)</span><br>
            Box 320<br>
            La Ronge, SK<br>
            Canada S0J 1L0<br>
            Tel: (306) 425-2382<br>
            Fax: (306) 425-2455</p>
            </td>
            </tr>
            <tr>
            <td align="left" valign="top"><span class="copyBoldBlack">Transwest Air - Fond du Lac</span><br>
            Tel: (306) 686-2147</td>
            <td align="left" valign="top"><span class="copyBoldBlack">Transwest Air - Stony Rapids</span><br>
            Tel: (306) 439-2040</td>
            <td align="left" valign="top"><span class="copyBoldBlack">Transwest Air - Wollaston</span><br>
            Tel: (306) 633-2022<br>
            <span class="copyBoldBlack">Transwest Air- La Ronge Terminal</span><br>
            Tel: (306) 425-2774</td>
            </tr>
            <tr>
            <td align="left" valign="top">
            <p><span class="copyBold">Transwest Air - Buffalo Narrows</span><br>
            Buffalo Narrows Airport<br>
            Buffalo Narrows, Sask<br>
            Tel: (306) 235-4373<br>
            Fax: (306) 235-4622</p>
            </td>
            <td align="left" valign="top">&nbsp;</td>
            <td align="left" valign="top">&nbsp;</td>
            </tr>
            </tbody>
            </table>`;
      case 'Report Baggage Irregularity':
        return ` <div id="page_content_container">
           
            <h1 id="mainH1" class="pageTitle">Report Baggage Irregularity</h1>
            <p>&nbsp;</p>
            <p><span class="copyBoldItalicDarkGray">At Transwest Air, we take pride in delievering you and your cargo safely to your destination. If however you need to report missing or damaged items, please contact us through the form below. Every effort will be made to rectify the situation and we will respond as soon as possible.</span></p>
            <p>&nbsp;</p>
            <div class="clear"></div>
            <div class="clear"></div>
            </div>`;
      default:
        return '';
    }
  }
}
