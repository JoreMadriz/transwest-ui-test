import { AxiosResponse } from 'axios';
import axios from '../utils/apiService';
import { handleErrorMessage } from '../components/error-message/error-message.observable';
import ApiError from '../utils/apiError';
// import { Observable } from 'rxjs';

export default class HttpService {
  public static async get(url: string, headers: any) {
    return axios
      .get(url, { headers })
      .then((response: AxiosResponse) => {
        return response.data;
      })
      .catch((error: ApiError) => {
        handleErrorMessage(error.payload);
        return Promise.reject(error);
      });
  }
  public static async post(url: string, headers: any, body: any) {
    return axios
      .post(url, body, { headers })
      .then((response: AxiosResponse) => {
        return response.data;
      })
      .catch((error: ApiError) => {
        handleErrorMessage(error.payload);
        return Promise.reject(error);
      });
  }
}
