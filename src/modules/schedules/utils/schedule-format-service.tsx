import React from 'react';

export const getTitle = (text: string, styleClass: any) => {
  return <h2 className={styleClass}>{text}</h2>;
};

export const getParagraph = (text: string, styleClass: any) => {
  return <p className={styleClass}>{text}</p>;
};

export const getLinkParagraph = (text: string, styleClass: any, reference: string) => {
  return (
    <p className={styleClass}>
      <a href={reference}>{text}</a>
    </p>
  );
};

export const getLinkTitle = (text: string, styleClass: any, reference: string) => {
  return (
    <h2 className={styleClass}>
      <a href={reference}>{text}</a>
    </h2>
  );
};
