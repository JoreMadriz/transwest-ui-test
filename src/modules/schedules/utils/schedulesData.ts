import MenuItem from '../../../shared/models/menu-Item';
import location from '../../booking/components/airport-selection/models/location';

export interface Schedule {
  name: string;
  inboundLocation: location;
  scheduleoutboundDate: string;
  scheduleinboundDate: string;
  routes: FlightRoute[];
}

export interface FlightRoute {
  outboundLocation: location;
  flights: Flight[];
}

export interface Flight {
  flightNumber: string;
  departureTime: string;
  arrivalTime: string;
  stops: number;
  days: string[];
}

export const schedulesUrls: any = {
  ExpressAir: { name: 'ExpressAir: Saskatoon/Regina', uri: '/schedules/ExpressAir' },
  Fond_du_lac: { name: 'Fond du Lac', uri: '/schedules/Fond_du_lac' },
  La_ronge: { name: 'La Ronge', uri: '/schedules/La_ronge' }
};

export const schedulesUris: MenuItem[] = [
  schedulesUrls['ExpressAir'],
  schedulesUrls['Fond_du_lac'],
  schedulesUrls['La_ronge']
];

const schedulesData: Schedule[] = [
  {
    name: 'ExpressAir',
    inboundLocation: {
      Id: 1,
      Code: 'XYZ',
      Name: 'Regina',
      Address: {
        Address: '',
        City: '',
        Province: '',
        Country: 1,
        PostalCode: ''
      },
      WayOrder: 1
    },
    scheduleoutboundDate: '01/13/2019',
    scheduleinboundDate: '06/04/2019',
    routes: [
      {
        outboundLocation: {
          Id: 1,
          Code: 'XYZ',
          Name: 'Saskatoon',
          Address: {
            Address: '',
            City: '',
            Province: '',
            Country: 1,
            PostalCode: ''
          },
          WayOrder: 1
        },
        flights: [
          {
            flightNumber: '102',
            departureTime: '07:15am',
            arrivalTime: '10: 40am',
            stops: 2,
            days: ['Tuesday', 'Friday']
          },
          {
            flightNumber: '102',
            departureTime: '07:15am',
            arrivalTime: '11: 05am',
            stops: 2,
            days: ['Tuesday', 'Wednesday', 'Thursday']
          },
          {
            flightNumber: '280',
            departureTime: '02:45am',
            arrivalTime: '05:55pm',
            stops: 2,
            days: ['Tuesday', 'Wednesday', 'Thursday']
          }
        ]
      },
      {
        outboundLocation: {
          Id: 1,
          Code: 'XYZ',
          Name: 'La Ronge',
          Address: {
            Address: '',
            City: '',
            Province: '',
            Country: 1,
            PostalCode: ''
          },
          WayOrder: 1
        },
        flights: [
          {
            flightNumber: '102',
            departureTime: '08:10am',
            arrivalTime: '10:40am',
            stops: 2,
            days: ['Tuesday', 'Friday']
          },
          {
            flightNumber: '102',
            departureTime: '08:10am',
            arrivalTime: '11:05am',
            stops: 2,
            days: ['Monday', 'Wednesday', 'Thursday']
          }
        ]
      }
    ]
  },
  {
    name: 'Fond du Lac',
    inboundLocation: {
      Id: 1,
      Code: 'ZYC',
      Name: 'Fond du Lac',
      Address: {
        Address: '',
        City: '',
        Province: '',
        Country: 1,
        PostalCode: ''
      },
      WayOrder: 1
    },
    scheduleoutboundDate: '01/13/2019',
    scheduleinboundDate: '06/04/2019',
    routes: [
      {
        outboundLocation: {
          Id: 1,
          Code: 'XYZ',
          Name: 'La Ronge',
          Address: {
            Address: '',
            City: '',
            Province: '',
            Country: 1,
            PostalCode: ''
          },
          WayOrder: 1
        },
        flights: [
          {
            flightNumber: '102',
            departureTime: '07:15am',
            arrivalTime: '08:55am',
            stops: 2,
            days: ['Monday', 'Wednesday', 'Thursday']
          },
          {
            flightNumber: '502',
            departureTime: '01:00pm',
            arrivalTime: '02:40pm',
            stops: 2,
            days: ['Weekly']
          },
          {
            flightNumber: '280',
            departureTime: '02:45am',
            arrivalTime: '05:55pm',
            stops: 2,
            days: ['Tuesday', 'Wednesday', 'Thursday']
          }
        ]
      },
      {
        outboundLocation: {
          Id: 1,
          Code: 'XYZ',
          Name: 'Prince Albert',
          Address: {
            Address: '',
            City: '',
            Province: '',
            Country: 1,
            PostalCode: ''
          },
          WayOrder: 1
        },
        flights: [
          {
            flightNumber: '102',
            departureTime: '08:10am',
            arrivalTime: '08:55am',
            stops: 2,
            days: ['Tuesday', 'Friday']
          },
          {
            flightNumber: '502',
            departureTime: '08:10am',
            arrivalTime: '09:05am',
            stops: 2,
            days: ['Monday', 'Wednesday', 'Thursday']
          }
        ]
      }
    ]
  },
  {
    name: 'La Ronge',
    inboundLocation: {
      Id: 1,
      Code: 'ZYC',
      Name: 'La Ronge',
      Address: {
        Address: '',
        City: '',
        Province: '',
        Country: 1,
        PostalCode: ''
      },
      WayOrder: 1
    },
    scheduleoutboundDate: '01/13/2019',
    scheduleinboundDate: '06/04/2019',
    routes: [
      {
        outboundLocation: {
          Id: 1,
          Code: 'ZYC',
          Name: 'Saskatoon',
          Address: {
            Address: '',
            City: '',
            Province: '',
            Country: 1,
            PostalCode: ''
          },
          WayOrder: 1
        },
        flights: [
          {
            flightNumber: '102',
            departureTime: '05:55pm',
            arrivalTime: '05:55pm',
            stops: 2,
            days: ['Monday', 'Wednesday', 'Thursday']
          },
          {
            flightNumber: '502',
            departureTime: '05:55pm',
            arrivalTime: '05:55pm',
            stops: 2,
            days: ['Weekly']
          },
          {
            flightNumber: '280',
            departureTime: '05:55pm',
            arrivalTime: '05:55pm',
            stops: 2,
            days: ['Tuesday', 'Wednesday', 'Thursday']
          }
        ]
      },
      {
        outboundLocation: {
          Id: 1,
          Code: 'ZYC',
          Name: 'Prince Albert',
          Address: {
            Address: '',
            City: '',
            Province: '',
            Country: 1,
            PostalCode: ''
          },
          WayOrder: 1
        },
        flights: [
          {
            flightNumber: '102',
            departureTime: '08:10am',
            arrivalTime: '08:55am',
            stops: 2,
            days: ['Tuesday', 'Friday']
          },
          {
            flightNumber: '502',
            departureTime: '08:10am',
            arrivalTime: '09:05am',
            stops: 2,
            days: ['Monday', 'Wednesday', 'Thursday']
          }
        ]
      }
    ]
  }
];

const schedules: any = {
  ExpressAir: schedulesData[0],
  Fond_du_lac: schedulesData[1],
  La_ronge: schedulesData[2]
};

export default schedules;
