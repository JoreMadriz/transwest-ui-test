import { PADDING_2 } from '../../shared/theme/gutters';

export default {
  container: {
    paddingLeft: `${PADDING_2}px`,
    paddingRight: `${PADDING_2}px`
  },
  title: {
    fontWeight: 'bold' as 'bold'
  }
};
