import React from 'react';
import { Grid, Hidden } from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';

import MainInformation from './components/main-information/main-information';
import map from '../../shared/assets/images/map.png';
import schedules from './utils/schedulesData';
import ScheduleState from './schedule-state';
import SchedulesTemplate from './components/schedules-template/schedules-template';
import { schedulesUris, schedulesUrls } from './utils/schedulesData';
import SectionSpacer from '../../shared/theme/components/section-spacer/section-spacer';
import SideMenu from '../../shared/components/side-menu/side-menu';
import { SpacerType } from '../../shared/theme/components/section-spacer/SpacerType';
import styles from './schedule.styles';
import SectionBreadcrumb from '../booking/components/section-breadcrumb/section-breadcrumb';

interface ScheduleProps {
  match: any;
  history: any;
  classes: any;
}

export class Schedules extends React.Component<ScheduleProps, ScheduleState> {
  private static readonly MAINPAGE = '/';
  private static readonly BASEURL = '/schedules/';

  constructor(props: ScheduleProps) {
    super(props);
    this.state = {
      selectedUri: '/'
    };
  }

  componentDidUpdate() {
    const { page } = this.props.match.params;

    if (!page && this.state.selectedUri != Schedules.MAINPAGE) {
      this.setState({ selectedUri: Schedules.MAINPAGE });
    } else if (`${Schedules.BASEURL}${page}` != this.state.selectedUri) {
      const dataFromUrl = schedulesUrls[page];
      if (dataFromUrl) {
        this.setState({ selectedUri: dataFromUrl.uri });
      }
    }
  }

  componentDidMount() {
    const { page } = this.props.match.params;
    const dataFromUrl = schedulesUrls[page];
    if (dataFromUrl) {
      this.setState({ selectedUri: dataFromUrl.uri });
    }
  }

  private selectSchedule = (uri: string): any => {
    return async () => {
      await this.setState({
        ...this.state,
        selectedUri: uri
      });
    };
  };

  private getScheduleToDisplay = (selectedUri: string) => {
    return selectedUri != '/' ? (
      <SchedulesTemplate
        selectedUri={selectedUri}
        schedule={schedules[selectedUri.replace(Schedules.BASEURL, '')]}
      />
    ) : (
      <Hidden mdDown>
        <Grid>
          <img src={map} />
        </Grid>
      </Hidden>
    );
  };

  private onChangeAllSchedules(uri: string) {
    this.props.history.push(uri);
    return async () => {
      await this.setState({
        selectedUri: uri
      });
    };
  }

  public render() {
    const { selectedUri } = this.state;
    const { selectSchedule } = this;
    const { classes } = this.props;
    const { container } = classes;
    return (
      <Grid className={container}>
        <Hidden mdUp>
          {selectedUri != Schedules.MAINPAGE && (
            <Grid>
              <SectionSpacer spacerType={SpacerType.TINY} />
              <SectionBreadcrumb
                handleStepChange={() => {
                  return this.onChangeAllSchedules(Schedules.BASEURL);
                }}
              >
                {'All Schedules'}
              </SectionBreadcrumb>
              <SectionSpacer spacerType={SpacerType.TINY} />
            </Grid>
          )}
        </Hidden>

        <Hidden mdUp>{selectedUri === Schedules.MAINPAGE && <MainInformation />}</Hidden>

        <Hidden mdDown>
          <MainInformation />
        </Hidden>

        <Grid container spacing={8}>
          <Hidden mdDown>
            <Grid item md={3} xs={12}>
              <SideMenu
                handleItemClick={selectSchedule}
                menuItems={schedulesUris}
                selectedUri={selectedUri}
              />
            </Grid>
          </Hidden>

          <Hidden mdUp>
            {selectedUri === Schedules.MAINPAGE && (
              <Grid item md={3} xs={12}>
                <SideMenu
                  handleItemClick={selectSchedule}
                  menuItems={schedulesUris}
                  selectedUri={selectedUri}
                />
              </Grid>
            )}
          </Hidden>

          <Grid item md={6} xs={12}>
            <div>{this.getScheduleToDisplay(selectedUri)}</div>
          </Grid>
        </Grid>
      </Grid>
    );
  }
}

export default withStyles(styles)(Schedules);
