import React from 'react';
import {
  Grid,
  Table,
  TableHead,
  TableCell,
  TableRow,
  TableBody,
  withStyles,
  Typography
} from '@material-ui/core';

import AirportSelectionData from '../../../booking/components/airport-selection/models/airport-selection-data';
import { Schedule } from '../../utils/schedulesData';
import location from '../../../booking/components/airport-selection/models/location';
import { scheduleDetailStyles } from './schedule-detail.styles';
import SectionSpacer from '../../../../shared/theme/components/section-spacer/section-spacer';
import { SpacerType } from '../../../../shared/theme/components/section-spacer/SpacerType';
import { triggers } from '../../../booking/booking.observable';
import { Link } from 'react-router-dom';
import CustomButton from '../../../../shared/components/custom-button/custom-button';

export interface IScheduleDetailProps {
  schedule: Schedule;
  classes: any;
}

function reformatDays(days: string[]): string {
  let finalData: string = '';
  if (days[0].includes('Weekly')) {
    return (finalData = 'Weekly');
  }
  days.forEach((day) => {
    finalData += `${day.substr(0, 3)}, `;
  });

  return finalData.substr(0, finalData.length - 2);
}

function buildAirportSelectionData(
  inboundLocation: location,
  outboundLocation: location
): AirportSelectionData {
  return {
    departureAirport: inboundLocation,
    destinationAirport: outboundLocation,
    isOneWayTrip: true,
    promoCode: ''
  };
}

const ScheduleDetail = (props: IScheduleDetailProps) => {
  const BOOKING_PATH = '/booking';
  const BUTTON_TEXT = 'Book Now';
  const { schedule, classes } = props;
  const { inboundLocation, routes, scheduleoutboundDate, scheduleinboundDate } = schedule;
  const {
    containerItem,
    tableContainer,
    title,
    subtitle,
    tableHead,
    headTitle,
    border,
    table,
    cell,
    linkButton
  } = classes;

  return (
    <Grid container>
      <Grid item className={containerItem} xs={12}>
        <Typography
          className={subtitle}
        >{`THIS SCHEDULE IS IN EFFECT FROM ${scheduleoutboundDate} to ${scheduleinboundDate}`}</Typography>
        <Typography>{'Schedules are subject to change without notice.'}</Typography>
        <Typography>{'Please confirm flight times on day of departure.'}</Typography>
        <SectionSpacer spacerType={SpacerType.TINY} />
      </Grid>

      <Grid item xs={12}>
        {routes.map((route) => {
          const { outboundLocation, flights } = route;
          return (
            <div className={tableContainer} key={outboundLocation.Name}>
              <Typography align="left" className={title}>
                {`${inboundLocation.Name} to ${outboundLocation.Name}`}
              </Typography>
              <Table padding="dense" className={table}>
                <TableHead className={tableHead}>
                  <TableRow>
                    <TableCell className={`${headTitle} ${border}`}>Flight</TableCell>
                    <TableCell className={`${headTitle} ${border}`}>Depart</TableCell>
                    <TableCell className={`${headTitle} ${border}`}>Arrival</TableCell>
                    <TableCell className={`${headTitle} ${border}`}>Stops</TableCell>
                    <TableCell className={`${headTitle} ${border}`}>Days</TableCell>
                    <TableCell className={`${headTitle} ${border}`}> </TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {flights.map((flight, index) => {
                    const { flightNumber, departureTime, arrivalTime, days, stops } = flight;
                    return (
                      <TableRow className={index % 2 != 0 ? tableHead : ''} key={index}>
                        <TableCell className={`${border} ${cell}`}>{flightNumber}</TableCell>
                        <TableCell className={`${border} ${cell}`}>{departureTime}</TableCell>
                        <TableCell className={`${border} ${cell}`}>{arrivalTime}</TableCell>
                        <TableCell className={border}>{stops}</TableCell>
                        <TableCell className={border}>{reformatDays(days)}</TableCell>
                        <TableCell className={border}>
                          {
                            <Link className={linkButton} to={BOOKING_PATH}>
                              <CustomButton
                                text={BUTTON_TEXT}
                                onClickAction={() => {
                                  triggers.onChangeAirportSelection(
                                    buildAirportSelectionData(inboundLocation, outboundLocation)
                                  );
                                }}
                                path={BOOKING_PATH}
                              />
                            </Link>
                          }
                        </TableCell>
                      </TableRow>
                    );
                  })}
                </TableBody>
              </Table>
            </div>
          );
        })}
      </Grid>
    </Grid>
  );
};

export default withStyles(scheduleDetailStyles)(ScheduleDetail);
