import { PRIMARY_COLOR, SECONDARY_COLOR, BACKGROUND_COLOR1 } from '../../../../shared/theme/colors';
import { MARGIN_2 } from '../../../../shared/theme/gutters';

export const scheduleDetailStyles = {
  containerItem: {
    textAlign: 'left' as 'left'
  },
  subtitle: {
    fontWeight: 'bold' as 'bold'
  },
  tableContainer: {
    overflowX: 'auto' as 'auto',
    marginBottom: '3%'
  },
  title: {
    fontWeight: 'bold' as 'bold',
    fontSize: '20px'
  },
  subTitle: {
    color: SECONDARY_COLOR,
    textAlign: 'left' as 'left',
    fontWeight: 'normal' as 'normal',
    fontSize: '120%'
  },
  paragraph: {
    marginBottom: MARGIN_2,
    color: PRIMARY_COLOR
  },
  tableHead: {
    backgroundColor: BACKGROUND_COLOR1,
    maxHeight: '48px !important'
  },
  headTitle: {
    color: PRIMARY_COLOR,
    fontWeight: 'bold' as 'bold'
  },
  table: {
    border: `1px solid ${BACKGROUND_COLOR1}`
  },
  border: {
    borderBottom: `1px solid ${BACKGROUND_COLOR1}`
  },
  cell: {
    whiteSpace: 'nowrap' as 'nowrap'
  },
  linkButton: {
    textDecoration: 'none' as 'none'
  }
};
