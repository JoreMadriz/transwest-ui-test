const styles = {
  smallTitle: {
    fontWeight: 'bold' as 'bold',
    textDecoration: 'underline' as 'underline'
  },
  textBox: {
    marginBottom: '5px'
  },
  littleTextBox: {
    marginBottom: '8px'
  }
};

export default styles;
