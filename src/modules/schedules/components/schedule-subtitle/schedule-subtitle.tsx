import React from 'react';
import { Grid, Typography, withStyles } from '@material-ui/core';

import styles from './schedule-subtitle.style';

export interface ScheduleSubtitleProps {
  classes: any;
}

export interface ScheduleSubtitleState {}

class ScheduleSubtitle extends React.Component<ScheduleSubtitleProps> {
  render() {
    const { classes } = this.props;
    const { smallTitle, textBox, littleTextBox, container } = classes;
    return (
      <Grid container className={container}>
        <Grid item md={4} xs={12}>
          <Typography align="left" color="primary">
            {'Here is the complete list of schedules now in effect.'}
          </Typography>
          <br />
          <Typography align="left" color="primary">
            {'Please note that schedules are subject to change without notice.'}
          </Typography>
          <Typography align="left" color="primary">
            {'Please confirm departure and arrival times on the day of travel.'}
          </Typography>
        </Grid>
        <Grid item md={4} xs={12}>
          <Grid className={textBox}>
            <Typography className={smallTitle} align="left">
              {'Winter Schedule #1 (PDF)'}
            </Typography>
            <Typography align="left">{'Effective December 9, 2018 - January 12, 2019'}</Typography>
          </Grid>
          <Grid>
            <Typography className={smallTitle} align="left">
              {'Winter Schedule #2 (PDF)'}
            </Typography>
            <Typography align="left">{'Effective January 13, 2019 - April 6, 2019'}</Typography>
          </Grid>
        </Grid>
        <Grid item md={4} xs={12}>
          <Typography align="left" className={`${smallTitle} ${littleTextBox}`}>
            {'Local Domestic Tariff (PDF)'}
          </Typography>
          <Typography align="left" className={`${smallTitle} ${littleTextBox}`}>
            {'Domestic Licence (PDF)'}
          </Typography>
          <Typography align="left" className={`${smallTitle} ${littleTextBox}`}>
            {'Cargo Rates (PDF)'}
          </Typography>
        </Grid>
      </Grid>
    );
  }
}

export default withStyles(styles)(ScheduleSubtitle);
