import { MARGIN_2 } from '../../../../shared/theme/gutters';

const styles = (theme: any) => {
  return {
    paragraph: {
      fontWeight: 'bold' as 'bold',
      noWrap: 'false' as 'false'
    },
    container: {
      marginTop: `${MARGIN_2}px`,
      marginBottom: `${MARGIN_2}px`
    },
    smParagraph: {
      fontSize: '16px'
    },
    image: {
      width: '100%',
      height: 'auto' as 'auto',
      [theme.breakpoints.up('md')]: {
        maxWidth: '600px'
      }
    }
  };
};

export default styles;
