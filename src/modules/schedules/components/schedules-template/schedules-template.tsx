import React from 'react';
import { Typography, withStyles, Grid } from '@material-ui/core';

import imagePlaceholder from '../../../../shared/assets/images/placeholder.png';
import { Schedule } from '../../utils/schedulesData';
import ScheduleDetail from '../schedule-detail/schedule-detail';
import SectionSpacer from '../../../../shared/theme/components/section-spacer/section-spacer';
import { SpacerType } from '../../../../shared/theme/components/section-spacer/SpacerType';
import styles from './schedules-template.style';

export interface SchedulesTemplateProps {
  classes: any;
  schedule: Schedule;
  selectedUri: string;
}

export interface SchedulesTemplateState {}

const getFirstPage = (classes: any) => {
  const { paragraph, smallParagraph, image } = classes;
  return (
    <Grid>
      <Grid item xs={12}>
        <Typography variant="h6" align="left" className={paragraph}>
          {'Transwest Air flights connect Saskatoon and Regina - '}
        </Typography>
        <Typography variant="h6" align="left" className={paragraph}>
          {'perfect for the business traveler.'}
        </Typography>
      </Grid>

      <SectionSpacer spacerType={SpacerType.TINY} />

      <Grid item md={6} xs={12}>
        <Typography className={smallParagraph} align="left">
          {`Flights depart from a private VIP terminal, with simplified security. Park your car and board within minutes ­ 
          or take time to relax in our luxury waiting room.`}
        </Typography>
      </Grid>

      <Grid container alignContent="flex-start">
        <img className={image} src={imagePlaceholder} />
      </Grid>

      <SectionSpacer spacerType={SpacerType.TINY} />
    </Grid>
  );
};

class SchedulesTemplate extends React.Component<SchedulesTemplateProps, SchedulesTemplateState> {
  render() {
    const { children, classes, schedule, selectedUri } = this.props;
    const { container } = classes;
    const baseURL = '/schedules/ExpressAir';
    return (
      <Grid container className={container}>
        {schedule && (
          <Grid item md={6} xs={12}>
            <Typography align="left" variant="h2">
              {schedule.name}
            </Typography>
            <SectionSpacer spacerType={SpacerType.SMALL} />
          </Grid>
        )}
        <SectionSpacer spacerType={SpacerType.SMALL} />
        {selectedUri === baseURL && getFirstPage(classes)}
        {schedule && <ScheduleDetail schedule={schedule} />}
        {children}
      </Grid>
    );
  }
}

export default withStyles(styles)(SchedulesTemplate);
