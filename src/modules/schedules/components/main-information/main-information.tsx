import React from 'react';
import { Grid, Typography } from '@material-ui/core';

import SectionSpacer from '../../../../shared/theme/components/section-spacer/section-spacer';
import { SpacerType } from '../../../../shared/theme/components/section-spacer/SpacerType';
import ScheduleSubtitle from '../schedule-subtitle/schedule-subtitle';

class MainInformation extends React.Component {
  private TITLE: string = 'Schedules';
  render() {
    return (
      <Grid>
        <SectionSpacer spacerType={SpacerType.MEDIUM} />
        <Grid item md={6} xs={12}>
          <Typography align="left" color="primary" variant="h3">
            {this.TITLE}
          </Typography>
        </Grid>
        <SectionSpacer spacerType={SpacerType.MEDIUM} />
        <ScheduleSubtitle />
        <SectionSpacer spacerType={SpacerType.SMALL} />
      </Grid>
    );
  }
}

export default MainInformation;
