import moment from 'moment';
import { Subject, combineLatest } from 'rxjs';

import BookingTriggers from './models/booking.triggers';
import { createReservationRequest } from './services/booking-reservation.adapter/booking-reservation.adapter';
import {
  getDefaultAirportData,
  getDefaultDatesInfo,
  getDefaultFlightData
} from './utils/default-props.util';
import { getDefaultCheckout } from './utils/checkout-default-props.util';
import {
  getDefaultPassengerData,
  getPassengerInformation,
  getDefaultPassengerInfo
} from './utils/passenger-default-props.util';
import getNewRange from './services/date-selector/date-selector.service';
import HttpService from '../../shared/services/http.service';
import PassengerSelectionService from './services/passenger-selection-service/passenger-selection.service';
import PassengerType from './models/PassengerType';
import PassengerInfo from './components/passenger-information/model/passenger-info';
import ReservationRequest from './services/booking-reservation.adapter/models/reservation-request';
import Ticket from './components/flight-selection/components/ticket-selection/models/ticket';
import CardInfo from './components/checkout/models/card-info';
import BillingInfo from './components/checkout/models/billing-info';
import { cardInfoValidation } from './utils/card-validation.util';
import {
  billingInfoValidation,
  containErrors,
  REQUIRED_TERMS_CONDITIONS
} from './utils/validation-utils';
import { convertConfirmSeat } from './utils/checkout.utils';
import { convertMonthlyFareBody, convertToCalendarPrices } from './utils/best-fare.util';
import { handleErrorMessage } from '../../shared/components/error-message/error-message.observable';
import Step from './models/Step';

export const state = {
  airportSelectionData: getDefaultAirportData(),
  bookingDatesInfo: getDefaultDatesInfo(),
  passengerData: getDefaultPassengerData(),
  reservedTickets: {},
  sessionId: '',
  flightData: getDefaultFlightData(),
  passengerInfo: getDefaultPassengerInfo(),
  checkoutInfo: getDefaultCheckout(),
  reservationTicketsIds: [],
  initialStep: Step.AIRPORT_SELECTION
};

const seatReservationObs = new Subject();

export const BookingStateObs = new Subject();

export const BookingObservable = combineLatest(BookingStateObs, (bookingState: any) => {
  return { ...bookingState };
});

export const triggers: BookingTriggers = {
  onChangeAirportSelection: (airportSelectionData: any) => {
    const observableState = state;
    observableState.airportSelectionData = airportSelectionData;
    observableState.bookingDatesInfo = getDefaultDatesInfo();
    observableState.passengerData = getDefaultPassengerData();
    observableState.flightData = getDefaultFlightData();
    observableState.passengerInfo = getDefaultPassengerInfo();
    observableState.checkoutInfo = getDefaultCheckout();
    observableState.initialStep = Step.PASSENGERS_SELECTION;
    BookingStateObs.next(observableState);
  },
  onChangePassengerInformation: (passengerInfo: PassengerInfo) => {
    const observableState = state;
    observableState.passengerInfo = passengerInfo;
    BookingStateObs.next(observableState);
  },
  createPassengerInformation: () => {
    const observableState = state;
    observableState.passengerInfo = getPassengerInformation(state.passengerData);
    BookingStateObs.next(observableState);
  },
  onDateClickSelection: (date: Date) => {
    const observableState = state;
    const { bookingDatesInfo, airportSelectionData } = observableState;
    const { isOneWayTrip } = airportSelectionData;
    const { isOutboundDateSelected, prices } = bookingDatesInfo;
    let { outboundDate, inboundDate } = bookingDatesInfo;
    outboundDate = outboundDate ? outboundDate : moment().toDate();
    inboundDate = inboundDate ? inboundDate : moment().toDate();
    ({ outboundDate, inboundDate } = getNewRange(isOutboundDateSelected, outboundDate, date));
    observableState.bookingDatesInfo = {
      outboundDate,
      inboundDate,
      isOutboundDateSelected: isOneWayTrip ? false : !isOutboundDateSelected,
      prices: prices
    };

    BookingStateObs.next(observableState);
  },
  onDatePaginationClick: (isOutbound: boolean, newDate: Date) => {
    const observableState = state;
    const { bookingDatesInfo } = observableState;
    isOutbound
      ? (bookingDatesInfo.outboundDate = newDate)
      : (bookingDatesInfo.inboundDate = newDate);
    BookingStateObs.next(observableState);
  },
  onChangePassengersQuantity: (type: PassengerType, isAdding: boolean) => {
    const observableState = state;
    const { passengerData } = observableState;
    let passengerSelectionService: PassengerSelectionService = new PassengerSelectionService();

    observableState.passengerData = passengerSelectionService.getNewPassengerValues(
      passengerData,
      isAdding,
      type
    );

    BookingStateObs.next(observableState);
  },
  reserveSeats: () => {
    const reservationRequest: ReservationRequest = createReservationRequest(
      {
        airportSelection: state.airportSelectionData,
        dateSelection: state.bookingDatesInfo,
        passengerSelection: state.passengerData,
        flightSelection: state.flightData
      },
      state.sessionId
    );
    HttpService.post('Booking/ReserveSeats', {}, reservationRequest)
      .then((response: any) => {
        const observableState = state;
        observableState.reservationTicketsIds = response.data;
        console.log(observableState.reservationTicketsIds);
        seatReservationObs.next(observableState);
      })
      .catch((error) => {
        console.log(error);
        //TODO send a toast with a message and call logging endpoint
      });
  },
  onTicketClick: (isOutbound: boolean, ticket: Ticket) => {
    return () => {
      const observableState = state;
      const type = isOutbound ? 'outboundTicket' : 'inboundTicket';
      observableState.flightData[type] = ticket;
      BookingStateObs.next(observableState);
    };
  },
  getSessionId: () => {
    HttpService.post('/Booking/BeginSession', {}, {})
      .then((response: any) => {
        const observableState = state;
        observableState.sessionId = response.sessionId;
        BookingStateObs.next(observableState);
      })
      .catch((error) => {
        console.log(error);
        //TODO send a toast with a message and call logging endpoint
      });
  },
  onChangeCardInfo: (cardInfo: CardInfo) => {
    const observableState = state;
    observableState.checkoutInfo.cardInfo = cardInfo;
    BookingStateObs.next(observableState);
  },
  onChangeBillingInfo: (billingInfo: BillingInfo) => {
    const observableState = state;
    observableState.checkoutInfo.billingInfo = billingInfo;
    BookingStateObs.next(observableState);
  },
  onPayNow: () => {
    const { sessionId, reservationTicketsIds, passengerInfo, checkoutInfo } = state;
    const { billingInfo, cardInfo, isTermsAgreed } = state.checkoutInfo;
    cardInfo.errors = cardInfoValidation(cardInfo);
    billingInfo.errors = billingInfoValidation(billingInfo);
    isTermsAgreed.error = isTermsAgreed.isAgreed ? '' : REQUIRED_TERMS_CONDITIONS;
    const isCardInfoValid = !containErrors(cardInfo.errors);
    const isBillingInfoValid = !containErrors(billingInfo.errors);
    if (isCardInfoValid && isBillingInfoValid && isTermsAgreed.isAgreed) {
      const confirmSeatsRequest = convertConfirmSeat(
        passengerInfo,
        checkoutInfo,
        sessionId,
        reservationTicketsIds
      );
      HttpService.post('/Booking/ConfirmSeats', {}, confirmSeatsRequest)
        .then((data) => {
          console.log(data);
          handleErrorMessage('Payment was successful', 'green');
        })
        .catch((error) => {
          console.log(error);
          handleErrorMessage('There was a problem processing your payment options');
        });
    }
    state.checkoutInfo.billingInfo = billingInfo;
    state.checkoutInfo.cardInfo = cardInfo;
    state.checkoutInfo.isTermsAgreed = isTermsAgreed;

    BookingStateObs.next(state);
  },
  getCalendarPrices: () => {
    const { airportSelectionData, sessionId, passengerData } = state;
    const { departureAirport, destinationAirport } = airportSelectionData;
    const requestBody = convertMonthlyFareBody(
      departureAirport!.Id,
      destinationAirport!.Id,
      sessionId,
      passengerData
    );
    HttpService.post('/Booking/GetBestFareForDates', {}, requestBody)
      .then((data) => {
        console.log(data);

        const calendarPrices = convertToCalendarPrices(data);
        state.bookingDatesInfo.prices = calendarPrices;
        BookingStateObs.next(state);
      })
      .catch((error) => {
        console.log(error);
      });
  },
  onChangeTerms: () => {
    const { checkoutInfo } = state;
    checkoutInfo.isTermsAgreed.isAgreed = !checkoutInfo.isTermsAgreed.isAgreed;
    state.checkoutInfo = {
      ...checkoutInfo
    };

    BookingStateObs.next(state);
  }
};
