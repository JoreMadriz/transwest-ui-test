import dateSelectorService from './date-selector.service'
import moment from 'moment'
describe('Date selector service test suite', () => {
    it('should swap the date objects when the outboundDate is greather than the selectedDate',() => {
        const outboundDate = moment().add(1,'days').toDate();
        const newDate = moment().toDate();
        const result = dateSelectorService(true,outboundDate, newDate);
        const expectedResult = {outboundDate: newDate, inboundDate: outboundDate}
        expect(result).toEqual(expectedResult);
    });
    it('should return an object with two dates ordered', () => {
        const outboundDate = moment().toDate()
        const newDate = moment().add(2,'days').toDate();
        const result = dateSelectorService(true,outboundDate, newDate);
        const expectedResult = {outboundDate, inboundDate: newDate}
        expect(result).toEqual(expectedResult)
        expect(result)
    });
    it('should reset the object with only the first date', ()=> {
        const outboundDate = moment().toDate()
        const newDate = moment().add(1,'months').toDate();
        const result = dateSelectorService(false,outboundDate, newDate);
        const expectedResult = {outboundDate: newDate,inboundDate: null}
        expect(result).toEqual(expectedResult)
    })
})