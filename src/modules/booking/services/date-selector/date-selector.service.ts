const getNewRange = (isOutboundDateSelected: boolean, outboundDate: Date, selectedDate: Date) => {
  if (isOutboundDateSelected) return getDateRange(outboundDate, selectedDate);
  return { outboundDate: selectedDate, inboundDate: null };
};

const getDateRange = (outboundDate: Date, selectedDate: Date) => {
  let inboundDate;
  if (outboundDate > selectedDate) {
    inboundDate = outboundDate;
    outboundDate = selectedDate;
    return { outboundDate, inboundDate };
  }
  return { outboundDate, inboundDate: selectedDate };
};
export default getNewRange;
