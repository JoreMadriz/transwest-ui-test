import CardValidator from 'card-validator';
import Step from '../../models/Step';
import BookingProps from '../../booking.props';
import MainPassenger from '../../components/passenger-information/model/main-passenger';
import Passenger from '../../components/passenger-information/model/Passenger';
import UnderagePassenger from '../../components/passenger-information/model/underage-passenger';

export default class StepValidationService {
  validateAirportSelection = (props: BookingProps) => {
    const { airportSelectionData } = props;
    return (
      !!airportSelectionData &&
      (!!airportSelectionData.departureAirport && !!airportSelectionData.destinationAirport)
    );
  };

  validateBookingDates = (props: BookingProps) => {
    const { bookingDatesInfo, airportSelectionData } = props;
    const { isOneWayTrip } = airportSelectionData;
    const { inboundDate, outboundDate } = bookingDatesInfo;
    return isOneWayTrip ? !!outboundDate : inboundDate && outboundDate;
  };
  validateTickets = (props: BookingProps) => {
    const { flightData, airportSelectionData } = props;
    const { inboundTicket, outboundTicket } = flightData;
    const { isOneWayTrip } = airportSelectionData;
    const isOutboundTicketValid = outboundTicket.flightId !== 0;
    const isInboundTicketValid = inboundTicket.flightId !== 0;
    return (
      (isOneWayTrip && isOutboundTicketValid) || (isOutboundTicketValid && isInboundTicketValid)
    );
  };

  private validateExistingErrors = (fieldErrors: string[]): boolean => {
    return fieldErrors.length === 0;
  };

  private validateMainPassenger = (mainPassenger: MainPassenger): boolean => {
    if (!mainPassenger) return false;
    const { errors, mainErrors } = mainPassenger;
    return (
      this.validateExistingErrors(mainErrors.code) &&
      this.validateExistingErrors(mainErrors.confirmEmail) &&
      this.validateExistingErrors(mainErrors.email) &&
      this.validateExistingErrors(errors.firstName) &&
      this.validateExistingErrors(errors.lastName) &&
      this.validateExistingErrors(mainErrors.phoneNumber)
    );
  };

  private validateAdultsInfo = (adultPassengers: Passenger[]): boolean => {
    return adultPassengers.reduce((previousValue, currentValue) => {
      const { errors } = currentValue;
      return (
        previousValue &&
        this.validateExistingErrors(errors.firstName) &&
        this.validateExistingErrors(errors.lastName)
      );
    }, true);
  };

  private validateUnderageInfo = (underagePassengers: UnderagePassenger[]): boolean => {
    return underagePassengers.reduce((previousValue, currentValue) => {
      const { errors, underageErrors } = currentValue;
      return (
        previousValue &&
        this.validateExistingErrors(errors.firstName) &&
        this.validateExistingErrors(errors.lastName) &&
        this.validateExistingErrors(underageErrors.birthDate)
      );
    }, true);
  };

  validatePassengerInfo = (props: BookingProps) => {
    const { passengerInfo } = props;
    return (
      this.validateMainPassenger(passengerInfo.mainPassenger) &&
      this.validateAdultsInfo(passengerInfo.adultPassengers) &&
      this.validateUnderageInfo(passengerInfo.underagePassengers)
    );
  };
  validateCheckout = (props: BookingProps) => {
    const { checkoutInfo } = props;
    const { billingInfo, cardInfo, isTermsAgreed } = checkoutInfo;
    const isBillingInfoValid = Object.values(billingInfo).every((elem) => elem !== '');
    const isCardValid = CardValidator.number(cardInfo.cardNumber.toString()).isValid || true;
    const isExpDateValid =
      CardValidator.expirationDate(cardInfo.expirationDate.toString()).isValid || true;
    const isCCVValid = CardValidator.cvv(cardInfo.cvv.toString()).isValid || true;
    return isBillingInfoValid && isCardValid && isExpDateValid && isCCVValid && isTermsAgreed;
  };
  private readonly validations: any = {
    AIRPORT_SELECTION: this.validateAirportSelection,
    PASSENGERS_SELECTION: () => {
      return true;
    },
    DATE_SELECTION: this.validateBookingDates,
    FLIGHT_SELECTION: this.validateTickets,
    PASSENGER_INFORMATION: this.validatePassengerInfo,
    CHECKOUT: () => {
      return true;
    },
  };

  public getValidation(currentStep: Step) {
    return this.validations[Step[currentStep]];
  }
}
