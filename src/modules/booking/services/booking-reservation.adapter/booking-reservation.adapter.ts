import BookingData from '../../models/BookingData';
import ReservationRequest from './models/reservation-request';
import RouteSelection from './models/route-selection';
import AirportSelectionData from '../../components/airport-selection/models/airport-selection-data';
import PassengerData from '../../models/PassengerData';
import FlightSelectionData from '../../models/FlightSelectionData';

const getRoute = (
  destination: string,
  origin: string,
  direction: 0 | 1,
  flightDate: Date,
  groupId: number,
  fareBasisId: number
): RouteSelection => {
  return {
    Destination: destination,
    Direction: direction,
    FlightDate: flightDate,
    IsSpecified: true,
    Origin: origin,
    GroupId: groupId,
    FareBasisId: fareBasisId
  };
};
const getSeatCount = (passengerData: PassengerData) => {
  return (
    passengerData.adultsQuantity +
    passengerData.babiesQuantity +
    passengerData.childrenQuantity +
    passengerData.teensQuantity
  );
};

export const createReservationRequest = (
  bookingData: BookingData,
  sessionId: string
): ReservationRequest => {
  const airportSelection: AirportSelectionData | undefined = bookingData!.airportSelection;
  const dateSelection = bookingData!.dateSelection;
  const passengerData: PassengerData = bookingData!.passengerSelection;
  const flightSelectionData: FlightSelectionData = bookingData!.flightSelection;

  const reservationRequest: ReservationRequest = {
    SessionId: sessionId,
    SeatCount: getSeatCount(passengerData),
    OutboundRoute: getRoute(
      airportSelection!.destinationAirport!.Code,
      airportSelection!.departureAirport!.Code,
      1,
      dateSelection.outboundDate!,
      flightSelectionData.outboundTicket.groupId,
      flightSelectionData.outboundTicket.fareBasisId
    ),
    InboundRoute: airportSelection!.isOneWayTrip
      ? undefined
      : getRoute(
          airportSelection!.departureAirport!.Code,
          airportSelection!.destinationAirport!.Code,
          0,
          dateSelection.inboundDate!,
          flightSelectionData.inboundTicket.groupId,
          flightSelectionData.inboundTicket.fareBasisId
        )
  };
  return reservationRequest;
};
