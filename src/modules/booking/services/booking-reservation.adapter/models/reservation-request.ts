import RouteSelection from './route-selection';

export default interface ReservationRequest {
  SessionId: string;
  SeatCount?: number;
  AssociatedTickedId?: number;
  OutboundRoute: RouteSelection;
  InboundRoute: RouteSelection | undefined;
}
