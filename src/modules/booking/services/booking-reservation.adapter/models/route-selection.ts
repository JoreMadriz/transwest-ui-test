export default interface RouteSelection {
  FareBasisId?: number;
  GroupId?: number;
  FlightDate: Date;
  Origin: string;
  Destination: string;
  Direction: 1 | 0;
  IsSpecified: true;
}
