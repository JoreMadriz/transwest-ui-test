import PassengerData from '../../models/PassengerData';
import PassengerType from '../../models/PassengerType';

export default class PassengerSelectionService {
  getNewPassengerValues = (
    passengerData: PassengerData,
    isAdding: boolean,
    type: PassengerType
  ): PassengerData => {
    return getPassengerValues(passengerData, isAdding, type);
  };
}

function getPassengerValues(passengerData: PassengerData, isAdding: boolean, type: PassengerType) {
  let {
    adultsQuantity,
    teensQuantity,
    childrenQuantity,
    babiesQuantity,
    totalPassengers
  } = passengerData;

  const passengerMap: any = {
    ADULTS: () => {
      adultsQuantity = getNewPassengerValue(passengerData.adultsQuantity, isAdding, 1);
    },
    TEENS: () => {
      teensQuantity = getNewPassengerValue(passengerData.teensQuantity, isAdding, 0);
    },
    CHILDREN: () => {
      childrenQuantity = getNewPassengerValue(passengerData.childrenQuantity, isAdding, 0);
    },
    BABY: () => {
      babiesQuantity = getNewPassengerValue(passengerData.babiesQuantity, isAdding, 0);
    },
    TOTAL_PASSENGER: () => {
      totalPassengers = adultsQuantity + teensQuantity + childrenQuantity + babiesQuantity;
    }
  };

  passengerMap[PassengerType[type]]();
  passengerMap[PassengerType[4]]();

  const newPassengerData: PassengerData = {
    adultsQuantity: adultsQuantity,
    teensQuantity: teensQuantity,
    childrenQuantity: childrenQuantity,
    babiesQuantity: babiesQuantity,
    totalPassengers: totalPassengers
  };

  return newPassengerData;
}

function getNewPassengerValue(passengerQuantity: number, isAdding: boolean, range: number): number {
  if (!(passengerQuantity === range && !isAdding)) {
    return alterPassengerValue(isAdding, passengerQuantity);
  }
  return passengerQuantity;
}

function alterPassengerValue(isAdding: boolean, value: number): number {
  return isAdding ? ++value : --value;
}
