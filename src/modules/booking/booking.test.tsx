import Enzyme, { shallow } from 'enzyme';
import React from 'react';
import { Booking } from './booking';
import BookingState from './booking.state';
import BookingProps from './booking.props';
import {
  getBookingTriggersProps,
  getDefaultAirportData,
  getDefaultBookingDate,
  getDefaultFlightData,
  getDefaultPassengerInfo,
} from './utils/default-props.util';

import { getDefaultPassengerData } from './utils/passenger-default-props.util';
import { getDefaultCheckout } from './utils/checkout-default-props.util';
import Step from './models/Step';

type WrapperType = Enzyme.ShallowWrapper<BookingProps, BookingState, Booking>;

describe('Booking Container test suite', () => {
  function getBookingWrapper(props: BookingProps): WrapperType {
    return shallow(<Booking {...props} />);
  }

  function getDefaultBookingProps(): BookingProps {
    return {
      triggers: getBookingTriggersProps(),
      sessionId: '',
      airportSelectionData: getDefaultAirportData(),
      bookingDatesInfo: getDefaultBookingDate(),
      flightData: getDefaultFlightData(),
      passengerData: getDefaultPassengerData(),
      checkoutInfo: getDefaultCheckout(),
      passengerInfo: getDefaultPassengerInfo(),
      initialStep: Step.AIRPORT_SELECTION
    };
  }

  it('Should increase the step number after step is valid', async () => {
    const bookingTest = getBookingWrapper(getDefaultBookingProps());
    await bookingTest.setState({ isNotStepValid: false, currentStep: 1 });
    const rightPane = bookingTest.find('WithStyles(RightPane)');
    const initStep = bookingTest.state().currentStep;
    await rightPane
      .props()
      //@ts-ignore
      .handleContinue();
    const increasedStep = bookingTest.state().currentStep;
    expect(initStep).toBeLessThan(increasedStep);
  });
  it('Should remain the same step number after step 6', async () => {
    const bookingTest = getBookingWrapper(getDefaultBookingProps());
    await bookingTest.setState({ currentStep: 6 });
    const rightPane = bookingTest.find('WithStyles(RightPane)');
    const initStep = bookingTest.state().currentStep;
    await rightPane
      .props()
      //@ts-ignore
      .handleContinue();
    const resultStep = bookingTest.state().currentStep;
    expect(initStep).toEqual(resultStep);
  });
});
