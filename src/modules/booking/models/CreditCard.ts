export default interface CreditCard {
  CardNumber: string;
  ExpiryDate: string;
  CSC: string;
}
