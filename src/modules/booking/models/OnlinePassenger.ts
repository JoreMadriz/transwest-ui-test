import ContactInfo from './ContactInfo';

export default interface OnlinePassenger {
  FirstName: string;
  LastName: string;
  PassengerType: 'M' | 'F' | 'C';
  HasInfant: boolean;
  InfantName?: string;
  Birthdate: Date;
  ResComments?: string;
  ContactInfo?: ContactInfo;
  MobilityImpaired: boolean;
  MobilityComment?: string;
}
