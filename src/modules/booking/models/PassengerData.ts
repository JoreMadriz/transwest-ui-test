export default interface PassengerData {
  adultsQuantity: number;
  teensQuantity: number;
  childrenQuantity: number;
  babiesQuantity: number;
  totalPassengers: number;
}
