export default interface BookingTriggers {
  getSessionId: Function;
  createPassengerInformation: Function;
  onChangeAirportSelection: Function;
  onChangePassengerInformation: Function;
  onChangePassengersQuantity: Function;
  onDateClickSelection: Function;
  onDatePaginationClick: Function;
  onTicketClick: Function;
  reserveSeats: Function;
  onChangeTerms: Function;
  onChangeCardInfo: Function;
  onChangeBillingInfo:Function;
  onPayNow:Function;
  getCalendarPrices: Function;
}
