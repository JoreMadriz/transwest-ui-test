import Ticket from "../components/flight-selection/components/ticket-selection/models/ticket";

export default interface FlightSelectionData {
    inboundTicket:Ticket,
    outboundTicket:Ticket
}