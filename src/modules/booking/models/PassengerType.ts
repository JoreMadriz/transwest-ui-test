enum PassengerType {
  ADULTS = 0,
  TEENS = 1,
  CHILDREN = 2,
  BABY = 3,
  TOTAL_PASSENGER = 4
}
export default PassengerType;
