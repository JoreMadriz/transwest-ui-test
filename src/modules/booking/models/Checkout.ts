import BillingInfo from '../components/checkout/models/billing-info';
import CardInfo from '../components/checkout/models/card-info';

export default interface Checkout {
  cardInfo: CardInfo;
  billingInfo: BillingInfo;
  isTermsAgreed: {
    isAgreed: boolean,
    error:string
  }
}
