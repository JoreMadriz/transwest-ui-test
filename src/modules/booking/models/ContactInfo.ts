export default interface ContactInfo {
  Email: string;
  PrimaryPhone: string;
}
