import Address from "../components/airport-selection/models/Address";
import CreditCard from "./CreditCard";

export default interface Payment {
    FirstName: string,
    LastName: string,
    Address:Address,
    CreditCard: CreditCard
}