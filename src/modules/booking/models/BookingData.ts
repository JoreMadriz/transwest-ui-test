import AirportSelectionData from '../components/airport-selection/models/airport-selection-data';
import BookingCalendar from '../components/date-selector/models/booking-calendar';
import FlightSelectionData from './FlightSelectionData';
import PassengerData from './PassengerData';

export default interface BookingData {
  airportSelection: AirportSelectionData | undefined;
  dateSelection: BookingCalendar;
  flightSelection: FlightSelectionData;
  passengerSelection: PassengerData;
}
