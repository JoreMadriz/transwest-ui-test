import moment from 'moment';
import { Subject } from 'rxjs';

import HttpObservableFactory from '../../shared/react-rx/http-observable.factory';
import BookingCalendar from './components/date-selector/models/booking-calendar';
import { BookingStateObs, triggers, state } from './booking.observable';
import PassengerData from './models/PassengerData';
import PassengerType from './models/PassengerType';

HttpObservableFactory.getObservable = jest.fn(() => {
  return new Subject();
});
HttpObservableFactory.postObservable = jest.fn(() => {
  return new Subject();
});

function triggerPassengerSelection(type: PassengerType, isAdding: boolean) {
  const { airportSelectionData, bookingDatesInfo } = state;
  BookingStateObs.next({
    airportSelectionData,
    bookingDatesInfo,
    passengerData: buildMockPassengerData(1, 0, 0, 0, 0)
  });
  triggers.onChangePassengersQuantity(type, isAdding);
}

function buildMockPassengerData(
  adultsQuantity: number,
  teensQuantity: number,
  childrenQuantity: number,
  babiesQuantity: number,
  totalPassengers: number
): PassengerData {
  return {
    adultsQuantity,
    teensQuantity,
    childrenQuantity,
    babiesQuantity,
    totalPassengers
  };
}

describe('booking observable test suite', () => {
  it('Should set the outbound date only', () => {
    const currentDate = moment().toDate();
    triggers.onDateClickSelection(currentDate);
    const { bookingDatesInfo } = state;

    expect(bookingDatesInfo.outboundDate).toEqual(currentDate);
    expect(bookingDatesInfo.inboundDate).toBe(null);
  });

  it('Should set outbound and inbound dates', async () => {
    const dayAfter = moment()
      .add(1, 'days')
      .toDate();
    const currentDate = moment().toDate();
    const { airportSelectionData } = state;
    const mockBookingDatesInfo: BookingCalendar = {
      outboundDate: currentDate,
      inboundDate: null,
      isOutboundDateSelected: true,
      prices:[]
    };
    BookingStateObs.next({ airportSelectionData, bookingDatesInfo: mockBookingDatesInfo });

    triggers.onDateClickSelection(dayAfter);
    const { bookingDatesInfo } = state;
    const { outboundDate, inboundDate, isOutboundDateSelected } = bookingDatesInfo;
    expect(moment(outboundDate || new Date()).format('D')).toEqual(moment(currentDate).format('D'));
    expect(inboundDate).toBe(dayAfter);
    expect(isOutboundDateSelected).toBeFalsy();
  });
  it('Should maintain the adult value in the passenger data when the user tries to decrement it and its value is 1.', () => {
    triggerPassengerSelection(PassengerType.ADULTS, false);
    const { passengerData } = state;
    expect(passengerData.adultsQuantity).toBe(1);
  });
  it('Should add one to adult value in the passenger data when the user click the add button.', () => {
    triggerPassengerSelection(PassengerType.ADULTS, true);
    const { passengerData } = state;
    expect(passengerData.adultsQuantity).toBe(2);
  });
  it('Should maintain the teen value in the passenger data when the user tries to decrement it and its value is 0', () => {
    triggerPassengerSelection(PassengerType.TEENS, false);
    const { passengerData } = state;
    expect(passengerData.teensQuantity).toBe(0);
  });
  it('Should add one to teen value in the passenger data when the user click the add button.', () => {
    triggerPassengerSelection(PassengerType.TEENS, true);
    const { passengerData } = state;
    expect(passengerData.teensQuantity).toBe(1);
  });
  it('Should maintain the children value in the passenger data when the user tries to decrement it and its value is 0.', () => {
    triggerPassengerSelection(PassengerType.CHILDREN, false);
    const { passengerData } = state;
    expect(passengerData.childrenQuantity).toBe(0);
  });
  it('Should add one to children value in the passenger data when the user click the add button.', () => {
    triggerPassengerSelection(PassengerType.CHILDREN, true);
    const { passengerData } = state;
    expect(passengerData.childrenQuantity).toBe(1);
  });
  it('Should maintain the baby value in the passenger data when the user tries to decrement it and its value is 0.', () => {
    triggerPassengerSelection(PassengerType.BABY, false);
    const { passengerData } = state;
    expect(passengerData.babiesQuantity).toBe(0);
  });
  it('Should add one to baby value in the passenger data when the user click the add button.', () => {
    triggerPassengerSelection(PassengerType.BABY, true);
    const { passengerData } = state;
    expect(passengerData.babiesQuantity).toBe(1);
  });
 
});
