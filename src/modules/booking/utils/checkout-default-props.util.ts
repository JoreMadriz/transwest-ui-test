import CardInfo from '../components/checkout/models/card-info';
import BillingInfo from '../components/checkout/models/billing-info';
import Checkout from '../models/Checkout';

export function getDefaultCardInfo(): CardInfo {
  return {
    cardNumber: 0,
    expirationDate: 0,
    cvv: 0,
    errors: {
      cardNumber: [],
      cvv: [],
      expirationDate: []
    }
  };
}

export function getDefaultBillingInfo(): BillingInfo {
  return {
    firstName: '',
    lastName: '',
    address: '',
    city: '',
    postalCode: '',
    country: '',
    errors: {
      firstName: [],
      lastName: [],
      address: [],
      city: [],
      postalCode: [],
      country: []
    }
  };
}
export function getDefaultCheckout(): Checkout {
  return {
    cardInfo: getDefaultCardInfo(),
    billingInfo: getDefaultBillingInfo(),
    isTermsAgreed: {
      isAgreed: false,
      error: ''
    }
  };
}
