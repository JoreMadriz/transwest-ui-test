
import CardInfo from "../components/checkout/models/card-info";
import CardValidator from 'card-validator'
import { CreditCardType } from "../components/checkout/components/credit-card-info/credit-card.enum";

const REQUIRED_FIELD_MESSAGE: string = 'This field should have a value';
const INVALID_CARD_NUMBER = 'The card number is invalid';
const INVALID_EXPIRATION_DATE= 'The expiration date is invalid';
const INVALID_CVV = 'The cvv is invalid';

export const cardInfoValidation = (cardInfo: CardInfo) => {
    const {cardNumber, errors, cvv, expirationDate} = cardInfo
    errors.cardNumber = cardNumberValidation(cardNumber.toString());
    errors.expirationDate = expirationDateValidation(expirationDate.toString());
    errors.cvv = cvvValidation(cardNumber.toString(),cvv.toString());
    return errors;
}

const cardNumberValidation = (cardNumber:string) => {
  if(cardNumber === '0') return [REQUIRED_FIELD_MESSAGE];
  if(!CardValidator.number(cardNumber).isValid) return [INVALID_CARD_NUMBER]
  return []
} 

const expirationDateValidation = (expirationDate:string) => {
    if(expirationDate === '0') return [REQUIRED_FIELD_MESSAGE];
    if(!CardValidator.expirationDate(expirationDate).isValid) return [INVALID_EXPIRATION_DATE];
    return []
}
const cvvValidation = (cardNumber:string, cvv: string) => {
    if(cvv === '0') return [REQUIRED_FIELD_MESSAGE];
    const card = CardValidator.number(cardNumber).card;
    const isAmex = card ? card.type === CreditCardType.AMERICAN : false;
    const cvvLength = isAmex ? 4 : 3
    if(!CardValidator.cvv(cvv, cvvLength).isValid) return [INVALID_CVV];
    return []
}