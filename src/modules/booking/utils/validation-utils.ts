import BillingInfo from '../components/checkout/models/billing-info';

export const REQUIRED_FIELD_MESSAGE: string = 'This field should have a value';
export const REQUIRED_TERMS_CONDITIONS: string = 'Please accept our terms and conditions';
export const isInvalidStringField = (field: string): boolean => {
  return !(!!field && field !== '');
};

export const billingInfoValidation = (billingInfo: BillingInfo) => {
  const { address, city, country, errors, firstName, lastName, postalCode } = billingInfo;
  errors.firstName = isInvalidStringField(firstName) ? [REQUIRED_FIELD_MESSAGE] : [];
  errors.lastName = isInvalidStringField(lastName) ? [REQUIRED_FIELD_MESSAGE] : [];
  errors.address = isInvalidStringField(address) ? [REQUIRED_FIELD_MESSAGE] : [];
  errors.city = isInvalidStringField(city) ? [REQUIRED_FIELD_MESSAGE] : [];
  errors.country = isInvalidStringField(country) ? [REQUIRED_FIELD_MESSAGE] : [];
  errors.postalCode = isInvalidStringField(postalCode) ? [REQUIRED_FIELD_MESSAGE] : [];
  return errors;
};

export const containErrors = (errors: object): boolean => {
  const containErrors = Object.values(errors).some((errorValue) => errorValue.length > 0);
  return containErrors;
};
