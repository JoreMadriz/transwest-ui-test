import BookingTriggers from '../models/booking.triggers';
import BookingCalendar from '../components/date-selector/models/booking-calendar';
import FlightSelectionData from '../models/FlightSelectionData';
import Ticket from '../components/flight-selection/components/ticket-selection/models/ticket';
import AirportSelectionData from '../components/airport-selection/models/airport-selection-data';
import BookingData from '../models/BookingData';
import TicketSelectionState from '../components/flight-selection/components/ticket-selection/models/ticket-selection-state';
import { RequestState } from '../components/flight-selection/components/ticket-selection/models/request-state';
import TicketSelectionProps from '../components/flight-selection/components/ticket-selection/ticket-selection.props';
import { getDefaultPassengerData } from './passenger-default-props.util';
import PassengerInfo from '../components/passenger-information/model/passenger-info';
import { Gender } from '../components/passenger-information/model/Passenger';
import CardInfo from '../components/checkout/models/card-info';

export function getDefaultAirportSelectionData(): AirportSelectionData {
  return {
    departureAirport: {
      Address: {
        Address: 'test',
        City: 'test',
        Country: 1,
        PostalCode: '11',
        Province: '11'
      },
      Code: '1',
      Id: 1,
      Name: 'test 1',
      WayOrder: 2
    },
    destinationAirport: {
      Address: {
        Address: 'test',
        City: 'test',
        Country: 1,
        PostalCode: '11',
        Province: '11'
      },
      Code: '2',
      Id: 2,
      Name: 'test 2',
      WayOrder: 2
    },
    isOneWayTrip: true,
    promoCode: ''
  };
}

export function getDefaultTicketsInfo(): TicketSelectionState {
  return {
    tickets: [getDefaultTicket()],
    requestState: RequestState.isLoading
  };
}
export function getDefaultCardInfo(): CardInfo {
  return {
    cardNumber: 0,
    expirationDate: 0,
    cvv: 0,
    errors: {
      cardNumber: [],
      cvv: [],
      expirationDate: []
    }
  };
}

export function getDefaultTicketSelectionProps(): TicketSelectionProps {
  return {
    airportSelectionData: getDefaultAirportSelectionData(),
    getTickets: jest.fn(),
    handlePaginationClick: jest.fn(),
    handleTicketClick: jest.fn(),
    isOutboundDate: true,
    nextDayNotValid: false,
    passengerData: getDefaultPassengerData(),
    prevDayNotValid: false,
    sessionId: '',
    ticketDate: new Date(),
    ticketsInfo: getDefaultTicketsInfo()
  };
}
export function getBookingTriggersProps(): BookingTriggers {
  return {
    onChangeAirportSelection: jest.fn(),
    onDateClickSelection: jest.fn(),
    onChangePassengersQuantity: jest.fn(),
    onTicketClick: jest.fn(),
    getSessionId: jest.fn(),
    onDatePaginationClick: jest.fn(),
    reserveSeats: jest.fn(),
    createPassengerInformation: jest.fn(),
    onChangePassengerInformation: jest.fn(),
    onChangeTerms: jest.fn(),
    onChangeBillingInfo: jest.fn(),
    onPayNow: jest.fn(),
    onChangeCardInfo: jest.fn(),
    getCalendarPrices: jest.fn()
  };
}
export function getDefaultBookingDate(): BookingCalendar {
  return {
    inboundDate: null,
    isOutboundDateSelected: true,
    outboundDate: null,
    prices: []
  };
}
export function getDefaultBookingData(): BookingData {
  return {
    airportSelection: getDefaultAirportData(),
    dateSelection: getDefaultBookingDate(),
    flightSelection: getDefaultFlightData(),
    passengerSelection: getDefaultPassengerData()
  };
}

export function getDefaultDatesInfo(): BookingCalendar {
  return {
    outboundDate: null,
    inboundDate: null,
    isOutboundDateSelected: false,
    prices: []
  };
}

export function getCustomDatesInfo(validOutbound: boolean, validInbound: boolean): BookingCalendar {
  return {
    inboundDate: validInbound
      ? new Date('Mon Mar 06 2019 00: 00: 00 GMT - 0600(Central Standard Time)')
      : null,
    isOutboundDateSelected: false,
    outboundDate: validOutbound
      ? new Date('Wed Mar 06 2019 00: 00: 00 GMT - 0600(Central Standard Time)')
      : null,
    prices: []
  };
}

export function getDefaultPassengerInfo(): PassengerInfo {
  return {
    adultPassengers: [],
    mainPassenger: {
      code: '',
      confirmEmail: '',
      email: '',
      firstName: '',
      gender: Gender.FEMALE,
      lastName: '',
      phoneNumber: '',
      type: 0,
      typeIdentifier: 1,
      errors: {
        firstName: [],
        lastName: []
      },
      mainErrors: {
        code: [],
        confirmEmail: [],
        email: [],
        phoneNumber: []
      }
    },
    underagePassengers: []
  };
}

export function getDefaultFlightData(): FlightSelectionData {
  return {
    inboundTicket: getDefaultTicket(),
    outboundTicket: getDefaultTicket()
  };
}

export function getDefaultTicket(): Ticket {
  return {
    flightId: 0,
    arrivalTime: '',
    connections: 0,
    duration: '',
    stops: 0,
    stopsCodes: [],
    departTime: '',
    destination: '',
    origin: '',
    price: 0,
    groupId: 2,
    fareBasisId: 0,
    direction: true,
    isSpecified: true,
    fees: [],
    subTotal: 0
  };
}

export function getCustomTicket(flightIdNumber: number): Ticket {
  return {
    flightId: flightIdNumber,
    arrivalTime: '',
    connections: 0,
    duration: '',
    stops: 0,
    stopsCodes: [],
    departTime: '',
    destination: '',
    origin: '',
    price: 0,
    groupId: 0,
    fareBasisId: 0,
    direction: true,
    isSpecified: true,
    fees: [],
    subTotal: 0
  };
}

export function getDefaultAirportData(): AirportSelectionData {
  return {
    departureAirport: undefined,
    destinationAirport: undefined,
    isOneWayTrip: false,
    promoCode: ''
  };
}
