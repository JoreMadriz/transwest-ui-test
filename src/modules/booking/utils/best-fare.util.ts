import PassengerData from '../models/PassengerData';
import moment from 'moment';
import CalendarPrices from '../components/date-selector/models/calendar-prices';
export const a = 1;
export function convertMonthlyFareBody(
  originId: number,
  destinationId: number,
  sessionId: string,
  passengers: PassengerData
) {
  const { adultsQuantity, childrenQuantity, babiesQuantity } = passengers;
  const children = childrenQuantity + babiesQuantity;
  const currentDate = moment().toDate();
  const endDate = moment()
    .add(1, 'months')
    .endOf('month')
    .toDate();
  return {
    SessionId: sessionId,
    OriginId: originId,
    DestinationId: destinationId,
    StartDate: currentDate,
    EndDate: endDate,
    NumAdults: adultsQuantity,
    NumChildren: children,
    IncludeExcursion: true,
    IncludeExcursionCustomerFares: true
  };
}

export function convertToCalendarPrices(data: any): CalendarPrices[] {
  const flightOptions = data.FlightOptions;
  const calendarPrices: CalendarPrices[] = flightOptions.map((option: any) => {
    const fees = option.Fare.Fees;
    const price =
      option.Total + fees.reduce((previousValue: any, fee: any) => previousValue + fee.Value, 0);
    return {
      date: option.Date,
      price
    };
  });
  return calendarPrices;
}
