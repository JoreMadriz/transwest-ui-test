import PassengerInfo from '../components/passenger-information/model/passenger-info';
import PassengerData from '../models/PassengerData';
import Passenger, { Gender } from '../components/passenger-information/model/Passenger';
import UnderagePassenger from '../components/passenger-information/model/underage-passenger';
import PassengerType from '../models/PassengerType';

const IS_UNTOUCHED: string = 'isUntouched';

export function getDefaultPassengerInfo(): PassengerInfo {
  return {
    adultPassengers: [],
    mainPassenger: {
      code: '',
      confirmEmail: '',
      email: '',
      firstName: '',
      gender: Gender.FEMALE,
      lastName: '',
      phoneNumber: '',
      type: 0,
      typeIdentifier: 1,
      errors: {
        firstName: [IS_UNTOUCHED],
        lastName: [IS_UNTOUCHED]
      },
      mainErrors: {
        email: [IS_UNTOUCHED],
        confirmEmail: [IS_UNTOUCHED],
        code: [IS_UNTOUCHED],
        phoneNumber: [IS_UNTOUCHED]
      }
    },
    underagePassengers: []
  };
}
export function getDefaultPassengerData(): PassengerData {
  return {
    adultsQuantity: 1,
    teensQuantity: 0,
    childrenQuantity: 0,
    babiesQuantity: 0,
    totalPassengers: 1
  };
}

export function getPassengerInformation(passengerData: PassengerData): PassengerInfo {
  const adultPassengers: Passenger[] = [];
  const underagePassengers: UnderagePassenger[] = [];

  for (let index: number = 1; index < passengerData.adultsQuantity; index++) {
    adultPassengers.push(createAdultPassenger(index + 1));
  }

  createUnderagePassengersByType(
    passengerData.babiesQuantity,
    underagePassengers,
    PassengerType.BABY
  );
  createUnderagePassengersByType(
    passengerData.childrenQuantity,
    underagePassengers,
    PassengerType.CHILDREN
  );
  createUnderagePassengersByType(
    passengerData.teensQuantity,
    underagePassengers,
    PassengerType.TEENS
  );

  return {
    mainPassenger: {
      code: '',
      confirmEmail: '',
      email: '',
      firstName: '',
      gender: Gender.FEMALE,
      lastName: '',
      phoneNumber: '',
      type: PassengerType.ADULTS,
      typeIdentifier: 1,
      errors: {
        firstName: [IS_UNTOUCHED],
        lastName: [IS_UNTOUCHED]
      },
      mainErrors: {
        email: [IS_UNTOUCHED],
        confirmEmail: [IS_UNTOUCHED],
        code: [IS_UNTOUCHED],
        phoneNumber: [IS_UNTOUCHED]
      }
    },
    adultPassengers: adultPassengers,
    underagePassengers: underagePassengers
  };
}

function createUnderagePassengersByType(
  quantity: number,
  underagePassengers: UnderagePassenger[],
  type: PassengerType
) {
  for (let index: number = 1; index <= quantity; index++) {
    underagePassengers.push(createUnderagePassenger(type, index));
  }
}

function createAdultPassenger(typeIdentifier: number): Passenger {
  return {
    firstName: '',
    gender: Gender.FEMALE,
    lastName: '',
    type: PassengerType.ADULTS,
    typeIdentifier,
    errors: {
      firstName: [IS_UNTOUCHED],
      lastName: [IS_UNTOUCHED]
    }
  };
}

function createUnderagePassenger(type: PassengerType, typeIdentifier: number): UnderagePassenger {
  return {
    birthDate: null,
    firstName: '',
    gender: Gender.FEMALE,
    lastName: '',
    type,
    typeIdentifier,
    errors: {
      firstName: [IS_UNTOUCHED],
      lastName: [IS_UNTOUCHED]
    },
    underageErrors: {
      birthDate: [IS_UNTOUCHED]
    }
  };
}
