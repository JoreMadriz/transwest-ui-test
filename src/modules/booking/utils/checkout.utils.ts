import Checkout from '../models/Checkout';
import Payment from '../models/Payment';
import PassengerInfo from '../components/passenger-information/model/passenger-info';
import Passenger, { Gender } from '../components/passenger-information/model/Passenger';
import OnlinePassenger from '../models/OnlinePassenger';
import MainPassenger from '../components/passenger-information/model/main-passenger';
import UnderagePassenger from '../components/passenger-information/model/underage-passenger';

export const convertCheckoutToPayment = (checkout: Checkout): Payment => {
  const { billingInfo, cardInfo } = checkout;
  const CANADA_COUNTRY_ID = 1;
  return {
    FirstName: billingInfo.firstName,
    LastName: billingInfo.lastName,
    Address: {
      Address: billingInfo.address,
      Country: CANADA_COUNTRY_ID,
      PostalCode: billingInfo.postalCode,
      City: billingInfo.city,
      Province: 'Hello world'
    },
    CreditCard: {
      CardNumber: cardInfo.cardNumber.toString(),
      ExpiryDate: cardInfo.expirationDate.toString(),
      CSC: cardInfo.cvv.toString()
    }
  };
};

export const convertPassengerInfo = (passengerInfo: PassengerInfo) => {
  const { adultPassengers, mainPassenger, underagePassengers } = passengerInfo;
  const passengersOnlineInfo: OnlinePassenger[] = [];
  passengersOnlineInfo.push(convertMainPassenger(mainPassenger));
  adultPassengers.forEach((adult) => {
    passengersOnlineInfo.push(convertAdultPassenger(adult));
  });
  underagePassengers.forEach((underage) => {
    passengersOnlineInfo.push(convertUnderagePassenger(underage));
  });
  return passengersOnlineInfo;
};

const convertMainPassenger = (passenger: MainPassenger): OnlinePassenger => {
  return {
    ...convertAdultPassenger(passenger),
    ContactInfo: {
      Email: passenger.email,
      PrimaryPhone: passenger.phoneNumber
    }
  };
};

const convertUnderagePassenger = (passenger: UnderagePassenger): OnlinePassenger => {
  return {
    ...convertAdultPassenger(passenger),
    PassengerType: 'C'
  };
};

const convertAdultPassenger = (passenger: Passenger): OnlinePassenger => {
  return {
    FirstName: passenger.firstName,
    LastName: passenger.lastName,
    Birthdate: new Date(),
    HasInfant: false,
    MobilityImpaired: false,
    PassengerType: Gender.FEMALE === passenger.gender ? 'F' : 'M'
  };
};

export const convertConfirmSeat = (
  passengerInfo: PassengerInfo,
  checkout: Checkout,
  sessionId: string,
  reservationRequest: any
) => {
  return {
    SessionId: sessionId,
    TicketIds: reservationRequest,
    OriginalTicketIds: reservationRequest.map(() => null),
    Passengers: convertPassengerInfo(passengerInfo),
    PaymentDetails: convertCheckoutToPayment(checkout)
  };
};
