import AirportSelectionData from './components/airport-selection/models/airport-selection-data';
import BookingCalendar from './components/date-selector/models/booking-calendar';
import BookingTriggers from './models/booking.triggers';
import FlightSelectionData from './models/FlightSelectionData';
import PassengerData from './models/PassengerData';
import PassengerInfo from './components/passenger-information/model/passenger-info';
import Checkout from './models/Checkout';
import Step from './models/Step';

export default interface BookingProps {
  airportSelectionData: AirportSelectionData;
  bookingDatesInfo: BookingCalendar;
  passengerData: PassengerData;
  checkoutInfo: Checkout;
  sessionId: string;
  flightData: FlightSelectionData;
  triggers: BookingTriggers;
  passengerInfo: PassengerInfo;
  initialStep: Step;
}
