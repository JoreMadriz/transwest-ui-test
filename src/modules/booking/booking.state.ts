export default interface BookingState {
  currentStep: number;
  isNotStepValid: boolean;
}
