import PassengerType from '../../../../models/PassengerType';

export default interface PassengerCounterProps {
  classes: any;
  title: string;
  range: string;
  passengerQuantity: number;
  onChangePassengersQuantity: Function;
  type: PassengerType;
}
