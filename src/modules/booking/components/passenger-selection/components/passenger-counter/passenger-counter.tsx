import React from 'react';
import PassengerCounterProps from './passenger-counter.props';
import { Grid, Typography, withStyles, IconButton } from '@material-ui/core';
import styles from './passenger-counter.style';

import AddCircleOutline from '@material-ui/icons/AddCircleOutline';
import RemoveCircleOutline from '@material-ui/icons/RemoveCircleOutline';

export const PassengerCounter = (props: PassengerCounterProps) => {
  const { title, range, classes, passengerQuantity, onChangePassengersQuantity, type } = props;
  const { container, buttonText } = classes;
  return (
    <Grid container className={container}>
      <Grid item xs={6} md={8}>
        <Typography align="left" variant="h5">
          {title}
        </Typography>
        <Typography align="left" variant="h6">
          {range}
        </Typography>
      </Grid>

      <Grid container flex-direction="row" item xs={6} md={4}>
        <IconButton onClick={() => onChangePassengersQuantity(type, false)}>
          <RemoveCircleOutline />
        </IconButton>
        <Grid className={buttonText} item>
          <Typography variant="h5">{passengerQuantity}</Typography>
        </Grid>
        <IconButton onClick={() => onChangePassengersQuantity(type, true)}>
          <AddCircleOutline />
        </IconButton>
      </Grid>
    </Grid>
  );
};

export default withStyles(styles)(PassengerCounter);
