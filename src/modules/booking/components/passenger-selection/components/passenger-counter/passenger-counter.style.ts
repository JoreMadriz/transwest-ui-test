import { PADDING_4, PADDING_6 } from '../../../../../../shared/theme/gutters';

const styles = (theme: any) => ({
  container: {
    marginBottom: PADDING_4,
    marginLeft: PADDING_6
  },
  buttonText: {
    margin: 'auto 0'
  }
});
export default styles;
