import Enzyme, { shallow } from 'enzyme';
import React from 'react';

import { PassengerCounter } from './passenger-counter';
import PassengerCounterProps from './passenger-counter.props';
import PassengerType from '../../../../models/PassengerType';

type WrapperType = Enzyme.ShallowWrapper<PassengerCounterProps, {}, PassengerCounter>;

describe('Passenger Counter test', () => {
  function getDefaultProps(): PassengerCounterProps {
    return {
      classes: {},
      title: 'Test',
      range: 'Test',
      passengerQuantity: 1,
      onChangePassengersQuantity: jest.fn(),
      type: PassengerType.ADULTS
    };
  }

  function getPassengerCounterWrapper(props: PassengerCounterProps): WrapperType {
    return shallow(<PassengerCounter {...props} />);
  }

  function clickButtonByName(buttonName: string, passengerCounter: any) {
    const passengerCounterButton = passengerCounter
      .findWhere((node: any) => node.name() === buttonName)
      .parent();
    passengerCounterButton.simulate('click');
  }

  it('should call the click action when the remove button is clicked', () => {
    const defaultProps = getDefaultProps();
    const clickActionMock = defaultProps.onChangePassengersQuantity;
    const passengerCounter = getPassengerCounterWrapper(defaultProps);
    clickButtonByName('pure(RemoveCircleOutlineIcon)', passengerCounter);
    expect(clickActionMock).toHaveBeenCalled();
  });
  it('should call the click action when the add button is clicked', () => {
    const defaultProps = getDefaultProps();
    const clickActionMock = defaultProps.onChangePassengersQuantity;
    const passengerCounter = getPassengerCounterWrapper(defaultProps);
    clickButtonByName('pure(AddCircleOutlineIcon)', passengerCounter);
    expect(clickActionMock).toHaveBeenCalled();
  });
});
