import { PADDING_4 } from '../../../../shared/theme/gutters';

const styles = {
  title: {
    marginBottom: PADDING_4,
    marginLeft: PADDING_4
  }
};

export default styles;
