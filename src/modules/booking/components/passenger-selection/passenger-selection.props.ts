import PassengerData from '../../models/PassengerData';

export interface PassengerSelectionProps {
  classes: any;
  passengerData: PassengerData;
  onChangePassengersQuantity: Function;
}
