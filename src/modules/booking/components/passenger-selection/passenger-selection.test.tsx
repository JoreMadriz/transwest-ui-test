import React from 'react';
import { shallow } from 'enzyme';
import { PassengerSelection } from './passenger-selection';
import PassengerData from '../../models/PassengerData';

function getPassengerData(): PassengerData {
  return {
    adultsQuantity: 1,
    teensQuantity: 1,
    childrenQuantity: 1,
    babiesQuantity: 1,
    totalPassengers: 4
  };
}
describe('Passenger Selection test suite', () => {
  function getPassengerSelectionWrapper() {
    return shallow(
      <PassengerSelection
        classes={{}}
        passengerData={getPassengerData()}
        onChangePassengersQuantity={() => {}}
      />
    );
  }

  it('should render the component structure properly', () => {
    const passengerSelection = getPassengerSelectionWrapper();
    expect(passengerSelection).toMatchSnapshot();
  });
});
