import { withStyles } from '@material-ui/core';
import React from 'react';

import BookingSectionTitle from '../booking-section-title/booking-section-title';
import PassengerCounter from './components/passenger-counter/passenger-counter';
import PassengerType from '../../models/PassengerType';
import { PassengerSelectionProps } from './passenger-selection.props';
import styles from './passenger-selection.style';

const ADULTS: string = 'Adults';
const CHILDREN: string = 'Children';
const BABY: string = 'Baby';

const ADULTS_RANGE: string = '15+ years';
const CHILDREN_RANGE: string = '2-11 years';
const BABY_RANGE: string = 'Under 2 years';

export const PassengerSelection = (props: PassengerSelectionProps) => {
  const { passengerData, onChangePassengersQuantity } = props;
  const { adultsQuantity, childrenQuantity, babiesQuantity } = passengerData;
  return (
    <div>
      <BookingSectionTitle>How many passengers?</BookingSectionTitle>
      <PassengerCounter
        type={PassengerType.ADULTS}
        onChangePassengersQuantity={onChangePassengersQuantity}
        passengerQuantity={adultsQuantity}
        title={ADULTS}
        range={ADULTS_RANGE}
      />
      <PassengerCounter
        type={PassengerType.CHILDREN}
        onChangePassengersQuantity={onChangePassengersQuantity}
        passengerQuantity={childrenQuantity}
        title={CHILDREN}
        range={CHILDREN_RANGE}
      />
      <PassengerCounter
        type={PassengerType.BABY}
        onChangePassengersQuantity={onChangePassengersQuantity}
        passengerQuantity={babiesQuantity}
        title={BABY}
        range={BABY_RANGE}
      />
    </div>
  );
};

export default withStyles(styles)(PassengerSelection);
