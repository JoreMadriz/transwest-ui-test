import Passenger from './Passenger';

interface MainPassenger extends Passenger {
  email: string;
  confirmEmail: string;
  code: string;
  phoneNumber: string;
  mainErrors: {
    email: string[];
    confirmEmail: string[];
    code: string[];
    phoneNumber: string[];
  };
}

export default MainPassenger;
