import MainPassenger from './main-passenger';
import Passenger from './Passenger';
import UnderagePassenger from './underage-passenger';

interface PassengerInfo {
  mainPassenger: MainPassenger;
  adultPassengers: Passenger[];
  underagePassengers: UnderagePassenger[];
}

export default PassengerInfo;
