import PassengerType from '../../../models/PassengerType';

export default interface Passenger {
  firstName: string;
  lastName: string;
  gender: Gender;
  type: PassengerType;
  typeIdentifier: number;
  errors: {
    firstName: string[];
    lastName: string[];
  };
}

export enum Gender {
  FEMALE = 0,
  MALE = 1
}
