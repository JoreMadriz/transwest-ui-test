import Passenger from './Passenger';

interface UnderagePassenger extends Passenger {
  birthDate: Date | null;
  underageErrors: {
    birthDate: string[];
  };
}

export default UnderagePassenger;
