import React from 'react';
import { Grid, withStyles, LinearProgress } from '@material-ui/core';

import AdultInformation from './components/adult-info/adult-info';
import BookingSectionTitle from '../../../booking/components/booking-section-title/booking-section-title';
import MainPassenger from './model/main-passenger';
import MainPassengerInfo from './components/main-passenger-info/main-passenger-info';
import Passenger from './model/Passenger';
import PassengerInfo from './model/passenger-info';
import { PassengerInformationProps } from './passenger-information.props';
import SectionBreadCrumb from '../section-breadcrumb/section-breadcrumb';
import SectionSpacer from '../../../../shared/theme/components/section-spacer/section-spacer';
import { SpacerType } from '../../../../shared/theme/components/section-spacer/SpacerType';
import SpecialServices from './components/special-services/special-services';
import Steps from '../../models/Step';
import styles from './passenger-information.style';
import UnderageInfo from './components/underage-info/underage-info';
import UnderagePassenger from './model/underage-passenger';

export class PassengerInformation extends React.Component<PassengerInformationProps, {}> {
  constructor(props: PassengerInformationProps) {
    super(props);
    props.triggers.createPassengerInformation();
  }

  onMainPassengerInfoChange = (mainPassenger: MainPassenger) => {
    this.props.triggers.onChangePassengerInformation({
      ...this.props.passengerInfo,
      mainPassenger
    });
  };

  onAdultPassengerInfoChange = (adultInfo: Passenger, index: number) => {
    const validPassengerInfo: PassengerInfo = this.props.passengerInfo as PassengerInfo;
    const newAdultsList = [...validPassengerInfo.adultPassengers];
    newAdultsList[index] = adultInfo;
    this.props.triggers.onChangePassengerInformation({
      ...validPassengerInfo,
      adultPassengers: newAdultsList
    });
  };

  onUnderagePassengerInfoChange = (underageInfo: UnderagePassenger, index: number) => {
    const validPassengerInfo: PassengerInfo = this.props.passengerInfo as PassengerInfo;
    const newUnderageList = [...validPassengerInfo.underagePassengers];
    newUnderageList[index] = underageInfo;
    this.props.triggers.onChangePassengerInformation({
      ...validPassengerInfo,
      underagePassengers: newUnderageList
    });
  };

  render() {
    const { passengerInfo } = this.props;
    if (!passengerInfo || Object.entries(passengerInfo).length === 0) {
      return (
        <Grid>
          <LinearProgress color="secondary" />
        </Grid>
      );
    }

    const validPassengerInfo: PassengerInfo = passengerInfo as PassengerInfo;

    return (
      <Grid>
        <SectionBreadCrumb
          step={Steps.FLIGHT_SELECTION}
          handleStepChange={this.props.handleBackToStartClick}
        >
          Back to flight selection
        </SectionBreadCrumb>

        <BookingSectionTitle>Add Passenger Info</BookingSectionTitle>
        <MainPassengerInfo
          passenger={validPassengerInfo.mainPassenger}
          onMainPassengerInfoChange={this.onMainPassengerInfoChange}
        />
        <SectionSpacer spacerType={SpacerType.TINY} />
        {validPassengerInfo.adultPassengers.map((adultPassenger, index) => {
          return (
            <div key={index}>
              <AdultInformation
                index={index}
                passenger={adultPassenger}
                key={index}
                onAdultInfoChange={this.onAdultPassengerInfoChange}
              />
              <SectionSpacer spacerType={SpacerType.TINY} />
            </div>
          );
        })}
        {validPassengerInfo.underagePassengers.map((underagePassenger, index) => {
          return (
            <div key={index}>
              <UnderageInfo
                passenger={underagePassenger}
                index={index}
                onUnderageInfoChange={this.onUnderagePassengerInfoChange}
              />
              <SectionSpacer spacerType={SpacerType.TINY} />
            </div>
          );
        })}
        <SpecialServices />
        <SectionSpacer spacerType={SpacerType.TINY} />
      </Grid>
    );
  }
}

export default withStyles(styles)(PassengerInformation);
