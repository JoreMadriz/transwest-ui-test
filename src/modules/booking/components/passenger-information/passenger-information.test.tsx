import React from 'react';
import { shallow } from 'enzyme';

import { getBookingTriggersProps } from '../../utils/default-props.util';
import PassengerType from '../../models/PassengerType';
import PassengerInfo from './model/passenger-info';
import { PassengerInformation } from './passenger-information';
import { PassengerInformationProps } from './passenger-information.props';

function getPassengerInfo(): PassengerInfo {
  return {
    mainPassenger: {
      firstName: '',
      lastName: '',
      gender: 0,
      type: PassengerType.ADULTS,
      typeIdentifier: 0,
      email: '',
      confirmEmail: '',
      code: '',
      phoneNumber: '',
      errors: {
        firstName: [],
        lastName: []
      },
      mainErrors: {
        email: [],
        confirmEmail: [],
        code: [],
        phoneNumber: []
      }
    },
    adultPassengers: [
      {
        firstName: '',
        lastName: '',
        gender: 0,
        type: PassengerType.ADULTS,
        typeIdentifier: 0,
        errors: {
          firstName: [],
          lastName: []
        }
      },
      {
        firstName: '',
        lastName: '',
        gender: 0,
        type: PassengerType.ADULTS,
        typeIdentifier: 0,
        errors: {
          firstName: [],
          lastName: []
        }
      }
    ],
    underagePassengers: [
      {
        firstName: '',
        lastName: '',
        gender: 0,
        type: PassengerType.ADULTS,
        typeIdentifier: 0,
        birthDate: new Date('25/06/1996'),
        errors: {
          firstName: [],
          lastName: []
        },
        underageErrors: {
          birthDate: []
        }
      }
    ]
  };
}

function getPassengerInformationProps(): PassengerInformationProps {
  return {
    classes: {},
    passengerInfo: getPassengerInfo(),
    triggers: getBookingTriggersProps(),
    currentStep: 5,
    handleBackToStartClick: jest.fn()
  };
}

describe('Passenger Information test suite', () => {
  function getBasicInfoWrapper(props: PassengerInformationProps) {
    return shallow(<PassengerInformation {...props} />);
  }
  it('should render one adult component correctly', () => {
    const passengerInfoComponent = getBasicInfoWrapper(getPassengerInformationProps());
    const mainPassengerComponent = passengerInfoComponent.find(
      'WithStyles(MainPassengerInformation)'
    );
    expect(mainPassengerComponent.length).toBe(1);
  });

  it('should render one adult component correctly', () => {
    const passengerInfoComponent = getBasicInfoWrapper(getPassengerInformationProps());
    console.log(passengerInfoComponent.debug());
    const adultInfoComponent = passengerInfoComponent.find('WithStyles(AdultInformation)');
    expect(adultInfoComponent.length).toBe(2);
  });

  it('should render underage component correctly', () => {
    const passengerInfoComponent = getBasicInfoWrapper(getPassengerInformationProps());
    const adultInfoComponent = passengerInfoComponent.find('WithStyles(UnderageInfo)');
    expect(adultInfoComponent.length).toBe(1);
  });
});
