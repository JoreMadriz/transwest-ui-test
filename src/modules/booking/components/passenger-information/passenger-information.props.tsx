import BookingTriggers from '../../models/booking.triggers';
import PassengerInfo from './model/passenger-info';

export interface PassengerInformationProps {
  classes: any;
  passengerInfo: PassengerInfo | {};
  triggers: BookingTriggers;
  currentStep: number;
  handleBackToStartClick: Function;
}
