import React from 'react';
import { shallow } from 'enzyme';

const passenger: Passenger = {
  firstName: '',
  lastName: '',
  gender: 0,
  type: PassengerType.ADULTS,
  typeIdentifier: 1
};

import AdultInfo from './adult-info';
import Passenger from '../../model/Passenger';
import PassengerType from '../../../../models/PassengerType';
describe('Adult info test suite', () => {
  function getServiceMapWrapper() {
    return shallow(<AdultInfo passenger={passenger} index={0} onAdultInfoChange={jest.fn()} />);
  }

  it('should render the component structure properly', () => {
    const adultInfo = getServiceMapWrapper();
    expect(adultInfo).toMatchSnapshot();
  });
});
