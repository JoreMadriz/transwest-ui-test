import React from 'react';
import { Grid, Typography, withStyles } from '@material-ui/core';

import BasicInformation from '../basic-information/basic-information';
import styles from './adult-info.styles';
import Passenger from '../../model/Passenger';

export interface AdultInformationProps {
  classes: any;
  passenger: Passenger;
  index: number;
  onAdultInfoChange: Function;
}

class AdultInformation extends React.Component<AdultInformationProps, {}> {
  private readonly WARNING_TEXT: string =
    'IMPORTANT - You must insert your name and surname exactly as they appear on your identity documents';
  render() {
    const { classes, index, passenger, onAdultInfoChange } = this.props;
    const { container, text, title } = classes;
    const { typeIdentifier } = passenger;
    return (
      <Grid>
        <Typography className={title} align="left">{`Adult ${typeIdentifier}`}</Typography>
        <Grid container className={container}>
          <Typography className={text}>{this.WARNING_TEXT}</Typography>
        </Grid>
        <BasicInformation
          index={index}
          passenger={passenger}
          handleInformationChange={onAdultInfoChange}
        />
      </Grid>
    );
  }
}

export default withStyles(styles)(AdultInformation);
