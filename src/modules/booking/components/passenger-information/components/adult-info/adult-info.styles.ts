import { BORDER_COLOR, PRIMARY_COLOR } from '../../../../../../shared/theme/colors';
import { MARGIN_2 } from '../../../../../../shared/theme/gutters';

const styles = {
  container: {
    backgroundColor: '#F1F9FF',
    border: `1px solid ${BORDER_COLOR}`,
    borderBottom: 'transparent'
  },
  text: {
    fontSize: '16px',
    margin: MARGIN_2
  },
  title: {
    fontSize: '16px',
    color: PRIMARY_COLOR,
    marginBottom: MARGIN_2
  }
};

export default styles;
