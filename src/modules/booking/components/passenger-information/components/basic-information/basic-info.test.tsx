import React from 'react';
import { shallow } from 'enzyme';

import { BasicInformation, BasicInformationState } from './basic-information';
import BasicInformationProps from './basic-information.props';
import PassengerType from '../../../../models/PassengerType';
import { REQUIRED_FIELD_MESSAGE } from '../../../../utils/validation-utils';

function getBasicInfoProps(): BasicInformationProps {
  return {
    passenger: {
      firstName: '',
      lastName: '',
      gender: 0,
      type: PassengerType.ADULTS,
      typeIdentifier: 0,
      errors: {
        firstName: [],
        lastName: []
      }
    },
    index: 0,
    handleInformationChange: jest.fn(),
    classes: {}
  };
}

describe('Basic Info test suite', () => {
  const passengerData = { firstName: 'Test1', lastName: 'Test2', gender: 1 };

  function getBasicInfoWrapper(props: BasicInformationProps) {
    return shallow(<BasicInformation {...props} />);
  }

  const testErrorHandlingOnElement = (componentName: string, component: any) => {
    const customInputComponent = component.findWhere(
      //@ts-ignore
      (node) => node.name() === 'WithStyles(CustomInput)' && node.props().name === componentName
    );
    const event = { target: { value: '' } };
    customInputComponent.props().onChangeHandler(event);
  };

  const testValueChangeOnElement = async (componentName: string, expectedValue: string) => {
    const defaultProps = getBasicInfoProps();
    const basicInfoComponent = getBasicInfoWrapper(defaultProps);
    const customInputComponent = basicInfoComponent.findWhere(
      (node) => node.name() === 'WithStyles(CustomInput)' && node.props().name === componentName
    );
    customInputComponent.props().onChangeHandler({ target: { value: expectedValue } });
    await expect(basicInfoComponent.state(componentName)).toEqual(expectedValue);
  };
  it('should change the first name input correctly', () => {
    testValueChangeOnElement('firstName', passengerData.firstName);
  });
  it('should change the last name input correctly', () => {
    testValueChangeOnElement('lastName', passengerData.lastName);
  });
  it('should show the error message in case the firstName input is empty', () => {
    const defaultProps = getBasicInfoProps();
    const basicInfoComponent = getBasicInfoWrapper(defaultProps);
    testErrorHandlingOnElement('firstName', basicInfoComponent);
    const state = basicInfoComponent.state() as BasicInformationState;
    expect(state.firstNameErrors.includes(REQUIRED_FIELD_MESSAGE)).toBeTruthy();
  });
  it('should show the error message in case the lastName input is empty', () => {
    const defaultProps = getBasicInfoProps();
    const basicInfoComponent = getBasicInfoWrapper(defaultProps);
    testErrorHandlingOnElement('lastName', basicInfoComponent);
    const state = basicInfoComponent.state() as BasicInformationState;
    expect(state.lastNameErrors.includes(REQUIRED_FIELD_MESSAGE)).toBeTruthy();
  });
});
