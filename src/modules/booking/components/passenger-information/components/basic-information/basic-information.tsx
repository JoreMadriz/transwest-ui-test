import React from 'react';
import {
  Grid,
  Radio,
  RadioGroup,
  FormControlLabel,
  FormControl,
  FormLabel,
  withStyles
} from '@material-ui/core';
import BasicInformationProps from './basic-information.props';
import CustomInput from '../../../checkout/components/custom-input/custom-input';
import styles from './basic-information.style';
import Passenger from '../../model/Passenger';
import { isInvalidStringField, REQUIRED_FIELD_MESSAGE } from '../../../../utils/validation-utils';

export interface BasicInformationState {
  firstName: string;
  firstNameErrors: string[];
  lastName: string;
  lastNameErrors: string[];
  gender: number;
}
export class BasicInformation extends React.Component<
  BasicInformationProps,
  BasicInformationState
> {
  private readonly IS_UNTOUCHED: string = 'isUntouched';
  constructor(props: BasicInformationProps) {
    super(props);
    this.state = {
      firstName: '',
      lastName: '',
      gender: 0,
      firstNameErrors: [this.IS_UNTOUCHED],
      lastNameErrors: [this.IS_UNTOUCHED]
    };
  }

  private createPassenger(): Passenger {
    return {
      firstName: this.state.firstName,
      lastName: this.state.lastName,
      gender: this.state.gender,
      type: this.props.passenger.type,
      typeIdentifier: this.props.passenger.typeIdentifier,
      errors: {
        firstName: this.state.firstNameErrors,
        lastName: this.state.lastNameErrors
      }
    };
  }

  handleFirstName = async (value: string) => {
    const errorList: string[] = [];
    if (isInvalidStringField(value)) {
      errorList.push(REQUIRED_FIELD_MESSAGE);
    }
    await this.setState({
      firstName: value,
      firstNameErrors: errorList
    });
    this.props.handleInformationChange(this.createPassenger(), this.props.index);
  };

  handleLastName = async (value: string) => {
    const errorList: string[] = [];
    if (isInvalidStringField(value)) {
      errorList.push(REQUIRED_FIELD_MESSAGE);
    }
    await this.setState({
      lastName: value,
      lastNameErrors: errorList
    });
    this.props.handleInformationChange(this.createPassenger(), this.props.index);
  };

  handleGenderChange = async (newValue: number) => {
    await this.setState({
      gender: newValue
    });
    this.props.handleInformationChange(this.createPassenger(), this.props.index);
  };

  render() {
    const { classes, passenger } = this.props;
    const { firstName, lastName, errors } = passenger;
    const { form, input, label, elasticBox, elasticBoxLg } = classes;
    const { gender } = this.state;
    return (
      <FormControl className={form} fullWidth>
        <CustomInput
          onChangeHandler={(event: any) => {
            this.handleFirstName(event.target.value);
          }}
          name="firstName"
          className={input}
          placeholder="First Name"
          value={firstName}
          errors={errors.firstName}
        />
        <CustomInput
          onChangeHandler={(event: any) => {
            this.handleLastName(event.target.value);
          }}
          name="lastName"
          className={input}
          placeholder="Last Name"
          value={lastName}
          errors={errors.lastName}
        />
        <Grid container>
          <FormLabel className={`${label} ${elasticBox}`}>Gender</FormLabel>
          <RadioGroup
            id="gender-radio"
            onChange={(event, value) => {
              this.handleGenderChange(parseInt(value));
            }}
            className={elasticBoxLg}
            value={'' + gender}
            row
          >
            <FormControlLabel value="0" control={<Radio />} label="Female" />
            <FormControlLabel value="1" control={<Radio />} label="Male" />
          </RadioGroup>
        </Grid>
      </FormControl>
    );
  }
}

export default withStyles(styles)(BasicInformation);
