import { BORDER_COLOR, PRIMARY_COLOR } from '../../../../../../shared/theme/colors';

const styles = {
  form: {
    border: `1px solid ${BORDER_COLOR}`,
    boxSizing: 'border-box' as 'border-box'
  },

  input: {
    borderBottom: `1px solid ${BORDER_COLOR}`,
    minHeight: '37px',
    color: `${PRIMARY_COLOR}`,
    '&::placeholder': {
      color: `${PRIMARY_COLOR}`,
      opacity: 1,
      fontSize: '14px'
    },
    '&:focus': {
      color: `${PRIMARY_COLOR}`
    },
    '& label': {
      color: `${PRIMARY_COLOR}`
    },
    '& input': {
      borderBottom: 'transparent',
      fontSize: '14px',
      '&::focus ': {
        color: `${PRIMARY_COLOR}`
      }
    }
  },
  label: {
    margin: 'auto 0',
    marginLeft: '10px',
    fontSize: '14px',
    color: `${PRIMARY_COLOR}`,
    textAlign: 'left' as 'left'
  },
  elasticBoxLg: {
    flexGrow: 2
  },
  elasticBox: {
    flexGrow: 1
  }
};

export default styles;
