import Passenger from '../../model/Passenger';

export default interface BasicInformationProps {
  classes: any;
  index: number;
  passenger: Passenger;
  handleInformationChange: Function;
}
