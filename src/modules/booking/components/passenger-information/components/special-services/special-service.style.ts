import { BORDER_COLOR, PRIMARY_COLOR } from '../../../../../../shared/theme/colors';
import { MARGIN_2 } from '../../../../../../shared/theme/gutters';

const styles = {
  container: {
    backgroundColor: '#F1F9FF',
    border: `1px solid ${BORDER_COLOR}`,
    minWidth: '55px'
  },
  text: {
    fontSize: '16px',
    margin: MARGIN_2
  },
  message: {
    padding: '10px',
    border: `1px solid ${BORDER_COLOR}`,
    borderTop: 'transparent',
    minWidth: '55px',
    paddingBottom: '0px ',
    width: '100%'
  },
  messageText: {
    fontSize: '12px',
    margin: MARGIN_2,
    textAlign: 'left' as 'left'
  },
  input: {
    marginTop: '0px !important',
    paddingLeft: '10px',
    '& input': {
      borderBottom: `1px solid ${PRIMARY_COLOR}`
    }
  }
};
export default styles;
