import React from 'react';
import { Grid, Typography, withStyles, TextField } from '@material-ui/core';

import styles from './special-service.style';

export interface SpecialServicesProps {
  classes: any;
}

const SpecialServices = (props: SpecialServicesProps) => {
  const { classes } = props;
  const { container, text, message, messageText } = classes;
  const SPECIAL_SERVICES: string = 'Special Services';
  const MESSAGE: string = `If the traveler is a person with a disability and requires extra assistance, 
    please describe the nature of the disability so that we are better able to assist you with your traveling needs.`;
  return (
    <Grid container>
      <Grid container className={container}>
        <Typography className={text}>{SPECIAL_SERVICES}</Typography>
      </Grid>
      <Grid className={message}>
        <Typography className={messageText}>{MESSAGE}</Typography>
        <TextField fullWidth id="standard-bare" margin="normal" />
      </Grid>
    </Grid>
  );
};

export default withStyles(styles)(SpecialServices);
