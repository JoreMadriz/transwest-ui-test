import moment from 'moment';
import React from 'react';
import { TextField, Grid, withStyles, Typography } from '@material-ui/core';

import BasicInformation from '../basic-information/basic-information';
import PassengerType from '../../../../models/PassengerType';
import Passenger from '../../model/Passenger';
import styles from './underage-info.style';
import SectionSpacer from '../../../../../../shared/theme/components/section-spacer/section-spacer';
import { SpacerType } from '../../../../../../shared/theme/components/section-spacer/SpacerType';
import UnderagePassenger from '../../model/underage-passenger';

export interface UnderageInfoProps {
  classes: any;
  passenger: UnderagePassenger;
  index: number;
  onUnderageInfoChange: Function;
}

export class UnderageInfo extends React.Component<UnderageInfoProps> {
  constructor(props: UnderageInfoProps) {
    super(props);
    this.state = {
      basicInformation: null,
      dateOfBirth: null
    };
  }

  private getTypeTitle = (type: PassengerType): string => {
    switch (type) {
      case PassengerType.BABY:
        return 'Baby';
      case PassengerType.TEENS:
        return 'Teen';
      case PassengerType.CHILDREN:
        return 'Child';
      default:
        return '';
    }
  };

  handleBasicInformation = (passengerBasicInfo: Passenger) => {
    const { underageErrors, birthDate } = this.props.passenger;
    this.props.onUnderageInfoChange(
      { ...passengerBasicInfo, underageErrors, birthDate },
      this.props.index
    );
  };

  handleBirthDateChange = (dateOfBirth: string) => {
    let errorList: string[] = [];
    const newBirthDate = moment(dateOfBirth);
    if (!newBirthDate.isValid()) {
      errorList.push('Invalid date');
    }
    if (newBirthDate.year() > moment().year()) {
      errorList.push('Invalid date');
    }
    this.props.onUnderageInfoChange(
      {
        ...this.props.passenger,
        birthDate: newBirthDate,
        underageErrors: { birthDate: errorList }
      },
      this.props.index
    );
  };

  render() {
    const { classes, index, passenger } = this.props;
    const { typeIdentifier, type } = passenger;
    const { datePicker, box, label, container, title } = classes;

    return (
      <Grid>
        <Typography className={title} align="left">{`${this.getTypeTitle(
          type
        )} ${typeIdentifier}`}</Typography>
        <BasicInformation
          passenger={passenger}
          index={index}
          handleInformationChange={this.handleBasicInformation}
        />
        <Grid container className={container}>
          <Grid className={box}>
            <TextField
              onChange={(event) => {
                this.handleBirthDateChange(event.target.value);
              }}
              className={`${label} ${datePicker}`}
              id="date"
              label="Date of birth"
              type="date"
              InputLabelProps={{
                shrink: true
              }}
              InputProps={{ disableUnderline: true }}
            />
          </Grid>
        </Grid>
        <SectionSpacer spacerType={SpacerType.TINY} />
      </Grid>
    );
  }
}

export default withStyles(styles)(UnderageInfo);
