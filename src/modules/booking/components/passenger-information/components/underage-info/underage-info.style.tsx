import { PRIMARY_COLOR, BORDER_COLOR } from '../../../../../../shared/theme/colors';
import { MARGIN_2 } from '../../../../../../shared/theme/gutters';

const styles = {
  container: {
    textAlign: 'left' as 'left',
    border: `1px solid ${BORDER_COLOR}`,
    borderTop: 'transparent'
  },
  label: {
    color: `${PRIMARY_COLOR}`,
    '& label': {
      color: `${PRIMARY_COLOR}`
    }
  },
  box: {
    paddingTop: '10px',
    marginLeft: '10px'
  },
  datePicker: {
    '& input::-webkit-calendar-picker-indicator': {
      display: 'none' as 'none'
    },
    '& input[type="date"]::-webkit-input-placeholder': {
      visibility: 'hidden' as 'hidden'
    }
  },
  title: {
    fontSize: '16px',
    color: PRIMARY_COLOR,
    marginBottom: MARGIN_2
  }
};

export default styles;
