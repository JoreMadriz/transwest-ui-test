import React from 'react';
import { shallow } from 'enzyme';

import PassengerType from '../../../../models/PassengerType';
import { UnderageInfo } from './underage-info';
import { UnderageInfoProps } from './underage-info';
import UnderagePassenger from '../../model/underage-passenger';

function getUnderageInfoProps(): UnderageInfoProps {
  return {
    passenger: {
      firstName: '',
      lastName: '',
      gender: 0,
      type: PassengerType.TEENS,
      typeIdentifier: 0,
      birthDate: null,
      errors: {
        firstName: [],
        lastName: []
      },
      underageErrors: {
        birthDate: []
      }
    },
    index: 0,
    onUnderageInfoChange: jest.fn(),
    classes: {}
  };
}

describe('Underage Info test suite', () => {
  function getBasicInfoWrapper(props: UnderageInfoProps) {
    return shallow(<UnderageInfo {...props} />);
  }

  it('should render the basic info component', () => {
    const underageInfoComponent = getBasicInfoWrapper(getUnderageInfoProps());
    const adultInfoComponent = underageInfoComponent.find('WithStyles(BasicInformation)');
    expect(adultInfoComponent.length).toBe(1);
  });
  it('should change the birth date input correctly', () => {
    const defaultProps = getUnderageInfoProps();
    const handleChangeMock = defaultProps.onUnderageInfoChange;
    const underageInfoComponent = getBasicInfoWrapper(defaultProps);
    const testDate: string = '06-06-1996';
    underageInfoComponent.find('#date').simulate('change', { target: { value: testDate } });
    expect(handleChangeMock).toHaveBeenCalled();
    //@ts-ignore
    const usedPassenger = handleChangeMock.mock.calls[0][0] as UnderagePassenger;
    expect(JSON.stringify(usedPassenger.birthDate)).toEqual(JSON.stringify(new Date(testDate)));
  });
  it('should show an error when birth date field is invalid', () => {
    const defaultProps = getUnderageInfoProps();
    const underageInfoComponent = getBasicInfoWrapper(defaultProps);
    const handleChangeMock = defaultProps.onUnderageInfoChange;
    underageInfoComponent.find('#date').simulate('change', { target: { value: '05/05/2029' } });
    const invalidBirthDate = ['Invalid date'];
    defaultProps.passenger.birthDate = new Date('05/05/2029');
    defaultProps.passenger.underageErrors.birthDate = invalidBirthDate;
    //@ts-ignore
    const usedPassenger = handleChangeMock.mock.calls[0][0] as UnderagePassenger;
    expect(handleChangeMock).toHaveBeenCalled();
    expect(JSON.stringify(usedPassenger)).toEqual(JSON.stringify(defaultProps.passenger));
  });
});
