import { BORDER_COLOR, PRIMARY_COLOR } from '../../../../../../shared/theme/colors';

const styles = {
  form: {
    borderLeft: `1px solid ${BORDER_COLOR}`,
    borderRight: `1px solid ${BORDER_COLOR}`,
    boxSizing: 'border-box' as 'border-box'
  },
  formControlContainer: {},
  input: {
    borderBottom: `1px solid ${BORDER_COLOR}`,
    minHeight: '37px',
    '&::placeholder': {
      color: `${PRIMARY_COLOR}`,
      opacity: 1,
      fontSize: '14px'
    },

    '& input': {
      borderBottom: 'transparent',
      fontSize: '14px',
      '&::focus ': {
        color: `${PRIMARY_COLOR}`
      }
    }
  },
  smallBox: {
    borderRight: `1px solid ${BORDER_COLOR}`,
    borderBottom: `1px solid ${BORDER_COLOR}`,
    '& input': {
      paddingLeft: '10px'
    },
    '& div': {
      width: '100%'
    }
  },
  bigBox: {
    textAlign: 'left' as 'left'
  }
};

export default styles;
