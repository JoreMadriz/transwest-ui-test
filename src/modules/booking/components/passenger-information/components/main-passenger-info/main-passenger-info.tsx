import { Grid, FormControl, withStyles } from '@material-ui/core';
import React from 'react';

import AdultInformation from '../adult-info/adult-info';
import CustomInput from '../../../checkout/components/custom-input/custom-input';
import MainPassenger from '../../model/main-passenger';
import Passenger from '../../model/Passenger';
import styles from './main-passenger-info.style';
import { isInvalidStringField, REQUIRED_FIELD_MESSAGE } from '../../../../utils/validation-utils';

export interface MainPassengerInformationProps {
  classes: any;
  passenger: MainPassenger;
  onMainPassengerInfoChange: Function;
}

export class MainPassengerInformation extends React.Component<MainPassengerInformationProps> {
  private readonly EMAIL_REGEX: RegExp = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])+\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])/;
  private readonly MAIN_PASSENGER_INDEX: number = -1;
  private readonly EMAIL_CONFIRMATION_MATCH_ERROR: string = 'Confirmation email should match email';

  constructor(props: MainPassengerInformationProps) {
    super(props);
  }

  handleAdultInformationChange = (adultInfo: Passenger) => {
    const { passenger } = this.props;
    const { code, confirmEmail, email, phoneNumber, mainErrors } = passenger;
    this.props.onMainPassengerInfoChange({
      ...adultInfo,
      code,
      confirmEmail,
      email,
      phoneNumber,
      mainErrors
    });
  };

  handleEmailChange = (email: string) => {
    const mainErrors = { ...this.props.passenger.mainErrors };
    mainErrors.email = [];
    if (!this.EMAIL_REGEX.test(email)) {
      mainErrors.email.push('Email has an incorrect format');
    }
    if (isInvalidStringField(email)) {
      mainErrors.email.push(REQUIRED_FIELD_MESSAGE);
      this.setState({
        emailErrors: mainErrors.email
      });
    }
    if (this.props.passenger.confirmEmail !== email) {
      if (!mainErrors.confirmEmail.includes(this.EMAIL_CONFIRMATION_MATCH_ERROR))
        mainErrors.confirmEmail.push(this.EMAIL_CONFIRMATION_MATCH_ERROR);
    } else {
      mainErrors.confirmEmail = mainErrors.confirmEmail.filter(
        (error) => error !== this.EMAIL_CONFIRMATION_MATCH_ERROR
      );
    }

    this.props.onMainPassengerInfoChange({ ...this.props.passenger, email, mainErrors });
  };

  handleConfirmationEmailChange = (confirmEmail: string) => {
    const mainErrors = { ...this.props.passenger.mainErrors };
    mainErrors.confirmEmail = [];
    if (!this.EMAIL_REGEX.test(confirmEmail)) {
      mainErrors.confirmEmail.push('Email has an incorrect format');
    }
    if (isInvalidStringField(confirmEmail)) {
      mainErrors.confirmEmail.push(REQUIRED_FIELD_MESSAGE);
    }
    if (this.props.passenger.email !== confirmEmail) {
      mainErrors.confirmEmail.push(this.EMAIL_CONFIRMATION_MATCH_ERROR);
    }

    this.props.onMainPassengerInfoChange({ ...this.props.passenger, confirmEmail, mainErrors });
  };

  handleCodeChange = (code: string) => {
    const mainErrors = { ...this.props.passenger.mainErrors };
    mainErrors.code = [];
    if (isInvalidStringField(code)) {
      mainErrors.code.push(REQUIRED_FIELD_MESSAGE);
    }
    this.props.onMainPassengerInfoChange({ ...this.props.passenger, code, mainErrors });
  };

  handleDestinationMobileNumber = (phoneNumber: string) => {
    const mainErrors = { ...this.props.passenger.mainErrors };
    mainErrors.phoneNumber = [];
    if (isInvalidStringField(phoneNumber)) {
      mainErrors.phoneNumber.push(REQUIRED_FIELD_MESSAGE);
    }
    this.props.onMainPassengerInfoChange({ ...this.props.passenger, phoneNumber, mainErrors });
  };

  render() {
    const { classes, passenger } = this.props;
    const { email, code, confirmEmail, mainErrors, phoneNumber } = passenger;
    const { formControlContainer, input } = classes;
    return (
      <Grid>
        <AdultInformation
          passenger={passenger}
          index={this.MAIN_PASSENGER_INDEX}
          onAdultInfoChange={this.handleAdultInformationChange}
        />
        <FormControl className={`${classes.form} ${formControlContainer}`} fullWidth>
          <CustomInput
            onChangeHandler={(event: any) => {
              this.handleEmailChange(event.target.value);
            }}
            name="email"
            className={input}
            placeholder="Email"
            value={email}
            errors={mainErrors.email}
          />
          <CustomInput
            onChangeHandler={(event: any) => {
              this.handleConfirmationEmailChange(event.target.value);
            }}
            name="confirmEmail"
            className={input}
            placeholder="Confirm email"
            value={confirmEmail}
            errors={mainErrors.confirmEmail}
          />
          <Grid container>
            <Grid item className={classes.smallBox} xs={3}>
              <CustomInput
                onChangeHandler={(event: any) => {
                  this.handleCodeChange(event.target.value);
                }}
                name="code"
                placeholder="Code"
                value={code}
                errors={mainErrors.code}
              />
            </Grid>
            <Grid className={`${classes.input} ${classes.bigBox}`} item xs={9}>
              <CustomInput
                onChangeHandler={(event: any) => {
                  this.handleDestinationMobileNumber(event.target.value);
                }}
                name="phoneNumber"
                placeholder="Destination Mobile Number"
                value={phoneNumber}
                errors={mainErrors.phoneNumber}
              />
            </Grid>
          </Grid>
        </FormControl>
      </Grid>
    );
  }
}

export default withStyles(styles)(MainPassengerInformation);
