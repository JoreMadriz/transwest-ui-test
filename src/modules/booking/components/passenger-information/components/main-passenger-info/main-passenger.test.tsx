import React from 'react';
import Enzyme, { shallow } from 'enzyme';

import { MainPassengerInformation } from './main-passenger-info';
import { MainPassengerInformationProps } from './main-passenger-info';
import PassengerType from '../../../../models/PassengerType';
import { REQUIRED_FIELD_MESSAGE } from '../../../../utils/validation-utils';

type MainPassengerType = Enzyme.ShallowWrapper<MainPassengerInformationProps>;
function getMainPassengerProps(): MainPassengerInformationProps {
  return {
    passenger: {
      firstName: 'Test',
      lastName: 'Test',
      gender: 0,
      type: PassengerType.ADULTS,
      typeIdentifier: 0,
      email: '',
      confirmEmail: '',
      code: '',
      phoneNumber: '',
      errors: {
        firstName: [],
        lastName: []
      },
      mainErrors: {
        email: [],
        confirmEmail: [],
        code: [],
        phoneNumber: []
      }
    },
    onMainPassengerInfoChange: jest.fn(),
    classes: {}
  };
}

describe('Main Passenger test suite', () => {
  const passengerData = {
    firstName: 'Test1',
    lastName: 'Test2',
    gender: 1,
    email: 'test@test.com',
    confirmEmail: 'test@test.com',
    code: '',
    phoneNumber: '88888888'
  };

  function getMainPassengerInfoWrapper(props: MainPassengerInformationProps): MainPassengerType {
    return shallow(<MainPassengerInformation {...props} />);
  }

  const INCORRECT_FORMAT: string = 'Email has an incorrect format';
  const EMAIL_CONFIRMATION_MATCH_ERROR: string = 'Confirmation email should match email';

  const testEmptyErrorHandlingOnElement = (
    componentName: string,
    component: any,
    value: string
  ) => {
    const customInputComponent = component.findWhere(
      //@ts-ignore
      (node) => node.name() === 'WithStyles(CustomInput)' && node.props().name === componentName
    );
    const event = { target: { value: value } };
    customInputComponent.props().onChangeHandler(event);
  };

  const testValueChangeOnElement = async (componentName: string, expectedValue: string) => {
    const defaultProps = getMainPassengerProps();
    const mainPassengerComponent = getMainPassengerInfoWrapper(defaultProps);
    const customInputComponent = mainPassengerComponent.findWhere(
      (node) => node.name() === 'WithStyles(CustomInput)' && node.props().name === componentName
    );
    customInputComponent
      .props()
      .onChangeHandler(
        customInputComponent.simulate('change', { target: { value: expectedValue } })
      );
    await expect(mainPassengerComponent.state(componentName)).toEqual(expectedValue);
  };
  it('should render the adult info component', () => {
    const mainPassengerInfoComponent = getMainPassengerInfoWrapper(getMainPassengerProps());
    const adultInfoComponent = mainPassengerInfoComponent.find('WithStyles(AdultInformation)');
    expect(adultInfoComponent.length).toBe(1);
  });
  it('should change the email input correctly', () => {
    testValueChangeOnElement('email', passengerData.email);
  });
  it('should change the confirm email input correctly', () => {
    testValueChangeOnElement('confirmEmail', passengerData.confirmEmail);
  });
  it('should change the code input correctly', () => {
    testValueChangeOnElement('code', passengerData.code);
  });
  it('should change the phone number input correctly', () => {
    testValueChangeOnElement('phoneNumber', passengerData.phoneNumber);
  });
  it("should show an error in case email field is empty & doesn't have the right format", () => {
    let defaultProps: MainPassengerInformationProps = getMainPassengerProps();
    const mainPassengerComponent = getMainPassengerInfoWrapper(defaultProps);
    testEmptyErrorHandlingOnElement('email', mainPassengerComponent, '');
    const emailError = [INCORRECT_FORMAT, REQUIRED_FIELD_MESSAGE];
    defaultProps.passenger.email = '';
    defaultProps.passenger.mainErrors.email = emailError;
    expect(defaultProps.onMainPassengerInfoChange).toHaveBeenCalledWith(defaultProps.passenger);
  });
  it('should show an error when confirm email does not match email, is empty and does not match the format ', () => {
    let defaultProps: MainPassengerInformationProps = getMainPassengerProps();
    defaultProps.passenger.email = 'email@test.com';
    const mainPassengerComponent = getMainPassengerInfoWrapper(defaultProps);
    testEmptyErrorHandlingOnElement('confirmEmail', mainPassengerComponent, '');
    const emailError = [INCORRECT_FORMAT, REQUIRED_FIELD_MESSAGE, EMAIL_CONFIRMATION_MATCH_ERROR];
    defaultProps.passenger.confirmEmail = '';
    defaultProps.passenger.mainErrors.confirmEmail = emailError;
    expect(defaultProps.onMainPassengerInfoChange).toHaveBeenCalledWith(defaultProps.passenger);
  });
  it('should show an error when code input is empty', () => {
    let defaultProps: MainPassengerInformationProps = getMainPassengerProps();
    const mainPassengerComponent = getMainPassengerInfoWrapper(defaultProps);
    testEmptyErrorHandlingOnElement('code', mainPassengerComponent, '');
    const codeError = [REQUIRED_FIELD_MESSAGE];
    defaultProps.passenger.code = '';
    defaultProps.passenger.mainErrors.code = codeError;
    expect(defaultProps.onMainPassengerInfoChange).toHaveBeenCalledWith(defaultProps.passenger);
  });
  it('should show an error when phone number input is empty', () => {
    let defaultProps: MainPassengerInformationProps = getMainPassengerProps();
    const mainPassengerComponent = getMainPassengerInfoWrapper(defaultProps);
    testEmptyErrorHandlingOnElement('phoneNumber', mainPassengerComponent, '');
    const phoneNumberError = [REQUIRED_FIELD_MESSAGE];
    defaultProps.passenger.phoneNumber = '';
    defaultProps.passenger.mainErrors.phoneNumber = phoneNumberError;
    expect(defaultProps.onMainPassengerInfoChange).toHaveBeenCalledWith(defaultProps.passenger);
  });
});
