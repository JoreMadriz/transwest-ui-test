import { combineLatest, Subject, BehaviorSubject } from 'rxjs';

import AirportSelectionTriggers from './models/airport-selection.triggers';
import { handleErrorMessage } from '../../../../shared/components/error-message/error-message.observable';
import HttpService from '../../../../shared/services/http.service';

const state = { outboundLocations: [], inboundLocations: [] };
const AIRPORTS_ENDPOINT: string = '/Airport/GetAirports';
const AIRPORT_DESTINATIONS_ENDPOINT: string = '/Airport/GetAirportDestinations';
const AIRPORT_ERROR_MESSAGE: string = `There was an error while retrieving the airport list, 
  please contact support if the problem persists`;

const outboundAirportObs = new Subject();

const inboundAirportObs = new BehaviorSubject({ locations: [] });

outboundAirportObs.subscribe((data: any) => {
  state.outboundLocations = data.locations.data;
});

inboundAirportObs.subscribe((data: any) => {
  state.inboundLocations = data.locations.data;
});

const airportSelectionObservable = combineLatest(
  outboundAirportObs,
  inboundAirportObs,
  (outboundLocations, inboundLocations) => {
    return {
      outboundLocations,
      inboundLocations
    };
  }
);

export const triggers: AirportSelectionTriggers = {
  reloadDepartureList: () => {
    HttpService.get(AIRPORTS_ENDPOINT, {})
      .then((data) => {
        outboundAirportObs.next({ locations: data });
      })
      .catch((error) => {
        handleErrorMessage(AIRPORT_ERROR_MESSAGE);
        console.log(`should call the logger endpoint here ${error}`);
      });
  },
  onDepartureAirportSelected: (airportCode: string) => {
    inboundAirportObs.next({ locations: [] });
    HttpService.post(AIRPORT_DESTINATIONS_ENDPOINT, {}, { AirportCode: airportCode })
      .then((data) => {
        inboundAirportObs.next({ locations: data });
      })
      .catch((error) => {
        handleErrorMessage(AIRPORT_ERROR_MESSAGE);
        console.log(`should call the logger endpoint here ${error}`);
      });
  }
};
export default airportSelectionObservable;
