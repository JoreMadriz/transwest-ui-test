export default interface AirportSelectionState {
  isSelectionDialogOpen: boolean;
  dialogTitle: string;
  dialogType: string;
}
