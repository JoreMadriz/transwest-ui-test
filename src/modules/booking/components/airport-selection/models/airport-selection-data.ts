import Location from './location';

export default interface AirportSelectionData {
  departureAirport: Location | undefined;
  destinationAirport: Location | undefined;
  isOneWayTrip: boolean;
  promoCode: string;
}
