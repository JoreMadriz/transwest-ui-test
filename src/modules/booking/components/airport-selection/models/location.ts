import Address from './Address';

export default interface location {
  Id: number;
  Code: string;
  Name: string;
  Address: Address;
  WayOrder: number;
}
