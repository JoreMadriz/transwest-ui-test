export default interface AirportSelectionTriggers {
    reloadDepartureList: Function;
    onDepartureAirportSelected: Function;
}