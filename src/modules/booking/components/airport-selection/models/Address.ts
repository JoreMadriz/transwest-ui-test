export default interface Address {
  Address: string;
  City: string;
  Province: string;
  Country: number;
  PostalCode: string;
}
