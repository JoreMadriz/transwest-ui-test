import AirportSelectionData from './models/airport-selection-data';
import AirportList from './components/airport-selection-dialog/models/airportList';
import AirportSelectionTriggers from './models/airport-selection.triggers';

export default interface AirportSelectionProps {
  classes: any;
  triggers: AirportSelectionTriggers;
  onChangeAirportSelection: Function;
  onDepartureAirportSelected: Function;
  airportSelectionData: AirportSelectionData | undefined;
  outboundLocations: AirportList | null;
  inboundLocations: AirportList | null;
}
