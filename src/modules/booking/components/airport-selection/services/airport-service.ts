import Location from '../models/location';

export default interface AirportService {
  getAirportList(searchCriteria: string): Location[];
}
