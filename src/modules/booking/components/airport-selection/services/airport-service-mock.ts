import AirportService from './airport-service';
import Location from '../models/location';

export default class AirportServiceMock implements AirportService {
  getAirportList(searchCriteria: string): Location[] {
    return [
      {
        Address: {
          Address: 'test address',
          City: 'test city',
          Country: 1,
          PostalCode: '12334',
          Province: 'test province'
        },
        Code: 'testCode',
        Id: 1,
        Name: 'airportTest1',
        WayOrder: 1
      },
      {
        Address: {
          Address: 'test address',
          City: 'test city',
          Country: 1,
          PostalCode: '12334',
          Province: 'test province'
        },
        Code: 'testCode',
        Id: 2,
        Name: 'airportTest2',
        WayOrder: 1
      },
      {
        Address: {
          Address: 'test address',
          City: 'test city',
          Country: 1,
          PostalCode: '12334',
          Province: 'test province'
        },
        Code: 'testCode',
        Id: 3,
        Name: 'airportTest3',
        WayOrder: 1
      },
      {
        Address: {
          Address: 'test address',
          City: 'test city',
          Country: 1,
          PostalCode: '12334',
          Province: 'test province'
        },
        Code: 'testCode',
        Id: 4,
        Name: 'airportTest4',
        WayOrder: 1
      },
      {
        Address: {
          Address: 'test address',
          City: 'test city',
          Country: 1,
          PostalCode: '12334',
          Province: 'test province'
        },
        Code: 'testCode',
        Id: 5,
        Name: 'airportTest5',
        WayOrder: 1
      },
      {
        Address: {
          Address: 'test address',
          City: 'test city',
          Country: 1,
          PostalCode: '12334',
          Province: 'test province'
        },
        Code: 'testCode',
        Id: 6,
        Name: 'airportTest6',
        WayOrder: 1
      },
      {
        Address: {
          Address: 'test address',
          City: 'test city',
          Country: 1,
          PostalCode: '12334',
          Province: 'test province'
        },
        Code: 'testCode',
        Id: 7,
        Name: 'airportTest7',
        WayOrder: 1
      },
      {
        Address: {
          Address: 'test address',
          City: 'test city',
          Country: 1,
          PostalCode: '12334',
          Province: 'test province'
        },
        Code: 'testCode',
        Id: 8,
        Name: 'airportTest8',
        WayOrder: 1
      },
      {
        Address: {
          Address: 'test address',
          City: 'test city',
          Country: 1,
          PostalCode: '12334',
          Province: 'test province'
        },
        Code: 'testCode',
        Id: 9,
        Name: 'airportTest9',
        WayOrder: 1
      }
    ];
  }
}
