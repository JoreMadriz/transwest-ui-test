interface IAirportSelectionCardProps {
  title: string;
  airport: string;
  classes: any;
  clickAction: any;
  isEnabled: boolean;
}

export default IAirportSelectionCardProps;
