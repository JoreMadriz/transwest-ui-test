import { BORDER_COLOR, DISABLED } from '../../../../../../shared/theme/colors';
import { PADDING_2 } from '../../../../../../shared/theme/gutters';

const styles = (theme: any) => ({
  container: {
    [theme.breakpoints.up('md')]: {
      padding: '1.5em'
    },
    padding: '0.7em',
    border: `1px solid ${BORDER_COLOR}`,
    marginBottom: PADDING_2
  },
  disabledContainer: {
    border: `1px solid ${DISABLED} !important`
  },
  disabledText: {
    color: `${DISABLED} !important`
  },
  titleSize: {
    [theme.breakpoints.up('md')]: {
      fontSize: '20px'
    },
    fontSize: '14px'
  },
  subtitle: {
    [theme.breakpoints.up('md')]: {
      fontSize: '25px'
    },
    fontSize: '18px'
  }
});

export default styles;
