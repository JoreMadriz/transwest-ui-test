import React from 'react';
import { Typography, Grid } from '@material-ui/core/';
import { withStyles } from '@material-ui/core/styles';
import styles from './airport-selection-card.style';

import AirportSelectionCardProps from './airport-selection-card.props';

export class AirportSelectionCard extends React.Component<AirportSelectionCardProps, {}> {
  render() {
    const { title, airport, classes, clickAction, isEnabled } = this.props;
    const { container, titleSize, subtitle } = classes;
    let { disabledContainer, disabledText } = classes;
    disabledContainer = isEnabled ? '' : disabledContainer;
    disabledText = isEnabled ? '' : disabledText;
    return (
      <Grid
        className={`${container} ${disabledContainer}`}
        container={true}
        direction="column"
        justify="flex-start"
        onClick={clickAction}
      >
        <Typography className={`${titleSize} ${disabledText}`} align="left" color="textPrimary">
          {title}
        </Typography>

        <Typography className={`${subtitle} ${disabledText}`} align="left" color="textPrimary">
          {airport}
        </Typography>
      </Grid>
    );
  }
}

export default withStyles(styles)(AirportSelectionCard);
