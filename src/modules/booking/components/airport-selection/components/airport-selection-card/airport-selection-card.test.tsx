import Enzyme, { shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import React from 'react';

import { AirportSelectionCard } from './airport-selection-card';
import AirportSelectionCardProps from './airport-selection-card.props';

Enzyme.configure({ adapter: new Adapter() });

type WrapperType = Enzyme.ShallowWrapper<AirportSelectionCardProps, {}, AirportSelectionCard>;

describe('Airport Selection Card test', () => {
  let wrapper: WrapperType | null;
  const title = 'Test';
  const airport = 'Other test';

  const clickActionMock = jest.fn();

  const app = (): WrapperType => {
    if (!wrapper) {
      wrapper = shallow(
        <AirportSelectionCard
          classes={{}}
          title={title}
          airport={airport}
          clickAction={clickActionMock}
        />
      );
    }
    return wrapper;
  };
  beforeEach(() => {
    wrapper = null;
  });

  it('Should show the dialog', () => {
    const airportSelectionComp = app();
    const foundComponent = airportSelectionComp.find('WithStyles(Grid)');
    foundComponent.simulate('click');
    expect(clickActionMock).toHaveBeenCalled();
  });
});
