import AirportList from '../airport-selection-dialog/models/airportList';

interface AirportSelectionDialogMobileProps {
  open: boolean;
  handleClose: Function;
  classes: any;
  type: string;
  airports: AirportList | null;
  selectDestinationAirport: Function;
}

export default AirportSelectionDialogMobileProps;
