import Clear from '@material-ui/icons/Clear';
import React from 'react';

import {
  List,
  Drawer,
  ListItem,
  ListItemText,
  Typography,
  withStyles,
  Grid,
  IconButton,
  Divider,
  CircularProgress
} from '@material-ui/core';

import AirportSelectionDialogMobileProps from './airport-selection-dialog-mobile.props';
import styles from './airport-selection-dialog-mobile.style';
import SectionSpacer from '../../../../../../shared/theme/components/section-spacer/section-spacer';
import { SpacerType } from '../../../../../../shared/theme/components/section-spacer/SpacerType';

const AirportSelectionDialog = (props: AirportSelectionDialogMobileProps) => {
  const DEPARTURE: string = 'departure';
  const { classes, open, handleClose, type, airports, selectDestinationAirport } = props;
  const { rootDrawer, heading, dialogTitle, divider, airportOption } = classes;
  const locations = airports ? airports.locations : null;
  const airportsData = locations ? locations.data : null;
  return (
    <Drawer
      classes={{ paper: rootDrawer }}
      open={open}
      onClose={() => {
        handleClose();
      }}
    >
      <div
        tabIndex={0}
        role="button"
        onKeyDown={() => {
          handleClose();
        }}
      >
        <Grid className={heading} container justify="space-between">
          <Typography className={dialogTitle} variant="h4">{`${type} ${
            type === DEPARTURE ? 'From' : 'To'
          }`}</Typography>
          <IconButton onClick={() => handleClose()}>
            <Clear fontSize="large" color="secondary" />
          </IconButton>
        </Grid>
        <SectionSpacer spacerType={SpacerType.MEDIUM} />
        {airportsData && airportsData.length !== 0 ? (
          <List>
            {airportsData!.map((airport) => {
              return (
                <div
                  key={airport.Id}
                  onClick={() => {
                    selectDestinationAirport(airport, type);
                  }}
                >
                  <ListItem button>
                    <ListItemText className={airportOption}>{airport.Name}</ListItemText>
                  </ListItem>
                  <SectionSpacer spacerType={SpacerType.SMALL} />
                  <Divider classes={{ root: divider }} color="secondary" />
                </div>
              );
            })}
            <ListItem button />
          </List>
        ) : (
          <Grid container justify="center">
            <CircularProgress color="secondary" className={classes.progress} />
          </Grid>
        )}
      </div>
    </Drawer>
  );
};

export default withStyles(styles)(AirportSelectionDialog);
