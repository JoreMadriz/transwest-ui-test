import { LIGHT_PRIMARY_COLOR } from '../../../../../../shared/theme/colors';

export default {
  rootDrawer: {
    width: '100%'
  },
  heading: {
    padding: '30px'
  },
  airportOption: {
    '&>span': {
      fontSize: '20px'
    }
  },
  dialogTitle: {
    fontWeight: 600,
    textTransform: 'capitalize' as 'capitalize',
    margin: 'auto 0'
  },
  divider: {
    backgroundColor: LIGHT_PRIMARY_COLOR
  }
};
