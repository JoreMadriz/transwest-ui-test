import React from 'react';
import { Grid, Typography, IconButton, withStyles } from '@material-ui/core/';
import ToggleButtonOn from '@material-ui/icons/ToggleOn';
import ToggleButtonOff from '@material-ui/icons/ToggleOff';

import styles from './toggle-button.style';
import ToggleButtonProps from './toggle-button.props';

export class ToggleButton extends React.Component<ToggleButtonProps> {
  private ROUND_TRIP: string = 'Round Trip';
  private ONE_WAY: string = 'One Way';

  render() {
    const { classes, handleToggleClick, isRoundTrip: roundTrip } = this.props;
    const { text, boldText, button } = classes;
    return (
      <Grid container direction="row" justify="flex-start" alignItems={'center'}>
        <Grid item>
          <Typography className={roundTrip ? boldText : text} align="left" color="textPrimary">
            {this.ROUND_TRIP}
          </Typography>
        </Grid>

        <Grid item>
          {roundTrip && (
            <IconButton onClick={handleToggleClick}>
              <ToggleButtonOff className={button} />
            </IconButton>
          )}

          {!roundTrip && (
            <IconButton onClick={handleToggleClick}>
              <ToggleButtonOn className={button} />
            </IconButton>
          )}
        </Grid>

        <Grid item>
          <Typography className={!roundTrip ? boldText : text} align="left" color="textPrimary">
            {this.ONE_WAY}
          </Typography>
        </Grid>
      </Grid>
    );
  }
}

export default withStyles(styles)(ToggleButton);
