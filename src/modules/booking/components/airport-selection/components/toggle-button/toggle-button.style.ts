const styles = {
  text: {
    fontSize: '20px'
  },
  button: {
    fontSize: '70px'
  },
  boldText: {
    fontWeight: 'bold' as 'bold',
    fontSize: '20px'
  }
};

export default styles;
