export default interface ToggleButtonProps {
  classes: any;
  handleToggleClick: any;
  isRoundTrip: boolean;
}
