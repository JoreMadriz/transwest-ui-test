import React from 'react';
import { shallow } from 'enzyme';
import { ToggleButton } from './toggle-button';

describe('Toggle Button test suite', () => {
  function getToggleButtonWrapper() {
    return shallow(<ToggleButton classes={{}} handleToggleClick={() => {}} isRoundTrip={true} />);
  }

  it('should render the component structure properly', () => {
    const toggleButton = getToggleButtonWrapper();
    expect(toggleButton).toMatchSnapshot();
  });
});
