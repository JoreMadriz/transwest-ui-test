export default interface ISearchBarProps {
  onInputChange: CallableFunction;
}
