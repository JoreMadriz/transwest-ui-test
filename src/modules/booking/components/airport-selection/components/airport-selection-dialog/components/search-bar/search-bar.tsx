import debounce from 'lodash/debounce';
import { TextField, InputAdornment } from '@material-ui/core';
import Search from '@material-ui/icons/Search';
import React from 'react';

import ISearchBarProps from './search-bar-props';
import ISearchBarState from './search-bar-state';

export default class SearchBar extends React.Component<ISearchBarProps, ISearchBarState> {
  constructor(props: ISearchBarProps) {
    super(props);
    this.handleSearchBarTextChange = debounce(this.handleSearchBarTextChange, 500);
  }

  private handleSearchBarTextChange = (input: string) => {
    this.props.onInputChange(input);
  };

  render() {
    return (
      <TextField
        onChange={(event) => {
          this.handleSearchBarTextChange(event.target.value)
        }
        }
        label="Search Airports"
        InputProps={{
          startAdornment: (
            <InputAdornment position="start">
              <Search />
            </InputAdornment>
          )
        }}
      />
    );
  }
}
