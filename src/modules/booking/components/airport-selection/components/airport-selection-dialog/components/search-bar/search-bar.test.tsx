import Enzyme, { shallow } from 'enzyme';
import debounce from 'lodash/debounce';
import React from 'react';

import ISearchBarProps from './search-bar-props';
import ISearchBarState from './search-bar-state';
import SearchBar from './search-bar';

jest.mock('lodash/debounce');

type SearchBarType = Enzyme.ShallowWrapper<ISearchBarProps, ISearchBarState, SearchBar>;

describe('Search bar test suite', () => {
  beforeEach(() => {
    // @ts-ignore
    debounce.mockImplementation((method: any) => method);
  });

  function getSearchBarDefaultProps(): ISearchBarProps {
    return {
      onInputChange: jest.fn()
    };
  }

  function getSearchBarWrapper(props: ISearchBarProps): SearchBarType {
    return shallow(<SearchBar onInputChange={props.onInputChange} />);
  }

  it('calls the prop method onInput change after the search value changes', () => {
    const expectedValue: string = 'expectedValue';
    const searchBarProps: ISearchBarProps = getSearchBarDefaultProps();
    const searchBar = getSearchBarWrapper(searchBarProps);
    const textField = searchBar.find('TextField');
    textField.simulate('change', { target: { value: expectedValue } });
    expect(searchBarProps.onInputChange).toHaveBeenCalledWith(expectedValue);
  });
});
