import Location from '../../../../models/location';

export default interface IAirportListProps {
  airportLocations: Location[] | null;
  selectAirport: Function;
  classes: any;
  dialogType: string;
}
