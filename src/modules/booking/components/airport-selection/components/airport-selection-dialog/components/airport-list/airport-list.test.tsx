import Enzyme, { shallow } from 'enzyme';
import React from 'react';

import AirportList from './airport-list';
import IAirportListProps from './airport-list-props';

describe('Airport List test suite', () => {
  function getAirportListDefaultProps(): IAirportListProps {
    return {
      airportLocations: [
        {
          Adress: {
            Address: 'test address',
            City: 'test city',
            Country: 1,
            PostalCode: '12334',
            Province: 'test province'
          },
          Code: 'testCode',
          Id: 1,
          Name: 'airportTest1',
          WayOrder: 1
        },
        {
          Adress: {
            Address: 'test address',
            City: 'test city',
            Country: 1,
            PostalCode: '12334',
            Province: 'test province'
          },
          Code: 'testCode',
          Id: 1,
          Name: 'airportTest2',
          WayOrder: 1
        }
      ],
      selectAirport: jest.fn(),
      classes: {},
      dialogType: 'departure'
    };
  }

  function getAirportListWrapper(props: IAirportListProps) {
    return shallow(
      <AirportList
        airportLocations={props.airportLocations}
        selectAirport={props.selectAirport}
        dialogType={props.dialogType}
      />
    );
  }

  it('should render the same amount of airport names as the list supplied', () => {
    const airportList = getAirportListWrapper(getAirportListDefaultProps()).dive();
    const airportListItems = airportList.find('WithStyles(Typography)');
    expect(airportListItems.length).toBe(2);
  });

  it('shoud call the select airport method and pass the correct airport to the parent component', () => {
    const airportListProps = getAirportListDefaultProps();
    const expectedAirportToSelect = airportListProps.airportLocations
      ? airportListProps.airportLocations[0]
      : null;
    const airportList = getAirportListWrapper(airportListProps).dive();
    const airportListFirstItem = airportList.find('WithStyles(Typography)').first();
    airportListFirstItem.simulate('click');
    expect(airportListProps.selectAirport).toHaveBeenCalledWith(
      expectedAirportToSelect,
      airportListProps.dialogType
    );
  });
});
