import React from 'react';
import { Typography, Grid, withStyles, CircularProgress } from '@material-ui/core';

import airportListStyles from './airport-list.styles';
import IAirportListProps from './airport-list-props';

const AirportList = (props: IAirportListProps) => {
  const { airportLocations, selectAirport, classes, dialogType } = props;
  const { container, listItem, listText } = classes;
  return (
    <Grid container className={container} spacing={16} flex-direction="column">
      {airportLocations && airportLocations.length > 0 ? (
        <>
          {airportLocations!.map((airport) => {
            return (
              <Grid className={listItem} key={airport.Id} item xs={4}>
                <Typography
                  className={listText}
                  onClick={() => {
                    selectAirport(airport, dialogType);
                  }}
                >
                  <span>{`${airport.Name} `}</span>{ `${airport.Code}`}
                </Typography>
              </Grid>
            );
          })}
        </>
      ) : (
        <Grid container justify="center">
          <CircularProgress color="secondary" className={classes.progress} />
        </Grid>
      )}
    </Grid>
  );
};

export default withStyles(airportListStyles)(AirportList);
