import { MARGIN_2 } from '../../../../../../../../shared/theme/gutters';

const airportListStyles = {
  container: {
    marginTop: MARGIN_2
  },
  listItem: {
    cursor: 'pointer' as 'pointer'
  },
  listText: {
    fontSize: '18px',
    fontWeight: 500,
    '& span': {
      fontWeight: 600
    }
  }
};

export default airportListStyles;
