import React from 'react';
import Fuse from 'fuse.js';

import AirportList from './components/airport-list/airport-list';
import CustomDialog from '../../../../../../shared/components/custom-dialog/custom-dialog';
import Location from '../../models/location';
import SearchBar from './components/search-bar/search-bar';
import IAirportSelectionDialogProps from './airport-selection-dialog-props';
import IAirportSelectionDialogState from './airport-selection-dialog-state';

enum AirportListKeys {
  name = 'Name'
}

export default class AirportSelectionDialog extends React.Component<
  IAirportSelectionDialogProps,
  IAirportSelectionDialogState
> {
  private readonly AIRPORT_SEARCH_OPTIONS = {
    keys: [AirportListKeys.name],
    shouldSort: true,
    tokenize: true,
    matchAllTokens: true,
    threshold: 0.2,
    location: 0,
    distance: 32,
    maxPatternLength: 32,
    minMatchCharLength: 1
  };

  constructor(props: IAirportSelectionDialogProps) {
    super(props);
    this.state = {
      filter: ''
    };
  }

  private onSearchBarChange = (searchValue: string) => {
    this.setState({
      filter: searchValue
    });
  };

  private filterAirportListBySearchValue(searchValue: string, airportList: Location[] | null) {
    if (searchValue === '' || !airportList) {
      return airportList;
    }
    const fuzySearchList = new Fuse(airportList, this.AIRPORT_SEARCH_OPTIONS);
    return fuzySearchList.search(searchValue);
  }

  render() {
    const { type, open, selectAirport, handleDialogClose, airportList } = this.props;
    const { filter } = this.state;
    const locations = airportList ? airportList.locations : null;
    const airportsData = locations ? locations.data : null;
    return (
      <CustomDialog
        open={open}
        handleClose={handleDialogClose}
        confirmationButton={false}
        confirmationMessage={'Select an airport to travel'}
        title={type}
      >
        <SearchBar
          onInputChange={(searchValue: string) => {
            this.onSearchBarChange(searchValue);
          }}
        />
        <AirportList
          dialogType={type}
          selectAirport={selectAirport}
          airportLocations={this.filterAirportListBySearchValue(filter, airportsData)}
        />
      </CustomDialog>
    );
  }
}
