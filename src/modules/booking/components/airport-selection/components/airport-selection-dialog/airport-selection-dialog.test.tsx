import Enzyme, { shallow } from 'enzyme';
import React from 'react';

import AirportSelectionDialog from './airport-selection-dialog';
import IAirportSelectionDialogProps from './airport-selection-dialog-props';
import IAirportSelectionDialogState from './airport-selection-dialog-state';

type airportSelectionDialogType = Enzyme.ShallowWrapper<
  IAirportSelectionDialogProps,
  IAirportSelectionDialogState,
  AirportSelectionDialog
>;

describe('Airport selection dialog test suite', () => {
  function getAirportSelectionDefaultProps(): IAirportSelectionDialogProps {
    return {
      handleDialogClose: jest.fn(),
      selectAirport: jest.fn(),
      type: 'testType',
      open: true,
      airportList: {
        locations: {
          data: []
        }
      }
    };
  }

  function getAirportSelectionDialogWrapper(
    props: IAirportSelectionDialogProps
  ): airportSelectionDialogType {
    return shallow(<AirportSelectionDialog {...props} />);
  }

  it("updates the state after the search bar changes it's value", () => {
    const expectedValue: string = 'newValue';
    const airportSelectionDialog = getAirportSelectionDialogWrapper(
      getAirportSelectionDefaultProps()
    );
    const searchBar = airportSelectionDialog.find('SearchBar');
    // @ts-ignore
    searchBar.props().onInputChange(expectedValue);
    expect(airportSelectionDialog.state().filter).toBe(expectedValue);
  });

  it('does not filter the airport list when the filter is an empty string', async () => {
    const defaultProps = getAirportSelectionDefaultProps();
    const airportSelectionDialog = getAirportSelectionDialogWrapper(defaultProps);
    await airportSelectionDialog.setState({
      filter: ''
    });

    const airportList = airportSelectionDialog.find('WithStyles(AirportList)');
    // @ts-ignore
    expect(airportList.props().airportLocations).toEqual(defaultProps.airportList.locations.data);
  });

  it('does not filter the airport list when the airport list is null', () => {
    const airportSelectionDialog = getAirportSelectionDialogWrapper(
      getAirportSelectionDefaultProps()
    );

    airportSelectionDialog.setProps({
      airportList: null
    });
    const airportList = airportSelectionDialog.find('WithStyles(AirportList)');
    // @ts-ignore
    expect(airportList.props().airportLocations).toBeNull();
  });
});
