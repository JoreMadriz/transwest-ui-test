import Location from '../../../models/location';

export default interface AirportList {
  locations: {
    data: Location[];
  };
}
