import AirportList from './models/airportList';

export default interface IAirportSelectionDialogProps {
  type: string;
  open: boolean;
  selectAirport: Function;
  handleDialogClose: Function;
  airportList: AirportList | null;
}
