import Location from '../../models/location';

export default interface IAirportCardManagerProps {
  clickAction: any;
  classes: any;
  departureAirport: Location | undefined;
  destinationAirport: Location | undefined;
  swapAction: any;
  destinationsEnabled: boolean;
}
