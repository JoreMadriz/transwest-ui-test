import { PADDING_5 } from '../../../../../../shared/theme/gutters';
import { PRIMARY_COLOR, WHITE, SECONDARY_COLOR } from '../../../../../../shared/theme/colors';

const styles = (theme: any) => ({
  cards: {
    marginRight: PADDING_5,
    cursor: 'pointer' as 'pointer',
    position: 'relative' as 'relative'
  },
  icon: {
    color: ` ${PRIMARY_COLOR}`,
    backgroundColor: ` ${WHITE}`,
    '&:hover': {
      color: ` ${SECONDARY_COLOR}`
    },
    [theme.breakpoints.up('md')]: {
      fontSize: '60px'
    },
    fontSize: '47px'
  },
  container: {
    position: 'absolute' as 'absolute',
    bottom: '35%',
    [theme.breakpoints.up('md')]: {
      left: '5%'
    },
    [theme.breakpoints.between('md', 'lg')]: {
      left: '3%'
    }
  }
});

export default styles;
