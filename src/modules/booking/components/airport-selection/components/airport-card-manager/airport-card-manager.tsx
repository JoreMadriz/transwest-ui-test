import React from 'react';
import { Component } from 'react';
import { withStyles, Grid } from '@material-ui/core';
import SwapVerticalCirle from '@material-ui/icons/SwapVerticalCircle';

import styles from './airport-card-manager.style';
import IAirportCardManagerProps from './airport-card-manager.props';
import AirportSelectionCard from '../airport-selection-card/airport-selection-card';

export class AirportCardManager extends Component<IAirportCardManagerProps> {
  private readonly DEPARTURE_TITLE: string = 'Departure from';
  private readonly DESTINATION_TITLE: string = 'Destination from';
  private readonly DEPARTURE_DEFAULT_NAME: string = 'Departure Airport';
  private readonly DESTINATION_DEFAULT_NAME: string = 'Destination Airport';
  private readonly DEPARTURE: string = 'departure';
  private readonly DESTINATION: string = 'destination';

  render() {
    const {
      classes,
      clickAction,
      destinationAirport,
      departureAirport,
      swapAction,
      destinationsEnabled
    } = this.props;

    const { cards, icon, container } = classes;
    return (
      <div className={cards}>
        <AirportSelectionCard
          clickAction={() => {
            clickAction(this.DEPARTURE);
          }}
          title={this.DEPARTURE_TITLE}
          airport={departureAirport ? departureAirport.Name : this.DEPARTURE_DEFAULT_NAME}
          isEnabled={true}
        />

        <Grid className={container} justify="flex-end" container>
          <Grid item xs={3}>
            <SwapVerticalCirle className={icon} onClick={swapAction} />
          </Grid>
        </Grid>

        <AirportSelectionCard
          clickAction={() => {
            if (destinationsEnabled) {
              clickAction(this.DESTINATION);
            }
          }}
          title={this.DESTINATION_TITLE}
          airport={destinationAirport ? destinationAirport.Name : this.DESTINATION_DEFAULT_NAME}
          isEnabled={destinationsEnabled}
        />
      </div>
    );
  }
}

export default withStyles(styles)(AirportCardManager);
