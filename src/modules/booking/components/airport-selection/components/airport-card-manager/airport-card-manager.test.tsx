import React from 'react';
import Enzyme, { shallow } from 'enzyme';

import { AirportCardManager } from './airport-card-manager';
import AirportCardManagerProps from './airport-card-manager.props';
import location from '../../models/location';

const DEPARTURE_AIRPORT_NAME = 'airportTest2';
const DESTINATION_AIRPORT_NAME = 'airportTest1';

const departureTest: location = {
  Address: {
    Address: 'test address',
    City: 'test city',
    Country: 1,
    PostalCode: '12334',
    Province: 'test province'
  },
  Code: 'testCode',
  Id: 1,
  Name: DEPARTURE_AIRPORT_NAME,
  WayOrder: 1
};

const destinationTest: location = {
  Address: {
    Address: 'test address',
    City: 'test city',
    Country: 1,
    PostalCode: '12334',
    Province: 'test province'
  },
  Code: 'testCode',
  Id: 1,
  Name: DESTINATION_AIRPORT_NAME,
  WayOrder: 1
};

type WrapperType = Enzyme.ShallowWrapper<AirportCardManagerProps, {}, AirportCardManager>;

describe('Airport Card Manager test suite', () => {
  const DEPARTURE_TITLE: string = 'Departure from';
  const DESTINATION_TITLE: string = 'Destination from';
  const DEPARTURE: string = 'departure';
  const DESTINATION: string = 'destination';
  const DEPARTURE_DEFAULT_NAME: string = 'Departure Airport';
  const DESTINATION_DEFAULT_NAME: string = 'Destination Airport';

  function getDefaultCardManagerProps(): AirportCardManagerProps {
    return {
      clickAction: jest.fn(),
      departureAirport: departureTest,
      destinationAirport: destinationTest,
      swapAction: jest.fn(),
      classes: {}
    };
  }

  function getCardManagerWithoutDataProps(): AirportCardManagerProps {
    return {
      clickAction: jest.fn(),
      departureAirport: undefined,
      destinationAirport: undefined,
      swapAction: jest.fn(),
      classes: {}
    };
  }

  function getCardManagerWrapper(props: AirportCardManagerProps): WrapperType {
    return shallow(<AirportCardManager {...props} />);
  }

  it('should call the click action with the departure value when the departure card is clicked', () => {
    const defaultProps = getDefaultCardManagerProps();
    const clickActionMock = defaultProps.clickAction;
    const airportCardManager = getCardManagerWrapper(defaultProps);
    const airportSelectionCard = airportCardManager.findWhere(
      (node) =>
        node.name() === 'WithStyles(AirportSelectionCard)' && node.props().title === DEPARTURE_TITLE
    );
    airportSelectionCard.props().clickAction();
    expect(clickActionMock).toHaveBeenCalledWith(DEPARTURE);
  });

  it('should call the click action with the destination value when the destination card is clicked', () => {
    const defaultProps = getDefaultCardManagerProps();
    const clickActionMock = defaultProps.clickAction;
    const airportCardManager = getCardManagerWrapper(defaultProps);
    const airportSelectionCard = airportCardManager.findWhere(
      (node) =>
        node.name() === 'WithStyles(AirportSelectionCard)' &&
        node.props().title === DESTINATION_TITLE
    );
    airportSelectionCard.props().clickAction();
    expect(clickActionMock).toHaveBeenCalledWith(DESTINATION);
  });

  function getSelectionCardAirportValue(title: string, props: AirportCardManagerProps) {
    const airportCardManager = getCardManagerWrapper(props);
    const airportSelectionCard = airportCardManager.findWhere(
      (node) => node.name() === 'WithStyles(AirportSelectionCard)' && node.props().title === title
    );
    return airportSelectionCard.props().airport;
  }

  it('should show the Airport Name when the departure card is render ', () => {
    const airportName = getSelectionCardAirportValue(DEPARTURE_TITLE, getDefaultCardManagerProps());
    expect(airportName).toBe(DEPARTURE_AIRPORT_NAME);
  });

  it('should show the Airport Name when the destination card is render ', () => {
    const airportName = getSelectionCardAirportValue(
      DESTINATION_TITLE,
      getDefaultCardManagerProps()
    );
    expect(airportName).toBe(DESTINATION_AIRPORT_NAME);
  });

  it('should show the default departure title when the departure card is render ', () => {
    const airportName = getSelectionCardAirportValue(
      DEPARTURE_TITLE,
      getCardManagerWithoutDataProps()
    );
    expect(airportName).toBe(DEPARTURE_DEFAULT_NAME);
  });

  it('should show the default destination title when the destination card is render ', () => {
    const airportName = getSelectionCardAirportValue(
      DESTINATION_TITLE,
      getCardManagerWithoutDataProps()
    );
    expect(airportName).toBe(DESTINATION_DEFAULT_NAME);
  });
});
