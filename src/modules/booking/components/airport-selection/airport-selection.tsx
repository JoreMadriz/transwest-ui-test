import React from 'react';
import { withStyles, Hidden } from '@material-ui/core';

import AirportCardManager from './components/airport-card-manager/airport-card-manager';
import AirportSelectionDialog from './components/airport-selection-dialog/airport-selection-dialog';
import AirportSelectionProps from './airport-selections.props';
import AirportSelectionState from './airport-selection.state';
import AirportSelectionDialogMobile from './components/airport-selection-dialog-mobile/airport-selection-dialog-mobile';
import Location from './models/location';
import ToggleButton from './components/toggle-button/toggle-button';
import BookingSectionTitle from '../booking-section-title/booking-section-title';
import withObservableStream from '../../../../shared/react-rx/withObservableStream';

import airportSelectionObservable, { triggers } from './airport-selection.observable';

export class AirportSelection extends React.Component<
  AirportSelectionProps,
  AirportSelectionState
> {
  private DEPARTURE_DIALOG_TITLE: string = 'Leaving from';
  private readonly DEPARTURE: string = 'departure';
  private readonly DESTINATION: string = 'destination';

  constructor(props: AirportSelectionProps) {
    super(props);
    this.state = {
      isSelectionDialogOpen: false,
      dialogTitle: this.DEPARTURE_DIALOG_TITLE,
      dialogType: this.DEPARTURE
    };
  }

  componentDidMount() {
    this.props.triggers.reloadDepartureList();
  }

  private handleAirportSwapping = () => {
    const destinationAirport: Location | undefined = this.props.airportSelectionData
      ? this.props.airportSelectionData.destinationAirport
      : undefined;
    const departureAirport: Location | undefined = this.props.airportSelectionData
      ? this.props.airportSelectionData.departureAirport
      : undefined;
    this.setState({
      isSelectionDialogOpen: false
    });

    this.props.onChangeAirportSelection({
      ...this.props.airportSelectionData,
      destinationAirport: departureAirport,
      departureAirport: destinationAirport
    });

    if (destinationAirport) {
      this.props.triggers.onDepartureAirportSelected(destinationAirport.Code);
    }
  };

  private selectDestinationAirport = async (airport: Location, dialogType: string) => {
    await this.setState({
      isSelectionDialogOpen: false
    });
    if (dialogType === this.DEPARTURE) {
      this.props.onChangeAirportSelection({
        ...this.props.airportSelectionData,
        departureAirport: airport,
        destinationAirport: undefined
      });
      this.props.triggers.onDepartureAirportSelected(airport.Code);
    } else if (dialogType === this.DESTINATION) {
      this.props.onChangeAirportSelection({
        ...this.props.airportSelectionData,
        destinationAirport: airport
      });
    }
  };

  private handleCardSelection = (type: string) => {
    this.setState({ isSelectionDialogOpen: true, dialogType: type });
  };

  private handleOnToggleClick = () => {
    const isOneWayTrip = this.props.airportSelectionData
      ? this.props.airportSelectionData.isOneWayTrip
      : false;

    this.props.onChangeAirportSelection({
      ...this.props.airportSelectionData,
      isOneWayTrip: !isOneWayTrip
    });
  };

  private handleSelectionDialogClose = () => {
    this.setState({
      isSelectionDialogOpen: false
    });
  };

  render() {
    const { isSelectionDialogOpen, dialogType } = this.state;
    const { airportSelectionData, inboundLocations, outboundLocations } = this.props;
    const isOneWayTrip = airportSelectionData ? airportSelectionData.isOneWayTrip : false;
    const locations = dialogType === this.DEPARTURE ? outboundLocations : inboundLocations;
    const destinationsEnabled =
      !!inboundLocations!.locations.data && inboundLocations!.locations.data.length >= 0;
    return (
      <>
        <BookingSectionTitle>Where do you want to go!!</BookingSectionTitle>
        <AirportCardManager
          departureAirport={
            airportSelectionData ? airportSelectionData.departureAirport : undefined
          }
          destinationAirport={
            airportSelectionData ? airportSelectionData.destinationAirport : undefined
          }
          clickAction={this.handleCardSelection}
          swapAction={() => this.handleAirportSwapping()}
          destinationsEnabled={destinationsEnabled}
        />

        <ToggleButton handleToggleClick={this.handleOnToggleClick} isRoundTrip={!isOneWayTrip} />

        {isSelectionDialogOpen && (
          <>
            <Hidden smDown>
              <AirportSelectionDialog
                airportList={locations}
                selectAirport={this.selectDestinationAirport}
                open={true}
                type={dialogType}
                handleDialogClose={this.handleSelectionDialogClose}
              />
            </Hidden>
            <Hidden mdUp>
              <AirportSelectionDialogMobile
                selectDestinationAirport={this.selectDestinationAirport}
                airports={locations}
                open={true}
                type={dialogType}
                handleClose={this.handleSelectionDialogClose}
              />
            </Hidden>
          </>
        )}
      </>
    );
  }
}

export default withObservableStream(
  airportSelectionObservable,
  triggers,
  { outboundLocations: { locations: { data: [] } }, inboundLocations: { locations: { data: [] } } }
  // @ts-ignore
)(withStyles({})(AirportSelection));
