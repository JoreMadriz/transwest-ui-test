import Enzyme, { shallow } from 'enzyme';
import React from 'react';

import { AirportSelection } from './airport-selection';
import AirportSelectionProps from './airport-selections.props';
import AirportSelectionState from './airport-selection.state';
import { getDefaultAirportSelectionData } from '../../utils/default-props.util';

type AirportSelectionType = Enzyme.ShallowWrapper<
  AirportSelectionProps,
  AirportSelectionState,
  AirportSelection
>;

describe('Airport selection test suite', () => {


  function getAirportSelectionWrapper(props: AirportSelectionProps): AirportSelectionType {
    return shallow(<AirportSelection {...props} />);
  }

  function getAirportSelectionDefaultProps(): AirportSelectionProps {
    return {
      classes: {},
      onChangeAirportSelection: jest.fn(),
      airportSelectionData: getDefaultAirportSelectionData(),
      triggers: {
        onDepartureAirportSelected: jest.fn(),
        reloadDepartureList: jest.fn()
      },
      inboundLocations: null,
      outboundLocations: null
    };
  }

  it('opens the selection dialog with the appropriate type when the card selection manager fires the event', async () => {
    const departureType = 'Departure';
    const airportSelection = getAirportSelectionWrapper(getAirportSelectionDefaultProps());
    await airportSelection
      .find('WithStyles(AirportCardManager)')
      .props()
      // @ts-ignore
      .clickAction(departureType);
    expect(airportSelection.state().dialogType).toEqual(departureType);
    expect(airportSelection.state().isSelectionDialogOpen).toBeTruthy();
  });

  it('changes the value of the one way trip value on the parent when the toggle button component fires the event', () => {
    const originalAirportProps = getAirportSelectionDefaultProps();
    const airportSelection = getAirportSelectionWrapper(originalAirportProps);
    const toggleButton = airportSelection.find('WithStyles(ToggleButton)');
    // @ts-ignore
    toggleButton.props().handleToggleClick();

    expect(
      // @ts-ignore
      originalAirportProps.onChangeAirportSelection.mock.calls[0][0].isOneWayTrip
    ).toBeFalsy();
  });

  it('sets the isDialogOption open to false when the selection airport dialog notifies it has been closed', async () => {
    const airportSelection = getAirportSelectionWrapper(getAirportSelectionDefaultProps());
    await airportSelection.setState({ isSelectionDialogOpen: true });
    await airportSelection
      .find('AirportSelectionDialog')
      .props()
      // @ts-ignore
      .handleDialogClose();
    expect(airportSelection.state().isSelectionDialogOpen).toBeFalsy();
  });

  it('swaps the airports via the parent trigger and closes the airport selection dialog', async () => {
    const originalAirportProps = getAirportSelectionDefaultProps();
    const airportSelection = getAirportSelectionWrapper(originalAirportProps);
    const airportCardManager = airportSelection.find('WithStyles(AirportCardManager)');
    // @ts-ignore
    airportCardManager.props().swapAction();
    expect(airportSelection.state().isSelectionDialogOpen).toBeFalsy();
    // @ts-ignore
    expect(originalAirportProps.onChangeAirportSelection.mock.calls[0][0].departureAirport).toEqual(
      // @ts-ignore
      originalAirportProps.airportSelectionData.destinationAirport
    );

    expect(
      // @ts-ignore
      originalAirportProps.onChangeAirportSelection.mock.calls[0][0].destinationAirport
    ).toEqual(
      // @ts-ignore
      originalAirportProps.airportSelectionData.departureAirport
    );
  });
});
