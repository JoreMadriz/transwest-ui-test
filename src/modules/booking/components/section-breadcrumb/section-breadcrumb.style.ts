import { MARGIN_5 } from '../../../../shared/theme/gutters';
import { PRIMARY_COLOR } from '../../../../shared/theme/colors';

export const style = {
  servicesContainer: {
    marginLeft: MARGIN_5
  },
  arrowServices: {
    color: PRIMARY_COLOR
  },
  textServices: {
    fontSize: '16px',
    fontWeight: 600
  },
  textBooking: {
    textAlign: 'left' as 'left',
    fontSize: '20px'
  }
};

export default style;
