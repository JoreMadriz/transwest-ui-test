import React from 'react';
import { Grid, Typography, withStyles } from '@material-ui/core';
import { KeyboardArrowLeft } from '@material-ui/icons';

import Step from '../../models/Step';
import style from './section-breadcrumb.style';

interface SectionBreadCrumbProps {
  children: any;
  handleStepChange: Function;
  isServices?: boolean;
  step?: Step;
  classes: any;
}

const SectionBreadCrumb = (props: SectionBreadCrumbProps) => {
  const { children, handleStepChange, isServices, step, classes } = props;
  const { servicesContainer, textServices, arrowServices, textBooking } = classes;
  return (
    <Grid
      style={{ cursor: 'pointer ' }}
      container
      alignItems={'center'}
      className={isServices ? servicesContainer : ''}
      onClick={() => {
        handleStepChange(step);
      }}
    >
      <Grid item>
        <KeyboardArrowLeft className={isServices ? arrowServices : ''} color="secondary" />
      </Grid>
      <Grid item>
        <Typography className={isServices ? textServices : textBooking}>{children}</Typography>
      </Grid>
    </Grid>
  );
};

export default withStyles(style)(SectionBreadCrumb);
