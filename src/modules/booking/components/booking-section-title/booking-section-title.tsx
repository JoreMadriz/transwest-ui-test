import React from 'react';
import { withStyles, Typography, Hidden } from '@material-ui/core';

import styles from './booking-section-title.style';

interface BookingSectionTitleProps {
  classes: any;
  children: any;
}

const BookingSectionTitle = (props: BookingSectionTitleProps) => {
  const { classes, children } = props;
  const { root } = classes;
  return (
    <Hidden smDown>
      <Typography className={root} align="left" variant="h3">
        {children}
      </Typography>
    </Hidden>
  );
};

export default withStyles(styles)(BookingSectionTitle);
