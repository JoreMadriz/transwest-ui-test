import Step from "../../models/Step";

export default interface ContinueButtonProps {
    classes:any,
    enabled:boolean,
    handleContinue:any,
    currentStep: Step,
    price: number

}