import {
  PRIMARY_COLOR,
  WHITE,
  BORDER_COLOR,
  LIGHT_PRIMARY_COLOR,
  BACKGROUND_COLOR1
} from '../../../../shared/theme/colors';
import { PADDING_3 } from '../../../../shared/theme/gutters';

const styles = (theme: any) => ({
  container: {
    position: 'fixed' as 'fixed',
    bottom: 0,
    left: 0,
    [theme.breakpoints.up("sm")]: {
        position: 'absolute' as 'absolute',
        margin: '0',
    },
    border: `1px solid ${BORDER_COLOR}`,
    width: '100%',
    backgroundColor: WHITE,
  },
  totalPrice: {
    padding: `${PADDING_3}px 0`,
    backgroundColor: BACKGROUND_COLOR1,
    color: PRIMARY_COLOR,
    textTransform: 'capitalize' as 'capitalize'
  },
  continueButton: {
    width: '100%',
    backgroundColor: PRIMARY_COLOR,
    color: WHITE,
    padding: `${PADDING_3}px 0`,
    textAlign: 'center' as 'center',
    textTransform: 'capitalize' as 'capitalize',
    fontSize: '22pt',
    fontWeight: 500,

    '&:disabled': {
      backgroundColor: WHITE,
      color: LIGHT_PRIMARY_COLOR
    },
    '&:hover': {
      backgroundColor: LIGHT_PRIMARY_COLOR
    }
  }
});

export default styles;
