import React from 'react';
import { Grid, Button, withStyles, Typography } from '@material-ui/core';
import styles from './continue-button.style';
import ButtonContinueProps from './continue-button.props';
import SectionSpacer from '../../../../shared/theme/components/section-spacer/section-spacer';
import { SpacerType } from '../../../../shared/theme/components/section-spacer/SpacerType';
import Step from '../../models/Step';
import { GridSize } from '@material-ui/core/Grid';
class ContinueButton extends React.Component<ButtonContinueProps> {
  private TWO_COLUMNS = 6;
  private SINGLE_COLUMN = 12;
  render() {
    const { classes, enabled, handleContinue, currentStep } = this.props;
    const { continueButton, container, totalPrice } = classes;
    const isSummary = currentStep > Step.DATE_SELECTION;
    const buttonSize = isSummary ? this.TWO_COLUMNS : this.SINGLE_COLUMN;
    const displayText = currentStep != Step.CHECKOUT ? 'Continue' : 'Pay Now';

    return (
      <Grid container justify="flex-start" direction="row" className={container}>
        {isSummary && (
          <Grid item xs={6} className={totalPrice}>
            <Typography variant="h4">Total</Typography>
            <SectionSpacer spacerType={SpacerType.TINY} />
            <Typography variant="h4">${this.props.price.toFixed(2)}</Typography>
          </Grid>
        )}

        <Grid container item xs={buttonSize as GridSize}>
          <Button
            disabled={enabled}
            onClick={handleContinue}
            className={continueButton}
            color="default"
          >
           {displayText}
          </Button>
        </Grid>
      </Grid>
    );
  }
}

export default withStyles(styles)(ContinueButton);
