import {
  Radio,
  withStyles,
  Grid,
  Typography,
  FormHelperText,
  FormControlLabel
} from '@material-ui/core';
import RadioButtonUncheckedIcon from '@material-ui/icons/RadioButtonUnchecked';
import RadioButtonCheckedIcon from '@material-ui/icons/RadioButtonChecked';
import React from 'react';

import BillingInfo from './components/billing-info/billing-info';
import BookingSectionTitle from '../booking-section-title/booking-section-title';
import CreditCardInfo from './components/credit-card-info/credit-card-info';
import CheckoutProps from './checkout.props';
import CheckoutState from './checkout.state';
import CustomDialog from '../../../../shared/components/custom-dialog/custom-dialog';
import { SpacerType } from '../../../../shared/theme/components/section-spacer/SpacerType';
import SectionBreadCrumb from '../section-breadcrumb/section-breadcrumb';
import SectionSpacer from '../../../../shared/theme/components/section-spacer/section-spacer';
import Steps from '../../models/Step';
import styles from './checkout.styles';
import terms from './components/terms-and-conditions';
class CheckoutContainer extends React.Component<CheckoutProps, CheckoutState> {
  constructor(props: CheckoutProps) {
    super(props);
    this.state = {
      isDialogOpen: false
    };
  }

  private handleDialogOpen = () => {
    this.setState({ isDialogOpen: true });
  };
  private handleDialogClose = () => {
    this.setState({
      isDialogOpen: false
    });
  };

  render() {
    const {
      checkoutInfo,
      onChangeCardInfo,
      onChangeBillingInfo,
      onChangeTerms,
      classes
    } = this.props;
    const { isDialogOpen } = this.state;
    const { dialogText, form, radio, text, termsConditions, errorBorder } = classes;
    const { billingInfo, cardInfo, isTermsAgreed } = checkoutInfo;
    const isTermsError = isTermsAgreed.error !== '';
    const errorClass = isTermsError ? errorBorder : '';
    return (
      <>
        {isDialogOpen && (
          <CustomDialog
            open={isDialogOpen}
            handleClose={this.handleDialogClose}
            title={'Terms and conditions'}
          >
            <p className={dialogText}>
              {terms()
                .split('\n')
                .map(function(item, key) {
                  return (
                    <span key={key}>
                      {item}
                      <br />
                    </span>
                  );
                })}
            </p>
          </CustomDialog>
        )}
        <SectionBreadCrumb
          step={Steps.PASSENGER_INFORMATION}
          handleStepChange={this.props.handleStepChange}
        >
          Back to Passenger Information
        </SectionBreadCrumb>
        <BookingSectionTitle>Checkout</BookingSectionTitle>
        <CreditCardInfo cardInfo={cardInfo} onChange={onChangeCardInfo} />
        <SectionSpacer spacerType={SpacerType.SMALL} />
        <BillingInfo billingInfo={billingInfo} onChange={onChangeBillingInfo} />
        <SectionSpacer spacerType={SpacerType.SMALL} />
        <Grid container className={`${form} ${termsConditions} ${errorClass}`} direction="column">
          {isTermsAgreed.error !== '' && (
            <Grid item>
              <FormHelperText error>{isTermsAgreed.error}</FormHelperText>
            </Grid>
          )}
          <Grid container direction="row">
            <FormControlLabel
              value="I accept the terms and conditions"
              checked={isTermsAgreed.isAgreed}
              className={radio}
              control={
                <Radio
                  className={radio}
                  onClick={onChangeTerms}
                  icon={<RadioButtonUncheckedIcon fontSize="small" />}
                  checkedIcon={<RadioButtonCheckedIcon fontSize="small" />}
                />
              }
              label={
                <Typography>
                  I accept the{' '}
                  <a className={text} onClick={this.handleDialogOpen}>
                    terms and conditions
                  </a>
                </Typography>
              }
            />
          </Grid>
        </Grid>
      </>
    );
  }
}

export default withStyles(styles)(CheckoutContainer);
