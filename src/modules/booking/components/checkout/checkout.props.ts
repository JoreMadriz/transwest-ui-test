import Checkout from '../../models/Checkout';

export default interface CheckoutProps {
  checkoutInfo: Checkout;
  classes: any;
  onChangeCardInfo: Function;
  onChangeBillingInfo: Function;
  onChangeTerms: any;
  handleStepChange: Function;
}
