import React from 'react';
import { shallow } from 'enzyme';
import { BillingInfo } from './billing-info'
import { BillingInfoProps } from './billing.info.props';
import { getDefaultBillingInfo } from '../../../../utils/checkout-default-props.util';
import styles from './billing-info.style'
describe('billing info test suite', () => {
    function getBillingInfoWrapper() {
        const defaultProps: BillingInfoProps = {
            billingInfo: getDefaultBillingInfo(),
            classes:styles,
            onChangeNumber: jest.fn(),
            onChangeString: jest.fn(),
        }
        return shallow(<BillingInfo {...defaultProps}/>)
    }

    it('should render billing info form', () => {
        const shallowBilling=  getBillingInfoWrapper();
        expect(shallowBilling).toMatchSnapshot()
    })
}) 