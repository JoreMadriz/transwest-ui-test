import { BORDER_COLOR, PRIMARY_COLOR } from '../../../../../../shared/theme/colors';
import { MARGIN_2, MARGIN_5, MARGIN_3 } from '../../../../../../shared/theme/gutters';

const fontSize = '12pt';
const styles = {
  form: {
    border: `1px solid ${BORDER_COLOR}`,
    boxSizing: 'border-box' as 'border-box',
    display: 'block' as 'block',
    textAlign: 'left' as 'left'
  },
  textField: {
    borderBottom: `1px solid ${BORDER_COLOR}`
  },
  doubleInput: {
    borderRight: `1px solid ${BORDER_COLOR}`
  },
  title: {
    color: `${PRIMARY_COLOR}`,
    textAlign: 'left' as 'left',
    marginLeft: `${MARGIN_3}px`,
    fontWeight: 600,
    fontSize: fontSize,
    marginBottom: `${MARGIN_2}px`
  },
  creditCardImg: {
    '& img': {
      height: '24px',
      width: 'auto' as 'auto',
      margin: 0,
      padding: `2px`,
      borderRadius: '5px',
      border: `2px solid ${BORDER_COLOR}`
    },
    marginLeft: `${MARGIN_5}px`,
    marginBottom: `${MARGIN_3}px`
  }
};
export default styles;
