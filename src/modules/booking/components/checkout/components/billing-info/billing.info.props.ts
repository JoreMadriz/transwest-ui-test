import BillingInfo from "../../models/billing-info";

export interface BillingInfoProps {
  classes: any;
  billingInfo:BillingInfo;
  onChange:Function;
}
