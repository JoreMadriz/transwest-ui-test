import { FormControl, Grid, Typography, withStyles } from '@material-ui/core';
import React from 'react';

import { BillingInfoProps } from './billing.info.props';
import CustomInput from '../custom-input/custom-input';
import styles from './billing-info.style';

export class BillingInfo extends React.Component<BillingInfoProps> {

  private handleOnChangeField = (fieldValue: string, fieldName: string) => {
    const {onChange, billingInfo} = this.props;
    onChange({
      ...billingInfo,
      [fieldName]: fieldValue
    });
  }
  render() {
    const { billingInfo, classes } = this.props;
    const { firstName, lastName, address, city, postalCode, country, errors } = billingInfo;
    const { form, doubleInput, textField, title } = classes;
    const onChangeFunction = (event: any) =>
    this.handleOnChangeField(event.target.value, event.target.name);
    return (
      <>
        <Typography className={title}>Card Billing Info</Typography>
        <FormControl className={form} fullWidth>
          <CustomInput
            className={textField}
            placeholder={'First Name'}
            name={'firstName'}
            onChangeHandler={onChangeFunction}
            value={firstName}
            errors={errors.firstName}
          />
          <CustomInput
            className={textField}
            placeholder="Last Name"
            name="lastName"
            onChangeHandler={onChangeFunction}
            value={lastName}
            errors={errors.lastName}
          />
          <CustomInput
            className={textField}
            placeholder="Address"
            name="address"
            onChangeHandler={onChangeFunction}
            value={address}
            errors={errors.address}
          />
          <Grid container>
            <Grid item xs={6}>
              <CustomInput
                className={`${textField} ${doubleInput}`}
                placeholder="City"
                name="city"
                onChangeHandler={onChangeFunction}
                value={city}
                errors={errors.city}
              />
            </Grid>
            <Grid item xs={6}>
              <CustomInput
                className={textField}
                placeholder="Postal Code"
                name="postalCode"
                onChangeHandler={onChangeFunction}
                value={postalCode}
                errors={errors.postalCode}
              />
            </Grid>
          </Grid>
          <CustomInput
            placeholder="Country"
            name="country"
            onChangeHandler={onChangeFunction}
            value={country}
            errors={errors.country}
          />
        </FormControl>
      </>
    );
  }
}

export default withStyles(styles)(BillingInfo);
