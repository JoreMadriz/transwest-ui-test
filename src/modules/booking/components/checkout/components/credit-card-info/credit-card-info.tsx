import React from 'react';
import { Typography, Grid, withStyles, FormControl } from '@material-ui/core';

import CreditCardInfoProps from './credit-card-info.props';
import CustomInput from '../custom-input/custom-input';

import getCardLogo from './card-logo-handler';
import styles from '../billing-info/billing-info.style';
import { shouldUpdateNumber, cleanValue } from '../../utils/numbers.util';


export class CreditCardInfo extends React.Component<CreditCardInfoProps> {
  private cardNumberRegExp = /(\d{4})(?=\d)/g;
  private dateRegExp = /(\d{2})(?=\d)/g;
  private DEFAULT_VALUE = 0;
  private toCardNumberFormat = (cardNumber: number) => {
    return cardNumber.toString().replace(this.cardNumberRegExp, '$1 ');
  };
  private toDateFormat = (date: number) => {
    return date.toString().replace(this.dateRegExp, '$1/');
  };
  private handleOnChangeNumber = (number: string, name: string, limit: number) => {
    const { cardInfo, onChange } = this.props;
    const cleanNum = cleanValue(number);
    const shouldUpdate = shouldUpdateNumber(cleanNum, limit);
    if (shouldUpdate) {
      onChange({
        ...cardInfo,
        [name]: cleanNum
      });
    } else if (number.length === this.DEFAULT_VALUE) {
      onChange({
        ...cardInfo,
        [name]: 0
      });
    }
  };
  
  render() {
    const { cardInfo, classes } = this.props;

    const { cardNumber, cvv, expirationDate, errors } = cardInfo;
    const { title, form, doubleInput, textField, creditCardImg } = classes;
    const numberTxt = cardNumber === this.DEFAULT_VALUE ? '' : this.toCardNumberFormat(cardNumber);
    const dateFormat =
      expirationDate === this.DEFAULT_VALUE ? '' : this.toDateFormat(expirationDate);
    const cvvTxt = cvv === this.DEFAULT_VALUE ? '' : cvv.toString();
    const cardLogo = getCardLogo(cardNumber.toString());
    return (
      <>
        <Grid container direction="row">
          <Grid>
            <Typography className={title} align="left">
              Card information
            </Typography>
          </Grid>
          <Grid>
            <div className={creditCardImg}>
              <img src={cardLogo} alt="cardLogo" />
            </div>
          </Grid>
        </Grid>

        <FormControl className={form}>
          <Grid container>
            <Grid item xs={12}>
              <CustomInput
                onChangeHandler={(event: any) =>
                  this.handleOnChangeNumber(event.target.value, event.target.name, 16)
                }
                name="cardNumber"
                className={textField}
                placeholder="Card Number"
                errors={errors.cardNumber}
                value={numberTxt}
              />
            </Grid>
            <Grid item container xs={12}>
              <Grid item xs={6}>
                <CustomInput
                  onChangeHandler={(event: any) =>
                    this.handleOnChangeNumber(event.target.value, event.target.name, 4)
                  }
                  name="expirationDate"
                  className={doubleInput}
                  placeholder="Expiration Date"
                  errors={errors.expirationDate}
                  value={dateFormat}
                />
              </Grid>
              <Grid item xs={6}>
                <CustomInput
                  onChangeHandler={(event: any) =>
                    this.handleOnChangeNumber(event.target.value, event.target.name, 4)
                  }
                  errors={errors.cvv}
                  name="cvv"
                  placeholder="CCV Number"
                  value={cvvTxt}
                />
              </Grid>
            </Grid>
          </Grid>
        </FormControl>
      </>
    );
  }
}

export default withStyles(styles)(CreditCardInfo);
