import React from 'react';
import { shallow, ShallowWrapper } from 'enzyme';

import { CreditCardInfo } from './credit-card-info';
import CreditCardInfoProps from './credit-card-info.props';
import style from '../billing-info/billing-info.style';
import { getDefaultCardInfo } from '../../../../utils/checkout-default-props.util';

describe('card info test suite', () => {
  function getDefaultProps(): CreditCardInfoProps {
    return {
      classes: style,
      onChange: jest.fn(),
      cardInfo: getDefaultCardInfo()
    };
  }
  function getCardInfoWrapper(props: CreditCardInfoProps): ShallowWrapper {
    return shallow(<CreditCardInfo {...props} />);
  }
  function getInputValue(props: CreditCardInfoProps, inputName: string) {
    const shallowCardInfo = getCardInfoWrapper(props);
    const cardNameInput = shallowCardInfo.findWhere((node: ShallowWrapper) => {
      return node.prop('name') === inputName;
    });
    return cardNameInput.prop('value');
  }

  it('should set the values of the inputs as empty', () => {
    const defaultProps = getDefaultProps();
    const shallowCardInfo = getCardInfoWrapper(defaultProps);
    const inputs = shallowCardInfo.find('WithStyles(CustomInput)');
    inputs.forEach((node: ShallowWrapper) => {
      expect(node.prop('value')).toEqual('');
    });
  });
  it('should format the card number', () => {
    const defaultProps = getDefaultProps();
    defaultProps.cardInfo.cardNumber = 1234123412341234;
    const expectedValue = '1234 1234 1234 1234';
    const cardNameInputValue = getInputValue(defaultProps, 'cardNumber');
    expect(cardNameInputValue).toEqual(expectedValue);
  });
  it('should format the expiration date', () => {
    const defaultProps = getDefaultProps();
    defaultProps.cardInfo.expirationDate = 1510;
    const expectedValue = '15/10';
    const expirationValue = getInputValue(defaultProps, 'expirationDate');
    expect(expirationValue).toEqual(expectedValue);
  });
  it('should format the cvv', () => {
    const defaultProps = getDefaultProps();
    defaultProps.cardInfo.cvv = 1510;
    const expectedValue = '1510';
    const cvvInputValue = getInputValue(defaultProps, 'cvv');
    expect(cvvInputValue).toEqual(expectedValue);
  });
});
