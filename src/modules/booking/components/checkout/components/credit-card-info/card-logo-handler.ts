import CardValidation from 'card-validator';

import americanLogo from '../../../../../../shared/assets/images/american-express.png';
import { CreditCardType } from './credit-card.enum';
import defaultCardLogo from '../../../../../../shared/assets/images/default.png';
import discoverLogo from '../../../../../../shared/assets/images/discover.png';
import jcbLogo from '../../../../../../shared/assets/images/jcb.png';
import masterCardLogo from '../../../../../../shared/assets/images/mastercard.png';
import maestroLogo from '../../../../../../shared/assets/images/maestro.png';
import visaLogo from '../../../../../../shared/assets/images/visa.png';

const getCardLogo = (cardNumber: string) => {
  const cardInfo = CardValidation.number(cardNumber).card;
  if (!cardInfo) return defaultCardLogo;
  const { type } = cardInfo;
  if (type === CreditCardType.AMERICAN) return americanLogo;
  if (type === CreditCardType.DISCOVER) return discoverLogo;
  if (type === CreditCardType.JCB) return jcbLogo;
  if (type === CreditCardType.MASTERCARD) return masterCardLogo;
  if (type === CreditCardType.MAESTRO) return maestroLogo;
  if (type === CreditCardType.VISA) return visaLogo;
  return defaultCardLogo;
};

export default getCardLogo;
