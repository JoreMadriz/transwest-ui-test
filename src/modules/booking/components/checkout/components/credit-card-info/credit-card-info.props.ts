import CardInfo from "../../models/card-info";

export default interface CreditCardInfoProps {
    classes: any;
    cardInfo: CardInfo;
    onChange: Function;
  }