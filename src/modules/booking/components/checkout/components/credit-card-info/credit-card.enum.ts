export enum CreditCardType {
    MASTERCARD = 'mastercard',
    VISA= 'visa',
    AMERICAN= 'american-express',
    DISCOVER='discover',
    MAESTRO= 'maestro',
    JCB='jcb'
}