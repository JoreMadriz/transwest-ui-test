import americanLogo from '../../../../../../shared/assets/images/american-express.png';
import defaultCardLogo from '../../../../../../shared/assets/images/default.png';
import discoverLogo from '../../../../../../shared/assets/images/discover.png';
import getCardLogo from './card-logo-handler';
import jcbLogo from '../../../../../../shared/assets/images/jcb.png';
import masterCardLogo from '../../../../../../shared/assets/images/mastercard.png';
import maestroLogo from '../../../../../../shared/assets/images/maestro.png';
import visaLogo from '../../../../../../shared/assets/images/visa.png';


describe('card logo test suite', () => {
  it('should return the default logo', () => {
    const cardLogo = getCardLogo('');
    expect(cardLogo).toEqual(defaultCardLogo);
  });
  it('should return visa logo', () => {
    const cardLogo = getCardLogo('4111');
    expect(cardLogo).toEqual(visaLogo);
  });
  it('should return mastercard logo', () => {
    const cardLogo = getCardLogo('5211');
    expect(cardLogo).toEqual(masterCardLogo);
  });
  it('should return discover logo', () => {
    const cardLogo = getCardLogo('6011');
    expect(cardLogo).toEqual(discoverLogo);
  });
  it('should return american express logo', () => {
    const cardLogo = getCardLogo('3759');
    expect(cardLogo).toEqual(americanLogo);
  });
  it('should return maestro logo', () => {
    const cardLogo = getCardLogo('3759');
    expect(cardLogo).toEqual(americanLogo);
  });
  it('should return american express logo', () => {
    const cardLogo = getCardLogo('67623');
    expect(cardLogo).toEqual(maestroLogo);
  });
  it('should return jcb logo', () => {
    const cardLogo = getCardLogo('35391');
    expect(cardLogo).toEqual(jcbLogo);
  });
});
