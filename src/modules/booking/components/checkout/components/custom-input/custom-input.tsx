import { TextField, withStyles } from '@material-ui/core';
import React from 'react';

import { CustomInputProps } from './custom-input.props';
import styles from './custom-input.style';

export class CustomInput extends React.Component<CustomInputProps> {
  private readonly IS_UNTOUCHED: string = 'isUntouched';
  render() {
    const {
      className,
      placeholder,
      name,
      onChangeHandler,
      value,
      classes,
      validateField,
      errors
    } = this.props;
    const { helperText, errorBorder } = classes;
    const InputProps = {
      disableUnderline: true,
      classes: { input: classes.input },
    };
    const errorString = errors && !errors.includes(this.IS_UNTOUCHED) ? errors.join(', ') : '';
    const errorClass = errorString !== '' ? errorBorder : '';

    return (
      <TextField
        fullWidth
        onChange={onChangeHandler}
        className={`${className} ${errorClass}`}
        InputProps={InputProps}
        placeholder={placeholder}
        error
        label={errorString}
        InputLabelProps={{
          error: true,
          classes: { root: helperText },
          shrink: true
        }}
        name={name}
        onBlur={validateField}
        value={value}
      />
    );
  }
}

export default withStyles(styles)(CustomInput);
