
import { PRIMARY_COLOR, ERROR_COLOR } from '../../../../../../shared/theme/colors';
import { PADDING_1, PADDING_2 } from '../../../../../../shared/theme/gutters';


const fontSize = '12pt';
const styles = {
  input: {
    display: 'block' as 'block',
    padding: `0 ${PADDING_2}px`,
    height: '55px',
    fontSize: fontSize,
    color: `${PRIMARY_COLOR}`,
    '&::placeholder': {
      color: `${PRIMARY_COLOR}`,
      opacity: 1,
      fontSize: fontSize
    },
    '& label': {
      color: `${PRIMARY_COLOR}`
    }
  },
  errorBorder: {
    border: `1px solid ${ERROR_COLOR} !important`
  },
  helperText: {
    padding: `${PADDING_1}px 0 0 ${PADDING_2}px`,
    margin: 0
  }
};
export default styles;
