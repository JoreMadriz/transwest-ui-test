export interface CustomInputProps {
  classes: any;
  className?: string;
  placeholder: string;
  name: string;
  onChangeHandler: any;
  value: string;
  validateField?: any;
  errors: string[];
}
