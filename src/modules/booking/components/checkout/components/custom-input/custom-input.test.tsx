import React from 'react';
import { shallow } from 'enzyme';

import { CustomInput } from './custom-input';
import { CustomInputProps } from './custom-input.props';

describe('custom input test suite', () => {
  function getCustomInputWrapper() {
    const defaultProps: CustomInputProps = {
      classes: '',
      onChangeHandler: jest.fn(),
      name: 'test',
      placeholder: 'test input',
      value: 'test'
    };
    return shallow(<CustomInput {...defaultProps} />);
  }
  it('should match snapshot', () => {
      const shallowInput  =  getCustomInputWrapper();
      expect(shallowInput).toMatchSnapshot()
  });
});
