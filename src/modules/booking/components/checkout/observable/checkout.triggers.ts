export default interface CheckoutTriggers{
    onChangeNumber:Function,
    onChangeString: Function,
    validCardNumber:Function
}