export const shouldUpdateNumber = (num:string, limit:number) => {
    const isNumberRegExp = /^[0-9]*$/;
    return isNumberRegExp.test(num) && num.length <= limit;
    
}

export const cleanValue = (propValue: string) => {
    const noWhiteSpaceRegExp = /\s/g;
    return propValue.replace(noWhiteSpaceRegExp, '').replace('/', '');
    
  };
  