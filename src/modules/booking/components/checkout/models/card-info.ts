export default interface CardInfo {
  cardNumber: number;
  expirationDate: number;
  cvv: number;
  errors: {
    cardNumber: string[],
    expirationDate: string[],
    cvv: string[]
  }
}
