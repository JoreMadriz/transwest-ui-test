export default interface BillingInfo {
  firstName: string;
  lastName: string;
  address: string;
  city: string;
  postalCode: string;
  country: string;
  errors: {
    firstName: string[];
    lastName: string[];
    address: string[];
    city: string[];
    postalCode: string[];
    country: string[];
  }
}
