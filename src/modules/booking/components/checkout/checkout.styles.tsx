import { BORDER_COLOR, PRIMARY_COLOR, ERROR_COLOR } from '../../../../shared/theme/colors';
import { PADDING_3 } from '../../../../shared/theme/gutters';

const styles = {
  form: {
    border: `1px solid ${BORDER_COLOR}`,
    boxSizing: 'border-box' as 'border-box',
    textAlign: 'left' as 'left'
  },
  termsConditions: {
    paddingLeft: PADDING_3
  },
  errorBorder: {
    border: `1px solid ${ERROR_COLOR}`,
  },
  radio: {
    color: PRIMARY_COLOR,
  },
  text: {
    textDecoration: 'underline' as 'underline',
    '&:hover': {
      color: BORDER_COLOR,
      cursor: 'pointer' as 'pointer'
    }
  },
  dialogText: {
    maxHeight: '400px'
  }
};
export default styles;
