import { PADDING_0, PADDING_3, PADDING_1 } from '../../../../shared/theme/gutters';
const styles = (theme:any) => ( {
  leftPane: {
    [theme.breakpoints.up('md')]: {
      padding: `${PADDING_0}px ${PADDING_3}px`,
      overflowY: 'auto' as 'auto',
      overflowX: 'hidden' as 'hidden',
      maxHeight: '85vh'
    },
    padding: `${PADDING_0}px ${PADDING_1}px`,

  }
});
export default styles;
