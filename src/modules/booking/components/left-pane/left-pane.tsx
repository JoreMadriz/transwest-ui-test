import React from 'react';
import { Grid, withStyles } from '@material-ui/core';
import styles from './left-pane.style';
interface ILeftPane {
  children: any;
  classes: any;
}

const LeftPane = (props: ILeftPane) => {
  const { classes, children } = props;
  const { leftPane } = classes;
  return (
    <Grid className={leftPane} item xs={12} md={6}>
      {children}
    </Grid>
  );
};

export default withStyles(styles)(LeftPane);
