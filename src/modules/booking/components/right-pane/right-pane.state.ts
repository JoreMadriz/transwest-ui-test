export default interface RightPaneState {
  firstStepSelected: boolean;
  secondStepSelected: boolean;
  thirdStepSelected: boolean;
  fourthStepSelected: boolean;
}
