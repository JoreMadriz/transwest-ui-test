import BookingData from '../../models/BookingData';

export default interface Props {
  bookingData: BookingData;
  classes: any;
  currentStep: number;
  enabled: boolean;
  handleBackToStartClick: any;
  handleContinue: any;
  handleStepMenuClick: Function;
  isOneWayTrip: boolean;
  price: number;
}
