import { ArrowForward } from '@material-ui/icons';
import { Grid, Typography, withStyles } from '@material-ui/core';
import React from 'react';

import FlightSummaryProps from './flight-summary.props';
import { formatDate, formatTime } from '../../../../../../../../shared/utils/dateUtils';
import styles from './flight-summary.style';

export class FlightSummary extends React.Component<FlightSummaryProps> {
  public render() {
    const { ticketData, classes } = this.props;
    const {
      departTime,
      arrivalTime,
      stopsCodes,
      duration,
      origin,
      destination,
      flightId,
      price,
      stops
    } = ticketData;
    const { arrowForwardIcon, bigText, stepContainer, text, bold, nowrap } = classes;
    const stopsText = stops == 1 ? 'stop' : 'stops';
    const stopCodes = stopsCodes.length > 0 ? `(${stopsCodes})` : '';
    const travelLength = formatTime(duration);
    const codeshareNo = '280';
    return (
      <Grid className={stepContainer}>
        <Grid container direction={'column'}>
          <Grid item container justify={'space-between'} direction={'row'} alignItems="center">
            <Grid item>
              <Typography className={bigText} align="left" color="textPrimary">
                {formatDate(departTime, 'ddd, MMM D')}
              </Typography>
            </Grid>
            <Grid item>
              <Typography className={bigText} align="right" color="textPrimary">
                <span className={bold}>{formatDate(departTime, 'hh:mm A')}</span> {origin}
                <ArrowForward className={arrowForwardIcon} />{' '}
                <span className={bold}>{formatDate(arrivalTime, 'hh:mm A')}</span> {destination}
              </Typography>
            </Grid>
          </Grid>

          <Grid item container justify={'space-between'} alignItems="center" className={nowrap}>
            <Grid item>
              <Typography className={text} color="textPrimary">
                Flight #{flightId} - Codeshare {codeshareNo} - Operated by TWA
              </Typography>
            </Grid>
            <Grid item>
              <Typography className={text} align="right" color="textPrimary" id="stopsText">
                {travelLength} - {stops} {stopsText} {stopCodes}
              </Typography>
            </Grid>
            <Grid item>
              <Typography className={bigText} color="textPrimary">
                ${price.toFixed(2)}
              </Typography>
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    );
  }
}

export default withStyles(styles)(FlightSummary);
