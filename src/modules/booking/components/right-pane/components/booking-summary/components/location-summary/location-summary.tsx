import { Button, Grid, Typography, withStyles } from '@material-ui/core';
import React from 'react';

import { AirportSelectionStep } from '../../../booking-step-pane/components/booking-step/components/airport-selection-step/airport-selection-step';
import formatDate from '../../../../../../../../shared/utils/dateUtils';
import LocationSummaryProps from './location-summary.props';
import styles from './location-summary.styles';

export class LocationSummary extends React.Component<LocationSummaryProps> {
  public render() {
    const { bookingData, handleBackToStartClick, classes } = this.props;
    const { backToStart, midText, stepContainer, text } = classes;
    const { outboundDate } = bookingData.dateSelection;
    const { inboundDate } = bookingData.dateSelection;

    return (
      <Grid className={`${stepContainer}`}>
        <Grid container>
          <Grid xs={10} item>
            <AirportSelectionStep
              classes={''}
              isSelected={false}
              airportSelectionData={bookingData.airportSelection}
            />
          </Grid>
          <Grid xs={2} item container justify="flex-end">
            <Button
              disabled={false}
              onClick={handleBackToStartClick}
              className={backToStart}
              color="default"
            >
              Back to search
            </Button>
          </Grid>
        </Grid>

        {outboundDate && inboundDate && (
          <Grid container item justify="space-between" xs={6}>
            <Grid item xs={2}>
              <Typography className={text} align="left" color="textPrimary">
                Outbound <span className={midText}>{formatDate(outboundDate, 'MMM D')}</span>
              </Typography>
            </Grid>
            <Grid item xs={2}>
              <Typography className={text} align="left" color="textPrimary">
                Inbound <span className={midText}>{formatDate(inboundDate, 'MMM D')}</span>
              </Typography>
            </Grid>
            <Grid item xs={2}>
              <Typography className={text} align="left" color="textPrimary">
                Passengers{' '}
                <span className={midText}>{bookingData.passengerSelection.totalPassengers}</span>
              </Typography>
            </Grid>
          </Grid>
        )}
      </Grid>
    );
  }
}
export default withStyles(styles)(LocationSummary);
