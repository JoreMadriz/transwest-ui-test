import React from 'react';
import { shallow } from 'enzyme';

import { getDefaultTicket } from '../../../../../../utils/default-props.util';
import { FlightSummary } from './flight-summary';
import FlightSummaryProps from './flight-summary.props';

function getBookingSummaryProps(): FlightSummaryProps {
  return {
    ticketData: getDefaultTicket(),
    classes: {}
  };
}

describe('Booking summary test suite', () => {
  function getBookingSummaryWrapper(props: FlightSummaryProps) {
    const component = shallow(<FlightSummary {...props} />);
    return component;
  }

  it('should display stops in plural when the stop number is above 1', () => {
    const summaryProps = getBookingSummaryProps();
    summaryProps.ticketData.stops = 2;
    const bookingSummary = getBookingSummaryWrapper(summaryProps);
    const stopsText = bookingSummary.find('#stopsText');
    expect(stopsText.props().children.includes('stops')).toBeTruthy();
  });
});
