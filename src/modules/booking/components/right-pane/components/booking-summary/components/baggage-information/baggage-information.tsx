import { withStyles, Typography } from '@material-ui/core';
import React from 'react';

import styles from './baggage-information.style';
import CustomDialog from '../../../../../../../../shared/components/custom-dialog/custom-dialog';
import Paragraph from '../../../../../../../../shared/theme/components/paragraph/paragraph';
import SectionSpacer from '../../../../../../../../shared/theme/components/section-spacer/section-spacer';
import { SpacerType } from '../../../../../../../../shared/theme/components/section-spacer/SpacerType';

interface BaggageInformationProps {
  classes: any;
}

interface BaggageInformationState {
  isDialogOpen: boolean;
}

class BaggageInformation extends React.Component<BaggageInformationProps, BaggageInformationState> {
  constructor(props: BaggageInformationProps) {
    super(props);

    this.state = {
      isDialogOpen: false
    };
  }

  private handleDialogClose = () => {
    this.setState({
      isDialogOpen: false
    });
  };
  private handleDialogOpen = () => {
    this.setState({
      isDialogOpen: true
    });
  };

  render() {
    const { classes } = this.props;
    const { isDialogOpen } = this.state;
    const { baggageContainer, baggageText } = classes;
    return (
      <>
        {isDialogOpen && (
          <CustomDialog open={isDialogOpen} handleClose={this.handleDialogClose} title={''}>
            <Paragraph paragraphFont={12} title="Baggage Policy:">
              You are allowed up to 75lbs of checked baggage per <br />
              passenger. Every pound over with the subject to an additional <br />
              charge. <br />
              Baggage above the normal accepted limit may not be shipped <br />
              depend upon space availability. Other items such as <br />
              live animals have specific charges. <br />
            </Paragraph>
            <SectionSpacer spacerType={SpacerType.TINY} />
            <Paragraph paragraphFont={12} title="Carry-On Baggage:">
              Due to the various sizes of our aircraft, safety regulations <br />
              require that all carry-on baggage conform to the specifications <br />
              listed below: <br />
              No more than two (2) pieces, when combined, not to exceed <br />
              20cm x 43cm x 38cm (8"x17"x15"). see sizing device at each <br />
              base check-in counter. Maximum combined weight for cabin <br />
              baggage is 9kg (20lbs). Purses 25cm x 30cm x 20cm <br />
              (10"x12"x8") or less are not counted as one of the two pieces.
            </Paragraph>
          </CustomDialog>
        )}
        <div className={baggageContainer}>
          <Typography
            className={baggageText}
            onClick={() => {
              this.handleDialogOpen();
            }}
          >
            Baggage Information
          </Typography>
        </div>
      </>
    );
  }
}

export default withStyles(styles)(BaggageInformation);
