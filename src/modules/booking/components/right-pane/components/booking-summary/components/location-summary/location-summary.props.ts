import BookingData from '../../../../../../models/BookingData';

interface LocationSummaryProps {
  classes: any;
  bookingData: BookingData;
  handleBackToStartClick: any;
}

export default LocationSummaryProps;
