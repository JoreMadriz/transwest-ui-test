import React from 'react';
import {
  ExpansionPanel,
  ExpansionPanelSummary,
  Typography,
  ExpansionPanelDetails,
  withStyles,
  Grid,
  Table,
  TableRow,
  TableCell,
  TableHead,
  TableBody,
  TableFooter
} from '@material-ui/core';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import styles from './fare-breakdown.style';
import FareBreakdownProps from './fare-breakdown.props';
import SectionSpacer from '../../../../../../../../shared/theme/components/section-spacer/section-spacer';
import { SpacerType } from '../../../../../../../../shared/theme/components/section-spacer/SpacerType';
import FareBreakdownState from './fare-breakdown.state';
import CustomDialog from '../../../../../../../../shared/components/custom-dialog/custom-dialog';
export class FareBreakDown extends React.Component<FareBreakdownProps, FareBreakdownState> {
  constructor(props: FareBreakdownProps) {
    super(props);
    this.state = {
      isDialogOpen: false
    };
  }

  private getFeesSum(fees: any[]) {
    return fees.reduce((accumulator: number, fee: any) => {
      return accumulator + fee.Value;
    }, 0);
  }

  private handleDialogClose = () => {
    this.setState({
      isDialogOpen: false
    });
  };

  private handleDialogOpen = () => {
    this.setState({
      isDialogOpen: true
    });
  };

  render() {
    const { classes, flightSelection, passengerData, price } = this.props;
    const { isDialogOpen } = this.state;
    const totalPassenger = passengerData.adultsQuantity + passengerData.childrenQuantity;
    const { inboundTicket, outboundTicket } = flightSelection;
    const subtotal = (inboundTicket.subTotal + outboundTicket.subTotal) * totalPassenger;
    const fee = 0 + this.getFeesSum(inboundTicket.fees) + this.getFeesSum(outboundTicket.fees);
    const total = price * totalPassenger;
    const {
      fareContainer,
      summary,
      bold,
      underline,
      table,
      tableContainer,
      tableSubTitle
    } = classes;
    return (
      <>
        {isDialogOpen && (
          <CustomDialog
            open={isDialogOpen}
            handleClose={this.handleDialogClose}
            title={'Total, Taxes and Fees'}
          >
            <div className={tableContainer}>
              <Table className={table}>
                <TableHead>
                  <TableRow>
                    <TableCell>Price Breakdown</TableCell>
                    <TableCell>Outbound</TableCell>
                    {inboundTicket.fees.length > 0 && <TableCell>Inbound</TableCell>}
                    <TableCell>Total</TableCell>
                  </TableRow>
                </TableHead>
                <SectionSpacer spacerType={SpacerType.TINY} />
                <TableBody>
                  <TableRow>
                    <Typography className={tableSubTitle}>Air Transportation Charges</Typography>
                  </TableRow>
                  <TableRow>
                    <TableCell>Base Fare</TableCell>
                    <TableCell>{`$${outboundTicket.subTotal}`}</TableCell>
                    {inboundTicket.fees.length > 0 && (
                      <TableCell>{`$${inboundTicket.subTotal}`}</TableCell>
                    )}
                    <TableCell>{`$${subtotal.toFixed(2)}`}</TableCell>
                  </TableRow>
                  <TableRow>
                    <Typography className={tableSubTitle}>Taxes, Fees and Charges</Typography>
                  </TableRow>
                  {outboundTicket.fees.map((fee: any, index: number) => {
                    return (
                      <TableRow key={fee.Name}>
                        <TableCell>{fee.Name}</TableCell>
                        <TableCell>{`$${fee.Value}`}</TableCell>
                        {inboundTicket.fees.length > 0 &&
                          (inboundTicket.fees[index] ? (
                            <TableCell>{`$${inboundTicket.fees[index].Value}`}</TableCell>
                          ) : (
                            <TableCell>$0</TableCell>
                          ))}
                        <TableCell>
                          {`$${fee.Value +
                            (inboundTicket.fees[index] ? inboundTicket.fees[index].Value : 0)}`}
                        </TableCell>
                      </TableRow>
                    );
                  })}
                </TableBody>
                <TableFooter>
                  <TableRow>
                    <TableCell>
                      <b>Total</b>
                    </TableCell>
                    <TableCell> </TableCell>
                    {inboundTicket.fees.length > 0 && <TableCell> </TableCell>}
                    <TableCell>{`$${total.toFixed(2)}`}</TableCell>
                  </TableRow>
                </TableFooter>
              </Table>
            </div>
          </CustomDialog>
        )}
        <ExpansionPanel className={fareContainer}>
          <ExpansionPanelSummary
            classes={{ content: summary, expanded: summary }}
            expandIcon={<ExpandMoreIcon />}
          >
            <Typography align="right" variant="h5">
              Fare/Fee Breakdown
            </Typography>
          </ExpansionPanelSummary>
          <ExpansionPanelDetails>
            <Grid container>
              <Grid container direction={'row'}>
                <Grid container item xs={9} justify="flex-start">
                  <Typography align="left" className={bold}>
                    {totalPassenger} Passenger
                  </Typography>{' '}
                </Grid>
                <Grid container item xs={3} justify="flex-end">
                  <Typography className={bold}>Total</Typography>
                </Grid>
              </Grid>
              <Grid container>
                <SectionSpacer spacerType={SpacerType.TINY} />
              </Grid>
              <Grid container direction={'row'}>
                <Grid container item xs={9} justify="flex-start">
                  <Typography
                    align="left"
                    onClick={() => {
                      this.handleDialogOpen();
                    }}
                    className={underline}
                  >
                    Air Transportation Charges
                  </Typography>
                </Grid>
                <Grid container item xs={3} justify="flex-end">
                  <Typography id="subtotal">{`${subtotal.toFixed(2)}$`}</Typography>
                </Grid>
              </Grid>

              <Grid container direction={'row'}>
                <Grid container item xs={9} justify="flex-start" className={underline}>
                  <Typography
                    align="left"
                    onClick={() => {
                      this.handleDialogOpen();
                    }}
                  >
                    Taxes, Fees and Charges
                  </Typography>{' '}
                </Grid>
                <Grid item container xs={3} justify="flex-end">
                  <Typography id="fee">{`${fee.toFixed(2)}$`}</Typography>
                </Grid>
              </Grid>

              <Grid container>
                <SectionSpacer spacerType={SpacerType.TINY} />
              </Grid>
              <Grid container direction={'row'}>
                <Grid container item xs={9} justify="flex-start">
                  <Typography align="left" className={bold}>
                    Total
                  </Typography>
                </Grid>
                <Grid container item xs={3} justify="flex-end">
                  <Typography className={bold} id="total">
                    {`${total.toFixed(2)}$`}
                  </Typography>
                </Grid>
              </Grid>
            </Grid>
          </ExpansionPanelDetails>
        </ExpansionPanel>
      </>
    );
  }
}

export default withStyles(styles)(FareBreakDown);
