import { MARGIN_2 } from '../../../../../../../../shared/theme/gutters';

const styles = {
  baggageContainer: {
    margin: `auto ${MARGIN_2}px`
  },
  baggageText: {
    textDecoration: 'underline' as 'underline',
    cursor: 'pointer' as 'pointer'
  }
};

export default styles;
