import FlightSelectionData from "../../../../../../models/FlightSelectionData";
import PassengerData from "../../../../../../models/PassengerData";

export default interface FareBreakdownProps {
    classes:any,
    flightSelection: FlightSelectionData,
    passengerData: PassengerData,
    price:number
}