import {
  BORDER_COLOR,
  LIGHT_PRIMARY_COLOR,
  WHITE
} from '../../../../../../../../shared/theme/colors';

const styles = (theme: any) => ({
  stepContainer: {
    borderBottom: `1px solid ${BORDER_COLOR}`,
    padding: '1.5em',
    width: '100%'
  },

  text: {
    fontSize: '12px',
    fontFamily: 'Helvetica' as 'Helvetica',
    [theme.breakpoints.down('md')]: {
      whiteSpace: 'normal' as 'normal'
    },
    whiteSpace: 'nowrap' as 'nowrap'
  },

  midText: {
    fontSize: '16px',
    whiteSpace: 'nowrap' as 'nowrap'
  },

  backToStart: {
    backgroundColor: BORDER_COLOR,
    color: WHITE,
    fontSize: '12px',
    fontFamily: 'Helvetica' as 'Helvetica',
    borderTop: `1px solid ${BORDER_COLOR}`,
    borderRadius: '0px',
    minWidth: '93px',
    maxHeight: '24px',
    paddingTop: '0px',
    paddingBottom: '0px',
    textTransform: 'none' as 'none',
    whiteSpace: 'nowrap' as 'nowrap',
    fontWeight: 500,
    '&:hover': {
      backgroundColor: LIGHT_PRIMARY_COLOR
    }
  }
});

export default styles;
