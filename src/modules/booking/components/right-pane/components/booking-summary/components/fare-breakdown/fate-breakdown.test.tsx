import React from 'react';
import FareBreakDown from './fare-breakdown';
import FareBreakdownProps from './fare-breakdown.props';
import { getDefaultFlightData } from '../../../../../../utils/default-props.util';
import { getDefaultPassengerData } from '../../../../../../utils/passenger-default-props.util';
import { ShallowWrapper, shallow } from 'enzyme';

describe('Fare breakdown test suite', () => {
    function getDefaultProps (): FareBreakdownProps {
        return {
            classes: {},
            flightSelection: getDefaultFlightData(),
            passengerData: getDefaultPassengerData(),
            price: 0
        }
    }

    function getShallowFareBreakDown (props: FareBreakdownProps): ShallowWrapper {
        return shallow(<FareBreakDown {...props}/>).dive();
    }
    function findElementChildren (shallowFare: ShallowWrapper, id:string) {
       return shallowFare.findWhere((node: ShallowWrapper) =>  {
            return node.prop('id') === id;
        }).prop('children')
    }

    it('should calculate the subtotal to pay', () => {
        const defaultProps = getDefaultProps();
        defaultProps.flightSelection.inboundTicket.subTotal = 10;
        defaultProps.flightSelection.outboundTicket.subTotal = 20
        defaultProps.passengerData.adultsQuantity = 1;
        defaultProps.passengerData.childrenQuantity = 2;
        const shallowFare =  getShallowFareBreakDown(defaultProps);
        const expected =  (10+20)*3;
        const subTotal = findElementChildren(shallowFare, 'subtotal');
        const result = parseFloat(subTotal).toFixed(2);
        expect(result).toEqual(expected.toFixed(2));
    })
    it('should calculate the fees to pay', () => {
        const defaultProps = getDefaultProps();
        defaultProps.flightSelection.inboundTicket.fees = 10;
        defaultProps.flightSelection.outboundTicket.fees = 20
        defaultProps.passengerData.adultsQuantity = 1;;
        const shallowFare =  getShallowFareBreakDown(defaultProps);
        const expected =  (10+20)*1;
        const fee = findElementChildren(shallowFare, 'fee');
        const result = parseFloat(fee).toFixed(2);
        expect(result).toEqual(expected.toFixed(2));
    })
    it('should calculate the total to pay', () => {
        const defaultProps = getDefaultProps();
        defaultProps.price = 1000;
        defaultProps.passengerData.adultsQuantity = 1;;
        const shallowFare =  getShallowFareBreakDown(defaultProps);
        const expected =  1000;
        const total = findElementChildren(shallowFare, 'total');
        const result = parseFloat(total).toFixed(2);
        expect(result).toEqual(expected.toFixed(2));
    })
})