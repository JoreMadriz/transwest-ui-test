import Ticket from '../../../../../flight-selection/components/ticket-selection/models/ticket';

interface FlightSummaryProps {
  classes: any;
  ticketData: Ticket;
}

export default FlightSummaryProps;
