import {
  BACKGROUND_COLOR1,
  BORDER_COLOR,
  PRIMARY_COLOR
} from '../../../../../../../../shared/theme/colors';
import { MARGIN_1 } from '../../../../../../../../shared/theme/gutters';

const styles = (theme: any) => ({
  stepContainer: {
    borderBottom: `1px solid ${BORDER_COLOR}`,
    padding: '1.5em',
    width: '100%'
  },
  nowrap: {
    flexWrap: 'nowrap' as 'nowrap'
  },

  infoIcon: {
    fontSize: '14px',
    color: ` ${BORDER_COLOR}`
  },

  text: {
    fontSize: '12px',
    fontFamily: 'Helvetica' as 'Helvetica',
    textAlign: 'left' as 'left',
    flexWrap: 'nowrap' as 'nowrap'
  },

  bigText: {
    fontSize: '20px',
    fontFamily: 'Helvetica' as 'Helvetica',
    whiteSpace: 'nowrap' as 'nowrap',
    display: '-webkit-box' as '-webkit-box'
  },

  bold: {
    fontWeight: 600
  },

  arrowForwardIcon: {
    fontSize: '24px',
    color: ` ${PRIMARY_COLOR}`,
    backgroundColor: ` ${BACKGROUND_COLOR1}`,
    borderRadius: '15px',
    margin: `0px ${MARGIN_1}px`,
    position: 'relative' as 'relative',
    top: '6px'
  }
});

export default styles;
