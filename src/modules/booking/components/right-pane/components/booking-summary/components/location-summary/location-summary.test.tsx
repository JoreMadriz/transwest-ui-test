import React from 'react';
import { shallow, ShallowWrapper } from 'enzyme';

import {
  getDefaultAirportData,
  getDefaultFlightData,
  getCustomDatesInfo
} from '../../../../../../utils/default-props.util';
import { getDefaultPassengerData } from '../../../../../../utils/passenger-default-props.util';
import { LocationSummary } from './location-summary';
import LocationSummaryProps from './location-summary.props';


const mockFunction = jest.fn();

function getBookingSummaryProps(outDate: boolean, inDate: boolean): LocationSummaryProps {
  return {
    bookingData: {
      airportSelection: getDefaultAirportData(),
      dateSelection: getCustomDatesInfo(outDate, inDate),
      flightSelection: getDefaultFlightData(),
      passengerSelection: getDefaultPassengerData()
    },
    handleBackToStartClick: mockFunction,
    classes: {}
  };
}

describe('Booking summary test suite', () => {
  function getBookingSummaryWrapper(props: LocationSummaryProps) {
    const component = shallow(<LocationSummary {...props} />);
    return component;
  }

  function verifyComponents(elements: ShallowWrapper, airportQty: number, typhographyQty: number) {
    expect(elements.find('AirportSelectionStep').length).toBe(airportQty);
    expect(elements.find('WithStyles(Button)').length).toBe(1);
    expect(elements.find('WithStyles(Typography)').length).toBe(typhographyQty);
  }

  it('should not render the flight dates component since there is no data available', () => {
    const elements = getBookingSummaryWrapper(getBookingSummaryProps(false, false));
    verifyComponents(elements, 1, 0);
  });

  it('should not render the component since there is no inbound data available', () => {
    const elements = getBookingSummaryWrapper(getBookingSummaryProps(true, false));
    verifyComponents(elements, 1, 0);
  });

  it('should render the LocationSummary component since data is available', () => {
    const elements = getBookingSummaryWrapper(getBookingSummaryProps(true, true));
    verifyComponents(elements, 1, 3);
  });

  it('should receive the click event', () => {
    const elements = getBookingSummaryWrapper(getBookingSummaryProps(true, true));
    elements.find('WithStyles(Button)').simulate('click');
    expect(mockFunction.mock.calls.length).toEqual(1);
  });
});
