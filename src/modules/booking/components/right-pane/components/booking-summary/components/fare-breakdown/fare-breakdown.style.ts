import { MARGIN_2 } from '../../../../../../../../shared/theme/gutters';
import { PRIMARY_COLOR } from '../../../../../../../../shared/theme/colors';

const styles = {
  tableContainer: {
    maxHeight: '400px'
  },
  tableSubTitle: {
    fontWeight: 600,
    fontSize: '12x',
    padding: '0 24px'
  },
  table: {
    '& tr': {
      height: '25px'
    },
    '& th': {
      borderBottom: 'none' as 'none'
    },
    '& td': {
      borderBottom: 'none' as 'none'
    }
  },
  fareContainer: {
    border: 0,
    boxShadow: 'none'
  },
  summary: {
    margin: `${MARGIN_2}px  0 0 0`
  },
  bold: {
    fontWeight: 'bold' as 'bold'
  },
  underline: {
    textDecoration: 'underline' as 'underline',
    textDecorationColor: PRIMARY_COLOR
  }
};

export default styles;
