import BookingData from '../../../../models/BookingData';

interface BookingSummaryProps {
  bookingData: BookingData;
  handleBackToStartClick: any;
  price:number;
}

export default BookingSummaryProps;
