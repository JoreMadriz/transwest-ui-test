import { Grid } from '@material-ui/core';
import React from 'react';

import BaggageInformation from './components/baggage-information/baggage-information';
import BookingSummaryProps from './booking-summary.props';
import FlightSummary from './components/flight-summary/flight-summary';
import LocationSummary from './components/location-summary/location-summary';
import FareBreakdown from './components/fare-breakdown/fare-breakdown';

export class BookingSummary extends React.Component<BookingSummaryProps> {
  public render() {
    const { bookingData, handleBackToStartClick, price } = this.props;
    const { flightSelection, passengerSelection } = bookingData;
    const showOutbound = bookingData.flightSelection.outboundTicket.flightId != 0;
    const showInbound = bookingData.flightSelection.inboundTicket.flightId != 0;
    return (
      <Grid container={true} item={true}>
        <LocationSummary
          bookingData={bookingData}
          handleBackToStartClick={handleBackToStartClick}
        />
        {showOutbound && <FlightSummary ticketData={bookingData.flightSelection.outboundTicket} />}
        {showOutbound && showInbound && (
          <FlightSummary ticketData={bookingData.flightSelection.inboundTicket} />
        )}
        <Grid container>
          <Grid item xl={4} lg={5} md={5} xs={12} container justify="flex-start">
            <BaggageInformation />
          </Grid>
          <Grid item xl={4} lg={2} md={1} xs={12} container justify="flex-end" />
          <Grid item xl={4} lg={5} md={6} xs={12} container justify="flex-end">
            <FareBreakdown
              flightSelection={flightSelection}
              passengerData={passengerSelection}
              price={price}
            />
          </Grid>
        </Grid>
      </Grid>
    );
  }
}

export default BookingSummary;
