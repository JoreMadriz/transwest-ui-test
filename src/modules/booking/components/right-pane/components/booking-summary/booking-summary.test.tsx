import React from 'react';
import { shallow, ShallowWrapper } from 'enzyme';

import { BookingSummary } from './booking-summary';
import BookingSummaryProps from './booking-summary.props';
import {
  getDefaultAirportData,
  getDefaultBookingDate,
  getCustomTicket,
} from '../../../../utils/default-props.util';
import { getDefaultPassengerData } from '../../../../utils/passenger-default-props.util';

function getBookingSummaryProps(
  outboundFlightId: number,
  inboundFlightId: number
): BookingSummaryProps {
  return {
    bookingData: {
      airportSelection: getDefaultAirportData(),
      dateSelection: getDefaultBookingDate(),
      flightSelection: {
        inboundTicket: getCustomTicket(inboundFlightId),
        outboundTicket: getCustomTicket(outboundFlightId)
      },
      passengerSelection: getDefaultPassengerData()
    },
    handleBackToStartClick: jest.fn()
  };
}

describe('Booking summary test suite', () => {
  function getBookingSummaryWrapper(props: BookingSummaryProps) {
    return shallow(<BookingSummary {...props} />);
  }
  function verifyComponents(
    elements: ShallowWrapper,
    locationSummaryQty: number,
    flightSummaryQty: number
  ) {
    expect(elements.find('WithStyles(LocationSummary)').length).toBe(locationSummaryQty);
    expect(elements.find('WithStyles(FlightSummary)').length).toBe(flightSummaryQty);
  }

  it('should not render the FlightSummary component since there is no data available', () => {
    const elements = getBookingSummaryWrapper(getBookingSummaryProps(0, 0));
    verifyComponents(elements, 1, 0);
  });

  it('should render one FlightSummary component since there is no inbound data available', () => {
    const elements = getBookingSummaryWrapper(getBookingSummaryProps(10, 0));
    verifyComponents(elements, 1, 1);
  });

  it('should render the LocationSummary component since data is available', () => {
    const elements = getBookingSummaryWrapper(getBookingSummaryProps(1, 1));
    verifyComponents(elements, 1, 2);
  });
});
