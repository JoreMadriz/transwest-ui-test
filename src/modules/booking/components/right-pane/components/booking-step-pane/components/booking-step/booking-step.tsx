import CheckCircleOutline from '@material-ui/icons/CheckCircleOutline';
import { Grid, Typography, withStyles } from '@material-ui/core';
import React from 'react';

import BookingStepProps from './booking-step.props';
import styles from './booking-step.style';

class BookingStep extends React.Component<BookingStepProps> {
  private handleOnClick = () => {
    this.props.handleStepMenuClick(this.props.stepValue);
  };
  render() {
    const { classes, isCompleted, title, isSelected, children } = this.props;
    const {
      checkIcon,
      selectedCard,
      selectedCardText,
      stepContainer,
      swapButtonParent,
      swapButton
    } = classes;
    const isCardSelected = isSelected ? selectedCard : '';
    const isTextSelected = isSelected ? selectedCardText : '';

    return (
      <Grid
        onClick={this.handleOnClick}
        className={swapButtonParent}
        container={true}
        direction="row"
        justify="flex-start"
      >
        <Grid container={true} className={` ${isCardSelected} ${stepContainer} `}>
          <Grid direction="column" container={true} item={true} xs={10}>
            <Typography
              className={`${isTextSelected}`}
              variant="inherit"
              align="left"
              color="textPrimary"
            >
              {title}
            </Typography>
            {children}
          </Grid>

          {isCompleted && (
            <Grid className={swapButton} container={true} item={true} xs={2}>
              <CheckCircleOutline className={`${isTextSelected} ${checkIcon}`} />
            </Grid>
          )}
        </Grid>
      </Grid>
    );
  }
}

export default withStyles(styles)(BookingStep);
