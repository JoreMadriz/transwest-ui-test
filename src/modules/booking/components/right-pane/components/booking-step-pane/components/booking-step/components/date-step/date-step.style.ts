import { WHITE } from '../../../../../../../../../../shared/theme/colors';

const styles = {
  container: {
    paddingTop: '.5em'
  },
  datesPadding: {
    paddingLeft: '1.5em'
  },
  selectedCardText: {
    color: `${WHITE}`
  }
};
export default styles;
