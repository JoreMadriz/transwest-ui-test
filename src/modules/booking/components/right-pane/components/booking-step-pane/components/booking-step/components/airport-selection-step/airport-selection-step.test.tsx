import React from 'react';
import Enzyme, { shallow } from 'enzyme';

import { AirportSelectionStep } from './airport-selection-step';
import AirportSelectionStepProps from './airport-selection-step.props';

type AirportSelectionStepType = Enzyme.ShallowWrapper<
  AirportSelectionStepProps,
  {},
  AirportSelectionStep
>;

function getDefaultAirportSelectionStepProps(): AirportSelectionStepProps {
  return {
    isSelected: true,
    classes: {},
    airportSelectionData: {
      departureAirport: {
        Address: {
          Address: 'test',
          City: 'test',
          Country: 1,
          PostalCode: '11',
          Province: '11'
        },
        Code: '1',
        Id: 1,
        Name: 'test 1',
        WayOrder: 2
      },
      destinationAirport: {
        Address: {
          Address: 'test',
          City: 'test',
          Country: 1,
          PostalCode: '11',
          Province: '11'
        },
        Code: '2',
        Id: 2,
        Name: 'test 2',
        WayOrder: 2
      },
      isOneWayTrip: true,
      promoCode: ''
    }
  };
}

function getAirportSelectionStepWithoutDataProps(): AirportSelectionStepProps {
  return {
    isSelected: true,
    classes: {},
    airportSelectionData: {
      departureAirport: undefined,
      destinationAirport: undefined,
      isOneWayTrip: true,
      promoCode: ''
    }
  };
}

describe('Airport Selection Step test suite', () => {
  function getAirportSelectionStepWrapper(
    props: AirportSelectionStepProps
  ): AirportSelectionStepType {
    return shallow(<AirportSelectionStep {...props} />);
  }

  it('should render the LocationStep component since data is available', () => {
    const schedule = getAirportSelectionStepWrapper(getDefaultAirportSelectionStepProps());
    expect(schedule.find('WithStyles(LocationStep)').length).toBe(1);
    expect(schedule.find('WithStyles(Typography)').length).toBe(0);
  });

  it('should render the Typography component since there is no data available', () => {
    const schedule = getAirportSelectionStepWrapper(getAirportSelectionStepWithoutDataProps());
    expect(schedule.find('WithStyles(Typography)').length).toBe(1);
    expect(schedule.find('WithStyles(LocationStep)').length).toBe(0);
  });
});
