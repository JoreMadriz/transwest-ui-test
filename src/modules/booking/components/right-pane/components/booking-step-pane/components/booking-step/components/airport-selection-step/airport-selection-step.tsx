import React from 'react';
import { Grid, Typography } from '@material-ui/core/';
import { withStyles } from '@material-ui/core/styles';
import styles from './airport-selection-step.style';
import IAirportSelectionStepProps from './airport-selection-step.props';
import LocationStep from './place-step/location-step';

export class AirportSelectionStep extends React.Component<IAirportSelectionStepProps> {
  public render() {
    const { airportSelectionData, classes, isSelected } = this.props;
    const { container, selectedCardText } = classes;
    const SELECT_AIRPORT_DEFAULT_TEXT: string = 'Select Airports';
    const isDataAvailable =
      airportSelectionData!.departureAirport && airportSelectionData!.destinationAirport;
    return (
      <Grid className={container} container={true}>
        <Grid item={true} xs={9}>
          {(isDataAvailable && (
            <LocationStep
              destinationAirportName={airportSelectionData!.destinationAirport!.Name}
              departureAirportName={airportSelectionData!.departureAirport!.Name}
              isSelected={isSelected}
            />
          )) || (
            <Typography
              className={isSelected ? selectedCardText : ''}
              variant="h4"
              align="left"
              color="textPrimary"
            >
              {SELECT_AIRPORT_DEFAULT_TEXT}
            </Typography>
          )}
        </Grid>
      </Grid>
    );
  }
}
export default withStyles(styles)(AirportSelectionStep);
