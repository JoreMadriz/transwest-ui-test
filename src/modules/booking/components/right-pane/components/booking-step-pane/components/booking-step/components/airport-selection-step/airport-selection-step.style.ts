import { WHITE } from '../../../../../../../../../../shared/theme/colors';

const styles = {
  container: {
    paddingTop: '1em'
  },
  placePadding: {
    paddingLeft: '2em',
    paddingRight: '2en'
  },
  selectedCardText: {
    color: `${WHITE}`
  }
};

export default styles;
