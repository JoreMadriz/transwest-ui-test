import { BORDER_COLOR, PRIMARY_COLOR, WHITE } from '../../../../../../../../shared/theme/colors';

const styles = {
  stepContainer: {
    borderBottom: `1px solid ${BORDER_COLOR}`,
    padding: '1.5em'
  },

  checkIcon: {
    fontSize: '50px',
    color: ` ${PRIMARY_COLOR}`
  },
  swapButtonParent: {
    position: 'relative' as 'relative'
  },
  swapButton: {
    position: 'absolute' as 'absolute',
    left: '85%',
    top: '1.5em'
  },
  selectedCard: {
    background: `${PRIMARY_COLOR}`
  },
  selectedCardText: {
    color: `${WHITE}`
  }
};

export default styles;
