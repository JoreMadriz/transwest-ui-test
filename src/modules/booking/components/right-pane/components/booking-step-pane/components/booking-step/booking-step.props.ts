interface BookingStepProps {
  classes: any;
  handleStepMenuClick: Function;
  isCompleted: boolean;
  isSelected: boolean;
  stepValue: number;
  title: string;
}

export default BookingStepProps;
