interface IDateStepProps {
  date: Date | null;
  classes: any;
  isSelected: boolean;
  defaultMessage: string;
}

export default IDateStepProps;
