export default interface ILocationStepProps {
  departureAirportName: any;
  destinationAirportName: any;
  isSelected: boolean;
  classes: any;
}
