import React from 'react';
import { Grid, Typography } from '@material-ui/core/';
import { withStyles } from '@material-ui/core/styles';
import moment from 'moment';
import styles from './date-step.style';
import IDateStepProps from './date-step.props';

const DateStep = (props: IDateStepProps) => {
  const { classes, date, defaultMessage, isSelected } = props;
  const { selectedCardText } = classes;

  const selectedDate = date ? moment(date) : moment();
  return (
    <Grid className={classes.container} container={true} direction="row">
      {date ? (
        <>
          <Grid item={true} xs={2}>
            <Typography
              className={isSelected ? selectedCardText : ''}
              variant="h3"
              align="left"
              color="textPrimary"
            >
              {selectedDate.format('D')}
            </Typography>
          </Grid>
          <Grid
            container={true}
            className={classes.datesPadding}
            item={true}
            direction="column"
            justify="flex-start"
            xs={10}
          >
            <Typography
              className={isSelected ? selectedCardText : ''}
              variant="inherit"
              align="left"
              color="textPrimary"
            >
              {selectedDate.format('MMMM')}
            </Typography>
            <Typography
              className={isSelected ? selectedCardText : ''}
              variant="inherit"
              align="left"
              color="textPrimary"
            >
              {selectedDate.format('YYYY')}
            </Typography>
          </Grid>
        </>
      ) : (
        <Typography
          className={isSelected ? selectedCardText : ''}
          variant="h4"
          align="left"
          color="textPrimary"
        >
          {defaultMessage}
        </Typography>
      )}
    </Grid>
  );
};

export default withStyles(styles)(DateStep);
