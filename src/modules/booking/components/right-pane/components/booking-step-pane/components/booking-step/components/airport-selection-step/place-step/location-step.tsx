import React from 'react';
import { Typography } from '@material-ui/core/';
import CompareArrows from '@material-ui/icons/CompareArrows';
import { withStyles } from '@material-ui/core/styles';
import styles from '../airport-selection-step.style';
import ILocationStepProps from './location-step.props';

const LocationStep = (props: ILocationStepProps) => {
  const { classes, departureAirportName, destinationAirportName, isSelected } = props;
  const { selectedCardText } = classes;

  return (
    <Typography
      className={isSelected ? selectedCardText : ''}
      variant="h5"
      align="left"
      color="textPrimary"
    >
      {departureAirportName + ' '}
      <CompareArrows />
      {' ' + destinationAirportName}
    </Typography>
  );
};

export default withStyles(styles)(LocationStep);
