import React from 'react';
import { shallow } from 'enzyme';

import LocationStep from './location-step';

describe('Airport Card Manager test suite', () => {
  function getLocationStepWrapper() {
    return shallow(
      <LocationStep
        departureAirportName={'Test1'}
        destinationAirportName={'Test2'}
        isSelected={true}
      />
    );
  }

  it('should render the component structure properly', () => {
    const locationStep = getLocationStepWrapper();
    expect(locationStep).toMatchSnapshot();
  });
});
