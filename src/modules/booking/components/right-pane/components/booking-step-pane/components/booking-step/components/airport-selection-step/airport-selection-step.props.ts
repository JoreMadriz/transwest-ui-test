import AirportSelectionData from '../../../../../../../airport-selection/models/airport-selection-data';

interface IAirportSelectionStepProps {
  classes: any;
  isSelected: boolean;
  airportSelectionData: AirportSelectionData | undefined;
}

export default IAirportSelectionStepProps;
