import React from 'react';
import { Typography, withStyles } from '@material-ui/core';

import AirportSelectionStep from './components/booking-step/components/airport-selection-step/airport-selection-step';
import BookingStep from './components/booking-step/booking-step';
import BookingStepsPaneProps from './booking-steps-pane.props';
import DateStep from './components/booking-step/components/date-step/date-step';
import PassengerData from '../../../../models/PassengerData';
import Step from '../../../../models/Step';
import styles from './booking-steps-pane.style';

class BookingStepsPane extends React.Component<BookingStepsPaneProps> {
  private readonly INBOUND: string = 'Inbound';
  private readonly OUTBOUND: string = 'Outbound';
  private readonly PASSENGERS: string = 'Passengers';
  private readonly ROUND_TRIP: string = 'Round Trip';

  private buildPassengersValue = (passengerData: PassengerData): string => {
    let value: string = '';

    if (passengerData.adultsQuantity >= 1) {
      value += this.buildValue(passengerData.adultsQuantity, ' Adult', ' Adults');
    }

    value += this.conditionCheck(passengerData.teensQuantity, 'Teen', 'Teens');
    value += this.conditionCheck(passengerData.childrenQuantity, 'Child', 'Children');
    value += this.conditionCheck(passengerData.babiesQuantity, ' Infant', ' Infant');

    return value;
  };

  private buildValue = (passengerQuantity: number, singular: string, plural: string): string => {
    return `  ${passengerQuantity} ${passengerQuantity > 1 ? plural : singular}`;
  };

  private conditionCheck(passengerQuantity: number, singular: string, plural: string): string {
    if (passengerQuantity > 0) {
      return ` , ${this.buildValue(passengerQuantity, singular, plural)} `;
    }
    return '';
  }

  render() {
    const { classes, currentStep, handleStepMenuClick, isOneWayTrip, bookingData } = this.props;
    const { outboundDate } = bookingData.dateSelection || undefined;
    const { inboundDate } = bookingData.dateSelection || undefined;
    const isOutboundSelected = currentStep === Step.DATE_SELECTION && outboundDate === null;
    const isInboundSelected = currentStep === Step.DATE_SELECTION && inboundDate !== null;

    return (
      <>
        <BookingStep
          isSelected={currentStep === Step.AIRPORT_SELECTION}
          isCompleted={currentStep > Step.AIRPORT_SELECTION}
          title={this.ROUND_TRIP}
          stepValue={Step.AIRPORT_SELECTION}
          handleStepMenuClick={handleStepMenuClick}
        >
          <AirportSelectionStep
            isSelected={currentStep === Step.AIRPORT_SELECTION}
            airportSelectionData={bookingData.airportSelection}
          />
        </BookingStep>
        <BookingStep
          isSelected={currentStep === Step.PASSENGERS_SELECTION}
          isCompleted={currentStep > Step.PASSENGERS_SELECTION}
          title={this.PASSENGERS}
          stepValue={Step.PASSENGERS_SELECTION}
          handleStepMenuClick={handleStepMenuClick}
        >
          <Typography
            className={currentStep === Step.PASSENGERS_SELECTION ? classes.selectedText : ''}
            variant="h6"
            align="left"
            color="textPrimary"
          >
            {this.buildPassengersValue(bookingData.passengerSelection)}
          </Typography>
        </BookingStep>

        <BookingStep
          isSelected={isOutboundSelected}
          isCompleted={outboundDate !== null}
          title={this.OUTBOUND}
          stepValue={Step.DATE_SELECTION}
          handleStepMenuClick={handleStepMenuClick}
        >
          <DateStep
            isSelected={isOutboundSelected}
            date={outboundDate}
            defaultMessage="Select Outbound Flight"
          />
        </BookingStep>

        {!isOneWayTrip && (
          <BookingStep
            isSelected={isInboundSelected}
            isCompleted={inboundDate !== null}
            title={this.INBOUND}
            stepValue={Step.DATE_SELECTION}
            handleStepMenuClick={handleStepMenuClick}
          >
            <DateStep
              isSelected={isInboundSelected}
              date={inboundDate}
              defaultMessage="Select Inbound Flight"
            />
          </BookingStep>
        )}
      </>
    );
  }
}

export default withStyles(styles)(BookingStepsPane);
