import BookingData from '../../../../models/BookingData';

interface BookingStepsPaneProps {
  bookingData: BookingData;
  classes: any;
  currentStep: number;
  handleStepMenuClick: Function;
  isOneWayTrip: boolean;
}

export default BookingStepsPaneProps;
