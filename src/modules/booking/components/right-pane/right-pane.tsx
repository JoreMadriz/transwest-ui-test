import { Grid } from '@material-ui/core';
import React from 'react';
import { withStyles } from '@material-ui/core/styles';

import BookingStepsPane from './components/booking-step-pane/booking-steps-pane';
import BookingSummary from './components/booking-summary/booking-summary';
import ContinueButton from '../continue-button/continue-button';
import RightPaneProp from './right-pane.props';
import Step from '../../models/Step';
import styles from './right-pane.style';

class RightPane extends React.Component<RightPaneProp> {
  constructor(props: RightPaneProp) {
    super(props);
  }

  public render() {
    const {
      classes,
      currentStep,
      enabled,
      handleContinue,
      handleStepMenuClick,
      handleBackToStartClick,
      isOneWayTrip,
      bookingData,
      price
    } = this.props;
    const { rightPane } = classes;

    return (
      <Grid xs={6} item={true} className={rightPane} container={true} direction="column">
        {currentStep < Step.FLIGHT_SELECTION ? (
          <BookingStepsPane
            currentStep={currentStep}
            handleStepMenuClick={handleStepMenuClick}
            isOneWayTrip={isOneWayTrip}
            bookingData={bookingData}
          />
        ) : (
          <BookingSummary
            price={price}
            bookingData={bookingData}
            handleBackToStartClick={handleBackToStartClick}
          />
        )}
        <ContinueButton
          enabled={enabled}
          handleContinue={handleContinue}
          currentStep={currentStep}
          price={price}
        />
      </Grid>
    );
  }
}
export default withStyles(styles)(RightPane);
