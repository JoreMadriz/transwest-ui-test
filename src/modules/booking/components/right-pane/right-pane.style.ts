import {
  BORDER_COLOR,
  LIGHT_PRIMARY_COLOR,
  PRIMARY_COLOR,
  WHITE
} from '../../../../shared/theme/colors';
import { PADDING_3 } from '../../../../shared/theme/gutters';
const styles = {
  continueButton: {
    position: 'absolute' as 'absolute',
    bottom: 0,
    backgroundColor: PRIMARY_COLOR,
    color: WHITE,
    width: '100%',
    textAlign: 'center' as 'center',
    textTransform: 'capitalize' as 'capitalize',
    fontSize: '36px',
    padding: `${PADDING_3}px 0`,
    fontWeight: 500,
    borderTop: `1px solid ${BORDER_COLOR}`,
    '&:disabled': {
      backgroundColor: WHITE,
      color: LIGHT_PRIMARY_COLOR
    },
    '&:hover': {
      backgroundColor: LIGHT_PRIMARY_COLOR
    }
  },
  rightPane: {
    position: 'relative' as 'relative',
    border: `1px solid ${BORDER_COLOR}`,
    height: '85vh'
  },
  selectedText: {
    color: `${WHITE}`
  }
};

export default styles;
