
import { Grid, withStyles, CircularProgress, Typography } from '@material-ui/core';
import moment from 'moment';
import React from 'react';

import BookingSectionTitle from '../booking-section-title/booking-section-title';
import CalendarWrapper from './components/calendar-wrapper/calendar-wrapper';
import DateSelectorProps from './date-selector.props';
import SectionSpacer from '../../../../shared/theme/components/section-spacer/section-spacer';
import styles from './date-selector.style';
import { SpacerType } from '../../../../shared/theme/components/section-spacer/SpacerType';

class DateSelector extends React.Component<DateSelectorProps> {
  componentDidMount() {
    this.props.onGetPrices();
  }
  private getLoading() {
    return (
      <>
        <CircularProgress color="secondary" />
        <Typography variant="h5" align="center">Getting fares...</Typography>
        <SectionSpacer spacerType={SpacerType.TINY} />
      </>
    );
  }
  public render() {
    const { classes, handleDateClick, isOneWayTrip } = this.props;
    let { outboundDate, inboundDate, isOutboundDateSelected, prices } = this.props.bookingInfo;
    const { calendarContainer } = classes;
    outboundDate = outboundDate
      ? outboundDate
      : moment()
          .subtract(1, 'days')
          .toDate();
    inboundDate = inboundDate ? inboundDate : outboundDate;
    const currentDate = moment().toDate();
    const nextMonth = moment()
      .add(1, 'months')
      .toDate();

    return (
      <>
        <BookingSectionTitle>When do you want to leave?</BookingSectionTitle>
        {prices.length === 0 ? (
          this.getLoading()
        ) : (
          <Grid
            className={calendarContainer}
            direction="column"
            container
            justify="flex-start"
            spacing={24}
          >
            <Grid item>
              <CalendarWrapper
                isOneWayTrip={isOneWayTrip}
                isOutboundDateSelected={isOutboundDateSelected}
                inboundDate={inboundDate}
                outboundDate={outboundDate}
                handleDateClick={handleDateClick}
                monthDate={currentDate}
                monthPrices={prices}
              />
            </Grid>
            <Grid item>
              <CalendarWrapper
                isOneWayTrip={isOneWayTrip}
                isOutboundDateSelected={isOutboundDateSelected}
                inboundDate={inboundDate}
                outboundDate={outboundDate}
                handleDateClick={handleDateClick}
                monthDate={nextMonth}
                monthPrices={prices}
              />
            </Grid>
          </Grid>
        )}
      </>
    );
  }
}

export default withStyles(styles)(DateSelector);
