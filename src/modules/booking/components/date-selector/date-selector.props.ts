import BookingCalendar from './models/booking-calendar';
import AirportSelectionData from '../airport-selection/models/airport-selection-data';

export default interface DateSelectorProps {
  classes: any;
  bookingInfo: BookingCalendar;
  airportInfo: AirportSelectionData;
  handleDateClick: any;
  isOneWayTrip:boolean;
  onGetPrices: any;
}
