import DateRange from '../../models/date-range';
import moment from 'moment';
import CalendarRangeManager from './calendar-range-manager';
import MonthRange from '../month-range/month-range';
describe('Calendar range manager test suite', () => {

  let mockParams:DateRange;
  beforeEach(() => {
    mockParams = {
      inboundDate: moment().add(1,'months').toDate(),
      outboundDate: moment().toDate(),
      monthDate: moment().toDate(),
    }
  })
  it('should manage dates between months and call getBetweenMonthRangeOutbound', () => {
    const spyOutboundRange = jest.spyOn(MonthRange.prototype, 'getBetweenMonthRangeOutbound').mockImplementation(() => 'test');
    CalendarRangeManager.getRange(mockParams);
    expect(spyOutboundRange).toHaveBeenCalled();
    spyOutboundRange.mockClear();
  });
  it('should manage dates between months and call getBetweenMonthRangeInbound',() => {
    mockParams.monthDate = moment().add(1,'months').toDate();
    const spyInboundRange = jest.spyOn(MonthRange.prototype, 'getBetweenMonthRangeInbound').mockImplementation(() => 'test2');
    CalendarRangeManager.getRange(mockParams);
    expect(spyInboundRange).toHaveBeenCalled();
    spyInboundRange.mockClear();
  })
  it("should return true when date is greater than the end of month date", () => {
    const nextMonthDate = moment().add(1, 'months').toDate();
    const monthDate = moment().toDate();
    const result = CalendarRangeManager.getUndefinedCondition(nextMonthDate, monthDate)
    expect(result).toBeTruthy();
})
it('should return true when date is less than the beggining of month date', () => {
    const prevDate = moment().subtract(1, 'months').toDate();
    const monthDate = moment().toDate();
    const result = CalendarRangeManager.getUndefinedCondition(prevDate, monthDate)
    expect(result).toBeTruthy();
});
it('it should return false when date is between of the month of month date', () => {
    const date = moment().toDate();
    const monthDate = moment().toDate();
    const result = CalendarRangeManager.getUndefinedCondition(date, monthDate)
    expect(result).toBeFalsy();
})

});
