import DateRange from '../../models/date-range';
import moment from 'moment';
import MonthRange from '../month-range/month-range';
export default class CalendarRangeManager {
  public static getRange(rangeInfo: DateRange): Date[] | undefined {
    const calendarRange = new MonthRange();
    const { outboundDate, inboundDate, monthDate } = rangeInfo;

    if (this.getIsBetweenMonth(outboundDate, inboundDate)) {
      if (this.isOnSameMonth(outboundDate, monthDate))
        return calendarRange.getBetweenMonthRangeOutbound(outboundDate, monthDate);
      if (this.isOnSameMonth(inboundDate, monthDate))
        return calendarRange.getBetweenMonthRangeInbound(inboundDate, monthDate);
    }
    if (this.getUndefinedCondition(outboundDate, monthDate)) return undefined;
    return [outboundDate, inboundDate];
  }

  private static getIsBetweenMonth(outboundDate: Date, inboundDate: Date): Boolean {
    const outboundDateMonth = parseInt(moment(outboundDate).format('M'));
    const inboundDateMonth = parseInt(moment(inboundDate).format('M'));
    return outboundDateMonth < inboundDateMonth;
  }
  private static isOnSameMonth(date: Date, monthDate: Date) {
    return moment(date).format('M') === moment(monthDate).format('M');
  }
  public static getUndefinedCondition(date: Date, monthDate: Date) {
    const MONTH = 'month';
    const DAY = 'day';
    return (
      moment(monthDate)
        .startOf(MONTH)
        .isAfter(moment(date)) ||
      moment(monthDate)
        .endOf(MONTH)
        .isBefore(moment(date)) ||
      moment().startOf(DAY).isAfter(moment(date))
    );
  }
}
