
import moment from 'moment'
export default class MonthRange {
    private readonly MONTH = 'month'
   public  getBetweenMonthRangeOutbound(date:Date, monthDate:Date) {
        const endOfMonth = moment(monthDate).endOf(this.MONTH).toDate();
        return [date,endOfMonth];
    }
    public getBetweenMonthRangeInbound(date:Date, monthDate:Date) {
        const beginningOfMonth = moment(monthDate).startOf(this.MONTH).toDate();
        return [beginningOfMonth, date];
    }
}