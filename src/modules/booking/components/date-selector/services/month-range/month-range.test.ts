import CurrentMonthRange from "./month-range";
import moment from 'moment';

describe('Current month range test suite', () => {
    let currentMonthRange:CurrentMonthRange
    beforeAll(() => {
        currentMonthRange = new CurrentMonthRange();
    });
    it('should return the selected date and the last day of the month as array', () => {
        const selectedDate = moment().toDate();
        const monthDate = moment().toDate();
        const lastDateofMonth = moment().endOf('month').toDate();
        const result = currentMonthRange.getBetweenMonthRangeOutbound(selectedDate, monthDate);
        expect(result).toEqual([selectedDate, lastDateofMonth]);
    });
    it('should return the first date of the month and the selected date', () => {
        const selectedDate = moment().toDate();
        const monthDate = moment().toDate();
        const firstDateofMonth = moment().startOf('month').toDate();
        const result = currentMonthRange.getBetweenMonthRangeInbound(selectedDate, monthDate);
        expect(result).toEqual([firstDateofMonth, selectedDate]);
    })
   
})