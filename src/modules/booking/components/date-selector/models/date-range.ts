export default interface DateRange {
    outboundDate: Date,
    inboundDate: Date,
    monthDate: Date,
}