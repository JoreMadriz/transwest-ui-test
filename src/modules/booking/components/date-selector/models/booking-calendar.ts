import CalendarPrices from "./calendar-prices";

export default interface BookingCalendar {
  outboundDate: Date | null;
  inboundDate: Date | null;
  isOutboundDateSelected: boolean;
  prices: CalendarPrices[]
}
