export default interface CalendarPrices {
    price: number,
    date: Date,
}