import { CalendarTileProperties } from 'react-calendar';
import React from 'react';

import CalendarPrices from '../../models/calendar-prices';
import { isDateEqual } from '../../../../../../shared/utils/dateUtils';
const TileContent = (prices: CalendarPrices[]) => {
  return (props: CalendarTileProperties) => {

    const calendarPrice = prices.find(
      (price: CalendarPrices) =>{
        return isDateEqual(price.date, props.date)
      }
        
    );
    const price =  calendarPrice ? '$'+calendarPrice.price.toFixed(2) : ''
    return <div>{price}</div>;
  };
};

export default TileContent;
