import React from 'react';
import moment from 'moment';
import { shallow } from 'enzyme';
import tileContent from './tile-content';

describe('Tile content test suite', () => {
  function getServiceMapWrapper() {
    const props = 'props';
    const defaultDate = '01/30/2019'
    const defaultFormat = 'MM/DD/YYYY'
    const date = moment(defaultDate, defaultFormat).toDate();
    const TileContent = tileContent(props);
    return shallow(<TileContent date={date} view="month" />);
  }

  it('should render the component structure properly', () => {
    const serviceMap = getServiceMapWrapper();
    expect(serviceMap).toMatchSnapshot();
  });
});
