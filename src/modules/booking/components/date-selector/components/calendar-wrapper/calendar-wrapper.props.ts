import CalendarPrices from "../../models/calendar-prices";

export default interface ICalendarWrapperProps {
    outboundDate:Date,
    inboundDate:Date,
    isOutboundDateSelected:boolean,
    monthDate: Date,
    monthPrices:CalendarPrices[],
    handleDateClick:any,
    classes:any,
    isOneWayTrip:boolean
}