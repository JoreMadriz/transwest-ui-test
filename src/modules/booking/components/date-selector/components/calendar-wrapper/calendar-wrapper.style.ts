import { PRIMARY_COLOR, WHITE, BACKGROUND_DATE, LIGHT_PRIMARY_COLOR } from '../../../../../../shared/theme/colors';
import { MARGIN_2 } from '../../../../../../shared/theme/gutters';
const styles = (theme:any) => ({
  tile: {
    color: PRIMARY_COLOR,
    backgroundColor: BACKGROUND_DATE,
    '& div': {
      marginTop: MARGIN_2,
      color: LIGHT_PRIMARY_COLOR,
      fontSize: '7pt',
      [theme.breakpoints.up('sm')]: {
        fontSize: '10pt'
      },

    }
  },
  calendar: {
    width: '100%',
    border: '0',
    '& button': {
      border: `1px solid ${WHITE}`
    },
    '& .react-calendar__tile:enabled:hover': {
      backgroundColor: WHITE,
      color: PRIMARY_COLOR
    },
    '& .react-calendar__month-view__weekdays__weekday': {
      color: PRIMARY_COLOR
    },
    '& .react-calendar__tile--active, .react-calendar__tile--hasActive:enabled:hover, .react-calendar__tile--hasActive:enabled:focus, .react-calendar__tile--hasActive': {
      backgroundColor: PRIMARY_COLOR,
      color: WHITE
    }
  }
});

export default styles;
