import Calendar, { CalendarTileProperties } from 'react-calendar';
import React from 'react';
import { withStyles, Typography } from '@material-ui/core';
import moment from 'moment';

import CalendarRangeManager from '../../services/calendar-range-manager/calendar-range-manager';
import DateRange from '../../models/date-range';
import { isDateEqual } from '../../../../../../shared/utils/dateUtils';
import CalendarWrapperProps from './calendar-wrapper.props';
import styles from './calendar-wrapper.style';
import TileContent from '../tile-content/tile-content';

class CalendarWrapper extends React.Component<CalendarWrapperProps> {

  private tileDisable = (props: CalendarTileProperties) => {
    const { monthPrices } = this.props;
    const isDateNotWithPrice = !monthPrices.some((price) => isDateEqual(price.date, props.date));
    return isDateNotWithPrice;
  };
  public render() {
    const { classes, monthDate, handleDateClick, isOneWayTrip, outboundDate } = this.props;
    const tileContent = TileContent(this.props.monthPrices);
    const rangeInfo: DateRange = { ...this.props };
    const outBoundValue = CalendarRangeManager.getUndefinedCondition(outboundDate, monthDate) ? undefined : rangeInfo.outboundDate
    const calendarValue = isOneWayTrip ? outBoundValue : CalendarRangeManager.getRange(rangeInfo);
    const { calendar, tile} = classes
    return (
      <>
        <Typography align="left" variant="subtitle1" color="textPrimary">
          {moment(monthDate).format('MMMM YYYY')}
        </Typography>
        <Calendar
          className={calendar}
          activeStartDate={monthDate}
          minDate={moment().toDate()}
          showNavigation={false}
          tileContent={tileContent}
          tileClassName={tile}
          showNeighboringMonth={false}
          tileDisabled={this.tileDisable}
          selectRange={!isOneWayTrip}
          value={calendarValue}
          onClickDay={handleDateClick}
        />
      </>
    );
  }
}

export default withStyles(styles)(CalendarWrapper);
