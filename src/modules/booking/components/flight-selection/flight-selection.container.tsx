import moment from 'moment';
import React from 'react';

import BookingSectionTitle from '../booking-section-title/booking-section-title';
import FlightSelectionProps from './flight-selection.container.props';
import TicketSelection from './components/ticket-selection/ticket-selection';
import { triggers, state, observable } from './observable/ticket-selection.observable';
import SectionBreadCrumb from '../section-breadcrumb/section-breadcrumb';
import SectionSpacer from '../../../../shared/theme/components/section-spacer/section-spacer';
import Steps from '../../models/Step';
import { SpacerType } from '../../../../shared/theme/components/section-spacer/SpacerType';
import withObservableStream from '../../../../shared/react-rx/withObservableStream';

export class FlightSelectionContainer extends React.Component<FlightSelectionProps> {
  private DAY_DIFF = 1;

  private isOneDayAhead(outboundDate: Date, inboundDate: Date, isRoundTrip: boolean) {
    const outDate = moment(outboundDate);
    const inDate = moment(inboundDate);
    return isRoundTrip && inDate.diff(outDate, 'days') === this.DAY_DIFF;
  }
  private isToday(outboundDate: Date) {
    const outDate = moment(outboundDate);
    const prevDay = moment().subtract(1, 'days').startOf('days');
    return outDate.diff(prevDay, 'days') === this.DAY_DIFF
  }
  render() {
    const {
      bookingData,
      handlePaginationClick,
      handleTicketClick,
      handleStepChange,
      inboundTickets,
      isOneWayTrip,
      outboundTickets,
      passengerData,
      sessionId,
      triggers
    } = this.props;
    const { getTickets } = triggers;
    const { airportSelection, dateSelection } = bookingData;
    let airports = airportSelection!;
    const isRoundTrip = !isOneWayTrip;
    const { outboundDate, inboundDate } = dateSelection;
    const isOneDayAhead = this.isOneDayAhead(outboundDate!, inboundDate!, isRoundTrip);
    const isToday = this.isToday(outboundDate!);
    return (
      <>
        <SectionBreadCrumb handleStepChange={handleStepChange} step={Steps.DATE_SELECTION}>
          Back to search
        </SectionBreadCrumb>
        <BookingSectionTitle>Select Flights</BookingSectionTitle>
        <TicketSelection
          isOutboundDate={true}
          ticketDate={outboundDate!}
          handlePaginationClick={handlePaginationClick}
          airportSelectionData={airports}
          passengerData={passengerData}
          sessionId={sessionId}
          handleTicketClick={handleTicketClick}
          ticketsInfo={outboundTickets}
          getTickets={getTickets}
          prevDayNotValid={isToday}
          nextDayNotValid={isOneDayAhead}
        />
        <SectionSpacer spacerType={SpacerType.SMALL} />
        {isRoundTrip && (
          <TicketSelection
            isOutboundDate={false}
            ticketDate={inboundDate!}
            handlePaginationClick={handlePaginationClick}
            airportSelectionData={airports}
            passengerData={passengerData}
            sessionId={sessionId}
            handleTicketClick={handleTicketClick}
            ticketsInfo={inboundTickets}
            getTickets={getTickets}
            prevDayNotValid={isOneDayAhead}
          />
        )}
      </>
    );
  }
}

export default withObservableStream(observable, triggers, state)(
  // @ts-ignore
  FlightSelectionContainer
);
