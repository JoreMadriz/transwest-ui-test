import Ticket from '../components/ticket-selection/models/ticket';
import TicketRequest from '../components/ticket-selection/models/ticket-request';
import TicketSelectionProps from '../components/ticket-selection/ticket-selection.props';

export const convertToTicketRequest = (ticketInfo: TicketSelectionProps): TicketRequest => {
  const { airportSelectionData, sessionId, passengerData, ticketDate } = ticketInfo;
  const { isOutboundDate } = ticketInfo;
  const originId = isOutboundDate
    ? airportSelectionData.departureAirport!.Id
    : airportSelectionData.destinationAirport!.Id;
  const destinationId = isOutboundDate
    ? airportSelectionData.destinationAirport!.Id
    : airportSelectionData.departureAirport!.Id;
  let ticketRequest: TicketRequest = {
    sessionId,
    DepartDate: ticketDate,
    OriginId: originId,
    DestinationId: destinationId,
    NumAdults: passengerData.adultsQuantity,
    NumChildren:
      passengerData.teensQuantity + passengerData.childrenQuantity + passengerData.babiesQuantity
  };
  return ticketRequest;
};

const sumFee = (feeTaxArray: any): number => {
  return feeTaxArray.reduce((sum: number, fee: any) => sum + fee.Value, 0);
};

export const convertToTicket = (outbound: any, isOutboundDate: boolean): Ticket[] => {
  const tickets: Ticket[] = outbound.map(
    (flight: any): Ticket => {
      let stopsCodes = new Array();
      flight.Route.forEach((elem: any) => {
        stopsCodes.push(elem.Destination.Code);
      });
      stopsCodes.pop();
      const fees = sumFee(flight.Fares[0].Fees);
      return {
        flightId: flight.FlightNumber,
        arrivalTime: flight.Arrive,
        departTime: flight.Depart,
        connections: flight.Connections,
        duration: flight.Enroute,
        stops: flight.Stops,
        stopsCodes,
        origin: flight.Origin.Code,
        destination: flight.Destination.Code,
        price: flight.Fares[0].Total + fees,
        groupId: flight.GroupId,
        fareBasisId: flight.Fares[0].FareBasisId,
        subTotal: flight.Fares[0].SubTotal,
        fees: flight.Fares[0].Fees.concat(flight.Fares[0].Taxes),
        direction: isOutboundDate,
        isSpecified: true
      };
    }
  );
  return tickets;
};
