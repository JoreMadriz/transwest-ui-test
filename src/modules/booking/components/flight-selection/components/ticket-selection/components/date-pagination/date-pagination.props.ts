export default interface DatePaginationProps {
    paginationDate:Date,
    handlePaginationClick: Function,
    isOutBoundDate:boolean,
    classes:any,
    prevDayNotValid: boolean,
    nextDayNotValid: boolean
}