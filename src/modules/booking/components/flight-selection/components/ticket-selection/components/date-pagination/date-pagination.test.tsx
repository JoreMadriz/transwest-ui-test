import DatePaginationProps from './date-pagination.props';
import { shallow, ShallowWrapper } from 'enzyme';
import React from 'react';
import moment from 'moment';
import { DatePagination } from './date-pagination';

describe('Date pagination component suite', () => {
  const TODAY = new Date();
  const DEFAULT_PREV_DATE = moment(TODAY)
    .subtract(1, 'days')
    .toDate();
  const DEFAULT_NEXT_DATE = moment(TODAY)
    .add(1, 'days')
    .toDate();

  function getDefaultProps(): DatePaginationProps {
    return {
      classes: {},
      handlePaginationClick: jest.fn(),
      isOutBoundDate: true,
      paginationDate: TODAY,
      nextDayNotValid: false,
      prevDayNotValid: false
    };
  }
  function getDatePaginationWrapper(props: DatePaginationProps): ShallowWrapper {
    return shallow(<DatePagination {...props} />);
  }

  function validateHandleClickWithCorrectValues(
    componentSelector: string,
    DEFAULT_PREV_DATE: Date
  ) {
    const defaultProps = getDefaultProps();
    const shallowPagination = getDatePaginationWrapper(defaultProps);
    const leftArrowButton = shallowPagination.find(componentSelector);
    const button = componentSelector.includes('#') ? leftArrowButton : leftArrowButton.parent()
    button.simulate('click');
    expect(defaultProps.handlePaginationClick).toHaveBeenCalledWith(
      defaultProps.isOutBoundDate,
      DEFAULT_PREV_DATE
    );
  }

  it('it should render prevDay, nextDay buttons', () => {
    const defaultProps = getDefaultProps();
    const dateFormat = 'ddd, MMM D';
    const prevDayString = moment(defaultProps.paginationDate)
      .subtract(1, 'days')
      .format(dateFormat);
    const nextDayString = moment(defaultProps.paginationDate)
      .add(1, 'days')
      .format(dateFormat);

    const shallowPagination = getDatePaginationWrapper(defaultProps);
    const dateButtons = shallowPagination.find('WithStyles(Button)').getElements();
    const prevDayButton = dateButtons[0].props.children;
    const nextDayButton = dateButtons[1].props.children;
    expect(prevDayButton).toEqual(prevDayString);
    expect(nextDayButton).toEqual(nextDayString);
  });
  it('it should call handlePaginationClick with the correct values when lef arrow icon is clicked', () => {
    validateHandleClickWithCorrectValues('pure(KeyboardArrowLeftIcon)', DEFAULT_PREV_DATE);
  });

  it('it should call handlePaginationClick with the correct values when previous date button is clicked', () => {
    validateHandleClickWithCorrectValues('#previousDateButton', DEFAULT_PREV_DATE);
  });

  it('it should call handlePaginationClick the the correct values when next date button is clicked', () => {
    validateHandleClickWithCorrectValues('#nextDateButton', DEFAULT_NEXT_DATE);
  });

  it('it should call handlePaginationClick the the correct values when right arrow icon is clicked', () => {
    validateHandleClickWithCorrectValues('pure(KeyboardArrowRightIcon)', DEFAULT_NEXT_DATE);
  });
});
