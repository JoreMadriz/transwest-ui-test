import {
  BACKGROUND_DATE,
  BORDER_COLOR,
  DARKER_PRIMARY_COLOR,
  LIGHT_PRIMARY_COLOR,
  PRIMARY_COLOR
} from '../../../../../../../../shared/theme/colors';
import { PADDING_2, PADDING_3, PADDING_5 } from '../../../../../../../../shared/theme/gutters';

const styles = (theme: any) => ({
  ticketContainer: {
    border: `1px solid ${BORDER_COLOR}`,
    color: PRIMARY_COLOR,
    fontSize: '16px',
    textAlign: 'left' as 'left',
    cursor: 'pointer' as 'pointer',
    padding: `${PADDING_2}px ${PADDING_3}px ${PADDING_3}px ${PADDING_5}px`,
    [theme.breakpoints.down('sm')]: {
      fontSize: '14px',
      paddingLeft: PADDING_2
    },
    '&:hover': {
      backgroundColor: BACKGROUND_DATE
    },
    '&:selected': {
      backgroundColor: DARKER_PRIMARY_COLOR
    }
  },
  padding: {
    paddingBottom: PADDING_2
  },
  bold: {
    fontWeight: 600,
    whitespace: 'nowrap' as 'nowrap'
  },
  bottomText: {
    fontSize: '12px',
    color: LIGHT_PRIMARY_COLOR,
    [theme.breakpoints.down('sm')]: {
      fontSize: '10px'
    }
  },
  last: {
    textAlign: 'right' as 'right'
  }
});

export default styles;
