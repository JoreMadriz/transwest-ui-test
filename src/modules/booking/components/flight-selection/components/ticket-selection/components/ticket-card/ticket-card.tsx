import { Grid, withStyles } from '@material-ui/core';
import React from 'react';

import { formatDate, formatTime } from '../../../../../../../../shared/utils/dateUtils';
import styles from './ticket-card.style';
import TicketCardProps from './ticket-card.props';
export class TicketCard extends React.Component<TicketCardProps> {
  formatStops(list: Array<String>) {}
  render() {
    const { classes, ticketInfo, onCardClick } = this.props;
    const {
      arrivalTime,
      duration,
      departTime,
      destination,
      origin,
      price,
      stops,
      stopsCodes,
      flightId
    } = ticketInfo;
    const { ticketContainer, bold, padding, bottomText, last } = classes;
    const stopText = stopsCodes.length == 1 ? 'stop' : 'stops';
    const stopCodes =
      stopsCodes.length > 0 ? stopsCodes.toString().replace(new RegExp(',', 'g'), ', ') : '';
    const codeshareNo = '280';

    return (
      <Grid onClick={onCardClick} className={ticketContainer} container direction="row">
        <Grid item xs={5}>
          <Grid container direction="column" justify={'flex-start'} alignItems={'flex-start'}>
            <Grid item className={`${bold} ${padding} `}>
              {formatDate(departTime, 'H:mm A')} - {formatDate(arrivalTime, 'H:mm A')}
            </Grid>
            <Grid item className={bottomText}>
              Flight #{flightId} - Codeshare {codeshareNo}
            </Grid>
          </Grid>
        </Grid>
        <Grid item xs={2}>
          <Grid container direction="column" justify={'flex-start'} alignItems={'flex-start'}>
            <Grid item className={padding}>
              {formatTime(duration)}
            </Grid>
            <Grid item className={bottomText}>
              {origin} - {destination}
            </Grid>
          </Grid>
        </Grid>
        <Grid item xs={2}>
          <Grid container direction="column" alignItems={'flex-start'}>
            <Grid item className={padding}>
              {stops} {stopText}
            </Grid>
            <Grid item className={bottomText}>
              {stopCodes}
            </Grid>
          </Grid>
        </Grid>
        <Grid item xs={3}>
          <Grid container direction="column" alignItems={'flex-end'}>
            <Grid item className={`${bold} ${padding} ${last}`}>
              ${price.toFixed(2)}
            </Grid>
            <Grid item className={`${last} ${bottomText}`}>
              Operated by TWA
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    );
  }
}

export default withStyles(styles)(TicketCard);
