import React from 'react';
import { shallow, ShallowWrapper } from 'enzyme';

import { getDefaultTicket } from '../../../../../../utils/default-props.util';
import { TicketCard } from './ticket-card';
import TicketCardProps from './ticket-card.props';

describe('ticket card test suite', () => {
  function getDefaultProps(): TicketCardProps {
    return {
      classes: {},
      onCardClick: jest.fn(),
      ticketInfo: getDefaultTicket()
    };
  }
  function getTicketCardWrapper(props: TicketCardProps): ShallowWrapper {
    return shallow(<TicketCard {...props} />);
  }

  it('should call click event when the card is clicked', () => {
    const defaultProps = getDefaultProps();
    const mockClick = defaultProps.onCardClick;
    const shallowCard = getTicketCardWrapper(defaultProps);
    shallowCard.simulate('click');
    expect(mockClick).toHaveBeenCalledTimes(1);
  });
});
