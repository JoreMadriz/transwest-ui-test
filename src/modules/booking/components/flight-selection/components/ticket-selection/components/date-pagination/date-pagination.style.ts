import { BORDER_COLOR, PRIMARY_COLOR } from "../../../../../../../../shared/theme/colors";
import { PADDING_0, PADDING_2 } from "../../../../../../../../shared/theme/gutters";

const styles = (theme:any) => ({
    dateContainer: {
        backgroundColor: BORDER_COLOR,
        padding: `${PADDING_2}px`,
        '& button':{
            textTransform: 'capitalize' as 'capitalize',
            padding: `${PADDING_0}`,
            fontWeight: 500
        } 
    },
    currentDate: {
        [theme.breakpoints.down('sm')]: {
            fontSize:'12pt',
            lineHeight: 2,
        },
        fontSize:'18pt',
        color: PRIMARY_COLOR,
        fontWeight: 600,
    },
    dates: {
        [theme.breakpoints.down('sm')]: {
            fontSize:'9pt',
        },
        fontSize: '12pt',
        lineHeight: 2.2
    },

});

export default styles;