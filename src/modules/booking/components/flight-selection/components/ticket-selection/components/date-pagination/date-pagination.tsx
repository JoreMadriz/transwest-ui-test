import { Grid, IconButton, Button, withStyles } from '@material-ui/core';
import { KeyboardArrowLeft, KeyboardArrowRight } from '@material-ui/icons/';
import moment from 'moment';
import React from 'react';

import DatePaginationProps from './date-pagination.props';
import styles from './date-pagination.style';

const getDateFormat = (date: Date) => {
  const dateFormat = 'ddd, MMM D';
  return moment(date).format(dateFormat);
};
export const DatePagination = (props: DatePaginationProps) => {
  const {
    paginationDate,
    isOutBoundDate,
    handlePaginationClick,
    classes,
    nextDayNotValid,
    prevDayNotValid
  } = props;
  const { dateContainer, currentDate, dates } = classes;
  const prevDay = moment(paginationDate)
    .subtract(1, 'days')
    .toDate();
  const nextDay = moment(paginationDate)
    .add(1, 'days')
    .toDate();
  return (
    <Grid container justify="space-between" className={dateContainer}>
      <Grid item xs={1} container justify="flex-start">
        <IconButton
          disabled={prevDayNotValid}
          onClick={() => {
            handlePaginationClick(isOutBoundDate, prevDay);
          }}
        >
          <KeyboardArrowLeft fontSize="large" />
        </IconButton>
      </Grid>
      <Grid item xs={3} container justify="center">
        <Button
          onClick={() => {
            handlePaginationClick(isOutBoundDate, prevDay);
          }}
          id="previousDateButton"
          disabled={prevDayNotValid}
          fullWidth={true}
          className={dates}
        >
          {getDateFormat(prevDay)}
        </Button>
      </Grid>
      <Grid item className={currentDate} xs={4}>
        {getDateFormat(paginationDate)}
      </Grid>
      <Grid item xs={3} container justify="center">
        <Button
          onClick={() => {
            handlePaginationClick(isOutBoundDate, nextDay);
          }}
          id="nextDateButton"
          disabled={nextDayNotValid}
          fullWidth={true}
          className={dates}
        >
          {getDateFormat(nextDay)}
        </Button>
      </Grid>
      <Grid item xs={1} container justify="flex-end">
        <IconButton
          disabled={nextDayNotValid}
          onClick={() => {
            handlePaginationClick(isOutBoundDate, nextDay);
          }}
        >
          <KeyboardArrowRight fontSize="large" />
        </IconButton>
      </Grid>
    </Grid>
  );
};

export default withStyles(styles)(DatePagination);
