import React from 'react';
import { Typography, CircularProgress, withStyles } from '@material-ui/core';

import DatePagination from './components/date-pagination/date-pagination';
import { RequestState } from './models/request-state';
import SectionSpacer from '../../../../../../shared/theme/components/section-spacer/section-spacer';
import { SpacerType } from '../../../../../../shared/theme/components/section-spacer/SpacerType';
import styles from './ticket-selection.styles';
import Ticket from './models/ticket';
import TicketCard from './components/ticket-card/ticket-card';
import TicketSelectionProps from './ticket-selection.props';

class TicketSelection extends React.Component<TicketSelectionProps> {
  componentDidMount() {
    this.props.getTickets(this.props);
  }
  componentDidUpdate(prevProps: TicketSelectionProps) {
    if (prevProps.ticketDate !== this.props.ticketDate) {
      this.props.getTickets(this.props);
    }
  }

  private getNoTicketsMessage() {
    return (
      <>
        <Typography variant="h5">There is no flights for these days</Typography>
        <SectionSpacer spacerType={SpacerType.TINY} />
      </>
    );
  }
  private renderTickets(tickets: Ticket[]) {
    const { handleTicketClick, isOutboundDate } = this.props;
    return tickets.map((ticket) => {
      return (
        <div key={ticket.flightId}>
          <TicketCard ticketInfo={ticket} onCardClick={handleTicketClick(isOutboundDate, ticket)} />
        </div>
      );
    });
  }
  private getLoading() {
    return (
      <>
        <CircularProgress color="secondary" />
        <SectionSpacer spacerType={SpacerType.TINY} />
      </>
    );
  }
  private getRenderTickets() {
    const { ticketsInfo } = this.props;
    const { requestState, tickets } = ticketsInfo;
    if (requestState === RequestState.isLoading) return this.getLoading();
    if (requestState === RequestState.NoData) return this.getNoTicketsMessage();
    return this.renderTickets(tickets);
  }

  render() {
    const { ticketDate, handlePaginationClick, isOutboundDate, classes } = this.props;
    const { subTitle, title } = classes;
    let { nextDayNotValid, prevDayNotValid } = this.props;
    nextDayNotValid = nextDayNotValid || false;
    prevDayNotValid = prevDayNotValid || false;
    const renderTickets = this.getRenderTickets();
    const titleText = isOutboundDate ? 'Outbound' : 'Inbound';
    return (
      <>
        <Typography className={title}>{`Select ${titleText} Flight`}</Typography>
        <Typography className={subTitle}>Total price includes taxes + fees for 1 adult</Typography>
        <DatePagination
          isOutBoundDate={isOutboundDate}
          paginationDate={ticketDate}
          handlePaginationClick={handlePaginationClick}
          prevDayNotValid={prevDayNotValid}
          nextDayNotValid={nextDayNotValid}
        />
        {renderTickets}
      </>
    );
  }
}

export default withStyles(styles)(TicketSelection);
