import React from 'react';
import { shallow, ShallowWrapper } from 'enzyme';

import { getDefaultTicketSelectionProps } from '../../../../utils/default-props.util';
import { RequestState } from './models/request-state';
import TicketSelection from './ticket-selection';
import TicketSelectionProps from './ticket-selection.props';

describe('ticket selection test suite', () => {
  function getShallowTicketSelection(props: TicketSelectionProps): ShallowWrapper {
    return shallow(<TicketSelection {...props} />);
  }
  function testComponentExist(props: TicketSelectionProps, componentName: string, length: number) {
    const shallowTicket = getShallowTicketSelection(props);
    const component = shallowTicket.find(componentName);
    expect(component.length).toBe(length);
  }
  function dayValidation(props: TicketSelectionProps, dayNotValidType: string) {
    const shallowTicket = getShallowTicketSelection(props);
    const dayNotValid = shallowTicket.find('WithStyles(DatePagination)').prop(dayNotValidType);
    expect(dayNotValid).toBeFalsy();
  }
  it('should send prevDayNotValid as false if it is not provided', () => {
    const defaultProps = getDefaultTicketSelectionProps();
    defaultProps.prevDayNotValid = undefined;
    dayValidation(defaultProps, 'prevDayNotValid');
  });
  it('should send nextDayNotValid as false if it is not provided', () => {
    const defaultProps = getDefaultTicketSelectionProps();
    defaultProps.nextDayNotValid = undefined;
    dayValidation(defaultProps, 'nextDayNotValid');
  });
  it('should render isLoading component when is making a request', () => {
    const defaultProps = getDefaultTicketSelectionProps();
    testComponentExist(defaultProps, 'WithStyles(CircularProgress)', 1);
  });
  it('should render two Typographies components when the request shows no data', () => {
    const defaultProps = getDefaultTicketSelectionProps();
    defaultProps.ticketsInfo.requestState = RequestState.NoData;
    testComponentExist(defaultProps, 'WithStyles(Typography)', 2);
  });
  it('should render a Ticket card when the request is completed', () => {
    const defaultProps = getDefaultTicketSelectionProps();
    defaultProps.ticketsInfo.requestState = RequestState.Finished;
    testComponentExist(defaultProps, 'WithStyles(TicketCard)', 1);
  });
});
