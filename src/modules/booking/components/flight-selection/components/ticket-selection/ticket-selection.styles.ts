import { LIGHT_PRIMARY_COLOR, PRIMARY_COLOR } from '../../../../../../shared/theme/colors';
import { PADDING_1, PADDING_2 } from '../../../../../../shared/theme/gutters';

const styles = (theme: any) => ({
  title: {
    color: PRIMARY_COLOR,
    fontSize: '20px',
    textAlign: 'left' as 'left'
  },
  subTitle: {
    fontSize: '12px',
    color: LIGHT_PRIMARY_COLOR,
    paddingTop: PADDING_1,
    paddingBottom: PADDING_2,
    textAlign: 'left' as 'left'
  }
});

export default styles;
