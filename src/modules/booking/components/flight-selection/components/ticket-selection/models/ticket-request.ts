export default interface TicketRequest {
    sessionId: string,
    OriginId: number,
    DestinationId: number,
    DepartDate: Date,
    NumAdults: number,
    NumChildren:number
}