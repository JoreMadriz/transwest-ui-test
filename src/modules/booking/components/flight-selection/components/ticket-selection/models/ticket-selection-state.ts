import { RequestState } from './request-state';
import Ticket from './ticket';

export default interface TicketSelectionState {
  tickets: Ticket[];
  requestState: RequestState;
}
