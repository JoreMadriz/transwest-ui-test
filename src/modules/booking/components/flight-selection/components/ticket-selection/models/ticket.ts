export default interface Ticket {
  flightId: number;
  departTime: string;
  arrivalTime: string;
  connections: number;
  duration: string;
  stops: number;
  stopsCodes: Array<string>;
  origin: string;
  destination: string;
  price: number;
  groupId: number;
  fareBasisId: number;
  fees:any;
  subTotal:number;
  direction:boolean;
  isSpecified: boolean;
}
