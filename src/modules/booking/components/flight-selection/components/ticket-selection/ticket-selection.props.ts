import AirportSelectionData from '../../../airport-selection/models/airport-selection-data';
import PassengerData from '../../../../models/PassengerData';
import TicketSelectionState from './models/ticket-selection-state';
export default interface TicketSelectionProps {
  airportSelectionData: AirportSelectionData;
  handlePaginationClick: Function;
  isOutboundDate: boolean;
  passengerData: PassengerData;
  sessionId: string;
  ticketDate: Date;
  getTickets?: any;
  handleTicketClick: Function;
  ticketsInfo: TicketSelectionState;
  nextDayNotValid?: boolean;
  prevDayNotValid?: boolean;
  classes?: any;
}
