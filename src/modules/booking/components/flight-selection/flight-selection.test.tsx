import { shallow, ShallowWrapper } from 'enzyme';
import moment from 'moment';
import React from 'react';

import FlightSelectionProps from './flight-selection.container.props';
import { FlightSelectionContainer } from './flight-selection.container';
import { getDefaultBookingData } from '../../utils/default-props.util';
import { getDefaultPassengerData } from '../../utils/passenger-default-props.util';

import { RequestState } from './components/ticket-selection/models/request-state';

describe('flight selection test suite', () => {
  function getDefaultProps(): FlightSelectionProps {
    return {
      bookingData: getDefaultBookingData(),
      handlePaginationClick: jest.fn(),
      handleStepChange: jest.fn(),
      handleTicketClick: jest.fn(),
      inboundTickets: {
        requestState: RequestState.isLoading,
        tickets: []
      },
      isOneWayTrip: false,
      outboundTickets: {
        requestState: RequestState.isLoading,
        tickets: []
      },
      passengerData: getDefaultPassengerData(),
      sessionId: '',
      triggers: {
        getTickets: jest.fn()
      }
    };
  }

  function getShallowFlightSelection(props: FlightSelectionProps): ShallowWrapper {
    return shallow(<FlightSelectionContainer {...props} />);
  }
  function getIsToday(props: FlightSelectionProps): boolean {
    const shallowFlight = getShallowFlightSelection(props);
    const ticketSelection = shallowFlight.find('WithStyles(TicketSelection)');
    return ticketSelection.at(0).prop('prevDayNotValid');
  }
  function getIsOneDayAhead(props: FlightSelectionProps): boolean {
    const shallowFlight = getShallowFlightSelection(props);
    const ticketSelection = shallowFlight.find('WithStyles(TicketSelection)');
    return ticketSelection.at(0).prop('nextDayNotValid');
  }
  it('should send isToday as true', () => {
    const defaultProps = getDefaultProps();
    defaultProps.bookingData.dateSelection.outboundDate = moment().toDate();
    const isToday = getIsToday(defaultProps);
    expect(isToday).toBeTruthy();
  });
  it('should send isToday as false', () => {
    const defaultProps = getDefaultProps();
    defaultProps.bookingData.dateSelection.outboundDate = moment()
      .add(1, 'days')
      .toDate();
    const isToday = getIsToday(defaultProps);
    expect(isToday).toBeFalsy();
  });
  it('should send isOneDayAhead as true', () => {
    const defaultProps = getDefaultProps();
    defaultProps.bookingData.dateSelection.outboundDate = moment().toDate();
    defaultProps.bookingData.dateSelection.inboundDate = moment()
      .add(1, 'days')
      .toDate();
    const isOneDayAhead = getIsOneDayAhead(defaultProps);
    expect(isOneDayAhead).toBeTruthy();
  });
  it('should send isOneDayAhead as false when is OneWayTrip', () => {
    const defaultProps = getDefaultProps();
    defaultProps.bookingData.dateSelection.outboundDate = moment().toDate();
    defaultProps.bookingData.dateSelection.inboundDate = moment()
      .add(2, 'days')
      .toDate();
    const isOneDayAhead = getIsOneDayAhead(defaultProps);
    expect(isOneDayAhead).toBeFalsy();
  });
  it('should send isOneDayAhead as false when is OneWayTrip', () => {
    const defaultProps = getDefaultProps();
    defaultProps.isOneWayTrip = true;
    const isOneDayAhead = getIsOneDayAhead(defaultProps);
    expect(isOneDayAhead).toBeFalsy();
  });
  it('should render the second Ticket Selection', () => {
    const defaultProps = getDefaultProps();
    const shallowFlight = getShallowFlightSelection(defaultProps);
    const ticketSelection = shallowFlight.find('WithStyles(TicketSelection)');
    expect(ticketSelection.length).toBe(2);
  });
  it('should not render the second Ticket Selection when isOneWayTrip is true', () => {
    const defaultProps = getDefaultProps();
    defaultProps.isOneWayTrip = true;
    const shallowFlight = getShallowFlightSelection(defaultProps);
    const ticketSelection = shallowFlight.find('WithStyles(TicketSelection)');
    expect(ticketSelection.length).toBe(1);
  });
});
