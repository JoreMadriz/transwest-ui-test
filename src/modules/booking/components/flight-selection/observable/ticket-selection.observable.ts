import { convertToTicketRequest, convertToTicket } from '../services/ticket-selection.service';
import HttpService from '../../../../../shared/services/http.service';
import { RequestState } from '../components/ticket-selection/models/request-state';
import { Subject } from 'rxjs';
import TicketSelectionState from '../components/ticket-selection/models/ticket-selection-state';
import TicketSelectionProps from '../components/ticket-selection/ticket-selection.props';
import Ticket from '../components/ticket-selection/models/ticket';
import FlightSelectionTriggers from './flight-selection.triggers';

interface TicketObservableState {
  outboundTickets: TicketSelectionState;
  inboundTickets: TicketSelectionState;
}

function getTickets(): Ticket[] {
  return [];
}

export const state: TicketObservableState = {
  outboundTickets: {
    tickets: getTickets(),
    requestState: RequestState.isLoading
  },
  inboundTickets: {
    tickets: getTickets(),
    requestState: RequestState.isLoading
  }
};

export const observable = new Subject();
export const triggers: FlightSelectionTriggers = {
  getTickets: (ticketInfo: TicketSelectionProps) => {
    const { isOutboundDate } = ticketInfo;
    const requestBody = convertToTicketRequest(ticketInfo);
    const type = isOutboundDate ? 'outboundTickets' : 'inboundTickets';
    state[type].requestState = RequestState.isLoading;
    HttpService.post('/Booking/GetFlightOptions', {}, requestBody)
      .then((results: any) => {
        const { outbound } = results;
        if (outbound.length === 0) {
          state[type].requestState = RequestState.NoData;
        } else {
          state[type].tickets = convertToTicket(results.outbound, isOutboundDate);
          state[type].requestState = RequestState.Finished;
        }

        observable.next(state);
      })
      .catch((error) => error);
    observable.next(state);
  }
};
