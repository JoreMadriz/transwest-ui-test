import { triggers, state } from './ticket-selection.observable';
import HttpService from '../../../../../shared/services/http.service';
import { getDefaultTicketSelectionProps } from '../../../utils/default-props.util';
import TicketSelectionProps from '../components/ticket-selection/ticket-selection.props';
import { RequestState } from '../components/ticket-selection/models/request-state';
import { mockResponse, mockNoResponse } from './request-object.mock';

describe('Flight Selection Observable test suite', () => {
  it('it should be state as isLoading in outboundTicket', () => {
    expect(state.outboundTickets.requestState).toEqual(RequestState.isLoading);
  });
  async function setTickets(props: TicketSelectionProps, response: any) {
    jest.spyOn(HttpService, 'post').mockImplementation(() => Promise.resolve(response));
    await triggers.getTickets(props);
  }

  it('it should change the request state as Finished and add tickets in outboundTickets', async () => {
    const defaultProps = getDefaultTicketSelectionProps();
    await setTickets(defaultProps, mockResponse);
    const { requestState, tickets } = state.outboundTickets;
    expect(requestState).toEqual(RequestState.Finished);
    expect(tickets[0].flightId).toEqual(1);
  });
  it('it should change the request state as Finished and add tickets in inboundTickets ', async () => {
    const defaultProps = getDefaultTicketSelectionProps();;
    defaultProps.isOutboundDate = false;
    await setTickets(defaultProps, mockResponse);
    const { requestState, tickets } = state.inboundTickets;
    expect(requestState).toEqual(RequestState.Finished);
    expect(tickets[0].flightId).toEqual(1);
  });
  it('it should change the request state as NoData in outboundTickets ', async () => {
    const defaultProps = getDefaultTicketSelectionProps();
    await setTickets(defaultProps, mockNoResponse);
    expect(state.outboundTickets.requestState).toEqual(RequestState.NoData);
  });
  it('it should change the request state as NoData in inboundTickets ', async () => {
    const defaultProps = getDefaultTicketSelectionProps();
    defaultProps.isOutboundDate = false;
    await setTickets(defaultProps, mockNoResponse);
    expect(state.outboundTickets.requestState).toEqual(RequestState.NoData);
  });
});
