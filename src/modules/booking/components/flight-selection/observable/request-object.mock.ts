export const mockResponse = {
    "status": "sample string 1",
    "errors": {
      "sample string 1": "sample string 2",
      "sample string 3": "sample string 4"
    },
    "inbound": [
      {
        "FlightNumber": 1,
        "Origin": {
          "Id": 1,
          "Code": "sample string 2",
          "Name": "sample string 3",
          "Address": {
            "Address": "sample string 1",
            "City": "sample string 2",
            "Province": "sample string 3",
            "Country": 1,
            "PostalCode": "sample string 4"
          },
          "WayOrder": 1
        },
        "Destination": {
          "Id": 1,
          "Code": "sample string 2",
          "Name": "sample string 3",
          "Address": {
            "Address": "sample string 1",
            "City": "sample string 2",
            "Province": "sample string 3",
            "Country": 1,
            "PostalCode": "sample string 4"
          },
          "WayOrder": 1
        },
        "Depart": "2019-02-07T10:40:59.0143422-06:00",
        "Arrive": "2019-02-07T10:40:59.0143422-06:00",
        "Enroute": "00:00:00.1234567",
        "Stops": 5,
        "Connections": 6,
        "SeatsRemaining": 7,
        "Route": [
          {
            "FlightId": 1,
            "LegOrder": 2,
            "FlightNumber": 3,
            "Origin": {
              "Id": 1,
              "Code": "sample string 2",
              "Name": "sample string 3",
              "Address": {
                "Address": "sample string 1",
                "City": "sample string 2",
                "Province": "sample string 3",
                "Country": 1,
                "PostalCode": "sample string 4"
              },
              "WayOrder": 1
            },
            "Destination": {
              "Id": 1,
              "Code": "sample string 2",
              "Name": "sample string 3",
              "Address": {
                "Address": "sample string 1",
                "City": "sample string 2",
                "Province": "sample string 3",
                "Country": 1,
                "PostalCode": "sample string 4"
              },
              "WayOrder": 1
            },
            "Depart": "2019-02-07T10:40:59.0143422-06:00",
            "Arrive": "2019-02-07T10:40:59.0143422-06:00",
            "SeatsOnboard": 1,
            "SeatsAssigned": 1,
            "SeatsRemaining": 1
          },
          {
            "FlightId": 1,
            "LegOrder": 2,
            "FlightNumber": 3,
            "Origin": {
              "Id": 1,
              "Code": "sample string 2",
              "Name": "sample string 3",
              "Address": {
                "Address": "sample string 1",
                "City": "sample string 2",
                "Province": "sample string 3",
                "Country": 1,
                "PostalCode": "sample string 4"
              },
              "WayOrder": 1
            },
            "Destination": {
              "Id": 1,
              "Code": "sample string 2",
              "Name": "sample string 3",
              "Address": {
                "Address": "sample string 1",
                "City": "sample string 2",
                "Province": "sample string 3",
                "Country": 1,
                "PostalCode": "sample string 4"
              },
              "WayOrder": 1
            },
            "Depart": "2019-02-07T10:40:59.0143422-06:00",
            "Arrive": "2019-02-07T10:40:59.0143422-06:00",
            "SeatsOnboard": 1,
            "SeatsAssigned": 1,
            "SeatsRemaining": 1
          }
        ],
        "Fares": [
          {
            "FareBasisId": 1,
            "Name": "sample string 2",
            "Rule": "sample string 3",
            "Amount": 4.0,
            "Fees": [
              {
                "Name": "sample string 1",
                "Value": 1.0
              },
              {
                "Name": "sample string 1",
                "Value": 1.0
              }
            ],
            "Taxes": [
              {
                "Name": "sample string 1",
                "Value": 1.0
              },
              {
                "Name": "sample string 1",
                "Value": 1.0
              }
            ],
            "Total": 8.0,
            "SubTotal": 6.0
          },
          {
            "FareBasisId": 1,
            "Name": "sample string 2",
            "Rule": "sample string 3",
            "Amount": 4.0,
            "Fees": [
              {
                "Name": "sample string 1",
                "Value": 1.0
              },
              {
                "Name": "sample string 1",
                "Value": 1.0
              }
            ],
            "Taxes": [
              {
                "Name": "sample string 1",
                "Value": 1.0
              },
              {
                "Name": "sample string 1",
                "Value": 1.0
              }
            ],
            "Total": 8.0,
            "SubTotal": 6.0
          }
        ],
        "GroupId": 8,
        "MaximumCheckedBaggageWeight": 9.0,
        "MaximumCarryOnBaggageWeight": 10.0
      },
      {
        "FlightNumber": 1,
        "Origin": {
          "Id": 1,
          "Code": "sample string 2",
          "Name": "sample string 3",
          "Address": {
            "Address": "sample string 1",
            "City": "sample string 2",
            "Province": "sample string 3",
            "Country": 1,
            "PostalCode": "sample string 4"
          },
          "WayOrder": 1
        },
        "Destination": {
          "Id": 1,
          "Code": "sample string 2",
          "Name": "sample string 3",
          "Address": {
            "Address": "sample string 1",
            "City": "sample string 2",
            "Province": "sample string 3",
            "Country": 1,
            "PostalCode": "sample string 4"
          },
          "WayOrder": 1
        },
        "Depart": "2019-02-07T10:40:59.0143422-06:00",
        "Arrive": "2019-02-07T10:40:59.0143422-06:00",
        "Enroute": "00:00:00.1234567",
        "Stops": 5,
        "Connections": 6,
        "SeatsRemaining": 7,
        "Route": [
          {
            "FlightId": 1,
            "LegOrder": 2,
            "FlightNumber": 3,
            "Origin": {
              "Id": 1,
              "Code": "sample string 2",
              "Name": "sample string 3",
              "Address": {
                "Address": "sample string 1",
                "City": "sample string 2",
                "Province": "sample string 3",
                "Country": 1,
                "PostalCode": "sample string 4"
              },
              "WayOrder": 1
            },
            "Destination": {
              "Id": 1,
              "Code": "sample string 2",
              "Name": "sample string 3",
              "Address": {
                "Address": "sample string 1",
                "City": "sample string 2",
                "Province": "sample string 3",
                "Country": 1,
                "PostalCode": "sample string 4"
              },
              "WayOrder": 1
            },
            "Depart": "2019-02-07T10:40:59.0143422-06:00",
            "Arrive": "2019-02-07T10:40:59.0143422-06:00",
            "SeatsOnboard": 1,
            "SeatsAssigned": 1,
            "SeatsRemaining": 1
          },
          {
            "FlightId": 1,
            "LegOrder": 2,
            "FlightNumber": 3,
            "Origin": {
              "Id": 1,
              "Code": "sample string 2",
              "Name": "sample string 3",
              "Address": {
                "Address": "sample string 1",
                "City": "sample string 2",
                "Province": "sample string 3",
                "Country": 1,
                "PostalCode": "sample string 4"
              },
              "WayOrder": 1
            },
            "Destination": {
              "Id": 1,
              "Code": "sample string 2",
              "Name": "sample string 3",
              "Address": {
                "Address": "sample string 1",
                "City": "sample string 2",
                "Province": "sample string 3",
                "Country": 1,
                "PostalCode": "sample string 4"
              },
              "WayOrder": 1
            },
            "Depart": "2019-02-07T10:40:59.0143422-06:00",
            "Arrive": "2019-02-07T10:40:59.0143422-06:00",
            "SeatsOnboard": 1,
            "SeatsAssigned": 1,
            "SeatsRemaining": 1
          }
        ],
        "Fares": [
          {
            "FareBasisId": 1,
            "Name": "sample string 2",
            "Rule": "sample string 3",
            "Amount": 4.0,
            "Fees": [
              {
                "Name": "sample string 1",
                "Value": 1.0
              },
              {
                "Name": "sample string 1",
                "Value": 1.0
              }
            ],
            "Taxes": [
              {
                "Name": "sample string 1",
                "Value": 1.0
              },
              {
                "Name": "sample string 1",
                "Value": 1.0
              }
            ],
            "Total": 8.0,
            "SubTotal": 6.0
          },
          {
            "FareBasisId": 1,
            "Name": "sample string 2",
            "Rule": "sample string 3",
            "Amount": 4.0,
            "Fees": [
              {
                "Name": "sample string 1",
                "Value": 1.0
              },
              {
                "Name": "sample string 1",
                "Value": 1.0
              }
            ],
            "Taxes": [
              {
                "Name": "sample string 1",
                "Value": 1.0
              },
              {
                "Name": "sample string 1",
                "Value": 1.0
              }
            ],
            "Total": 8.0,
            "SubTotal": 6.0
          }
        ],
        "GroupId": 8,
        "MaximumCheckedBaggageWeight": 9.0,
        "MaximumCarryOnBaggageWeight": 10.0
      }
    ],
    "outbound": [
      {
        "FlightNumber": 1,
        "Origin": {
          "Id": 1,
          "Code": "sample string 2",
          "Name": "sample string 3",
          "Address": {
            "Address": "sample string 1",
            "City": "sample string 2",
            "Province": "sample string 3",
            "Country": 1,
            "PostalCode": "sample string 4"
          },
          "WayOrder": 1
        },
        "Destination": {
          "Id": 1,
          "Code": "sample string 2",
          "Name": "sample string 3",
          "Address": {
            "Address": "sample string 1",
            "City": "sample string 2",
            "Province": "sample string 3",
            "Country": 1,
            "PostalCode": "sample string 4"
          },
          "WayOrder": 1
        },
        "Depart": "2019-02-07T10:40:59.0143422-06:00",
        "Arrive": "2019-02-07T10:40:59.0143422-06:00",
        "Enroute": "00:00:00.1234567",
        "Stops": 5,
        "Connections": 6,
        "SeatsRemaining": 7,
        "Route": [
          {
            "FlightId": 1,
            "LegOrder": 2,
            "FlightNumber": 3,
            "Origin": {
              "Id": 1,
              "Code": "sample string 2",
              "Name": "sample string 3",
              "Address": {
                "Address": "sample string 1",
                "City": "sample string 2",
                "Province": "sample string 3",
                "Country": 1,
                "PostalCode": "sample string 4"
              },
              "WayOrder": 1
            },
            "Destination": {
              "Id": 1,
              "Code": "sample string 2",
              "Name": "sample string 3",
              "Address": {
                "Address": "sample string 1",
                "City": "sample string 2",
                "Province": "sample string 3",
                "Country": 1,
                "PostalCode": "sample string 4"
              },
              "WayOrder": 1
            },
            "Depart": "2019-02-07T10:40:59.0143422-06:00",
            "Arrive": "2019-02-07T10:40:59.0143422-06:00",
            "SeatsOnboard": 1,
            "SeatsAssigned": 1,
            "SeatsRemaining": 1
          },
          {
            "FlightId": 1,
            "LegOrder": 2,
            "FlightNumber": 3,
            "Origin": {
              "Id": 1,
              "Code": "sample string 2",
              "Name": "sample string 3",
              "Address": {
                "Address": "sample string 1",
                "City": "sample string 2",
                "Province": "sample string 3",
                "Country": 1,
                "PostalCode": "sample string 4"
              },
              "WayOrder": 1
            },
            "Destination": {
              "Id": 1,
              "Code": "sample string 2",
              "Name": "sample string 3",
              "Address": {
                "Address": "sample string 1",
                "City": "sample string 2",
                "Province": "sample string 3",
                "Country": 1,
                "PostalCode": "sample string 4"
              },
              "WayOrder": 1
            },
            "Depart": "2019-02-07T10:40:59.0143422-06:00",
            "Arrive": "2019-02-07T10:40:59.0143422-06:00",
            "SeatsOnboard": 1,
            "SeatsAssigned": 1,
            "SeatsRemaining": 1
          }
        ],
        "Fares": [
          {
            "FareBasisId": 1,
            "Name": "sample string 2",
            "Rule": "sample string 3",
            "Amount": 4.0,
            "Fees": [
              {
                "Name": "sample string 1",
                "Value": 1.0
              },
              {
                "Name": "sample string 1",
                "Value": 1.0
              }
            ],
            "Taxes": [
              {
                "Name": "sample string 1",
                "Value": 1.0
              },
              {
                "Name": "sample string 1",
                "Value": 1.0
              }
            ],
            "Total": 8.0,
            "SubTotal": 6.0
          },
          {
            "FareBasisId": 1,
            "Name": "sample string 2",
            "Rule": "sample string 3",
            "Amount": 4.0,
            "Fees": [
              {
                "Name": "sample string 1",
                "Value": 1.0
              },
              {
                "Name": "sample string 1",
                "Value": 1.0
              }
            ],
            "Taxes": [
              {
                "Name": "sample string 1",
                "Value": 1.0
              },
              {
                "Name": "sample string 1",
                "Value": 1.0
              }
            ],
            "Total": 8.0,
            "SubTotal": 6.0
          }
        ],
        "GroupId": 8,
        "MaximumCheckedBaggageWeight": 9.0,
        "MaximumCarryOnBaggageWeight": 10.0
      },
      {
        "FlightNumber": 1,
        "Origin": {
          "Id": 1,
          "Code": "sample string 2",
          "Name": "sample string 3",
          "Address": {
            "Address": "sample string 1",
            "City": "sample string 2",
            "Province": "sample string 3",
            "Country": 1,
            "PostalCode": "sample string 4"
          },
          "WayOrder": 1
        },
        "Destination": {
          "Id": 1,
          "Code": "sample string 2",
          "Name": "sample string 3",
          "Address": {
            "Address": "sample string 1",
            "City": "sample string 2",
            "Province": "sample string 3",
            "Country": 1,
            "PostalCode": "sample string 4"
          },
          "WayOrder": 1
        },
        "Depart": "2019-02-07T10:40:59.0143422-06:00",
        "Arrive": "2019-02-07T10:40:59.0143422-06:00",
        "Enroute": "00:00:00.1234567",
        "Stops": 5,
        "Connections": 6,
        "SeatsRemaining": 7,
        "Route": [
          {
            "FlightId": 1,
            "LegOrder": 2,
            "FlightNumber": 3,
            "Origin": {
              "Id": 1,
              "Code": "sample string 2",
              "Name": "sample string 3",
              "Address": {
                "Address": "sample string 1",
                "City": "sample string 2",
                "Province": "sample string 3",
                "Country": 1,
                "PostalCode": "sample string 4"
              },
              "WayOrder": 1
            },
            "Destination": {
              "Id": 1,
              "Code": "sample string 2",
              "Name": "sample string 3",
              "Address": {
                "Address": "sample string 1",
                "City": "sample string 2",
                "Province": "sample string 3",
                "Country": 1,
                "PostalCode": "sample string 4"
              },
              "WayOrder": 1
            },
            "Depart": "2019-02-07T10:40:59.0143422-06:00",
            "Arrive": "2019-02-07T10:40:59.0143422-06:00",
            "SeatsOnboard": 1,
            "SeatsAssigned": 1,
            "SeatsRemaining": 1
          },
          {
            "FlightId": 1,
            "LegOrder": 2,
            "FlightNumber": 3,
            "Origin": {
              "Id": 1,
              "Code": "sample string 2",
              "Name": "sample string 3",
              "Address": {
                "Address": "sample string 1",
                "City": "sample string 2",
                "Province": "sample string 3",
                "Country": 1,
                "PostalCode": "sample string 4"
              },
              "WayOrder": 1
            },
            "Destination": {
              "Id": 1,
              "Code": "sample string 2",
              "Name": "sample string 3",
              "Address": {
                "Address": "sample string 1",
                "City": "sample string 2",
                "Province": "sample string 3",
                "Country": 1,
                "PostalCode": "sample string 4"
              },
              "WayOrder": 1
            },
            "Depart": "2019-02-07T10:40:59.0143422-06:00",
            "Arrive": "2019-02-07T10:40:59.0143422-06:00",
            "SeatsOnboard": 1,
            "SeatsAssigned": 1,
            "SeatsRemaining": 1
          }
        ],
        "Fares": [
          {
            "FareBasisId": 1,
            "Name": "sample string 2",
            "Rule": "sample string 3",
            "Amount": 4.0,
            "Fees": [
              {
                "Name": "sample string 1",
                "Value": 1.0
              },
              {
                "Name": "sample string 1",
                "Value": 1.0
              }
            ],
            "Taxes": [
              {
                "Name": "sample string 1",
                "Value": 1.0
              },
              {
                "Name": "sample string 1",
                "Value": 1.0
              }
            ],
            "Total": 8.0,
            "SubTotal": 6.0
          },
          {
            "FareBasisId": 1,
            "Name": "sample string 2",
            "Rule": "sample string 3",
            "Amount": 4.0,
            "Fees": [
              {
                "Name": "sample string 1",
                "Value": 1.0
              },
              {
                "Name": "sample string 1",
                "Value": 1.0
              }
            ],
            "Taxes": [
              {
                "Name": "sample string 1",
                "Value": 1.0
              },
              {
                "Name": "sample string 1",
                "Value": 1.0
              }
            ],
            "Total": 8.0,
            "SubTotal": 6.0
          }
        ],
        "GroupId": 8,
        "MaximumCheckedBaggageWeight": 9.0,
        "MaximumCarryOnBaggageWeight": 10.0
      }
    ]
  }


  export const mockNoResponse = {
    "status": "sample string 1",
    "errors": {
      "sample string 1": "sample string 2",
      "sample string 3": "sample string 4"
    },
    "inbound": [],
    "outbound": []
  }
  