import PassengerData from '../../models/PassengerData';
import BookingData from '../../models/BookingData';
import TicketSelectionState from './components/ticket-selection/models/ticket-selection-state';
import FlightSelectionTriggers from './observable/flight-selection.triggers';

export default interface FlightSelectionProps {
  bookingData: BookingData;
  handlePaginationClick: Function;
  isOneWayTrip: boolean;
  passengerData: PassengerData;
  sessionId: string;
  handleTicketClick: Function;
  triggers: FlightSelectionTriggers;
  outboundTickets: TicketSelectionState;
  inboundTickets: TicketSelectionState;
  handleStepChange: Function;
}
