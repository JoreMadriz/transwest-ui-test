import React from 'react';
import { Grid, Hidden } from '@material-ui/core';

import AirportSelection from './components/airport-selection/airport-selection';
import BookingData from './models/BookingData';
import BookingState from './booking.state';
import BookingProps from './booking.props';
import { BookingObservable, triggers, state } from './booking.observable';
import CheckoutContainer from './components/checkout/checkout.container';
import ContinueButton from './components/continue-button/continue-button';
import DateSelector from './components/date-selector/date-selector';
import FlightSelectionContainer from './components/flight-selection/flight-selection.container';
import LeftPane from './components/left-pane/left-pane';
import PassengerSelection from './components/passenger-selection/passenger-selection';
import PassengerInformation from './components/passenger-information/passenger-information';
import RightPane from './components/right-pane/right-pane';
import Step from './models/Step';
import StepValidationService from './services/step-validation-service/step-validation.service';
import StepsMobileMenu from '../steps-mobile-menu/steps-mobile-menu';
import SectionSpacer from '../../shared/theme/components/section-spacer/section-spacer';
import { SpacerType } from '../../shared/theme/components/section-spacer/SpacerType';
import withObservableStream from '../../shared/react-rx/withObservableStream';

export class Booking extends React.Component<BookingProps, BookingState> {
  private readonly MAX_STEPS = 6;
  private readonly validationService: StepValidationService = new StepValidationService();

  constructor(props: any) {
    super(props);

    this.state = {
      currentStep: Step.AIRPORT_SELECTION,
      isNotStepValid: true
    };
  }
  componentDidMount() {
    this.props.triggers.getSessionId();
    const { initialStep } = this.props;
    this.setState({ currentStep: initialStep });
  }

  componentDidUpdate() {
    this.validateCurrentState();
  }

  private validateCurrentState() {
    const { currentStep, isNotStepValid } = { ...this.state };
    const isStepValid: boolean = this.validationService.getValidation(currentStep)(this.props);

    if (isNotStepValid !== !isStepValid) {
      this.setState({
        isNotStepValid: !isStepValid
      });
    }
  }

  private handleContinue = async () => {
    const { currentStep } = { ...this.state };
    const newState = { ...this.state };

    if (currentStep < this.MAX_STEPS) {
      newState.currentStep += 1;
      newState.isNotStepValid = false;
      await this.setState(newState);
    }

    if (currentStep === Step.FLIGHT_SELECTION) {
      this.props.triggers.reserveSeats();
    }
    if (currentStep === Step.CHECKOUT) {
      //todo PayNow
      this.props.triggers.onPayNow();
    }
  };

  private handleStepMenuClick = (newtStep: Step) => {
    const { currentStep } = this.state;
    if (currentStep > newtStep) {
      this.setState({
        currentStep: newtStep
      });
    }
  };

  private handleBackToStartClick = () => {
    this.setState({
      currentStep: Step.AIRPORT_SELECTION,
      isNotStepValid: true
    });
  };

  private handleStepChange = (newStep: Step) => {
    this.setState({
      currentStep: newStep
    });
  };

  private getPrice = (): number => {
    const { flightData, airportSelectionData } = this.props;
    const { isOneWayTrip } = airportSelectionData;
    const { inboundTicket, outboundTicket } = flightData;
    const price = isOneWayTrip ? outboundTicket.price : outboundTicket.price + inboundTicket.price;
    return price;
  };

  private getBookingData = () => {
    const BookingData: BookingData = {
      airportSelection: this.props.airportSelectionData,
      passengerSelection: this.props.passengerData,
      dateSelection: this.props.bookingDatesInfo,
      flightSelection: this.props.flightData
    };
    return BookingData;
  };

  public render() {
    const { currentStep, isNotStepValid } = this.state;
    const {
      bookingDatesInfo,
      airportSelectionData,
      passengerData,
      triggers,
      sessionId,
      passengerInfo,
      checkoutInfo
    } = this.props;
    const {
      onChangeAirportSelection,
      onChangePassengersQuantity,
      onDateClickSelection,
      onDatePaginationClick,
      onTicketClick,
      onChangeCardInfo,
      onChangeBillingInfo,
      onChangeTerms,
      getCalendarPrices
    } = triggers;
    const { isOneWayTrip } = airportSelectionData;
    const price = this.getPrice();
    return (
      <>
        <Grid direction="row" container={true} justify="flex-start">
          <Hidden mdUp>
            <StepsMobileMenu
              airportSelectionData={airportSelectionData}
              bookingDates={bookingDatesInfo}
              handleStepMenuClick={this.handleStepMenuClick}
              passengersQuantity={passengerData.totalPassengers}
            />
          </Hidden>

          <LeftPane>
            {currentStep === Step.AIRPORT_SELECTION && (
              <AirportSelection
                airportSelectionData={airportSelectionData}
                onChangeAirportSelection={onChangeAirportSelection}
              />
            )}
            {currentStep === Step.PASSENGERS_SELECTION && (
              <PassengerSelection
                onChangePassengersQuantity={onChangePassengersQuantity}
                passengerData={passengerData}
              />
            )}
            {currentStep === Step.DATE_SELECTION && !!bookingDatesInfo && (
              <DateSelector
                airportInfo={airportSelectionData}
                bookingInfo={bookingDatesInfo}
                handleDateClick={onDateClickSelection}
                isOneWayTrip={isOneWayTrip}
                onGetPrices={getCalendarPrices}
              />
            )}
            {currentStep === Step.FLIGHT_SELECTION && (
              <FlightSelectionContainer
                bookingData={this.getBookingData()}
                handleTicketClick={onTicketClick}
                handleStepChange={this.handleStepChange}
                handlePaginationClick={onDatePaginationClick}
                isOneWayTrip={isOneWayTrip}
                passengerData={passengerData}
                sessionId={sessionId}
              />
            )}
            {currentStep === Step.PASSENGER_INFORMATION && (
              <PassengerInformation
                handleBackToStartClick={this.handleStepChange}
                currentStep={currentStep}
                passengerInfo={passengerInfo}
                triggers={triggers}
              />
            )}
            {currentStep === Step.CHECKOUT && (
              <CheckoutContainer
                checkoutInfo={checkoutInfo}
                handleStepChange={this.handleStepChange}
                onChangeCardInfo={onChangeCardInfo}
                onChangeBillingInfo={onChangeBillingInfo}
                onChangeTerms={onChangeTerms}
              />
            )}
          </LeftPane>
          <Hidden smDown>
            <RightPane
              bookingData={this.getBookingData()}
              currentStep={currentStep}
              enabled={isNotStepValid}
              handleContinue={this.handleContinue}
              handleBackToStartClick={this.handleBackToStartClick}
              handleStepMenuClick={this.handleStepMenuClick}
              isOneWayTrip={isOneWayTrip}
              price={price}
            />
          </Hidden>
        </Grid>
        <Hidden mdUp>
          <SectionSpacer spacerType={SpacerType.LARGE} />
          <SectionSpacer spacerType={SpacerType.LARGE} />
          <ContinueButton
            currentStep={currentStep}
            enabled={isNotStepValid}
            handleContinue={this.handleContinue}
            price={price}
          />
        </Hidden>
      </>
    );
  }
}

export default withObservableStream(BookingObservable, triggers, state)(Booking);
