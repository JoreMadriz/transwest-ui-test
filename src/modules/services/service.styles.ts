import { MARGIN_5, MARGIN_12 } from '../../shared/theme/gutters';

export const styles = (theme: any) => ({
  title: {
    marginLeft: MARGIN_5,
    textAlign: 'left' as 'left',
    fontSize: '30px',
    [theme.breakpoints.down('xs')]: {
      flexDirection: 'column-reverse' as 'column-reverse'
    },
    flexDirection: 'row' as 'row'
  },
  margins: {
    marginRight: MARGIN_12,
    [theme.breakpoints.down('xs')]: {
      marginRight: '8vw'
    }
  }
});
export default styles;
