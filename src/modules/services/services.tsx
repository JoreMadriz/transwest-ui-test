import { Grid, Hidden, withStyles, Typography } from '@material-ui/core';
import React from 'react';

import MediaCard from './components/mediaCard/mediaCard';
import notFoundImage from '../../shared/assets/images/notfound.png';
import SectionBreadCrumb from '../booking/components/section-breadcrumb/section-breadcrumb';
import SectionSpacer from '../../shared/theme/components/section-spacer/section-spacer';
import ServicesProps from './services.props';
import ServicesTemplate from './components/template/services-template';
import { servicesUris, servicesData, servicesUrls } from './utils/servicesData';
import SideMenu from '../../shared/components/side-menu/side-menu';
import { SpacerType } from '../../shared/theme/components/section-spacer/SpacerType';
import styles from './service.styles';

interface ServicesState {
  selectedUri: string;
}

class Services extends React.Component<ServicesProps, ServicesState> {
  private static readonly TITLE: string = 'Services';
  private static readonly MAINPAGE = '/';
  private static readonly BASEURL = '/services/';

  constructor(props: ServicesProps) {
    super(props);
    this.state = {
      selectedUri: Services.MAINPAGE
    };
  }

  componentDidUpdate() {
    const { page } = this.props.match.params;

    if (!page && this.state.selectedUri !== Services.MAINPAGE) {
      this.setState({ selectedUri: Services.MAINPAGE });
    } else if (`${Services.BASEURL}${page}` !== this.state.selectedUri) {
      const dataFromUrl = servicesUrls[page];
      if (dataFromUrl) {
        this.setState({ selectedUri: dataFromUrl.uri });
      }
    }
  }

  componentDidMount() {
    const { page } = this.props.match.params;
    const dataFromUrl = servicesUrls[page];
    if (dataFromUrl) {
      this.setState({ selectedUri: dataFromUrl.uri });
    }
  }

  private selectServiceElem = (uri: string): any => {
    this.props.history.push(uri);
    return async () => {
      await this.setState({
        ...this.state,
        selectedUri: uri
      });
    };
  };

  private renderCardItems = () => {
    return servicesUris.map((item) => {
      const { name, uri } = item;
      return (
        <Grid key={name} item xs={6}>
          <MediaCard
            title={name}
            image={notFoundImage}
            handleClick={() => {
              return this.selectServiceElem(uri);
            }}
          />
          <SectionSpacer spacerType={SpacerType.MEDIUM} />
        </Grid>
      );
    });
  };

  render() {
    const { selectedUri } = this.state;
    const { classes } = this.props;
    const { title, margins } = classes;
    let isMainPage = selectedUri === Services.MAINPAGE;

    let serviceName = selectedUri.replace('/services/', '');

    return (
      <>
        {isMainPage ? (
          <>
            <SectionSpacer spacerType={SpacerType.LARGE} />
            <Typography className={title}>{Services.TITLE}</Typography>
          </>
        ) : (
          <>
            <SectionSpacer spacerType={SpacerType.MEDIUM} />
            <Hidden xsDown>
              <Typography className={title}>{Services.TITLE}</Typography>
            </Hidden>
            <Hidden smUp>
              <SectionBreadCrumb
                isServices={true}
                handleStepChange={() => {
                  return this.selectServiceElem(Services.BASEURL);
                }}
              >
                All services
              </SectionBreadCrumb>
            </Hidden>
          </>
        )}

        <SectionSpacer spacerType={SpacerType.MEDIUM} />
        <Grid className={title} container direction={'row'}>
          <Grid className={margins} item xs={12} sm={3}>
            <SideMenu
              handleItemClick={this.selectServiceElem}
              menuItems={servicesUris}
              selectedUri={selectedUri}
            />
          </Grid>
          <Grid container item md={8} sm={7} direction={'column'}>
            {isMainPage ? (
              <Hidden xsDown>
                <Grid item container justify={'flex-start'}>
                  {this.renderCardItems()}
                </Grid>
              </Hidden>
            ) : (
              <Grid item container justify={'flex-start'}>
                <Grid item>
                  <ServicesTemplate
                    eventHandler={this.selectServiceElem}
                    serviceData={servicesData[serviceName]}
                  />
                </Grid>
              </Grid>
            )}
          </Grid>
        </Grid>
        <SectionSpacer spacerType={SpacerType.MEDIUM} />
      </>
    );
  }
}

export default withStyles(styles)(Services);
