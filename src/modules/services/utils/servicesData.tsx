import { adsbService } from '../models/adsb';
import { avionicService } from '../models/avionics';
import { cargoRates } from '../models/cargoRates';
import { cargoService } from '../models/cargo';
import { charterService } from '../models/charters';
import { helicopterService } from '../models/helicopters';
import MenuItem from '../../../shared/models/menu-Item';

export interface ServiceData {
  title: string;
  image: string;
  content: string;
  rightPane?: string;
}

export const servicesUrls: any = {
  charters: { name: 'Charters', uri: '/services/charters' },
  helicopters: { name: 'Helicopter Services', uri: '/services/helicopters' },
  avionics: { name: 'Avionics', uri: '/services/avionics' },
  cargo: { name: 'Cargo', uri: '/services/cargo' },
  cargo_rates: { name: 'Cargo Rates', uri: '/services/cargo_rates' },
  adsb: { name: 'ADS-B', uri: '/services/adsb' }
};

export const servicesUris: MenuItem[] = [
  servicesUrls['charters'],
  servicesUrls['helicopters'],
  servicesUrls['avionics'],
  servicesUrls['cargo'],
  servicesUrls['adsb']
];

export const servicesData: any = {
  charters: charterService,
  helicopters: helicopterService,
  avionics: avionicService,
  cargo: cargoService,
  cargo_rates: cargoRates,
  adsb: adsbService
};
