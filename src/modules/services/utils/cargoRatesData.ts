const getDestinationJSON = (name: string, guarenteedPrice: number, regularPrice: number) => {
  return {
    name: name,
    Guarenteed: `$${guarenteedPrice.toFixed(2)}`,
    Regular: `$${regularPrice.toFixed(2)}`
  };
};

export const cargoRatesFromPlace = [
  {
    origin: 'Saskatoon',
    number: '(306) 665-2700',
    destination: [
      getDestinationJSON('Prince Albert', 0, 0),
      getDestinationJSON('La Ronge', 0, 0),
      getDestinationJSON('Stony Rapids', 0, 0),
      getDestinationJSON('Fond du Lac', 0, 0),
      getDestinationJSON('Points North', 0, 0),
      getDestinationJSON('Uranium City', 0, 0)
    ]
  },
  {
    origin: 'Prince Albert',
    number: '(306) 764-1428',
    destination: [
      getDestinationJSON('Saskatoon', 0, 0),
      getDestinationJSON('La Ronge', 0, 0),
      getDestinationJSON('Stony Rapids', 0, 0),
      getDestinationJSON('Fond du Lac', 0, 0),
      getDestinationJSON('Points North', 0, 0),
      getDestinationJSON('Uranium City', 0, 0)
    ]
  },
  {
    origin: 'La Ronge',
    number: '(306) 425-2774',
    destination: [
      getDestinationJSON('Saskatoon', 0, 0),
      getDestinationJSON('Prince Albert', 0, 0),
      getDestinationJSON('Stony Rapids', 0, 0),
      getDestinationJSON('Fond du Lac', 0, 0),
      getDestinationJSON('Points North', 0, 0),
      getDestinationJSON('Uranium City', 0, 0)
    ]
  },
  {
    origin: 'Stony Rapids',
    number: '(306) 439-2040',
    destination: [
      getDestinationJSON('Prince Albert', 0, 0),
      getDestinationJSON('La Ronge', 0, 0),
      getDestinationJSON('Stony Rapids', 0, 0),
      getDestinationJSON('Fond du Lac', 0, 0),
      getDestinationJSON('Points North', 0, 0),
      getDestinationJSON('Uranium City', 0, 0)
    ]
  },
  {
    origin: 'Fond du Lac',
    number: '(306) 665-2700',
    destination: [
      getDestinationJSON('Saskatoon', 0, 0),
      getDestinationJSON('Prince Albert', 0, 0),
      getDestinationJSON('La Ronge', 0, 0),
      getDestinationJSON('Stony Rapids', 0, 0),
      getDestinationJSON('Points North', 0, 0),
      getDestinationJSON('Uranium City', 0, 0)
    ]
  },
  {
    origin: 'Wollaston',
    number: '(306) 686-2147',
    destination: [
      getDestinationJSON('Saskatoon', 0, 0),
      getDestinationJSON('Prince Albert', 0, 0),
      getDestinationJSON('La Ronge', 0, 0),
      getDestinationJSON('Stony Rapids', 0, 0),
      getDestinationJSON('Fond du Lac', 0, 0),
      getDestinationJSON('Uranium City', 0, 0)
    ]
  },
  {
    origin: 'Points North',
    number: '(306) 633-2022',
    destination: [
      getDestinationJSON('Saskatoon', 0, 0),
      getDestinationJSON('Prince Albert', 0, 0),
      getDestinationJSON('La Ronge', 0, 0),
      getDestinationJSON('Stony Rapids', 0, 0),
      getDestinationJSON('Fond du Lac', 0, 0),
      getDestinationJSON('Uranium City', 0, 0)
    ]
  },
  {
    origin: 'Uranium City',
    number: '(306) 633-2022',
    destination: [
      getDestinationJSON('Saskatoon', 0, 0),
      getDestinationJSON('Prince Albert', 0, 0),
      getDestinationJSON('La Ronge', 0, 0),
      getDestinationJSON('Stony Rapids', 0, 0),
      getDestinationJSON('Fond du Lac', 0, 0),
      getDestinationJSON('Points North', 0, 0)
    ]
  }
];
