import notFoundImage from '../../../shared/assets/images/notfound.png';
import { PRIMARY_COLOR } from '../../../shared/theme/colors';

const HELICOPTERURL = 'http://www.NorthernShieldHelicopters.ca';

export const helicopterService = {
  title: 'Helicopter Services',
  image: notFoundImage,
  rightPane: `
<p><span style="text-decoration: underline;"><strong>CHARTER REQUEST FORM (NH). </strong></span></br>
We will respond to charter pricing requests within twenty four hours of receipt during business hours.</p>

<p>If you require immediate charter, call toll free <br />
1-877-989-2677 24 hours a day.</p>
`,
  content: `
<p>Northern Shield Helicopters has been providing helicopter services to 
Saskatchewan for over 50 years. We started with a Bell 47G, and have 
maintained a Bell fleet ever since. We currently operate two Bell 206 
JetRangers, one Bell 206LIV Long Ranger, two Bell 205 A1+&rsquo;s and three 
Bell 407&rsquo;s. We pride ourselves on offering the best flight crews and 
engineers who can meet your needs safely, efficiently, and with an 
emphasis on impeccable customer service.</p>

<p>Our 24 hour Dispatch team will work directly with our pilots to ensure 
that your satisfaction is met on every trip.</p>

<p>For more information on our helicopter services please visit 
<a href=${HELICOPTERURL} style="text-decoration: none; color:${PRIMARY_COLOR}">NorthernShieldHelicopters.ca/</a></p>

<p>Among the services we provide with our helicopter fleet are:<br />
Forest fire suppression <br />
Geo-technical and drill rig support <br />
Urban construction <br />
Power line and pipeline patrol <br />
Med-evac <br />
Passenger transport <br />
Crew changes in remote locations <br />
Sightseeing rides <br />
Aerial photography and cinematography <br />
General cargo slinging</p>
`
};
