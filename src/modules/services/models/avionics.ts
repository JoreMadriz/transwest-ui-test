import notFoundImage from '../../../shared/assets/images/notfound.png';
import { PRIMARY_COLOR } from '../../../shared/theme/colors';

const CONTACTUSROUTE = '#';

export const avionicService = {
  title: 'Avionics',
  image: notFoundImage,
  rightPane: `
<p>For more information on avionic services, contact the Avionics Shop Manager in Prince Albert at:</p>
<p><span style="text-decoration: underline;">Prince Albert: 764-1408/1404 </span></p>
<p>Or use the <a href=${CONTACTUSROUTE} style="text-decoration: underline; color:${PRIMARY_COLOR}">Contact Us Form.</a></p>
`,
  content: `
<p>Transwest Air is located at the Prince Albert Municipal Airport in Prince 
Albert, Saskatchewan and operates out of a hangar located at the airport.</p>

<p>Hangar space is provided to clients for avionics services or regular 
maintenance.</p>

<p>Over the years, Transwest Air avionics department has built a solid 
relationship and met the needs of a perse clientele. Since 1976, our 
avionics department has become known for outstanding service in 
avionics installation, retrofit and modification; component repair and 
overhaul, and troubleshooting on aircraft.</p>

<p>Our strong commitment to customer service, quality assurance and cost 
saving gives our clients a high degree of trust in our unparalleled 
expertise and reliable results.
</p>`
};
