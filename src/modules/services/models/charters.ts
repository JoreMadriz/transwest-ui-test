import notFoundImage from '../../../shared/assets/images/notfound.png';
import { PRIMARY_COLOR } from '../../../shared/theme/colors';

const LOGINROUTE = '#';
const LOCALINTLROUTE = '#';
const CHARTERTARIFFROUTE = '#';

export const charterService = {
  title: 'Charters',
  image: notFoundImage,
  rightPane: `
<p>If you need to charter an aircraft immediately, or 
require other information, please call one of the 
bases nearest you:</p>

<p>Prince Albert: (306) 764-1408/1404</br>
La Ronge: (306) 425-2382</br>
Stony Rapids: (306) 439-2040</br>
Wollaston: (306) 633-2022</br>
Lynn Lake, Manitoba: (204) 356-2457</br>
Or call toll free 1-877-989-2677 24 hours a day.</p>

<p><a href=${LOGINROUTE} style="text-decoration: underline;color:${PRIMARY_COLOR}"><strong>CHARTER LOG-IN </strong></a></br>
<a href=${LOCALINTLROUTE} style="text-decoration: underline;color:${PRIMARY_COLOR}"><strong>LOCAL INTERNATIONAL </strong></a></br>
<a href=${CHARTERTARIFFROUTE} style="text-decoration: underline;color:${PRIMARY_COLOR}"><strong>CHARTER TARIFF (PDF)</strong></a></p>
`,
  content: `
<p>TRANSWEST AIR HAS THE LARGEST FLEET OF FIXED WING AND ROTARY CHARTER 
AIRCRAFT IN SASKATCHEWAN, including float, cargo, med-evac and corporate travel aircraft. 
We also have a charter base located in Lynn Lake, Manitoba.</p>

<p>Our charter services are used for surveying, construction, employee transport, medical 
evacuation, corporate travel, line patrol, cargo movement, sight seeing, filming and much more.</p>

<p>Whether you have flown with us before, or are looking to book your first trip, our experienced 
Dispatch team will be happy to walk you through the process to find the aircraft that best suits 
your needs. We can also provide commissary onboard the flight, as well as customer parking at 
our hangars.</p>
`
};
