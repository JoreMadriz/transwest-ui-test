import notFoundImage from '../../../shared/assets/images/notfound.png';

export const cargoService = {
  title: 'Cargo',
  image: notFoundImage,
  content: `
<p>Transwest Air offers freight services to all the communities we serve.
SASKATOON FREIGHT DROP-OFF LOCATION </br>
3B Hangar Rd, J.G. Diefenbaker Airport, Saskatoon Saskachewan. </p>
<Click here for Freight Rates>
</br>

<p>PET ACCEPTANCE:</br>
TRANSWEST ACCEPTS PETS FOR TRAVEL AS CHECKED BAGGAGE 
TRAVELLING WITH A PASSENGER ONLY.</br>
All animals travel in the cargo hold of the aircraft, (the SAAB aircraft cargo 
area is the same temperature as the cabin area.) Due to the limited space 
in the cabins of our aircraft and the possibility of allergic reactions, animals 
will not be transported in the cabin area, with the exception of service 
animals that assist customers with a disability.</p>

<p>All animals must be in a hard-sided approved kennel</br>
Must have a leak proof bottom kennel</br>
Animal must be able to stand up and turn around comfortably</br>
Must not be shipped on any aircraft that is transporting dry ice</p>
</br>

<p>ACCEPTING PET AS BAGGAGE:</br>
Notice must be given when reservation is made that passenger is 
traveling with a pet (including type and size of pet as well as size of 
kennel)</p>

<p>Notes stating pet travelling must be entered in the reservation. The animal 
must be travelling on the same flight as passenger, and must check in at 
least 1 hour prior to departure with the pet. The animal is counted as the 
first piece of luggage, excess will be charged on other items if exceeding 
the 75 lb. limit. Handling fee of $20.00 plus GST is charged to the 
passenger and must be prepaid.</p>
`
};
