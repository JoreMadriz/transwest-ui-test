import notFoundImage from '../../../shared/assets/images/notfound.png';
import { PRIMARY_COLOR } from '../../../shared/theme/colors';

const FREIGHTRATES = '#';

export const cargoRates = {
  title: 'Cargo Rates',
  image: notFoundImage,

  content: `
<p>*Prices are quoted per pound. </br>
*Fuel surcharge 25% will be added to each waybill fare</p>

<p>Minimum charge on envelopes $10.00, boxes $15.00 plus tax. </br>
Live Animal Fee $20.00 </br>
Dangerous Goods Handling Charge $25.00 </br>
Bicycle Fee $25.00 </br>
Perishables / Frozen Goods $20.00 </br>
Rates are subject to change without notice.
<p>

<p><a href=${FREIGHTRATES} style="text-decoration: underline; color:${PRIMARY_COLOR}"><strong>Download Freight Rates (PDF)</strong> </a></p>

<p>*Guaranteed: delivered same day or next available flight.<br/>
*Regular: delivered within 5 business days.</p>
<p><strong>To book cargo service, please call the numbers below.</strong></p>
`
};
