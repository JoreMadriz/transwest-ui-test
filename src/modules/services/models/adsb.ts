import notFoundImage from '../../../shared/assets/images/notfound.png';
import { PRIMARY_COLOR } from '../../../shared/theme/colors';

const FAASITE = '#';

export const adsbService = {
  title: 'ADS-B',
  image: notFoundImage,
  content: `
<p>We have the People!</br>
We have the Equipment!</br>
We have the Capacity!</br>
Book your ADS-B installation now!</p>

<p>The FAA deadline for ADS-B compliance is fast approching, don&rsquo;t get 
caught up in the crunch and end up having to pay extra or having your 
aircraft grounded till you get your system in.</p>
<p>Over the years, Transwest Air avionics department has built a solid 
relationship and met the needs of a perse clientele. Since 1976, our 
avionics department has become known for outstanding service in 
avionics installation, retrofit and modification; component repair and 
overhaul, and troubleshooting on aircraft. Our Avionics department 
employs six people with a combined experience of over 104 years.</p>

<p>Our strong commitment to customer service, quality assurance and cost 
saving gives our clients a high degree of trust in our unparalleled 
expertise and reliable results.</p>

<p><strong>WHAT IS ADS-B AND WHY DO I NEED IT?</strong></p>

<p><a href=${FAASITE} style="text-decoration: underline; color:${PRIMARY_COLOR}">Please visit the FAA for answers to all questions about the ADS-B 
legislation.</a></p>

<p>To inquire about having your aircraft outfitted fill out the form below.</p>

<p>WE CAN UPGRADE YOUR AIRCRAFT</p>

<p>We have a broad range of aircraft that our AMO allows us to sign off on 
but we can install on any aircraft with final sign-off done by your 
maintenance.</p>

<p>Our AMO approved aircraft list:</br></br>
Bae Jetstream series aeroplanes </br>
Beech 1900 series </br>
Beech 99 series </br>
Beech King Air series </br>
Beech twin piston powered aeroplanes </br>
Cessna single/twin powered aircraft </br>
Cessna twin turbopropeller aircraft </br>
DeHavilland DHC-6 series </br>
DeHavilland single piston powered aircraft </br>
DeHavilland Turbo Beaver aircraft </br>
DeHavilland Turbo Otter aircraft </br>
Piper twin piston powered aircraft </br>
SAAB SF340 series </br>
Mitsubishi MU 2 series aircraft </br>
</br>
Rotary Aircraft</br></br>
Bell 206, 204/205 series</br>
Eurocopter AS350 Astar series</p>
`
};
