export default interface MediaCardProps {
  content?: string;
  image: string;
  title: string;
  classes: any;
  handleClick?: Function;
  url?: string;
  buttonText?: string;
  handleButton?: Function;
  isMainPage?: boolean;
}
