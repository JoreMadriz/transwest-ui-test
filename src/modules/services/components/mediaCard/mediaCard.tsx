import React, { Component } from 'react';
import Card from '@material-ui/core/Card';
import { CardActionArea, CardActions, Button } from '@material-ui/core';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';
import { withStyles } from '@material-ui/core/styles';

import MediaCardProps from './mediaCard.props';
import styles from './mediaCard.style';

class MediaCard extends Component<MediaCardProps> {
  render() {
    const { title, content, image, classes, handleClick, buttonText, handleButton, isMainPage } = this.props;
    const { card, contentClass, media, text, cardButton, mainPageCard } = classes;

    return (
      <Card
        raised={false}
        className={ isMainPage ? mainPageCard : card}
        onClick={() => {
          return handleClick ? handleClick() : null;
        }}
      >
        <CardActionArea>
          <CardMedia component="img" className={media} image={image} />
          <CardContent className={contentClass}>
            <Typography className={text}>{title}</Typography>
            <Typography align="left" component="p">{content}</Typography>
          </CardContent>
        </CardActionArea>
        {buttonText && (
          <CardActions>
            <Button className={cardButton}
              onClick={() => {
                return handleButton ? handleButton() : null;
              }}
            >
              {buttonText}
            </Button>
          </CardActions>
        )}
      </Card>
    );
  }
}

export default withStyles(styles)(MediaCard);
