import { BORDER_COLOR, PRIMARY_COLOR, WHITE } from '../../../../shared/theme/colors';
import { MARGIN_2 } from '../../../../shared/theme/gutters';

export const styles = (theme:any) => ({
  removeUnderline: {
    textDecoration: 'none' as 'none'
  },
  card: {
    fontSize: '12pt',
    maxWidth: '335px',
    border: `1px solid ${BORDER_COLOR}`,
    borderRadius: '0px',
    boxShadow: 'none' as 'none',
    marginRight: MARGIN_2
  },
  mainPageCard: {
    width: '100%',
    height: 'auto' as 'auto',
    fontSize: '12pt',
    [theme.breakpoints.down('md')]: {
      maxWidth: '400px',
    },
    [theme.breakpoints.up('lg')]: {
      maxWidth: '500px',
    },
    border: `1px solid ${BORDER_COLOR}`,
    borderRadius: '0px',
    boxShadow: 'none' as 'none',
  },
  contentClass: {
    height: '103px',
    padding: 0,
    paddingLeft: '25px',
    paddingRight: '10px',
    verticalAlign: 'middle' as 'middle',
    display: 'table-cell' as 'table-cell'
  },
  media: {
    maxHeight: '200px',
    borderBottom: `2px solid ${BORDER_COLOR}`
  },
  text: {
    fontSize: '18pt',
    textAlign: 'left' as 'left'
  },
  cardButton: {
    background: PRIMARY_COLOR,
    color: WHITE,
    marginLeft: '25px',
    fontWeight: 'normal' as 'normal',
    fontSize: '14px',
    textTransform: 'capitalize' as 'capitalize',
    marginBottom: '15px'
  }
});

export default styles;
