import React from 'react';
import { Grid, Typography, withStyles } from '@material-ui/core';

import { cargoRatesFromPlace } from '../../utils/cargoRatesData';
import CustomTable from '../customTable/customTable';
import SectionSpacer from '../../../../shared/theme/components/section-spacer/section-spacer';
import { SpacerType } from '../../../../shared/theme/components/section-spacer/SpacerType';

const styles = () => ({
  table: {
    maxWidth: '100%',
    flexBasis: '100%'
  },
  text: {
    fontSize: '20px'
  }
});

const cargoRates = (props: any): any => {
  const head = ['Destination', 'Guarenteed', 'Regular'];
  const data = cargoRatesFromPlace;
  const { classes } = props;
  const { table, text } = classes;

  return data.map((element, index) => {
    return (
      <Grid key={index} container>
        <Grid item container justify={'space-between'}>
          <Grid item>
            <Typography className={text}> {element.origin} to:</Typography>
          </Grid>
          <Grid item>
            <Typography className={text}>{element.number}</Typography>
          </Grid>
        </Grid>
        <Grid item className={table} xs={11}>
          <CustomTable data={element.destination} head={head} />
          <SectionSpacer spacerType={SpacerType.SMALL} />
        </Grid>
      </Grid>
    );
  });
};

export default withStyles(styles)(cargoRates);
