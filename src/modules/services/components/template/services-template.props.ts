import { ServiceData } from '../../utils/servicesData';

export interface ServicesTemplateProps {
  serviceData: ServiceData;
  eventHandler: Function;
  classes: any;
}
