import { PRIMARY_COLOR } from '../../../../shared/theme/colors';

export const styles = {
  titleStyle: {
    fontSize: '30px',
    textAlign: 'left' as 'left'
  },
  textPane: {
    textAlign: 'left' as 'left',
    fontSize: '14px'
  },
  text: {
    textAlign: 'left' as 'left',
    fontSize: '16px',
    color: PRIMARY_COLOR
  },
  imageStyle: {
    width: '100%'
  }
};
export default styles;
