import React from 'react';
import { Typography, withStyles, Grid } from '@material-ui/core';

import appRoutes from '../../../../shared/routing/routes';
import CargoRatesTables from '../cargo-rates-tables/cargo-rates-tables';
import { Link } from 'react-router-dom';
import SectionSpacer from '../../../../shared/theme/components/section-spacer/section-spacer';
import { ServicesTemplateProps } from './services-template.props';
import { SpacerType } from '../../../../shared/theme/components/section-spacer/SpacerType';
import styles from './services-template.style';

class ServicesTemplate extends React.Component<ServicesTemplateProps> {
  renderParts(content: any) {
    const { classes, eventHandler } = this.props;
    const splitString = 'Click here for Freight Rates';

    let parts = content.split(`<${splitString}>`);
    return parts.length === 1 ? (
      <Typography dangerouslySetInnerHTML={{ __html: content }} className={classes.text} />
    ) : (
      <>
        <Typography dangerouslySetInnerHTML={{ __html: parts[0] }} className={classes.text} />
        <Link
          to={appRoutes.services.path.replace('/:page', '/cargo_rates')}
          className={classes.text}
          onClick={() => {
            eventHandler('cargo_rates');
          }}
        >
          {splitString}
        </Link>
        <Typography dangerouslySetInnerHTML={{ __html: parts[1] }} className={classes.text} />
      </>
    );
  }

  render() {
    const { serviceData, classes } = this.props;
    const { title, image, content, rightPane } = serviceData;
    const { textPane, imageStyle, titleStyle } = classes;
    const isCargoRates = title === 'Cargo Rates';

    return (
      <Grid container>
        <Typography className={titleStyle}>{title}</Typography>
        <SectionSpacer spacerType={SpacerType.SMALL} />
        <Grid container direction={'row'} justify={'space-between'}>
          <Grid item md={7} xs={11}>
            {!isCargoRates && <img className={imageStyle} src={image} />}
            <Grid item container>
              {this.renderParts(content)}
              <SectionSpacer spacerType={SpacerType.SMALL} />
              {isCargoRates && <CargoRatesTables />}
            </Grid>
          </Grid>

          {rightPane && (
            <Grid item md={4} xs={11}>
              <Typography dangerouslySetInnerHTML={{ __html: rightPane }} className={textPane} />
            </Grid>
          )}
        </Grid>
      </Grid>
    );
  }
}

export default withStyles(styles)(ServicesTemplate);
