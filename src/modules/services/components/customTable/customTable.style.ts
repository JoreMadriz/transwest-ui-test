import { BACKGROUND_COLOR1, PRIMARY_COLOR } from '../../../../shared/theme/colors';
import { PADDING_3, PADDING_0 } from '../../../../shared/theme/gutters';

export const styles = {
  boldText: {
    fontWeight: 'bold' as 'bold'
  },
  tableHead: {
    color: PRIMARY_COLOR,
    backgroundColor: BACKGROUND_COLOR1,
    fontSize: '14px',
    padding: `${PADDING_0}px ${PADDING_0}px ${PADDING_0}px ${PADDING_3}px`
  },
  table: {
    border: `1px solid ${BACKGROUND_COLOR1}`
  },
  border: {
    borderBottom: `1px solid ${BACKGROUND_COLOR1}`,
    fontSize: '14px',
    whiteSpace: 'nowrap' as 'nowrap',
    padding: `${PADDING_0}px ${PADDING_0}px ${PADDING_0}px ${PADDING_3}px`
  }
};

export default styles;
