export default interface CustomTableProps {
  classes: any;
  eventHandler?: any;
  buttonPath?: string;
  data: any;
  head: Array<string>;
}
