import React, { ReactNode } from 'react';
import { Table, TableHead, TableRow, TableCell, TableBody, withStyles } from '@material-ui/core';
import CustomTableProps from './customTable.props';
import style from './customTable.style';

class CustomTable extends React.Component<CustomTableProps> {
  renderHead = (head: Array<string>, classes: any, cells: any = []) => {
    const { tableHead, border, boldText } = classes;
    return (
      <TableRow>
        {head.map((element) => {
          return (
            <TableCell className={`${tableHead} ${border} ${boldText}`} key={element}>
              {element}
            </TableCell>
          );
        })}
      </TableRow>
    );
  };

  renderBody = (data: Array<any>, classes: any): ReactNode => {
    const { tableHead, border } = classes;

    return data.map((row: any, index: number) => {
      return (
        <TableRow key={index} className={index % 2 != 0 ? tableHead : ''}>
          {Object.keys(row).map((element: any, routeIndex: number) => {
            return (
              <TableCell className={border} key={row.name + index + routeIndex}>
                {
                  //@ts-ignore
                  row[element]
                }
              </TableCell>
            );
          })}
        </TableRow>
      );
    });
  };

  render() {
    const { head, data, classes } = this.props;
    const { tableHead, table, border, boldText } = classes;
    return (
      <Table className={table}>
        <TableHead className={tableHead}>
          {this.renderHead(head, { tableHead, border, boldText })}
        </TableHead>
        <TableBody>{this.renderBody(data, { tableHead, border })}</TableBody>
      </Table>
    );
  }
}

export default withStyles(style)(CustomTable);
