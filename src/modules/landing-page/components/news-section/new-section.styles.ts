import { PADDING_4 } from '../../../../shared/theme/gutters';

const styles = (theme: any) => {
  return {
    image: {
      width: '100%',
      height: 'auto' as 'auto'
    },
    titleText: {
      lineHeight: 'initial' as 'initial',
      fontSize: '30px',
      [theme.breakpoints.down('xs')]: {
        fontSize: '6vw'
      },
      [theme.breakpoints.only('md')]: {
        fontSize: '2vw'
      }
    },
    bodyText: {
      fontSize: '20px',
      [theme.breakpoints.down('xs')]: {
        fontSize: '4vw'
      },
      [theme.breakpoints.only('md')]: {
        fontSize: '1.5vw'
      }
    },
    textBox: {
      [theme.breakpoints.up('md')]: {
        padding: PADDING_4
      },
      [theme.breakpoints.down('xs')]: {
        paddingTop: PADDING_4
      }
    }
  };
};

export default styles;
