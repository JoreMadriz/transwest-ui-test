import React from 'react';
import { Grid, withStyles, Typography } from '@material-ui/core';

import imagePlaceholder from '../../../../shared/assets/images/placeholder.png';
import styles from './new-section.styles';
interface NewsSectionProps {
  classes: any;
  title: string;
  body: string;
}

const NewsSection = (props: NewsSectionProps) => {
  const { title, body, classes } = props;
  const { image, titleText, bodyText, textBox } = classes;
  return (
    <Grid container>
      <Grid item md={6} xs={12}>
        <img className={image} src={imagePlaceholder} />
      </Grid>
      <Grid className={textBox} item md={4} xs={12}>
        <Typography align="left" className={titleText}>
          {title}
        </Typography>

        <Typography align="left" className={bodyText}>
          {body}
        </Typography>
      </Grid>
    </Grid>
  );
};

export default withStyles(styles)(NewsSection);
