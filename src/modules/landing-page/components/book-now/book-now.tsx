import React from 'react';
import { Grid, Typography, withStyles, Hidden } from '@material-ui/core';

import CustomButton from '../../../../shared/components/custom-button/custom-button';
import DropdownList from '../dropdown-list/dropdown-list';
import SectionSpacer from '../../../../shared/theme/components/section-spacer/section-spacer';
import { SpacerType } from '../../../../shared/theme/components/section-spacer/SpacerType';
import styles from './book-now.style';
import { ToggleButton } from '../../../booking/components/airport-selection/components/toggle-button/toggle-button';

export interface BookNowProps {
  classes: any;
}

export interface BookNowState {
  isRoundTrip: boolean;
}

class BookNow extends React.Component<BookNowProps, BookNowState> {
  constructor(props: BookNowProps) {
    super(props);
    this.state = {
      isRoundTrip: true
    };
  }

  private handleOnToggleClick = () => {
    const isRoundTrip: boolean = this.state.isRoundTrip;
    this.setState({
      ...this.state,
      isRoundTrip: !isRoundTrip
    });
  };

  private readonly PASSENGER_HELPER_TEXT: string =
    'Infants under the age of 2 travel free when accompanied by an adult';
  private readonly BUTTON_TEXT: string = 'Find Flights';
  render() {
    const { classes } = this.props;
    const { isRoundTrip } = this.state;
    const { box, button, helperText } = classes;
    return (
      <>
        <SectionSpacer spacerType={SpacerType.TINY} />
        <Typography align="left" variant="h4">
          Book Now!
        </Typography>

        <Hidden mdUp>
          <ToggleButton
            classes={{}}
            handleToggleClick={() => {
              this.handleOnToggleClick();
            }}
            isRoundTrip={isRoundTrip}
          />
        </Hidden>

        <Grid container>
          <Grid md={4} xs={12} item>
            <DropdownList className={box} title={'Departure Location '} />
          </Grid>
          <Grid md={4} xs={12} item>
            <DropdownList className={box} title={'Arrival Location'} />
          </Grid>
          <Grid md={2} xs={12} item>
            <DropdownList title={'Passengers'} />
            <Hidden mdUp>
              <Grid md={4} xs={12} item>
                <Typography className={helperText}>{this.PASSENGER_HELPER_TEXT}</Typography>
              </Grid>
            </Hidden>
          </Grid>
          <Grid className={button} md={2} xs={12} item>
            <CustomButton text={this.BUTTON_TEXT} onClickAction={() => {}} />
          </Grid>
        </Grid>

        <Grid container>
          <Grid md={4} xs={12} item>
            <Typography>{''}</Typography>
          </Grid>
          <Grid md={4} xs={12} item>
            <Typography>{''}</Typography>
          </Grid>
          <Hidden smDown>
            <Grid md={2} xs={12} item>
              <Typography className={helperText}>{this.PASSENGER_HELPER_TEXT}</Typography>
            </Grid>
          </Hidden>
          <Grid md={2} xs={12} item>
            <Typography>{''}</Typography>
          </Grid>
        </Grid>

        <Hidden mdDown>
          <ToggleButton
            classes={{}}
            handleToggleClick={() => {
              this.handleOnToggleClick();
            }}
            isRoundTrip={isRoundTrip}
          />
        </Hidden>
      </>
    );
  }
}

export default withStyles(styles)(BookNow);
