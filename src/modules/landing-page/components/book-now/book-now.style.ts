import { MARGIN_4, MARGIN_3 } from '../../../../shared/theme/gutters';

const styles = (theme: any) => {
  return {
    box: {
      marginRight: `${MARGIN_3}px`
    },
    button: {
      margin: 'auto 0',
      [theme.breakpoints.down('xs')]: {
        margin: '0 auto'
      },
      '& button': {
        marginTop: `${MARGIN_4}px`
      }
    },
    helperText: {
      fontSize: '1hv'
    }
  };
};

export default styles;
