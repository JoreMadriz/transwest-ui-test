import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';

import styles from './dropdown-list.styles';

interface DropdownListProps {
  classes: any;
  title: string;
}

interface DropdownListState {
  age: string;
  name: string;
  labelWidth: number;
}

class DropdownList extends React.Component<DropdownListProps, DropdownListState> {
  constructor(props: any) {
    super(props);

    this.state = {
      age: '',
      name: 'hai',
      labelWidth: 0
    };
  }

  handleChange = (event: any) => {
    this.setState({
      ...this.state,
      [event.target.name]: event.target.value
    });
  };

  render() {
    const { classes, title } = this.props;
    const { root, formControl, selectEmpty, labels } = classes;

    return (
      <form className={root} autoComplete="off">
        <FormControl className={formControl}>
          <Select
            disableUnderline={true}
            value={this.state.age}
            onChange={this.handleChange}
            displayEmpty
            name="age"
            className={selectEmpty}
          >
            <MenuItem value="">
              <em className={labels}>{title}</em>
            </MenuItem>
            <MenuItem value={10}>Ten</MenuItem>
            <MenuItem value={20}>Twenty</MenuItem>
            <MenuItem value={30}>Thirty</MenuItem>
          </Select>
        </FormControl>
      </form>
    );
  }
}

export default withStyles(styles)(DropdownList);
