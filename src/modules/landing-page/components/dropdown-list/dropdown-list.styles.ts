import { BORDER_COLOR, PRIMARY_COLOR } from '../../../../shared/theme/colors';

const styles = (theme: any) => ({
  root: {
    display: 'flex' as 'flex',
    flexWrap: 'wrap' as 'wrap'
  },
  formControl: {
    margin: theme.spacing.unit,
    width: '100%'
  },
  selectEmpty: {
    marginTop: theme.spacing.unit * 2,
    border: `1px solid ${BORDER_COLOR}`,
    padding: '10px',
    textDecoration: 'none' as 'none',
    textAlign: 'left' as 'left'
  },
  helperTextBox: {
    color: PRIMARY_COLOR,
    maxWidth: '196px'
  },
  labels: {
    fontStyle: 'normal' as 'normal'
  }
});

export default styles;
