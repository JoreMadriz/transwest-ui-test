import React from 'react';
import { Grid, withStyles } from '@material-ui/core';
import MediaCard from '../../../services/components/mediaCard/mediaCard';
import imagePlaceholder from '../../../../shared/assets/images/placeholder.png';
import style from './card-section.style';

interface CardSectionProps {
  classes: any;
}
class CardSection extends React.Component<CardSectionProps> {
  render() {
    const { cardContainer, card } = this.props.classes;
    return (
      <>
        <Grid container direction="row" className={cardContainer} justify="space-between">
          <Grid className={card} item xl={4} md={3} xs={12} container justify="center" >
            <MediaCard
              title="Car Rentals for you"
              image={imagePlaceholder}
              content="Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempo."
              buttonText="Find Flights"
              isMainPage={true}
            />
          </Grid>
          <Grid className={card} item xl={4} md={3} xs={12} container justify="center">
            <MediaCard
              title="Hotels for you"
              image={imagePlaceholder}
              content="Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempo."
              buttonText="More info"
              isMainPage={true}
            />
          </Grid>
          <Grid className={card} item xl={4} md={3} xs={12} container justify="center">
            <MediaCard
              title="Vacations for you"
              image={imagePlaceholder}
              content="Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempo."
              buttonText="More info"
              isMainPage={true}
            />
          </Grid>
        </Grid>
      </>
    );
  }
}

export default withStyles(style)(CardSection);
