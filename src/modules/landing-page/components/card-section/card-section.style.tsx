import { MARGIN_3 } from "../../../../shared/theme/gutters";

const style = (theme: any) => ({
  cardContainer: {
    [theme.breakpoints.up('md')]: {
      '& > div:first-child': {
        justifyContent: 'flex-start' as 'flex-start'
      },
      '& > div:last-child': {
        justifyContent: 'flex-end' as 'flex-end'
      },
    }
  },
  card:{
      marginBottom: `${MARGIN_3}px`
  }
});

export default style;
