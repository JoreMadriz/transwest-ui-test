import React from 'react';
import { Typography, withStyles, Grid, IconButton, Hidden } from '@material-ui/core';
import { KeyboardArrowLeft, KeyboardArrowRight } from '@material-ui/icons/';
import imagePlaceholder from '../../../../shared/assets/images/placeholder.png';
import styles from './chosen.style';

interface ChosenState {
  index: number;
}
interface ChosenProps {
  classes: any;
}
class Chosen extends React.Component<ChosenProps, ChosenState> {
  constructor(props: any) {
    super(props);
    this.state = {
      index: 0
    };
  }

  private sampleInfo = [
    { img: imagePlaceholder },
    { img: imagePlaceholder },
    { img: imagePlaceholder },
    { img: imagePlaceholder },
    { img: imagePlaceholder },
    { img: imagePlaceholder }
  ];
  private moveArray(step: number) {
    const { index } = this.state;
    const minimum = 0;
    const maxIndex = this.sampleInfo.length - 1;
    const sumStep = index + step;
    const newIndex = sumStep < minimum ? maxIndex : sumStep;

    this.setState({
      index: newIndex
    });
  }
  private renderImages = () => {
    const { image } = this.props.classes;
    const images = [];
    for (let index = 0; index < 3; index++) {
      const img = this.getImageElem(index);
      images.push(
        <Grid key={index} item xl={4} md={3} xs={12}>
          <img className={image} src={img} />
        </Grid>
      );
    }
    return images;
  };

  private getImageElem(step: number) {
    const eleIndex = (step + this.state.index) % this.sampleInfo.length;
    return this.sampleInfo[eleIndex].img;
  }
  render() {
    const { classes } = this.props;
    const { imageContainer, image, mobileArrow, right, left } = classes;
    const renderedImages = this.renderImages();
    return (
      <>
        <Typography align="left" variant="h4">
          Chosen for you
        </Typography>
        <Hidden smDown>
          <Grid container justify="flex-end" direction="row">
            <IconButton onClick={() => this.moveArray(-1)}>
              <KeyboardArrowLeft fontSize="large" />
            </IconButton>
            <IconButton onClick={() => this.moveArray(1)}>
              <KeyboardArrowRight fontSize="large" />
            </IconButton>
          </Grid>
          <Grid container className={imageContainer} direction="row" justify="space-between">
            {renderedImages}
          </Grid>
        </Hidden>
        <Hidden mdUp>
          <Grid className={imageContainer}>
            <IconButton className={`${mobileArrow} ${left}`} onClick={() => this.moveArray(-1)}>
              <KeyboardArrowLeft fontSize="large" />
            </IconButton>
            <IconButton className={`${mobileArrow} ${right}`} onClick={() => this.moveArray(1)}>
              <KeyboardArrowRight fontSize="large" />
            </IconButton>
            <Grid item lg={4} md={3} xs={12}>
              <img className={image} src={this.getImageElem(0)} />
            </Grid>
          </Grid>
        </Hidden>
      </>
    );
  }
}
export default withStyles(styles)(Chosen);
