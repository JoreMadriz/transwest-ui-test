import { MARGIN_5 } from "../../../../shared/theme/gutters";

const styles = (theme: any) => ({
  imageContainer: {
    [theme.breakpoints.down('sm')]:{
        position: 'relative' as 'relative',
        width: 'fit-content' as 'fit-content',
        margin: `${MARGIN_5}px auto ${MARGIN_5}px auto`
    },
    [theme.breakpoints.up('md')]: {
      '& :first-child': {
        textAlign: 'left' as 'left'
      },
      '& :last-child': {
        textAlign: 'right' as 'right'
      }
    }
  },
  image: {
    width: '100%',
    [theme.breakpoints.down('md')]: {
      maxWidth: '400px',
    },
    [theme.breakpoints.up('lg')]: {
      maxWidth: '500px',
    },
    height: 'auto' as 'auto'
  },
  mobileArrow: {
      position: 'absolute' as 'absolute',
      top:'35%',
      padding: 0
  },
  right: {
    right: 0,
  },
  left: {
      left: 0
  }
});

export default styles;
