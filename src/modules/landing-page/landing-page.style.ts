import { PADDING_8 } from '../../shared/theme/gutters';

const styles = {
  container: {
    margin: `0 ${PADDING_8}px`
  }
};

export default styles;
