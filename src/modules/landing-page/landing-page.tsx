import React from 'react';
import { Grid, withStyles, Hidden } from '@material-ui/core';

import BookNow from './components/book-now/book-now';
import NewsSection from './components/news-section/news-section';
import styles from './landing-page.style';
import SectionSpacer from '../../shared/theme/components/section-spacer/section-spacer';
import { SpacerType } from '../../shared/theme/components/section-spacer/SpacerType';
import Chosen from './components/chosen/chosen';
import CardSection from './components/card-section/card-section';

export interface LandingPageProps {
  classes: any;
}

export interface LandingPageState {}

class LandingPage extends React.Component<LandingPageProps, LandingPageState> {
  private readonly NEWS_SECTION_TITLE: string = 'Introducing our new route: Saskatoon to Winnipeg';
  private readonly NEWS_SECTION_BODY: string =
    'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut ero labore et dolore magna aliqua Ut enim.';
  render() {
    const { classes } = this.props;

    const { container } = classes;
    return (
      <Grid className={container}>
        <BookNow />
        <Hidden mdUp>
          <SectionSpacer spacerType={SpacerType.TINY} />
        </Hidden>
        <NewsSection title={this.NEWS_SECTION_TITLE} body={this.NEWS_SECTION_BODY} />
        <SectionSpacer spacerType={SpacerType.SMALL} />
        <Chosen />
        <SectionSpacer spacerType={SpacerType.SMALL} />
        <CardSection/>
      </Grid>
    );
  }
}

export default withStyles(styles)(LandingPage);
