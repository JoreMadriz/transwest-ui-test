import React from 'react';

import StaticPage from '../../shared/components/static-page/static-page';
import IMenuItem from '../../shared/components/models/menuItem';

const HelpCenter = () => {
  const menuItems: IMenuItem[] = [
    { name: 'Careers', uri: 'Careers' },
    { name: 'Report Baggage Irregularity', uri: 'Report Baggage Irregularity' }
  ];

  const menuTitle = 'Contact Us';

  return <StaticPage menuItems={menuItems} title={menuTitle} />;
};

export default HelpCenter;
