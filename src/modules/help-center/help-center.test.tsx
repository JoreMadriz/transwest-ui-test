import React from 'react';
import { shallow } from 'enzyme';

import HelpCenter from './help-center';
describe('Help Center test suite', () => {
  function getServiceMapWrapper() {
    return shallow(<HelpCenter />);
  }

  it('should render the component structure properly', () => {
    const helpCenter = getServiceMapWrapper();
    expect(helpCenter).toMatchSnapshot();
  });
});
