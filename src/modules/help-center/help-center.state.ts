export default interface IHelpCenterState {
  html: string;
  selectedIndex: number;
}
