import React from 'react';
import { Typography, Grid, withStyles, Hidden } from '@material-ui/core';
import { withRouter } from 'react-router-dom';
import styles from './footer.style';
import { Link } from 'react-router-dom';
import ContactPhoneColumn from './components/contact-phone/contact-phone';
import Newsletter from './components/newsletter/newsletter';
import SectionSpacer from '../../../../shared/theme/components/section-spacer/section-spacer';
import { SpacerType } from '../../../../shared/theme/components/section-spacer/SpacerType';
import routes from '../../../../shared/routing/routes';

interface FooterProps {
  classes: any;
  location: any;
}
class Footer extends React.Component<FooterProps> {
  render() {
    const { classes, location } = this.props;
    const invalidPath = 'booking'
    const { footerContainer, link, linkContainer } = classes;    
    const isNotBooking = !location.pathname.toLowerCase().includes(invalidPath);
    return (
      isNotBooking && (
        <>
          <SectionSpacer spacerType={SpacerType.LARGE} />
          <div className={footerContainer}>
            <Typography color="inherit" variant="h6">
              Transwest Air
            </Typography>
            <SectionSpacer spacerType={SpacerType.SMALL} />
            <Grid container direction="row">
              <Grid item xs={12} md={4} container direction="row">
                <Grid item container xs={4} md={4} direction="column">
                  <Grid className={linkContainer} item>
                    <Link className={link} to={routes.booking.path.replace(':step', '1')}>
                      Book
                    </Link>
                  </Grid>
                  <Grid item className={linkContainer}>
                    <Link className={link} to={routes.schedule.path}>
                      Schedules
                    </Link>
                  </Grid>
                  <Grid item className={linkContainer}>
                    <Link className={link} to={routes.services.path}>
                      Services
                    </Link>
                  </Grid>
                  <Grid item className={linkContainer}>
                    <Link className={link} to={routes.help.path}>
                      Help Center
                    </Link>
                  </Grid>
                </Grid>
                <Grid item container xs={4} md={4} direction="column">
                  <Grid className={linkContainer} item>
                    <Link className={link} to={routes.booking.path.replace(':step', '1')}>
                      About Us
                    </Link>
                  </Grid>
                  <Grid item className={linkContainer}>
                    <Link className={link} to={routes.booking.path.replace(':step', '1')}>
                      What's New
                    </Link>
                  </Grid>
                  <Grid item className={linkContainer}>
                    <Link className={link} to={routes.booking.path.replace(':step', '1')}>
                      Careers
                    </Link>
                  </Grid>
                  <Grid item className={linkContainer}>
                    <Link className={link} to={routes.booking.path}>
                      Agent Log-in
                    </Link>
                  </Grid>
                </Grid>
                <Grid item container xs={4} md={4} direction="column">
                  <Grid className={linkContainer} item>
                    <Link className={link} to={routes.booking.path.replace(':step', '1')}>
                      WestWind
                    </Link>
                  </Grid>
                  <Grid item className={linkContainer}>
                    <Link className={link} to={routes.booking.path.replace(':step', '1')}>
                      Corporate
                    </Link>
                  </Grid>
                  <Grid item className={linkContainer}>
                    <Link className={link} to={routes.booking.path.replace(':step', '1')}>
                      Site Map
                    </Link>
                  </Grid>
                  <Grid item className={linkContainer}>
                    <Link className={link} to={routes.booking.path.replace(':step', '1')}>
                      Privacy Policy
                    </Link>
                  </Grid>
                  <Grid item className={linkContainer}>
                    <Link className={link} to={routes.booking.path.replace(':step', '1')}>
                      Terms of Use
                    </Link>
                  </Grid>
                </Grid>
              </Grid>
              <Hidden mdUp>
                <SectionSpacer spacerType={SpacerType.MEDIUM} />
              </Hidden>
              <Grid item xs={12} md={4} container justify="flex-start">
                <Newsletter />
              </Grid>
              <Hidden mdUp>
                <SectionSpacer spacerType={SpacerType.MEDIUM} />
              </Hidden>
              <ContactPhoneColumn />
            </Grid>
            <span>© Transwest Air 2019 </span>
          </div>
        </>
      )
    );
  }
}
const StyledComponent = withStyles(styles)(Footer);
export default withRouter((props) => <StyledComponent {...props} />);


