import { PRIMARY_COLOR, WHITE } from '../../../../shared/theme/colors';
import { PADDING_28, PADDING_8, MARGIN_1, PADDING_6 } from '../../../../shared/theme/gutters';

const styles = (theme: any) => {
  return {
    footerContainer: {
      background: PRIMARY_COLOR,
      color: WHITE,
      padding: `${PADDING_8}px ${PADDING_28}px`,
      [theme.breakpoints.down('md')]: {
        padding: `${PADDING_8}px ${PADDING_6}px`
      }
    },
    linkContainer: {
      marginBottom: `${MARGIN_1}px`
    },
    link: {
      textDecoration: 'none',
      color: 'inherit',
      fontSize: '14px',
      fontWeight: 500
    },
    titles: {
      fontWeight: 400,
      fontSize: '20px'
    }
  };
};

export default styles;
