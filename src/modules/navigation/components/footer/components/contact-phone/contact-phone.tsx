import React from 'react';
import { Grid, Typography, withStyles } from '@material-ui/core';
import styles from './contact-phone.style';

interface ContactPhoneProps {
  classes: any;
}
class ContactPhoneColumn extends React.Component<ContactPhoneProps> {
  render() {
    const { classes } = this.props;
    const { titles } = classes;
    return (
      <Grid item container direction="column" xs={12} md={4}>
        <Grid container direction="row" justify="flex-start">
          <Grid>
            <Typography className={titles} color="inherit">
              Reservations
            </Typography>
            <Typography className={titles} color="inherit">
              1-800-667-9356
            </Typography>
          </Grid>
        </Grid>
        <Grid container direction="row" justify="flex-start">
          <Grid>
            <br />
            <Typography className={titles} color="inherit">
              24/7 Dispatch
            </Typography>
            <Typography className={titles} color="inherit">
              1-877-989-2677
            </Typography>
          </Grid>
        </Grid>
      </Grid>
    );
  }
}

export default withStyles(styles)(ContactPhoneColumn);
