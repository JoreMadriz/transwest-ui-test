import React from 'react';
import { Grid, SvgIcon, Typography, withStyles } from '@material-ui/core';
import styles from './social-media-icon.style'
interface SocialMediaProps {
  classes: any;
  socialMediaName: string;
}

class SocialMediaIcon extends React.Component<SocialMediaProps> {
  render() {
      const { classes, socialMediaName, children} = this.props
      const {icon} = classes;
    return (
      <Grid container direction="row">
        <Grid>
          <SvgIcon className={icon} fontSize="small" color="primary">
            {children}
          </SvgIcon>
        </Grid>
        <Grid>
          <Typography color="inherit">{socialMediaName}</Typography>
        </Grid>
      </Grid>
    );
  }
}

export default withStyles(styles)(SocialMediaIcon)