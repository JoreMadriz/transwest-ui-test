import { MARGIN_3, MARGIN_1 } from '../../../../../../../../shared/theme/gutters';

const styles = {
  icon: {
    marginRight: `${MARGIN_3}px`,
    marginBottom: `${MARGIN_1}px`
  }
};

export default styles;
