import { MARGIN_5, PADDING_2, PADDING_3 } from '../../../../../../shared/theme/gutters';
import { WHITE, PRIMARY_COLOR } from '../../../../../../shared/theme/colors';

const styles = {
  textField: {
    background: WHITE,
    borderRight: `1px solid ${WHITE}`,
    borderRadius: `7px`,
    minWidth: '270px',
    margin: `${MARGIN_5}px auto`
  },
  placeholder: {
    fontSize: '12px',
    borderStyle: 'solid' as 'solid',
    borderWidth: '1px 0px 1px 1px',
    borderColor: WHITE,
    borderRadius: `7px 0px 0px 7px`,

    background: PRIMARY_COLOR,
    color: `${WHITE}`,
    padding: `${PADDING_2}px 0 ${PADDING_2}px ${PADDING_3}px`,
    '&::placeholder': {
      color: `${WHITE}`,
      opacity: 1,
      fontSize: '12px'
    }
  },

  newsButton: {
    background: WHITE,
    color: PRIMARY_COLOR,
    borderRadius: '0',
    fontWeight: 500,
    fontSize: '10px'
  }
};
export default styles;
