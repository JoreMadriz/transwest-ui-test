import { WHITE } from '../../../../../shared/theme/colors';

export default {
  link: {
    textDecoration: 'none' as 'none'
  },
  drawerListItem: {
    fontSize: '36px',
    color: WHITE
  }
};
