import React from 'react';
import { Link } from 'react-router-dom';
import { withStyles, ListItem, Typography, ListItemText } from '@material-ui/core';

import styles from './navbar-mobile-item.style';

interface NavbarMobileItemProps {
  classes: any;
  handleDrawerClose: Function;
  textValue: string;
  routePath: string;
}

class NavbarMobileItem extends React.Component<NavbarMobileItemProps> {
  render() {
    const { classes, handleDrawerClose, textValue, routePath } = this.props;
    const { drawerListItem, link } = classes;

    return (
      <Link className={link} to={routePath}>
        <ListItem disableGutters={true} onClick={() => handleDrawerClose()} button>
          <ListItemText primary={<Typography className={drawerListItem}>{textValue}</Typography>} />
        </ListItem>
      </Link>
    );
  }
}

export default withStyles(styles)(NavbarMobileItem);
