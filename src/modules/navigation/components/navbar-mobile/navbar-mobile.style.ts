import { PRIMARY_COLOR, WHITE } from '../../../../shared/theme/colors';

const styles = () => ({
  root: {
    backgroundColor: PRIMARY_COLOR,
    color: WHITE,
    padding: '0px',
    maxHeight: '6vh'
  },
  mobileNavTextColor: {
    color: WHITE
  },
  drawerContainer: {
    padding: '30px'
  },
  logInButton: {
    backgroundColor: 'transparent' as 'transparent',
    color: WHITE,
    fontSize: '10px',
    border: `2px solid ${WHITE}`,
    padding: '0px'
  },
  iconMenu: {
    margin: '0 auto',
    padding: '0px'
  },
  title: {
    margin: 'auto' as 'auto'
  },

  rootDrawer: {
    width: '90%',
    background: PRIMARY_COLOR
  },
  drawerTitle: {
    fontSize: '20px',
    fontWeight: 500,
    textTransform: 'capitalize' as 'capitalize',
    margin: 'auto 0'
  },
  callsOption: {
    padding: '15px 20% 15px 0'
  },
  callText: {
    fontSize: '12px'
  },
  drawerText: {
    fontSize: '30px'
  }
});

export default styles;
