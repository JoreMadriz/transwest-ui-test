import React from 'react';

import { Clear, Call } from '@material-ui/icons';
import MenuIcon from '@material-ui/icons/Menu';
import {
  withStyles,
  AppBar,
  Toolbar,
  IconButton,
  Typography,
  Button,
  Grid,
  SwipeableDrawer,
  List
} from '@material-ui/core';

import routes from '../../../../shared/routing/routes';
import SectionSpacer from '../../../../shared/theme/components/section-spacer/section-spacer';
import styles from './navbar-mobile.style';
import { SpacerType } from '../../../../shared/theme/components/section-spacer/SpacerType';
import NavbarMobileItem from './components/navbar-mobile-item';

interface NavbarMobileProps {
  classes: any;
}

interface NavbarState {
  open: boolean;
}

class NavbarMobile extends React.Component<NavbarMobileProps, NavbarState> {
  constructor(props: NavbarMobileProps) {
    super(props);
    this.state = {
      open: false
    };
  }

  handleDrawerOpen = () => {
    this.setState({ open: true });
  };

  handleDrawerClose = () => {
    this.setState({ open: false });
  };

  render() {
    const { classes } = this.props;
    const {
      drawerTitle,
      drawerText,
      callsOption,
      callText,
      drawerContainer,
      iconMenu,
      logInButton,
      mobileNavTextColor,
      root,
      rootDrawer,
      title
    } = classes;
    const { open } = this.state;
    const propsPlaceholder = ':page';
    return (
      <AppBar className={root} position="fixed">
        <Toolbar disableGutters={!open}>
          <Grid container justify="space-evenly">
            <Grid
              onClick={() => {
                this.handleDrawerOpen();
              }}
              container
              item
              xs={2}
              justify="flex-start"
            >
              <IconButton
                className={iconMenu}
                color="inherit"
                aria-label="Open drawer"
                onClick={this.handleDrawerOpen}
              >
                <MenuIcon />
              </IconButton>
            </Grid>
            <Grid container item justify="center" xs={6}>
              <Typography className={title} variant="h6" color="inherit">
                Transwest Air
              </Typography>
            </Grid>
            <Grid container item justify="center" xs={4}>
              <Button className={logInButton}>Log-In</Button>
            </Grid>
          </Grid>
        </Toolbar>
        <SwipeableDrawer
          classes={{ paper: rootDrawer }}
          open={open}
          onOpen={() => {
            this.handleDrawerOpen();
          }}
          onClose={() => {
            this.handleDrawerClose();
          }}
        >
          <div className={drawerContainer}>
            <div>
              <Grid container justify="space-between">
                <Typography align="left" className={`${drawerTitle} ${mobileNavTextColor}`}>
                  Transwest Air
                </Typography>
                <IconButton onClick={() => this.handleDrawerClose()}>
                  <Clear fontSize="large" className={mobileNavTextColor} />
                </IconButton>
              </Grid>
              <SectionSpacer spacerType={SpacerType.MEDIUM} />
              <List>
                <NavbarMobileItem
                  handleDrawerClose={this.handleDrawerClose}
                  textValue="Book"
                  routePath={routes.booking.path}
                />
                <NavbarMobileItem
                  handleDrawerClose={this.handleDrawerClose}
                  textValue="Schedules"
                  routePath={routes.schedule.path.replace(propsPlaceholder, '')}
                />
                <NavbarMobileItem
                  handleDrawerClose={this.handleDrawerClose}
                  textValue="Services"
                  routePath={routes.services.path.replace(propsPlaceholder, '')}
                />
                <NavbarMobileItem
                  handleDrawerClose={this.handleDrawerClose}
                  textValue="HelpCenter"
                  routePath={routes.help.path}
                />
              </List>
            </div>
            <SectionSpacer spacerType={SpacerType.MEDIUM} />
            <div>
              <Grid
                className={callsOption}
                container
                justify="space-between"
                onClick={() => {
                  window.open('tel:1-800-667-9356');
                }}
              >
                <Typography align="left" className={`${drawerText} ${mobileNavTextColor}`}>
                  Reservations
                </Typography>
                <div>
                  <Call fontSize="large" className={mobileNavTextColor} />
                  <Typography align="center" className={`${mobileNavTextColor} ${callText}`}>
                    call
                  </Typography>
                </div>
              </Grid>
              <Grid
                className={callsOption}
                container
                justify="space-between"
                onClick={() => {
                  window.open('tel:1-877-989-2677');
                }}
              >
                <Typography align="left" className={`${drawerText} ${mobileNavTextColor}`}>
                  24/7 Dispatch
                </Typography>
                <div>
                  <Call fontSize="large" className={mobileNavTextColor} />
                  <Typography align="center" className={`${mobileNavTextColor} ${callText}`}>
                    call
                  </Typography>
                </div>
              </Grid>
            </div>
            <Grid container spacing={16} justify="flex-start">
              <Grid item>
                <img src="data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHg9IjBweCIgeT0iMHB4Igp3aWR0aD0iMjQiIGhlaWdodD0iMjQiCnZpZXdCb3g9IjAgMCAyMjQgMjI0IgpzdHlsZT0iIGZpbGw6IzAwMDAwMDsiPjxnIGZpbGw9Im5vbmUiIGZpbGwtcnVsZT0ibm9uemVybyIgc3Ryb2tlPSJub25lIiBzdHJva2Utd2lkdGg9IjEiIHN0cm9rZS1saW5lY2FwPSJidXR0IiBzdHJva2UtbGluZWpvaW49Im1pdGVyIiBzdHJva2UtbWl0ZXJsaW1pdD0iMTAiIHN0cm9rZS1kYXNoYXJyYXk9IiIgc3Ryb2tlLWRhc2hvZmZzZXQ9IjAiIGZvbnQtZmFtaWx5PSJub25lIiBmb250LXdlaWdodD0ibm9uZSIgZm9udC1zaXplPSJub25lIiB0ZXh0LWFuY2hvcj0ibm9uZSIgc3R5bGU9Im1peC1ibGVuZC1tb2RlOiBub3JtYWwiPjxwYXRoIGQ9Ik0wLDIyNHYtMjI0aDIyNHYyMjR6IiBmaWxsPSJub25lIj48L3BhdGg+PGcgaWQ9Im9yaWdpbmFsLWljb24iIGZpbGw9IiNmZmZmZmYiPjxwYXRoIGQ9Ik0xNzcuMzMzMzMsMjhoLTEzMC42NjY2N2MtMTAuMzEzMzMsMCAtMTguNjY2NjcsOC4zNTMzMyAtMTguNjY2NjcsMTguNjY2Njd2MTMwLjY2NjY3YzAsMTAuMzEzMzMgOC4zNTMzMywxOC42NjY2NyAxOC42NjY2NywxOC42NjY2N2g3MS4xMjkzM3YtNjQuOTY5MzNoLTIxLjg2OHYtMjUuNDMzMzNoMjEuODY4di0xOC43MTMzM2MwLC0yMS42OTA2NyAxMy4yNjI2NywtMzMuNTE2IDMyLjYyLC0zMy41MTZjNi41MjQsLTAuMDE4NjcgMTMuMDM4NjcsMC4zMTczMyAxOS41MjUzMywwLjk4djIyLjY4aC0xMy4zMjhjLTEwLjU0NjY3LDAgLTEyLjYsNC45ODQgLTEyLjYsMTIuMzM4Njd2MTYuMTkzMzNoMjUuMmwtMy4yNzYsMjUuNDMzMzNoLTIyLjA3MzMzdjY1LjAwNjY3aDMzLjQ2OTMzYzEwLjMxMzMzLDAgMTguNjY2NjcsLTguMzUzMzMgMTguNjY2NjcsLTE4LjY2NjY3di0xMzAuNjY2NjdjMCwtMTAuMzEzMzMgLTguMzUzMzMsLTE4LjY2NjY3IC0xOC42NjY2NywtMTguNjY2Njd6Ij48L3BhdGg+PC9nPjwvZz48L3N2Zz4=" />
              </Grid>
              <Grid item>
                <img src="data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHg9IjBweCIgeT0iMHB4Igp3aWR0aD0iMjQiIGhlaWdodD0iMjQiCnZpZXdCb3g9IjAgMCAyMjQgMjI0IgpzdHlsZT0iIGZpbGw6IzAwMDAwMDsiPjxnIGZpbGw9Im5vbmUiIGZpbGwtcnVsZT0ibm9uemVybyIgc3Ryb2tlPSJub25lIiBzdHJva2Utd2lkdGg9IjEiIHN0cm9rZS1saW5lY2FwPSJidXR0IiBzdHJva2UtbGluZWpvaW49Im1pdGVyIiBzdHJva2UtbWl0ZXJsaW1pdD0iMTAiIHN0cm9rZS1kYXNoYXJyYXk9IiIgc3Ryb2tlLWRhc2hvZmZzZXQ9IjAiIGZvbnQtZmFtaWx5PSJub25lIiBmb250LXdlaWdodD0ibm9uZSIgZm9udC1zaXplPSJub25lIiB0ZXh0LWFuY2hvcj0ibm9uZSIgc3R5bGU9Im1peC1ibGVuZC1tb2RlOiBub3JtYWwiPjxwYXRoIGQ9Ik0wLDIyNHYtMjI0aDIyNHYyMjR6IiBmaWxsPSJub25lIj48L3BhdGg+PGcgZmlsbD0iI2ZmZmZmZiI+PHBhdGggZD0iTTE3Ny4zMzMzMywyOGgtMTMwLjY2NjY3Yy0xMC4zMTMzMywwIC0xOC42NjY2Nyw4LjM1MzMzIC0xOC42NjY2NywxOC42NjY2N3YxMzAuNjY2NjdjMCwxMC4zMTMzMyA4LjM1MzMzLDE4LjY2NjY3IDE4LjY2NjY3LDE4LjY2NjY3aDEzMC42NjY2N2MxMC4zMTMzMywwIDE4LjY2NjY3LC04LjM1MzMzIDE4LjY2NjY3LC0xOC42NjY2N3YtMTMwLjY2NjY3YzAsLTEwLjMxMzMzIC04LjM1MzMzLC0xOC42NjY2NyAtMTguNjY2NjcsLTE4LjY2NjY3ek0xNTkuMTMzMzMsODguNzk3MzNjMCwwLjgwMjY3IDAsMS41OTYgMCwzLjIwMTMzYzAsMzAuMzk4NjcgLTIzLjIwMjY3LDY1LjYwNCAtNjUuNjA0LDY1LjYwNGMtMTIuNzk2LDAgLTI0Ljc5ODY3LC00LjAwNCAtMzUuMTk2LC0xMC4zOTczM2MxLjU5NiwwIDQuMDA0LDAgNS42LDBjMTAuMzk3MzMsMCAyMC44MDQsLTQuMDA0IDI4LjgwMjY3LC05LjYwNGMtMTAuMzk3MzMsMCAtMTguMzk2LC03LjE5NiAtMjEuNTk3MzMsLTE1Ljk5NzMzYzEuNTk2LDAgMy4yMDEzMywwLjgwMjY3IDQuMDA0LDAuODAyNjdjMi4zOTg2NywwIDQuMDA0LDAgNi40MDI2NywtMC44MDI2N2MtMTAuMzk3MzMsLTIuMzk4NjcgLTE4LjM5NiwtMTEuMiAtMTguMzk2LC0yMi40YzMuMjAxMzMsMS41OTYgNi40MDI2NywyLjM5ODY3IDEwLjM5NzMzLDMuMjAxMzNjLTYuNDAyNjcsLTUuNiAtMTAuMzk3MzMsLTEyLjAwMjY3IC0xMC4zOTczMywtMjAuMDAxMzNjMCwtNC4wMDQgMC44MDI2NywtNy45OTg2NyAzLjIwMTMzLC0xMS4yYzExLjIsMTMuNTk4NjcgMjgsMjMuMjAyNjcgNDcuMTk4NjcsMjMuOTk2YzAsLTEuNTk2IC0wLjgwMjY3LC0zLjIwMTMzIC0wLjgwMjY3LC01LjZjMCwtMTIuNzk2IDEwLjM5NzMzLC0yMy4yMDI2NyAyMy4yMDI2NywtMjMuMjAyNjdjNi40MDI2NywwIDEyLjc5NiwyLjM5ODY3IDE2LjgsNy4xOTZjNS42LC0wLjgwMjY3IDEwLjM5NzMzLC0zLjIwMTMzIDE0LjQwMTMzLC01LjZjLTEuNTk2LDUuNiAtNS42LDkuNjA0IC0xMC4zOTczMywxMi43OTZjNC43OTczMywtMC44MDI2NyA4LjgwMTMzLC0xLjU5NiAxMy41OTg2NywtNC4wMDRjLTMuMjIsNC44MTYgLTcuMjE0NjcsOC44MTA2NyAtMTEuMjE4NjcsMTIuMDEyeiI+PC9wYXRoPjwvZz48L2c+PC9zdmc+" />
              </Grid>
              <Grid item>
                <img src="data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHg9IjBweCIgeT0iMHB4Igp3aWR0aD0iMjQiIGhlaWdodD0iMjQiCnZpZXdCb3g9IjAgMCAyMjQgMjI0IgpzdHlsZT0iIGZpbGw6IzAwMDAwMDsiPjxnIGZpbGw9Im5vbmUiIGZpbGwtcnVsZT0ibm9uemVybyIgc3Ryb2tlPSJub25lIiBzdHJva2Utd2lkdGg9IjEiIHN0cm9rZS1saW5lY2FwPSJidXR0IiBzdHJva2UtbGluZWpvaW49Im1pdGVyIiBzdHJva2UtbWl0ZXJsaW1pdD0iMTAiIHN0cm9rZS1kYXNoYXJyYXk9IiIgc3Ryb2tlLWRhc2hvZmZzZXQ9IjAiIGZvbnQtZmFtaWx5PSJub25lIiBmb250LXdlaWdodD0ibm9uZSIgZm9udC1zaXplPSJub25lIiB0ZXh0LWFuY2hvcj0ibm9uZSIgc3R5bGU9Im1peC1ibGVuZC1tb2RlOiBub3JtYWwiPjxwYXRoIGQ9Ik0wLDIyNHYtMjI0aDIyNHYyMjR6IiBmaWxsPSJub25lIj48L3BhdGg+PGcgZmlsbD0iI2ZmZmZmZiI+PHBhdGggZD0iTTc0LjY2NjY3LDI4Yy0yNS43NjkzMywwIC00Ni42NjY2NywyMC44OTczMyAtNDYuNjY2NjcsNDYuNjY2Njd2NzQuNjY2NjdjMCwyNS43NjkzMyAyMC44OTczMyw0Ni42NjY2NyA0Ni42NjY2Nyw0Ni42NjY2N2g3NC42NjY2N2MyNS43NjkzMywwIDQ2LjY2NjY3LC0yMC44OTczMyA0Ni42NjY2NywtNDYuNjY2Njd2LTc0LjY2NjY3YzAsLTI1Ljc2OTMzIC0yMC44OTczMywtNDYuNjY2NjcgLTQ2LjY2NjY3LC00Ni42NjY2N3pNMTY4LDQ2LjY2NjY3YzUuMTUyLDAgOS4zMzMzMyw0LjE4MTMzIDkuMzMzMzMsOS4zMzMzM2MwLDUuMTUyIC00LjE4MTMzLDkuMzMzMzMgLTkuMzMzMzMsOS4zMzMzM2MtNS4xNTIsMCAtOS4zMzMzMywtNC4xODEzMyAtOS4zMzMzMywtOS4zMzMzM2MwLC01LjE1MiA0LjE4MTMzLC05LjMzMzMzIDkuMzMzMzMsLTkuMzMzMzN6TTExMiw2NS4zMzMzM2MyNS43NjkzMywwIDQ2LjY2NjY3LDIwLjg5NzMzIDQ2LjY2NjY3LDQ2LjY2NjY3YzAsMjUuNzY5MzMgLTIwLjg5NzMzLDQ2LjY2NjY3IC00Ni42NjY2Nyw0Ni42NjY2N2MtMjUuNzY5MzMsMCAtNDYuNjY2NjcsLTIwLjg5NzMzIC00Ni42NjY2NywtNDYuNjY2NjdjMCwtMjUuNzY5MzMgMjAuODk3MzMsLTQ2LjY2NjY3IDQ2LjY2NjY3LC00Ni42NjY2N3pNMTEyLDg0Yy0xNS40NjM5NywwIC0yOCwxMi41MzYwMyAtMjgsMjhjMCwxNS40NjM5NyAxMi41MzYwMywyOCAyOCwyOGMxNS40NjM5NywwIDI4LC0xMi41MzYwMyAyOCwtMjhjMCwtMTUuNDYzOTcgLTEyLjUzNjAzLC0yOCAtMjgsLTI4eiI+PC9wYXRoPjwvZz48L2c+PC9zdmc+" />
              </Grid>
            </Grid>
          </div>
        </SwipeableDrawer>
      </AppBar>
    );
  }
}

export default withStyles(styles)(NavbarMobile);
