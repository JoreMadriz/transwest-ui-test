import { PRIMARY_COLOR } from '../../../../shared/theme/colors';
import { PADDING_11, PADDING_1 } from '../../../../shared/theme/gutters';
const styles = {
  root: {
    flexGrow: 1
  },
  appBar: {},
  toolbar: {},
  logo: {
    fontWeight: 600,
    fontSize: '14.5pt'
  },
  login: {
    padding: `${PADDING_1}px ${PADDING_11}px`,
    fontWeight: 'bold' as 'bold',
    color: PRIMARY_COLOR,
    width: '120px',
    height: '40px',
    marginRight: '15px',
    fontSize: '10px',
    whiteSpace: 'nowrap' as 'nowrap',
    '&:hover': {
      color: PRIMARY_COLOR,
      border: `2px solid transparent`
    }
  },
  noLink: {
    fontWeight: 600,
    lineHeight: '2em',
    fontSize: '12px',
    whiteSpace: 'nowrap' as 'nowrap'
  },
  noLinkTitle: {
    textDecoration: 'none' as 'none',
    color: PRIMARY_COLOR
  },
  link: {
    textDecoration: 'none',
    color: PRIMARY_COLOR,
    fontSize: '12px',
    whiteSpace: 'nowrap' as 'nowrap'
  }
};

export default styles;
