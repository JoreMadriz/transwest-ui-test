import React from 'react';
import { AppBar, Toolbar, Typography, Button, Grid } from '@material-ui/core/';
import { withStyles } from '@material-ui/core/styles';

import { Link } from 'react-router-dom';
import NavbarProps from './navbar.props';
import routes from '../../../../shared/routing/routes';
import styles from './navbar.styles';

const Navbar = (props: NavbarProps) => {
  const { classes } = props;
  const propsPlaceholder = ':page';
  const { appBar, root, logo, link, noLink, login, toolbar, noLinkTitle } = classes;

  return (
    <div className={root}>
      <AppBar position="fixed" className={appBar} color="primary">
        <Toolbar className={toolbar}>
          <Grid container={true} direction="row" justify="flex-start" alignItems="center">
            <Grid item={true} xs={2}>
              <Typography className={logo} color="textPrimary">
                <Link className={noLinkTitle} to={'/'}>
                  Transwest Air
                </Link>
              </Typography>
            </Grid>
            <Grid item={true} xs={4}>
              <Grid
                container={true}
                direction="row"
                alignContent="space-between"
                alignItems="center"
                justify="flex-start"
              >
                <Grid item={true}>
                  <Button>
                    <Link className={link} to={routes.booking.path}>
                      Book
                    </Link>
                  </Button>
                </Grid>
                <Grid item={true}>
                  <Button>
                    <Link
                      className={classes.link}
                      to={routes.schedule.path.replace(propsPlaceholder, '')}
                    >
                      Schedule
                    </Link>
                  </Button>
                </Grid>
                <Grid item={true}>
                  <Button>
                    <Link
                      className={classes.link}
                      to={routes.services.path.replace(propsPlaceholder, '')}
                    >
                      Services
                    </Link>
                  </Button>
                </Grid>
                <Grid item={true}>
                  <Button>
                    <Link className={link} to={routes.help.path}>
                      Help Center
                    </Link>
                  </Button>
                </Grid>
              </Grid>
            </Grid>
            <Grid item={true} xs={6}>
              <Grid
                container={true}
                spacing={16}
                direction="row"
                justify="flex-end"
                alignItems="center"
              >
                <Grid item={true}>
                  <Typography className={noLink} variant="inherit" color="secondary">
                    Reservations 1-800-667-9356
                  </Typography>
                </Grid>
                <Grid item={true}>
                  <Typography className={noLink} color="secondary">
                    24/7 Dispatch 1-877-989-2677
                  </Typography>
                </Grid>
                <Grid item={true}>
                  <Button className={login} color="primary" variant="outlined">
                    Log-In
                  </Button>
                </Grid>
              </Grid>
            </Grid>
          </Grid>
        </Toolbar>
      </AppBar>
    </div>
  );
};

export default withStyles(styles)(Navbar);
