import BookingCalendar from '../booking/components/date-selector/models/booking-calendar';
import AirportSelectionData from '../booking/components/airport-selection/models/airport-selection-data';

export default interface StepsMobileMenuProps {
  classes: any;
  passengersQuantity: number;
  bookingDates: BookingCalendar;
  airportSelectionData: AirportSelectionData;
  handleStepMenuClick: Function;
}
