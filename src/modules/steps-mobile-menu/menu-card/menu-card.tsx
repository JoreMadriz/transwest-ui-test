import React from 'react';
import { Grid, withStyles, Typography } from '@material-ui/core';
import styles from './menu-card.style';
import { MenuCardsProps } from './menu-card.props';

export const MenuCard = (props: MenuCardsProps) => {
  const handleOnClick = () => {
    props.handleStepMenuClick(props.stepValue);
  };

  const { title, classes, children } = props;
  return (
    <Grid onClick={handleOnClick} xs={3} className={classes.items} item>
      <Typography className={classes.text} color="primary">
        {title}
      </Typography>
      {children}
    </Grid>
  );
};

export default withStyles(styles)(MenuCard);
