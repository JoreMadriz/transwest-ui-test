import { WHITE } from '../../../shared/theme/colors';

const styles = {
  items: {
    margin: '8px 1px',
    backgroundColor: WHITE,
    height: '54px'
  },
  text: {
    fontSize: '12px'
  }
};

export default styles;
