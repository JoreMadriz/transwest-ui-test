import { shallow, ShallowWrapper } from 'enzyme';
import React from 'react';

import { MenuCard } from './menu-card';
import { MenuCardsProps } from './menu-card.props';

type MenuCardType = ShallowWrapper<MenuCardsProps, {}>;

describe('Menu card test suite', () => {
  const getMenuCardDefaultProps = (): MenuCardsProps => {
    return {
      children: 'test',
      classes: '',
      handleStepMenuClick: jest.fn(),
      stepValue: 1,
      title: 'test card'
    };
  };

  const getMenuCardWrapper = (props: MenuCardsProps): MenuCardType => {
    return shallow(<MenuCard {...props} />);
  };

  it('calls the handle step menu props method when the card is clicked', () => {
    const defaultProps = getMenuCardDefaultProps();
    const menuCard = getMenuCardWrapper(defaultProps);
    menuCard.simulate('click');
    expect(defaultProps.handleStepMenuClick).toHaveBeenCalledWith(defaultProps.stepValue);
  });
});
