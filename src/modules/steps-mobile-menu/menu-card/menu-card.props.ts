export interface MenuCardsProps {
  title: string;
  classes: any;
  children: any;
  handleStepMenuClick: Function;
  stepValue: number;
}
