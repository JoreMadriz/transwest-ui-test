import React from 'react';
import { Grid, withStyles, Typography } from '@material-ui/core';
import SwapHoriz from '@material-ui/icons/SwapHoriz';
import Remove from '@material-ui/icons/Remove';
import styles from './steps-mobile-menu.style';
import moment from 'moment';

import MenuCard from './menu-card/menu-card';
import StepsMobileMenuProps from './steps-mobile-menu.props.ts';
import Step from '../booking/models/Step';

function formatDate(date: Date): string {
  const newDate = moment(date);
  let finalDate = `${newDate.format('D')} ${newDate.format('MMMM').substr(0, 3)}`;
  return finalDate;
}
class StepsMobileMenu extends React.Component<StepsMobileMenuProps, {}> {
  private readonly ROUNDTRIP: string = 'Roundtrip';
  private readonly PASSENGER: string = 'Passenger';
  private readonly OUTBOUND: string = 'Outbound';
  private readonly INBOUND: string = 'Inbound';
  render() {
    const {
      classes,
      passengersQuantity,
      bookingDates,
      airportSelectionData,
      handleStepMenuClick
    } = this.props;
    const { container, subtitle, icon } = classes;
    const { inboundDate, outboundDate } = bookingDates;

    return (
      <Grid className={container} container flex-direction="row">
        <MenuCard
          stepValue={Step.AIRPORT_SELECTION}
          handleStepMenuClick={handleStepMenuClick}
          title={this.ROUNDTRIP}
        >
          <Typography className={subtitle}>
            {(airportSelectionData.departureAirport &&
              airportSelectionData.departureAirport.Code) || <Remove className={icon} />}
            <SwapHoriz className={icon} />
            {(airportSelectionData.destinationAirport &&
              airportSelectionData.destinationAirport.Code) || <Remove className={icon} />}
          </Typography>
        </MenuCard>

        <MenuCard
          stepValue={Step.PASSENGERS_SELECTION}
          handleStepMenuClick={handleStepMenuClick}
          title={this.PASSENGER}
        >
          <Typography className={subtitle}>{passengersQuantity}</Typography>
        </MenuCard>

        <MenuCard
          stepValue={Step.DATE_SELECTION}
          handleStepMenuClick={handleStepMenuClick}
          title={this.OUTBOUND}
        >
          {(bookingDates.outboundDate && (
            <Typography className={subtitle}>{formatDate(outboundDate!)}</Typography>
          )) || <Remove className={icon} />}
        </MenuCard>

        <MenuCard
          stepValue={Step.DATE_SELECTION}
          handleStepMenuClick={handleStepMenuClick}
          title={this.INBOUND}
        >
          {(bookingDates.inboundDate && (
            <Typography className={subtitle}>{formatDate(inboundDate!)}</Typography>
          )) || <Remove className={icon} />}
        </MenuCard>
      </Grid>
    );
  }
}

export default withStyles(styles)(StepsMobileMenu);
