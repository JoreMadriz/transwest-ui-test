import { BORDER_COLOR, PRIMARY_COLOR } from '../../shared/theme/colors';

const styles = {
  container: {
    backgroundColor: BORDER_COLOR,
    marginBottom: '10px',
    flexDirection: 'row' as 'row',
    flexWrap: 'nowrap' as 'nowrap',
    paddingLeft: '4px',
    paddingRight: '4px'
  },
  subtitle: {
    fontSize: '14px',
    fontWeight: 'bold' as 'bold'
  },
  icon: {
    fontSize: '16px',
    color: PRIMARY_COLOR
  }
};

export default styles;
